# to do
- Remove animationSpeedFactor and transfer its purpose to frameDuration variable, since frameDuration is ignored when animationSpeedFactor is used
- Make power/torque graph in vehicle selection screen;
- Revise power curve profiles;
- Fix vehicle collision to account for vehicle height, making it possible to "jump over" other vehicles.
- Implement proper traffic collision (or its avoidance) behavior between them.
- Create non-wheeled vehicle examples
- Add support for multiple props per segment
- Create start/finish line props
- Improve vehicle selection state to allow filter/group by category
- Add support for up/down leaning sprites;
- Add support for camera-relative rotated sprites (apart from curving animation, for bikes, etc)
- Change course editor detail level to edit course properties (make preset styles work only as templates)
- Added support for multiple course styles in courses (position-dependant, completion-dependant, elapsed time-dependant)
- Add different terrains (in progress);
- Add option to specify custom vehicle portrait;
- Add option to specify custom vehicle rotating sprite (for showroom);
- Create more props to decorate the road (in progress);
- Add dynamic terrain variation;
- Add optional forced induction sounds;
- Add option for vehicle to specify custom smoke;
- Add gear shifting sounds;
- Add option for vehicle to specify custom gear shifting sound;
- Add a specifiable default vehicle shadow effect;
- Make shadow layer sprites stay on the ground when jumping;
- Add transition effects to each game state (fade in / fade out);
- Introduce dynamic horizontal and vertical tilt when making turns or passing slopes, for a better animation;
- Introduce dynamic zoom perturbations to give a fake sense of depth when accelerating or braking, for a better animation;
- Add vehicle shaking animation (when off-roading or in rough terrain);
- Add music for main menu and optionally for other states;
- Improve opponent car AI (in progress)
- Howl engine when selecting car (either a default engine howl or one using the car engine profile);
- Use a comma-separated list of gear ratio to specify gear ratios on vehicle properties;
- Add option to select music for next race, maybe with a music selection state;
- Add digital speedometer display HUD style;
- Add road ride sound, for cockpit or high speed/wind scenarios;
- Add more bike engine sound profiles;
- Add option to select characters for open vehicles (convertible cars, bikes, etc)
- Add grid layout for vehicle selection state
- Add support for road path splitting
- Add option to specify custom vehicle side profile sprite (for showroom or a side drag race mode);
- Change Pacejka scheme to simulate both rear and front wheels (not just the driven wheels), fixing braking simulation occurring just on driven wheels (also makes AWD behavior more realistic);
- Create vehicle editor state.
- Create career/championship mode (maybe with vehicle upgrades, shop, etc)
- Make course preview available optionally in course selection state (not possible until course drawing position can be specified without overdrawing left/up)

# bugs

- Fix the game's physics calculations regarding torque/power. Currently, torque is incorrect most of the time.
- Fix power at lower RPMs, as RPM is being set as its minimum before power computation
- Test specifying custom shadow sprite/layer positions.

# misc.

## On "jumping" slopes

Previous notes mentioned formula regarding the speed needed to jump of a slope, but these calculations are surpassed by actually implementing vertical speed (in slopes) and acceleration (gravity, basically).

## On camera/background behavior on slopes

By doing research, it was noted that there are 3 distinct camera/background behaviors on slopes on most pseudo-3D racing games:

#### Type 1: constant 90-degree camera polar angle, constant camera height, constant background height, with leaned vehicle sprites (Road Rash, tutorials, etc). 

In this scheme, the camera's polar angle is fixed at 90 degrees and its height also stays constant during slope ups and downs. The background stays at constant height as well and leaned vehicle sprites are
needed/recommended during slopes. The only thing that changes is the road and vehicle height, with varies with the road's actual altitude. Also, the road right beneath the vehicle should appear slopped/curved 
on slopes. This is the simplest approach, but has obvious problems when trying to implement anything more than simple hills. When trying to depict roads too high or too low, it gets outside the screen.

#### Type 2: constant 90-degree camera polar angle, quasi-constant camera height, quasi-constant background height, with leaned sprites (arcade versions of Out Run, Chase H.Q, etc).

In this scheme, the camera's polar angle is fixed at 90 degrees and its height also stays constant during slope ups and downs. However, the camera height will change slightly as the road's altitude changes.
But since this altitute-related change is small, the camera height will stay roughly the same during most of the gameplay. The background height also stays constant during ups and down and changes (slightly) 
only with road altitude. Leaned vehicle sprites are needed/recommended during slopes. Vehicle/road height, however, stays constant, as if they follow road altitude. Also, the road right beneath the vehicle 
should appear slopped/curved on slopes This is also a simple approach, and fixes the altitude problems with the Type 1 scheme. The downside is, together with the Type 1, that additional leaned vehicle sprites
are needed/recommended to get better visual results and besides, long slopes have a less immersive effect (like a following helicopter). 

#### Type 3: camera polar angle perpendicular to the ground slope, "dynamic" camera height, dynamic background height, with no leaned vehicle sprites (Lotus series, Top Gear series, etc).

In this scheme, the camera's polar angle is always perpendicular to the ground slope, as if the camera leaned as well. The camera height changes to stay at a constant distance to the ground. The background
height changes dynamically, both according to the camera polar angle and (optionally) to the road altitude, aiding the visual effect of actually being behicle the player's vehicle. In this scheme, no leaned
vehicle sprites are necessary, since the player is never supposed to see the vehicle from other polar angles (not to be confused with azimuth angles). The road's height (and hence, the vehicle's) stays the
same on screen, since the camera is following it. Also, the road right beneath the vehicle should appear flat at all times. This is a more complex approach, since it involves changing the geometry of road 
segment drawing, according to the vehicle's slope/leaning angle. The upside of this approach is that it does not require additional leaned vehicle sprites to get better visual results and besides, have a 
more immersive effect (like a following camera). Other game examples: Lamborghini American Challenge, Jaguar XJ220, Hang On

> Note that Types 2 and 3 can be combined to create a even more dynamic and immersive effect (as seen in the arcade version of Special Criminal Investivation, Hot Chase, Full Throttle).

##### Table with comparison:

	|  Type |            Camera polar angle             |             Background height             |               Camera height               | Vehicle/Road height | leaned sprites |     Game examples      |
	| ----- | ----------------------------------------- | ----------------------------------------- | ----------------------------------------- | ------------------- | -------------- | ---------------------- |
	|   A   | constant 90-degree                        | constant                                  | constant to the world                     | dynamic             | yes            | Road Rash              |
	|   B   | constant 90-degree                        | quasi-constant, varies with road altitude | quasi-constant, varies with road altitude | constant            | yes            | Out Run, Chase HQ      |
	|   C   | dynamic, perpendicular to the slope angle | dynamic                                   | constant to the ground                    | constant            | no             | Top Gear, Lotus series |


## some links about pseudo-3D graphics

### pseudo-3D tunnels and stuff
https://www.lexaloffle.com/bbs/?tid=35868

### pseudo-3D street light pole using primitives
https://twitter.com/chiptune/status/881615357907271681

### mode 7 info
https://www.coranac.com/tonc/text/mode7.htm

## On using INI files instead of .properties:

Instead of using .properties files, INI files could be used to give a little more structuring to the game's data files. They are basically .properties files with sections (something that is currently mimicked 
with comments), grouping together some properties. Next, information about INI files and APIs for some relevant libraries dealing with it:

- https://en.wikipedia.org/wiki/INI_file
- https://docs.python.org/3/library/configparser.html#configparser.ConfigParser.get
- http://ndevilla.free.fr/iniparser/html/index.html
- https://github.com/benhoyt/inih/blob/master/README.md#c-example
- https://github.com/madmurphy/libconfini/blob/master/MANUAL.md#formatting-the-values
- https://github.com/graniticio/inifile#accessing-properties

INI file support is being developed on the *futil* submodule. However there are some relevant C/C++ libraries to deal with INI files:

- https://github.com/ndevilla/iniparser
- https://github.com/benhoyt/inih
- https://github.com/madmurphy/libconfini
- https://github.com/graniticio/inifile

# brainstorm

- Speedometer design with dynamic backlight: lights up circular disc behind the speedometer's pointer.
- Toggle icons on vehicle selection state that indicates whether the sprite support multi-states, multi-frames, asymetrical sprites, leaned sprites, custom shadow, etc... 
- Intro for races based on different ideas (running camera to starting grid, etc)
- In vehicle selection, change behavior to select car first, then select appearance (use arcadish select effects (ex: daytona usa, etc))
- Instead of specifying tire friction coefficient for each vehicle directly (which would be difficult since it varies for each tire/surface combination), tire "archetypes" could be used instead (like "road", 
"racing", etc) which would apply a multiplier to the friction coefficient. For details, see https://vehiclephysics.com/blocks/tires/
