################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/gamestates/course_editor_state.cpp \
../src/gamestates/course_selection_state.cpp \
../src/gamestates/debug_workshop_state.cpp \
../src/gamestates/main_menu_retro_layout_state.cpp \
../src/gamestates/main_menu_simple_list_state.cpp \
../src/gamestates/options_menu_state.cpp \
../src/gamestates/pseudo3d_race_state.cpp \
../src/gamestates/pseudo3d_race_state_physics.cpp \
../src/gamestates/race_results_state.cpp \
../src/gamestates/radio_selection_state.cpp \
../src/gamestates/side_drag_race_state.cpp \
../src/gamestates/vehicle_selection_showroom_layout_state.cpp \
../src/gamestates/vehicle_selection_simple_list_state.cpp \
../src/gamestates/vehicle_selection_state.cpp 

OBJS += \
./src/gamestates/course_editor_state.o \
./src/gamestates/course_selection_state.o \
./src/gamestates/debug_workshop_state.o \
./src/gamestates/main_menu_retro_layout_state.o \
./src/gamestates/main_menu_simple_list_state.o \
./src/gamestates/options_menu_state.o \
./src/gamestates/pseudo3d_race_state.o \
./src/gamestates/pseudo3d_race_state_physics.o \
./src/gamestates/race_results_state.o \
./src/gamestates/radio_selection_state.o \
./src/gamestates/side_drag_race_state.o \
./src/gamestates/vehicle_selection_showroom_layout_state.o \
./src/gamestates/vehicle_selection_simple_list_state.o \
./src/gamestates/vehicle_selection_state.o 

CPP_DEPS += \
./src/gamestates/course_editor_state.d \
./src/gamestates/course_selection_state.d \
./src/gamestates/debug_workshop_state.d \
./src/gamestates/main_menu_retro_layout_state.d \
./src/gamestates/main_menu_simple_list_state.d \
./src/gamestates/options_menu_state.d \
./src/gamestates/pseudo3d_race_state.d \
./src/gamestates/pseudo3d_race_state_physics.d \
./src/gamestates/race_results_state.d \
./src/gamestates/radio_selection_state.d \
./src/gamestates/side_drag_race_state.d \
./src/gamestates/vehicle_selection_showroom_layout_state.d \
./src/gamestates/vehicle_selection_simple_list_state.d \
./src/gamestates/vehicle_selection_state.d 


# Each subdirectory must supply rules for building sources it contributes
src/gamestates/%.o: ../src/gamestates/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"BUILD_PATH/src" -I"BUILD_PATH/src_libs" -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


