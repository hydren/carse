################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/carse.cpp \
../src/course.cpp \
../src/course_gfx.cpp \
../src/course_map_gfx.cpp \
../src/course_spec_parser.cpp \
../src/engine_sound.cpp \
../src/engine_sound_profile_parser.cpp \
../src/game_logic.cpp \
../src/main.cpp \
../src/race_hud.cpp \
../src/race_hud_layout_parser.cpp \
../src/shared_resources.cpp \
../src/vehicle.cpp \
../src/vehicle_ai.cpp \
../src/vehicle_engine.cpp \
../src/vehicle_gfx.cpp \
../src/vehicle_mechanics.cpp \
../src/vehicle_spec_parser.cpp 

OBJS += \
./src/carse.o \
./src/course.o \
./src/course_gfx.o \
./src/course_map_gfx.o \
./src/course_spec_parser.o \
./src/engine_sound.o \
./src/engine_sound_profile_parser.o \
./src/game_logic.o \
./src/main.o \
./src/race_hud.o \
./src/race_hud_layout_parser.o \
./src/shared_resources.o \
./src/vehicle.o \
./src/vehicle_ai.o \
./src/vehicle_engine.o \
./src/vehicle_gfx.o \
./src/vehicle_mechanics.o \
./src/vehicle_spec_parser.o 

CPP_DEPS += \
./src/carse.d \
./src/course.d \
./src/course_gfx.d \
./src/course_map_gfx.d \
./src/course_spec_parser.d \
./src/engine_sound.d \
./src/engine_sound_profile_parser.d \
./src/game_logic.d \
./src/main.d \
./src/race_hud.d \
./src/race_hud_layout_parser.d \
./src/shared_resources.d \
./src/vehicle.d \
./src/vehicle_ai.d \
./src/vehicle_engine.d \
./src/vehicle_gfx.d \
./src/vehicle_mechanics.d \
./src/vehicle_spec_parser.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"BUILD_PATH/src" -I"BUILD_PATH/src_libs" -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


