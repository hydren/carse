# Carse course definition file reference (work-in-progress)
This is a reference for the course definition file for Carse.

## File types
A course definition file shall be a `.properties` file containing information about a course. Also, this file must be 
accompanied by a CSV file containing geometric data about the course.

### Encoding and line delimiters
Currently, it's encouraged to use UTF-8 as encoding and Unix line delimiters (LF), but most common encodings probably 
work as well.

### Fields
The parser reads specific key-value pairs contained in the file. Here, each key is referred as a *field*. 
Most fields can be ommited and default values are assumed. Generally, the same can be acomplished by setting the field's 
value as `default`. There are a few, however, that cannot be ommited, otherwise a error will be issued. Next, there is 
a list of all fields that are supported.

## General
Fields regarding general information about the course.

* `name`: *(required)* a name for this course. This will be displayed in-game;

* `music`: the filename of the music to be played on this course, by default. This field is optional and can be left 
unspecified, which will cause the course to have no music to be played.

* `portrait`: the filename of a image to be used as a portrait for this course on the selection menu. This field is 
optional and can be left unspecified, which will cause the course to use a default portrait.

The following fields are informative-only. Which means that they are optional and don't affect behavior:

* `authors`: makers of the definition file.

* `credits`: used to give credits for contributions for this course definition.

* `comments`: used to store comments about this course.

## Styling
The course presentation is composed of a parallax scrolling background, the road segments and sprites along them. Currently
only one background layer is supported.

* `sprite_sheet_file`: the filename of the image containing all the main sprites. If unspecified, a default image is 
used (won't look good, though). This image should contain all the sprites depicting the vehicle itself.

### Landscape


## Road segments
The course layout is defined by a list of **segments**. Each segment specifies details about a specific part of the course.

* `segment_count`: *(required)* the number of segments of this course.

TODO

## Decoration & ornaments (aka "props")
TODO

## Physics
TODO
