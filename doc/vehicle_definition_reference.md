# Carse vehicle definition file reference (beta 1.0)
This is a reference for the vehicle definition file for Carse.

## File type
A vehicle definition file shall be a `.properties` file containing information about a vehicle.

### Encoding and line delimiters
Currently, it's encouraged to use UTF-8 as encoding and Unix line delimiters (LF), but most common encodings probably 
work as well.

### Fields
The parser reads specific key-value pairs contained in the file. Here, each key is referred as a *field*. 
Most fields can be ommited and default values are assumed. Generally, the same can be acomplished by setting the field's 
value as `default`. There are a few, however, that cannot be ommited, otherwise a error will be issued. Next, there is 
a list of all fields that are supported.

## General
Fields regarding general information about the vehicle.

* `definition`: *(required)* specifies what this file describes. Must be set as `vehicle`, otherwise this file won't 
be recognized as a vehicle definition file;

* `name`: *(required)* a name for this vehicle. This will be displayed in-game;

* `vehicle_type`: the type of the vehicle (car, bike, etc). Default type is `car`. Currently, the only recognized types 
are `car` and `bike`. This is because, currently, only *transversally bipartite* wheeled vehicles are supported. This 
means vehicle setups with wheels divided in 2 axles: front wheel(s) and rear wheel(s).

> Example: for cars, trucks, buses, formula, etc, use `car`. For bikes, trikes, etc, use `bike`;

* `vehicle_category`: a comma-separated list of names of categories which this vehicle can be classified. This is used 
mainly used to filter CPU-controlled opponents or for ordering in vehicle selection screen. Specifying this field is 
optional and it can be left unspecified.

> Example: formula, off-road, kart

The following fields are informative-only. Which means that they are optional and don't affect performance or behavior:

* `authors`: makers of the definition file.

* `credits`: used to give credits for contributions for this vehicle definition.

* `comments`: used to store comments about this vehicle.

* `vehicle_brand`: The brand of the vehicle.

* `vehicle_year`: The model year of the vehicle.


## Chassis
Information about the vehicle's chassis.

* `vehicle_mass`: the vehicle's mass, in kg. Default value is 1250 (approx. 2750 lbs).

* `weight_distribution`: the vehicle's static weight distribution, given by the percentage of weight in the rear 
wheel(s), in the range [0, 1]. This indirectly specifies the percentage of weight in the front wheel(s) (by the formula 
F=1-R). The default value is 0.5, which means 50/50 weight distribution. If drivetrain type is specified, another weight
 distribution is set as default, following this scheme:
 
    - (FF) Front-engine, front-wheel-drive (FWD): 0.4
    - (FR) Front-engine, rear-wheel-drive (RWD): 0.45
    - (FA) Front-engine, all-wheel-drive (AWD): 0.425
    - (RR) Rear-engine, rear-wheel-drive (RWD): 0.65
    - (RA) Rear-engine, all-wheel-drive (AWD): 0.6
    - (MR) Mid-engine, rear-wheel-drive (RWD): 0.55
    - (MA) Mid-engine, all-wheel-drive (RWD): 0.565
    - Others: 0.5 

### Dimensions
Generally, specifying vehicle's dimensions is useful for more accurate vehicle sprite scaling, simulation and collision 
checking. However, they can generally be ommited and default sizes are used (sprite scale can be specified separately).

* `vehicle_width`: the vehicle's width, in millimetres. There is no fixed default value and, if left unspecified, a 
built-in value is generated, which depends on a number of factors.

* `vehicle_height`: the vehicle's height, in millimetres. There is no fixed default value and, if left unspecified, a 
built-in value is generated, which depends on a number of factors. This may be used to estimate the vehicle's center of
gravity height and the vehicle's wheelbase when they were not specified. This information can also be expressed by 
providing a width/height ratio in a different field, namely `vehicle_width_height_ratio`.

* `vehicle_length`: the vehicle's length, in millimetres. There is no fixed default value and, if left unspecified, a 
built-in value is generated, which depends on a number of factors.

* `wheelbase`: the vehicle's wheelbase, in millimetres. In other words, the distance between the centers of the front 
and rear wheels. It is used in some physics computations but can be left unspecified. If ommited, it's estimated as 
approx. 57% of the vehicle's length.
 
* `center_of_gravity_height`: the height of the vehicle's center of gravity, in millimetres. This is used in some 
physics computations but can be left unspecified. If ommited, it's estimated as half the vehicle's height.

* `vehicle_frontal_area`: the vehicle's frontal (cross-sectional) area, in square metres. It is used to compute air drag
 and air lift forces but can be left unspecified. If left unspecified or as default, it will be estimated from the plane
formed by vehicle's width and height (80% of its area for cars and 50% for bikes). Otherwise a hardcoded value is used, 
depending of the vehicle type.

### Aerodynamics
The vehicle's aerodynamics are important to determine the air drag force on the vehicle. It indirectly influences the
top speed by setting a terminal velocity. Also, for advanced simulation, it influences net acceleration at high speeds 
due to lifting forces and downforce. However, they can generally be ommited and default values are used.

* `drag_coefficient`: the vehicle's drag coefficiend (Cd). It is used to compute air drag force but can be left 
unspecified. If left unspecified or as default, a hardcoded value is used, depending of the vehicle type (0.31 for cars,
and 0.6 for bikes).

* `drag_area`: the vehicle's drag area (product of the drag coefficient and frontal area). It's an alternative way to 
specify both `drag coefficient` and `vehicle_frontal_area` together.

* `lift_coefficient`: the vehicle's lift coefficiend (Cl). It is used to compute air lifting force but can be left 
unspecified. If left unspecified or as default, a hardcoded value will be used, depending of the vehicle type (0.2 for 
cars, 0.1 for bikes).

* `lift_area`: a value that is an analogue of the drag area, but for the air lifting force, it is the product of the 
lift coefficient and frontal area. It's an alternative way to specify both `lift_coefficient` & `vehicle_frontal_area`.

### Wheels
For wheeled vehicles, information about the wheel(s) dimensions are needed for proper simulation. Also, unless the
wheels are tyreless, information about tire and rim dimensions can also be provided to improve simulation accuracy.

* `driven_wheels`: specifies which of the vehicle's wheels are driven. Currently, supported values are `front`, `rear` 
and `all`. Default value is `rear`. This field should be omitted for non-wheeled vehicles (which would be the same as to
 set as `none`).

* `drive_wheel`: same as `driven_wheels`, although this field is encouraged when only one wheel is driven - as in bikes.

#### Basic wheel dimensions
The most basic information about the wheels are diameter/radius and width (wheel count is inferred from vehicle type).
These info can be specified in different ways by the supported fields. The following ones, however, abstract away 
details about tire and rims, so it's recommended only for tyreless wheels or when the data is lacking.

* `wheel_diameter`: the diameter of the vehicle's wheels, in millimetres. Default value is 650 (approx. 25.6 in). 

* `wheel_radius`: the radius of the vehicle's wheels, in millimetres. This field can be used as an alternative to 
`wheel_diameter`, since it indirectly specifies diameter (because `radius = diameter/2`).

* `driven_wheels_diameter`: same as `wheel_diameter`, although this field is encouraged when not all wheels are driven - 
as in rear-wheel-drive or front-wheel-drive vehicles, for example. This is done to (in the future) be able to specify
each wheel dimension separately (currently, only driven wheels are relevant to specification).

* `drive_wheel_diameter`: same as `driven_wheels_diameter`, although this field is encouraged when only one wheel is 
driven - as in bikes, for example.

* `driven_wheels_radius`: same as `wheel_radius`, although this field is encouraged in the same circunstances as 
`driven_wheels_diameter` field.

* `drive_wheel_radius`: same as `driven_wheels_radius`, although this field is encouraged in the same circunstances as 
`drive_wheel_diameter`.

* `wheel_width`: the width of the vehicle's wheels, in millimetres. Default value is 245 (approx. 9.65 in).

* `driven_wheels_width`: same as `wheel_width`, although this field is encouraged in the same circunstances as 
`driven_wheels_diameter` field.

* `drive_wheel_width`: same as `driven_wheels_width`, although this field is encouraged in the same circunstances as 
`drive_wheel_diameter`.

#### Tire and rim dimensions
For better simulation of the vehicle's wheels, instead of specifying basic wheel dimensions, tire and rim dimensions can
be specified, improving accuracy.

* `tire_diameter`: same as `wheel_diameter`, although this field is encouraged when the wheels do have tires. 
Default value is the same, which is equivalent to the size of *245/50 R16* or *135/90* R16 tires.

* `tire_radius`: the radius of the tires, in millimetres. This field can be used as an alternative to `tire_diameter`,
since it indirectly specifies diameter (because `radius = diameter/2`).

* `tire_width`: same as `wheel_width`, although this field is encouraged in the same circunstances as `tire_diameter`.
Defaut value is the same, which is equivalent to the width of *245/50 R16* tires.

* `rim_diameter`: the diameter of the rims, in millimetres (**not inches**). Default value is 62.5% of tire diameter.

* `rim_radius`: the radius of the rims, in millimetres (**not inches**). This field can be used as an alternative to 
`rim_diameter`, since it indirectly specifies diameter (because `radius = diameter/2`).

* `driven_wheels_rims_diameter`: same as `rim_diameter`, although this field is encouraged in the same circunstances as 
`driven_wheels_diameter` field.

* `drive_wheel_rim_diameter`: same as `driven_wheels_rim_diameter`, although this field is encouraged when only one 
wheel is driven - as in bikes, for example.

* `driven_wheels_rims_radius`: same as `rim_radius`, although this field is encouraged in the same circunstances as 
`driven_wheels_diameter` field.

* `drive_wheel_rim_radius`: same as `driven_wheels_rims_radius`, although this field is encouraged in the same 
circunstances as `drive_wheel_diameter`.

> Note that rim width is assumed to be equal to tire width.

#### Tire code
Specifying the tire code is also supported. This is the easier and leaner way to specify the both the vehicle's tires 
and rims, since it requires specifying only one field. For this reason, this is the **recommended** way to specify the 
vehicle's wheels' dimensions. 
The supported format is a shortened form of the full notation, with only the purely geometrical data. 

- For ISO metric tire code, the supported notation is `www/aa Rdd`, where `www` is the tire width (in millimetres), 
`aa` is the "aspect ratio" (sidewall height to width) and `dd` is the rim diameter (in **inches**). 

> Example: 195/55 R16 translates to tire width of 195mm, height of the side-wall of the tire is 55% of the width (107 mm
 in this example) and rim width of 16-inches (410 mm). 
 Computing wheel diameter would give (2×195×55/100)+(16×25.4) = 621 mm or (195×55/1270)+16 = 24.44 inches.
 
- Also for ISO metric tire code, an alternative notation `www/ttt Rdd` is supported, where `www` is the tire width (in 
millimetres), `ttt` is the tire diameter (instead of the aspect ratio of the sidewall height) and `dd` is the rim 
diameter (in **inches**). This notation is only supported with values greater than 200 for `ttt`.

> To take the same previous example, a 16-inch rim would have a diameter of 406 mm. Adding twice the tire's sidewall 
height (2×107) makes a total of ~620 mm tire diameter. Hence, a 195/55R16 tire can alternatively be labelled 195/620R16.

In the future, USC notation (aka Light truck (LT) tire code) and historical tire codes shall be supported.

The fields used to specify tire code are:

* `tires`: the vehicle's tires, specified using the tire code of the wheels.

* `driven_wheels_tires`: same as `tires`, although this field is encouraged in the same circunstances as 
`driven_wheels_diameter` field.

* `drive_wheel_tire`: same as `driven_wheels_tires`, although this field is encouraged when only one wheel is driven - 
as in bikes, for example.

> Note that all wheel/tire/rim dimensions fields **override** dimensions inferred from tire code, regardless of order. 
This is useful for correcting innacurate dimension inferred from tire code (due to improper conversion, etc).

## Powertrain
The vehicle's powertrain data main purpose is to determine propulsive power, but additional details influences how
this power is delivered. Currently, powertrain fields are mostly focused on engines, specifically combustion engines. 
It is intended to support other engines/motors types in the future.

* `engine_maximum_rpm`: the maximum engine speed of the vehicle, in RPM (revolutions per minute). Normally, engines 
provide vehicle propulsion by transfering rotational speed to the wheels through the drivetrain. Since the wheels are 
linked to the engine, when those rotate faster, so does the engine. However, engine design limits the speed range of 
which it can operate safely. So most of them have a minimum operational RPM (a.k.a idle speed) and a maximum RPM, also 
called *redline*. It's common that either a *rev limiter* or a *speed governor* restrict engine speed to stay in range.
Currently, minimum RPM is hardcoded to 1000rpm for all vehicles, so only maximum RPM needs to be specified. Ingame, the 
engine will stop increasing RPM after this value, working like a "rev limiter". Typical values for cars are 5000-9000, 
with bikes possibly going up to 19000rpm, depending of the model. Default value is 7000 for cars, 10000 for bikes.

* `engine_maximum_power`: the maximum net power delivered by the vehicle's engine, in horsepower (hp). Default value for
this is 320 for cars, 100 for bikes.

> Note that the **horsepower** unit referred is *mechanical* horsepower, not *metric* horsepower. Although both units 
are commonly abbreviated as *hp*, they are **not the same**. *Mechanical* horsepower (aka *imperial* horsepower)
definition leads it to being approximately 745.7 Watts. Metric horsepower, on the other hand, is approximately 735.5 W, 
which is equivalent to 98.6% of an imperial mechanical horsepower. To make matters worse, since they are very close in 
value, most information sources intermix definitions indiscriminately, creating typos and accumulating rounding errors.
Example: 395 HP = 400 PS

> Metric horsepower is often called *PS*, from the german translation of horsepower *Pferdestärke*, but many other 
abbreviations exist in other languages: *CV* (from the italian *cavallo vapore*, spanish *caballo de vapor*, portuguese 
*cavalo-vapor*), *CH* (from the french *cheval-vapeur*), *CP* (from the romanian *cal-putere*), *HK* (from the Norwegian
 and Danish *hestekraft*, swedish *hästkraft*), *HV* (from the finnish *hevosvoima*), *KC* (from the ukrainian *кінська 
 сила*), *KM* (from polish *koń mechaniczny*, the slovenian *konjska moč*), *KS* (from the bosnian, croatian and serbian
 *konjska snaga*, the czech and slovak *konska sila*), *PK* (from dutch *paardenkracht*), *л. с.* (from the russian 
*лошадиная сила*), etc. Imperial mechanic horsepower is sometimes labeled *bhp*, from *brake* horsepower (although this 
refers to the measuring method).

> Also note that it's recommended to use **net** power rating instead of gross rating. This means to use ratings that 
 were measured using the engine fitted with all ancillaries and the exhaust system as used in the car, so to use a more
 real-life figure. Good ratings are the SAE net, DIN and JIS. Avoid SAE gross, if possible.
 
* `engine_power_band`: the engine's power band curve. Depending of the curve, peak power is alocated to lower or higher 
RPM range. If left unspecified, then `typical` is assumed.
The following curves are supported:

    - Typical: peak power in mid-high RPM, specified with `typical` or `default`;
    - Peaky: peak power in the high RPM, specified with `peaky`;
    - Torquey: peak power in the lower RPM, speficied with `torquey`;
    - Wide: peak power in the mid RPM, with a "flatter" curve, specified with `wide`;
    - Narrow: peak power in the mid RPM, with a "thinner" curve, specified with `narrow`;
    - Quasi-flat: almost flat torque curve, with peak power in redline, specified with `quasiflat`, `quasi-flat` or 
    `quasi flat`;
    - Typical electric: full torque from initial RPM, decreasing halfway, specified with `typical electric`;
    - Semi-torquey: peak power in the mid-low RPM, specified with `semitorquey`, `semi-torquey` or `semi torquey`;
    - Semi-peaky: peak power halfway between typical and peaky, specified with `semipeaky`, `semi-peaky` or `semi peaky`;
    - Extra-torquey: peak power in lower RPM than "torquey", specified with `extratorquey`, `extra-torquey` or 
    `extra torquey`;
    - Extra-peaky: peaky power in redline, specified with `extrapeaky`, `extra-peaky` or `extra peaky`;

The following is a table with the RPM of peak power and torque for each curve:
    
	| Type              | Peak power rpm range | Peak torque rpm range |
	|-------------------|:--------------------:|:---------------------:|
	| Typical (default) |         91.5%        |         55.8%         |
	| Torquey           |         73.2%        |         36.6%         |
	| Peaky             |         94.0%        |         63.4%         |
	| Wide              |         97.6%        |         55.1%         |
	| Narrow            |         84.3%        |         54.4%         |
	| Quasi-flat        |         100%         |          ~50%         |
	| Typical electric  |         83.7%        |           0%          |
	| Semi-torquey      |         80.9%        |         45.7%         |
	| Semi-peaky        |         92.7%        |         59.9%         |
	| Extra-torquey     |         67.5%        |         26.7%         |
	| Extra-peaky       |         100%         |         71.0%         |

> Note that the RPM mentioned in the table is written as a percentage of the engine's maximum RPM.

* `engine_location`: the location of the engine in the vehicle. It is used in some physics computations but can be left 
unspecified. It determines (together with `driven_wheels` field) the drivetrain type of the vehicle. Supported values 
are `front`, `rear` and `middle` (or `mid`). If unspecified or set as `default`, then `rear` will be assumed (except for 
bikes, of which `middle` is the default).

* `engine_displacement`: the cylinder volume swept by all of the pistons of an engine, in cubic centimetres. Currently 
it's not very relevant for computations, only being used to estimate unengaged engine inertia, so it can be left 
unspecified.

* `engine_cylinder_count`: the amount of cylinder in the engine. Currently it's not very relevant for computations, only
 being used to estimate unengaged engine inertia, so it can be left unspecified. It's usually inferred from the 
`engine_configuration` field.

* `engine_valve_count`: the amount of valves of the engine. Currently it's not very relevant for computations, only
 being used to estimate unengaged engine inertia, so it can be left unspecified.
 
* `engine_disengaged_rev_speed_factor`: an adjustement factor to engine rev speed when no gear is engaged. This field is
 optional and can be ommited, which will default to 1.0, which means "standard" speed. Use values bigger than 1 (for 
 example 1.5, for 150% speed) for faster revving, and values in the range [0, 1] (e.g. 0.75, for 75% speed) for slower 
 revving. Default value for bikes is 5.0.

The following fields are informative-only. Which means that they are optional and don't affect performance or behavior:

* `engine_position`: the spatial orientation of the engine in its bay. Usually `longitudinal` or `transverse`.

* `engine_configuration`: the configuration of the engine. For piston engines, it's their cylinder layout (ex: inline-4,
V8, flat-6, single-cylinder, etc). For wankel engines, it's recommended to set as the number of rotors (ex: 2-rotor).

* `engine_aspiration`: the engine's air intake induction. Usually a combustion engine is naturally aspirated or uses 
some form of forced-induction (such as turbocharger or supercharger). Recommended values are `naturally aspirated`, 
`turbocharged` or `supercharged`. For multiple units, these multiplicative prefixes are recommended: `twin`, `triple` or
`quad` (ex: twin-supercharged, quad-turbocharged). There is also `twincharged`, for compound turbocharger/supercharger.

* `engine_valvetrain`: for piston engines, it's the mechanical system that controls the operation of the intake/exhaust 
valves. Usually `DOHC`, `SOHC` or `OHV`, but some other types exist (`flathead`/`sidevalve`, `IOE`, etc).

* `engine_stroke_count`: the number of strokes to complete a cycle of the engine. Its usually 4 (four-stroke) or 2 (two-
stroke). By default, its 4.

### Transmission
The vehicle's transmission data is used to determine the vehicle's speed range, for each gear. For vehicles without 
wheels, these should be ommited altogether. CVT transmissions are not supported yet.

* `gear_count`: the number of gears on the transmission (excluding the reverse gear). Any positive integer can be used.
Default value is 5.

* `gear_ratios`: the ratios of the forward gears. Currently, can only be set as `default` or `custom`. By setting as 
`default`, a builtin set of ratios will be used. Setting as `custom` indicates that the ratios will be specified 
individually. In the future, it should support specifying gear ratios in a comma separated list directly in this field 
or even preset gear ratios, like "typical", "sports", "rally".

> Default ratios are generated by using the following recurrence relation:
>* `g(n) = g(n+1) + 0.1 * (t - n - b)`
>* `g(t) = 0.8`
>* where `g(n)` the gear ratio of the nth gear, `n` is the gear number and `t` is the gear count. The `b` parameter is 
only used for bikes, for which it has value 1.0 - otherwise it's 0.

* `gear_n_ratio`: the ratio of the nth gear, where `n` is should be the gear number (ex: `gear_1_ratio = 3.2`). Each 
gear should can be specified individually or be ommited and a default value is used.

* `gear_reverse_ratio`: the transmission's reverse gear ratio. Default value is 2.5.

* `gear_differential_ratio`: the transmission's differential ratio, also known as final drive ratio or axle ratio. This 
is the ratio that is applied to all gears. The default value is 4.75 for cars and 7.275 for bikes.

> Note that, for some vehicles (like bikes), this value should be set as the product of the final drive ratio and 
another value known as *primary drive ratio*, since specifying additional multiplying ratios are not supported.

> For fixed-gear vehicles, gear count, ratio and differential should all be set as 1. 

* `transmission_efficiency`: the average transmission efficiency of the engine. It is used in some physics computations 
but can be left unspecified. If left unspecified or as default, a 0.7 (70%) value is used.

## Animation
The vehicle's in-game animation consists of a set of animated sprites that are drawn on top of the scenary. Normally the 
sprite depicts the back of the vehicle, as if the camera was behind it. This mimicks a 3D perspective, a technique that 
is commonly called 2.5D or pseudo-3D. The vehicle's sprites can contain the following animations:

- *Turning animation*: when the vehicle is making a turn (i.e. when passing through a corner, or changing lanes), a 
different sprite is used (if available), which should depict the vehicle making such a turn - usually a back 3/4 view 
for cars or leaned view for bikes;

- *Running animation*: for either going straight or turning, the sprites can animated, having 2 or more frames per angle. 
For wheeled vehicles, the animation usually depicts wheel movement, changing light reflexes, etc;

- *Additional effects*: additional effects can be specified through other sprites, which will compose the final result. 
Currently, in addition to the main sprite, there are layers for shadow, brakelight, exhaust backfire and burnout smoke. 
All these effects are stored in differents sprites. 

### Main sprite
The main sprite file should be an image containing all the vehicle's images, organized side-by-side. Each image should 
be located inside an equally-sized region (called "frame") of the given sprite file. Also, frames are expected to be 
placed side-by-side, or top-down (or both), similar to text orientation on a page.

	                                   +-------+
	                                   |       |
	                                   |   0   |
	                                   |       |          +-------+-------+
	                                   |       |          |       |       |
	+-------+-------+-------+          +-------+          |   0   |   1   |
	|       |       |       |          |       |          |       |       |
	|   0   |   1   |   2   |    or    |   1   |    or    |       |       |
	|       |       |       |          |       |          +-------+-------+
	|       |       |       |          |       |          |       |
	+-------+-------+-------+          +-------+          |   2   |
	                                   |       |          |       |
	                                   |   2   |          |       |
	                                   |       |          +-------+
	                                   |       |
	                                   +-------+

There is a specific ordering expected for the images inside the sprite file. For example, "normal" sprite(s) (straight 
going, not turning) are expected to be placed first, and each "turning" sprites (if any) should be placed next, ordered 
by increasing depicted angle. To organize this better, animations are grouped together in **states**. Each state should
contain sprite(s) depicting different turn "angles". The first state is always the one with "normal" sprite(s) (straight 
going, not turning). Further states contains "turning" sprites, each with increasing angle.

> It's recommended, to place all frames of one state side-by-side in the sprite file.

For a vehicle with no turning animation, all images should depict the running straight animation, placed side-by-side:

	+-------+-------+-------+          
	|       |       |       |          
	|   0   |   1   |   2   |          
	|       |       |       |          
	|       |       |       |          
	+-------+-------+-------+          

> Frames 0, 1 and 2 are intercalated to produce the "running" animation.

For a vehicle with static "turning" animation, each images should depict different turn angles, placed top-down:

	+-------+
	|       |
	|   0   |
	|       |
	|       |
	+-------+
	|       |
	|   1   |
	|       |
	|       |
	+-------+
	|       |
	|   2   |
	|       |
	|       |
	+-------+

> Frame 0 is used to depict straight running, frame 1 for "slightly" turning and frame 2 for "hard" turning.
	                                   
For a vehicle with both "running" and "turning" animation, each row of frames belong to the same state, providing a 
"running" animation for each turn angle.

	+-------+-------+-------+          
	|       |       |       |          
	|   0   |   1   |   2   |          
	|       |       |       |          
	|       |       |       |          
	+-------+-------+-------+          
	|       |       |       |          
	|   3   |   4   |   5   |          
	|       |       |       |          
	|       |       |       |          
	+-------+-------+-------+          

> Frames 0, 1 and 2 are intercalated to produce the "running" animation for going "straight". 
> Frames 3, 4 and 5 are intercalated to produce the "running" animation for turning.

Each state can have a different number of frames like this:

	+-------+-------+
	|       |       |
	|   0   |   1   |
	|       |       |
	|       |       |
	+-------+-------+
	|       |
	|   2   |
	|       |
	|       |
	+-------+-------+-------+          
	|       |       |       |          
	|   3   |   4   |   5   |          
	|       |       |       |          
	|       |       |       |          
	+-------+-------+-------+          

> Frames 0 and 1 are intercalated to produce the "running" animation for going "straight". 
> Frame 2 is used to for "slightly" turning. 
> Frames 3, 4 and 5 are intercalated to produce the "running" animation for "hard" turning.

Frame size [width x height] need to be specified and are the same for all frames.

Turning sprites are expected to be depicted *towards left*. In-game, sprites are mirrowed to depict right-side turning. 
However, asymmetric sprites are supported, but need to be specified as such. If so, all right-side sprites are expected 
to be located after all left-side ones.

The PNG format is recommended for sprites sheets.

* `sprite_sheet_file`: the filename of the image containing all the main sprites. If unspecified, a default image is 
used (won't look good, though). This image should contain all the sprites depicting the vehicle itself.

* `sprite_state_count`: the number of animations/states (straight-going, turning, etc) on this sheet, arranged top-down.
Default is 1.

* `sprite_frame_width`: the width of the sprite's frames, in pixels. Default value is 56.

* `sprite_frame_height`: the height of the sprite's frames, in pixels. Default value is 36.

* `sprite_frame_count`: the number of frames of each animation state. Default value is 1 (single frame). This field 
specifies this value for all states at once.

> Example: `sprite_frame_count = 2`

* `sprite_stateN_frame_count`: the number of frames of animation state `N`, where `N` is a state number. This field is
used to specify frame count for each state individually. You can ommit this value for states that have only 1 frame.

> Example: 
    `sprite_state0_frame_count = 2`
    `sprite_state1_frame_count = 2`

> Note: state numbers starts at 0.

* `sprite_frame_duration`: the duration, in seconds, of each frame. If ommitted, a value of 0.25 (quarter of a second) 
is used by default, which should be OK for most cases. Smaller values gives faster animations. If zero or a negative 
value is specified, no frame cycling will occur at all (and thus, no animation). Because of that, a value of -1 is 
recommended for cases when there is a single frame only in each state. It's also possible to set this value as `dynamic` 
to request that the duration of frames change dynamically according to the vehicle's speed (which is needed for wheeled 
vehicles, for example). Specifying this value is optional and if you are unsure, set as default or leave unspecified.

* `sprite_animation_speed_factor`: a speed factor applied to the sprite's animation speed. This value works as a 
fast-forward or a slowdown to the sprite's animation. This is suitable for situations where the sprite sheet contains 
animation for more (or less) than one complete wheel animation cycle. If this value is ommitted or set as "default", 
it's assumed to be 1.0, which means normal speed (1x). Use values bigger than 1 (ex: 1.5, 2.0) for faster animation, and
values in the range [0, 1] (ex: 0.75, 0.4) for slower animation. If zero or a negative value is specified, no frame 
cycling will occur at all (and thus, no animation). Specifying this value is optional and if you are unsure, set as 
default or leave unspecified.

* `sprite_depicted_turn_angles`: a comma-separated list of approximate turn angles (azimuth), in degrees, depicted on 
each animation state. On vehicle sprites that contains turning animation, it's assumed that these turning animation 
sprites are equally-angle-spaced (which means that between any two consecutive sprites of the turning animation, the 
azimuthal angle between them is roughly the same) and that the greatest depicted azimuthal angle (and consequently, the 
last) is roughly 45 degrees. But in practice, the actual turning angle depicted on sprites may be very small, or very 
big, or even contain irregular-angle-spaced sprites. So specifying these azimuthal angles can prevent the vehicle's 
animation from "turning too slow" or "too fast", a weird "feeling" in-game that the vehicle is understeering or 
oversteering (drifting). Specifying these values is optional and if you are unsure, leave them unspecified altogether.

> Example: `sprite_depicted_turn_angles = 0, 15, 30`

> Note that, for all cases, values greater than 45 makes some sprites not show on screen since the maximum angle in-game 
is supposed to be 45 degrees (pi/4 radians).

* `sprite_stateN_turn_angle`: the approximate turn angle (azimuth), in degrees, depicted on animation state `N`, where 
`N` is a state number. This field provides an alternative way to specify turn angles, by specifying turn angle for each 
state individually.

> Example:
    `sprite_state0_turn_angle = 0`
    `sprite_state1_turn_angle = 15`
    `sprite_state2_turn_angle = 30`

* `sprite_max_depicted_turn_angle`: the maximum approximate turn angle (azimuth), in degrees, depicted in animation 
states. This field provides an alternative way to specify turn angles in which the max angle is specified and the other 
previous angles are estimated (interpolated actually). Default value is 45. Again, specifying this value is optional and 
if you are unsure, set as default or leave unspecified.

> Example: `sprite_max_depicted_turn_angle = 20`

* `sprite_depicted_turn_angle_progression`: type of angle progression used to estimate turn angles when using the 
`sprite_max_depicted_turn_angle`. This estimation, by default, assumes equally-angle-spaced, but can be specified to 
expect other types of angle progression depicted on sprites. Valid values for this are `linear` (equally-angle-spaced), 
`linear-with-threshold` (equally-angle-spaced with "fast" first angle) and `exponential` (exponetial-angle-spaced). 
Default value is `linear-with-threshold`.

> Example: 
    `sprite_max_depicted_turn_angle = 30`
    `sprite_depicted_turn_angle_progression = linear`
    
* `sprite_depicted_turn_angle_proximal_interpolation`: a boolean value that indicates that the state shown on animation 
will be the one with the closest angle to the current turn angle instead of the state with the highest angle less than 
current turn angle. Default value is `false`. Specifying this is optional and if you are unsure, leave it unspecified.

* `sprite_asymmetric`: indicates whether this sprite is horizontally symmetrical or not. If `true`, the sprites are 
expected to include right-leaning versions of each state (with the exception of the first state), after all the other 
sprites. Also, in cases where the sprite contains only a single state, setting this as true also triggers mirrowing of 
such sole sprite (which normally is not). Default value is `false`.

* `sprite_vehicle_width`: the vehicle's width (in pixels) as depicted on the sprite. Although optional, it is 
recommended to specify this to improve the alignment of effects, such as the tires' burnout smoke. If defaulted or 
non-specified, it is set as 85.7% of the sprite's frame width. This, together with the `vehicle_width` field, can be 
used to compute sprite's scale implicitly (though if is overriden by sprite scale fields).

* `sprite_vehicle_height`: the vehicle's height (in pixels) as depicted on the sprite. This, together with the 
`vehicle_height` field, can be used to compute sprite's scale implicitly (though if is overriden by sprite scale fields).

* `sprite_scale`: a scaling factor applied to the sprite. A value of 1.0 means no scaling. Although optional, it is 
recommended to specify this, otherwise the vehicle can appear too big or too small compared to the others. If real-world 
data about the vehicle's size is available, then the recommended way to obtain a suitable value for scale is to divide 
the vehicle's real-world size, in millimetres, by its size on the sprite, in pixels, and then multiply the result by a
24/895 scale factor (~0.027). Default value is 1.0.

> Example: real-world width is 1800mm, on-sprite width is 96px, then scale should be:
    `(1800/96) x (24/895) = 18.75 x 0.027 ≃ 0.5`
    
> In this case, a scale of 0.5 (half-size) is suitable.
    `sprite_scale = 0.5`
    
* `sprite_scale_x`: a scaling factor applied to the sprite horizontally. Default value is 1.0.

* `sprite_scale_y`: a scaling factor applied to the sprite vertically. Default value is 1.0.

* `sprite_keep_aspect_ratio`: a boolean value that indicates whether or not to keep sprite aspect ratio when scaling. 
Default value is `false`.

* `sprite_contact_offset`: The offset between the sprite's bottom and the depicted contact point of the vehicle. Usually 
the contact point(s) are the bottom of the vehicle's wheels, so this value can be fitted with ammount of pixels between 
the bottom of the sprite and bottom of the wheels. This is useful to specify when the sprite has a unusually large 
shadow, for example. When this value is specified as "default", a value of 2px is assumed. If left unspecified, the
offset is assumed to be 0.

### Shadow sprites
Shadow layer sprites parsing is similar to the main one, with the difference of always having only one frame per state. 
It is drawn before the vehicle main sprite, so to have the desired shadow compositioning effect. Note that these sprites 
should depict the vehicle's shadow preferably as it was being cast from a top-down positioned light source.

* `shadow_sprite_filename`: the filename of the image containing the sprite for an optional shadow underlay. If this 
field is specified, the shadow sprite will be drawn before the vehicle sprite. Note that the shadow image should be in 
the same scale as the vehicle sprite. If this field is assigned 'none', 'default' or is left unspecified, it's assumed 
that there is no shadow to draw/cast or it's already depicted on the main sprite. The sheet should include a number of 
sprites matching the number of animation states.

* `shadow_positionN`: the position of the shadow for the animation state `N` (where `N` is a state number) as a pair of
coordinates X and Y, in relation to the vehicle's sprite. These coordinates are used to position the shadow sprites on 
the screen; they probably should be different for each sprite state. It's suggested to fill these values by measuring 
the position of where should the shadow appear behind on each sprite state. Note that the number of positions should 
match the number of states. If nothing is specified, default positions will be generated with coordinates (0, 0).

> Example: `shadow_position0 = 10, 25`

* `shadow_positionN_x`: the horizontal position of the shadow for the animation state `N` (where `N` is a state number),
 in relation to the vehicle's sprite. This is an alternative syntax to specify shadow X and Y coordinates separately.

> Example: `shadow_position0_x = 10`
           `shadow_position0_y = 25`

* `shadow_positionN_y`: same as the `shadow_positionN_x`, but refers to the vertical position.

### Brakelights sprites
Brakelights overlay sprites parsing is similar to shadow underlay one, with a few more details. It is drawn after the 
vehicle main sprite, so to have the desired compositioning effect. The sprites must be either a single image or a sheet 
with a number of sprites matching the amount of states, option of which should be indicated in a specific field.

* `brakelights_sprite_filename`: the filename of the image containing the sprite for the brakelights effect overlay. 
Note that the actual brakelight image portraited in the sheet should be in the same scale as the vehicle sprite sheet. 
If set as `default`, a built-in asset will be used for the effect. If left unspecified, no brakelights will be drawn at 
all.

* `brakelights_multiple_sprites`: specifies whether the brakelight sprite is a single image or a sprite sheet. If false,
it's assumed that there only a single brakelight sprite on the image file, and it will be used for all animation states.
If true, the brakelight animation is assumed instead to have multiple sprites (sheet) on the image file, one per each 
animation state. Default value is false.

* `brakelights_mirrowed`: specifies whether the brakelight sprite will be drawn mirrowed as well. If true (default), a 
mirrowed version of the brakeligthts animation is also drawn in the opposite side of the vehicle. The position of the 
mirrowed brakelights is a mirrowed version of the brakelights positions, minus the animation width.

* `brakelights_positionN`: the position of the brakelights for the animation state `N` (where `N` is a state number) as 
a pair of coordinates X and Y, in relation to the vehicle's sprite. These coordinates are used to position brakelights 
effects on the screen when braking in the case that the `brakelights_multiple_sprites` property is set false. It is 
suggested to fill these values by measuring where should the brakelights appear on each sprite state (probably where the
 tail lights are depicted). Note that the number of positions should match the number of states. In case nothing is 
specified, a default position will be generated, but it's almost surely won't match appropriately.
If the `brakelights_multiple_sprites` property is true, the default positions will all be set zero.

> Example: `brakelights_position0 = 10, 25`

> In the case where the main is asymmetric, when specifying brakelights position for each state, positions for mirrowed 
versions of each state need to be specified as well. This is done by specifying an additional position with name equal 
to normal version plus a `b` syntax. For example: 
           `brakelights_position0 = 10, 25`  (state 0 sprite brakelights position)
           `brakelights_position1 = 12, 25`  (state 1 sprite brakelights position)
           `brakelights_position1b = 27, 25` (state 1b (mirror-version) sprite brakelights position)

* `brakelights_positionN_x`: the horizontal position of the brakelights for the animation state `N` (where `N` is a 
state number), in relation to the vehicle's sprite. This is an alternative syntax to specify shadow X and Y coordinates 
separately.

> Example: `brakelights_position0_x = 10`
           `brakelights_position0_y = 25`

* `brakelights_positionN_y`: same as the `brakelights_positionN_x`, but refers to the vertical position.

* `brakelights_sprite_scale`: specifies scaling factor applied to the brakelight sprite, with 1.0 meaning no scaling. If
 a non-default `brakelights_sprite_filename` property is specified, this scale is assumed to be the same as the main 
vehicle sprite's scale. Otherwise it is assumed to be 1.0.

* `brakelights_sprite_scale_x`: same as `brakelights_sprite_scale`, but applies only to horizontal scale.

* `brakelights_sprite_scale_y`: same as `brakelights_sprite_scale`, but applies only to vertical scale.

* `brakelights_sprite_offset_x`: specifies an horizontal offset applied to the brakelights sprites' positions, meant to 
center them when necessary (i.e. mirrowed sprites with surrounding alpha regions). If a custom value is specified for 
the `brakelights_sprite_filename` property, this value is defaulted to 0, otherwise, a builtin value is set to match the
 default brakelights sprite metrics.
 
* `brakelights_sprite_offset_y`: same as `brakelights_sprite_offset_x`, but refers to the vertical offset.

### Exhaust backfire animation
Backfire overlay sprites parsing is similar to brakelights overlay sprites ones, with the difference that its animation 
sprite is currently not customizable, using a built-in sprite for all vehicles. Only positioning is parsed for them.

* `backfire_positionN`: the position of the exhaust backfires for the animation state `N` (where `N` is a state number) 
as a pair of coordinates X and Y, in relation to the vehicle's sprite. These coordinates are used to position backfire 
effects on the screen; they probably should be different for each sprite state. It is suggested to fill these values by 
measuring where the exhaust tips are located on each sprite state. Note that the number of positions should match the 
number of states. If left unspecified, no backfire effect will be shown.

> Example: `backfire_position0 = 13, 28`

> In the case where the main is asymmetric, when specifying backfire position for each state, positions for mirrowed 
versions of each state need to be specified as well. This is done by specifying an additional position with name equal 
to normal version plus a `b` syntax. For example: 
           `backfire_position0 = 13, 28`  (state 0 sprite backfire position)
           `backfire_position1 = 15, 28`  (state 1 sprite backfire position)
           `backfire_position1b = 25, 28` (state 1b (mirror-version) sprite backfire position)

* `backfire_positionN_x`: the horizontal position of the exhaust backfires for the animation state `N` (where `N` is a 
state number), in relation to the vehicle's sprite. This is an alternative syntax to specify shadow X and Y coordinates 
separately.

> Example: `backfire_position0_x = 10`
           `backfire_position0_y = 25`

* `backfire_positionN_y`: same as the `backfire_positionN_x`, but refers to the vertical position.

* `backfire_mirrowed`: specifies whether the exhaust backfire sprite will be drawn mirrowed as well. If true, a mirrowed
 version of the backfire animation is also drawn in the opposite side of the vehicle. The position of the mirrowed 
backfire is a mirrowed version of the normal backfire position, minus the animation width. You should set to true if the
 vehicle sprite depicts a exhaust tip in each side of the rear, for example. By default, this value is set as false.
 
* `backfire_extra_position_offset`: the offset for the position of an extra backfire effect, relative to the main one. 
If specified, and non-zero, then an additional backfire effect is displayed, with position equal to the main backfire 
position (at current state), plus this offset. This is useful for vehicles depicting twin or quad exhaust tips. If left 
unspecified or set as zero or none, no backfire effect will be shown.

> Example: `backfire_extra_position_offset = 4, 0`

* `backfire_extra_position_offset_x`: same as `backfire_extra_position_offset`, but only specifies horizontal position.

> Example: `backfire_extra_position_offset_x = 4`

* `backfire_extra_position_offset_y`: same as `backfire_extra_position_offset_x`, but refers to the vertical position.

### Burnout smoke animation
Currently, burnout smoke animation is hardcoded and is the same for all vehicles, but may be customizable in the future.
Positioning for smoke sprites is computed from the `sprite_contact_offset` field and sprite depicted turn angles.

### Alternate sprites
In-game, alternative sprite sheets can be selected for vehicles. Because of this, multiple alternate appearances can be 
specified for each vehicle (useful for skins, alternate paintjobs, etc). Alternative sprites works the same way as the 
standard one (states, frames, shadow, etc), each one possessing its own metrics or inheriting from others. Each 
alternative sprite have its own index, starting at 0 (the standard sprite would be -1). Currently, up to 32 alternative 
sprites can be specified.

* `alternate_sprite_sheetN_file`: the filename of the image containing the Nth alternative sprites to the main sprites, 
where `N` is an alternative sprite index. All the standard sprite metrics specified in this file also applies to all 
alternative sprites.

> Example: 
    `alternate_sprite_sheet0_file = assets/sprites/car-sheet-coupe-black.png`
    `alternate_sprite_sheet1_file = assets/sprites/car-sheet-coupe-blue.png`
    `alternate_sprite_sheet2_file = assets/sprites/car-sheet-coupe-yellow.png`
    
> Note: alternate sprite indexes starts at 0.
    
* `alternate_sprite_sheetN_definition_file`: the filename of a `.properties` file containing animation related fields 
to specify an alternate sprite, where `N` is an alternative sprite index. This field is used when the alternate sprites 
have different metrics from the main ones. The sprite metrics specified in this file does not apply to alternative 
sprites specified this way.

> Example:
    `alternate_sprite_sheet0_definition_file = assets/sprites/car-alternate-appearance.properties`

* `alternate_sprite_sheetN_inherit_from`: an index of a previous alternate sprite to inherit all sprite metrics. This is
needed when alternate sprite files share metrics that are not the main ones.

> Example:
    `alternate_sprite_sheet0_file = assets/sprites/car-sheet-coupe-black.png`
    `alternate_sprite_sheet1_definition_file = assets/sprites/car-alternate-appearance.properties`
    `alternate_sprite_sheet2_file = assets/sprites/car-alternate-appearance-color2.png`
    `alternate_sprite_sheet2_inherit_from = 1`
In this case, alternate sprite index 2 inherits metrics from index 1 (instead of the main one)

## Sound
The vehicle's in-game sound consists of a set of sound samples, meant to reproduce the vehicle's sound when functioning.
These samples, together with settings specifying how and when to play these, constitutes a sound profile. Since most of 
the sounds refer mainly to vehicle's engine sounds, sound profiles are referred as engine sound profiles, and specifying 
vehicle sound is mostly just dealing with how its engine will sound like. Most engines of the same type sound the same
(i.e, V6 engines sound similar to each other), so there are built-in presets for most engine types' sounds to be reused.
However, a custom profile can be specified as well.

* `sound`: the vehicle's sound profile. Valid values are `default`, `none`, `custom` or a preset sound profile name. If 
set as `none`, no vehicle sound is played. If set as `default`, the default engine sound profile is used - which is also
 the case if an invalid value was set.

> Examples:
    `sound = none`     (no sound)
    `sound = default`  (default engine sound)

If a preset sound profile name is specfied, it will be used accordingly. Refer to the list of built-in preset sound 
profiles for a detailed look, but, basically, engine sound profiles' names are composed as `T_engine_default`, where `T`
 is an engine type (e.g. V-8, Flat-6, etc), but in **lowercase, without hyphens**.
 
> Examples:
    `sound = v8_engine_default`       (default V-8 engine sound)
    `sound = flat6_engine_default`    (default Flat-6 engine sound)
    `sound = inline4_engine_default`  (default Inline-4 engine sound)

For some engine types, there are multiple built-in presets so to specify one of these specifically, instead of using 
the default one, use specific engine sound profile names, composed as `T_engineN`, where `T` is the same engine type as
before and `N` the preset number. Again, refer to the list of built-in preset sound profiles for a detailed look.

> Examples:
    `sound = v8_engine1` (first V-8 engine sound option)
    `sound = v8_engine2` (second V-8 engine sound option)
    
Presets sometimes are only aliases to other presets. The default preset, for example, is always an alias to one of these
 presets. There are some alternative alias presets that are composed as `default_N_cylinder_engine`, where `N` is the 
number of cylinders of the engine. A few special cases exist. Again, refer to the list of built-in preset sound profiles
 for a detailed look.

> Examples:
    `sound = default_3_cylinder_engine`             (default 3 cylinder engine sound option)
    `sound = default_wankel_engine`                 (default Wankel engine sound option)
    `sound = default_two_stroke_4_cylinder_engine`  (default 2-stroke 4-cylinder engine sound option)
 
If set as `custom`, then a custom sound profile will be expected - sound samples and settings are to be specified.

### Custom profiles
When specifying a custom sound profile, each sound sample filename used must be specified. For each sample, settings 
like volume and pitching can be configured. For engine sounds, at least 2 samples are recommended: one for idling engine
 sound and another for revving engine sound. To deliver a satisfactory result, samples should depict engine sound with 
engine speed as steady as possible (otherwise sound will oscilate unpleasantly). Also, for accurate results, depicted 
engine speed (RPM) should be properly specified for each sound sample. The OGG format is recommended for sound samples.

* `sound_rpm_pitching`: indicates wether the sound samples should be pitched according to the vehicle's current engine 
speed. If set as `true`, each sample will be pitched (using its original depicted engine RPM as basis) to match current 
vehicle's engine speed. For example, if some sound sample originally depicts engine speed at 2000rpm, while being played
in-game, it will be pitched 2x when the player's vehicle is running at 4000rpm engine speed. If set as `false`, no 
pitching will be performed at all, and samples will be played in normal pitch regardless of speed. This field is 
optional and can be ommited, which will default its value to `true`.

* `sound_count`: the number of sounds samples used in this vehicle's sound profile. Sound samples will be read up to 
this value, if possible. By default, this value is 16.

* `soundN`: *(required)* the filename of the Nth sound sample used in this sound profile, where `N` is a positive 
integer, referred here as **sound index**. When dealing with engine sounds, it's recommended to use sound samples 
depicting engine speed as steady as possible.

* `soundN_rpm`: the RPM (engine speed) range for the Nth sound sample, where `N` is the sound index. This value is the 
initial RPM of which this sample will be played. If left unspecified or set as -1, it's attempted to set default values,
 but usually they end up being unsuitable.
 
* `soundN_depicted_rpm`: the RPM (engine speed) depicted by the Nth sound sample when played at normal pitch / playback 
speed, where `N` is the sound index. Specifying this value correctly is sometimes difficult, requiring information 
regarding the circumstances where the sample was recorded, but is essential to give accurate results. If set as -1 or 
left unspecified, it will be assumed to be the same as the RPM range of the sample.

* `soundN_pitch_factor`: a pitch factor for the Nth sound sample, where `N` is the sound index. This is an alternative 
way to adjust sample pitch manually instead of relying on depicted/current RPM calculation. Using this field is a bit 
tougher because it specifies pitch factor at this sample's declared RPM range (pitch will change accordingly to current 
engine speed). Use values bigger than 1 (e.g. 1.5, for 50% higher pitch) for higher pitch / faster playback speed and 
values in the range [0, 1] (e.g. 0.75, for 25% lower pitch) for lower pitch / slower playback speed.

> The relationship of range RPM, depicted RPM and pitch factor is: `depictedRpm = rangeRpm * pitchFactor`

* `soundN_volume_factor`: a volume factor for the Nth sound sample, where `N` is the sound index. This field is meant to
 be used to adjust samples volumes to the same levels (i.e. some sample is louder/quieter than the others). Note that 
this value is merely a multiplier for the sound's volume, since its absolute volume depends on a lot of factors. If 
ommitted, this value is assumed to be 1.0, which means normal volume. Use values values in the range [0, 1] (e.g. 0.75 
for 75% volume) for lower volume (quieter). Values bigger than 1 (ex: 1.5, for 150% volume) would work for higher volume
 (louder), but may not be supported, depending on the sound library carse was built against.
