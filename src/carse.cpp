/*
 * carse_game.cpp
 *
 *  Created on: 5 de dez de 2016
 *      Author: hydren
 */

#include <carse.hpp>
#include "gamestates/pseudo3d_race_state.hpp"
#include "gamestates/main_menu_simple_list_state.hpp"
#include "gamestates/main_menu_retro_layout_state.hpp"
#include "gamestates/vehicle_selection_simple_list_state.hpp"
#include "gamestates/vehicle_selection_showroom_layout_state.hpp"
#include "gamestates/course_selection_state.hpp"
#include "gamestates/options_menu_state.hpp"
#include "gamestates/course_editor_state.hpp"
#include "gamestates/radio_selection_state.hpp"
#include "gamestates/race_results_state.hpp"
#include "gamestates/side_drag_race_state.hpp"
#include "gamestates/debug_workshop_state.hpp"

Carse::Carse()
: Game("Carse", null, 800, 600), logic(GameLogic::getInstance()), sharedResources(null)
{
	this->maxFps = 60;
}

Carse::~Carse()
{
	if(sharedResources != null)
		delete sharedResources;
}

void Carse::initialize()
{
	this->sharedResources = new SharedResources();
	this->sharedResources->sndCursorIn.setVolume(logic.masterVolume);
	this->sharedResources->sndCursorOut.setVolume(logic.masterVolume);
	this->sharedResources->sndCursorMove.setVolume(logic.masterVolume);

	this->logic.initialize();

	this->addState(new Pseudo3DRaceState(this));

	if(not logic.raceOnlyMode)
	{
		this->addState(new MainMenuSimpleListState(this));
		this->addState(new MainMenuRetroLayoutState(this));
		this->addState(new VehicleSelectionSimpleListState(this));
		this->addState(new VehicleSelectionShowroomLayoutState(this));
		this->addState(new CourseSelectionState(this));
		this->addState(new OptionsMenuState(this));
		this->addState(new CourseEditorState(this));
		this->addState(new RadioSelectionState(this));
		this->addState(new RaceResultsState(this));
		this->addState(new SideDragRaceState(this));
		this->addState(new DebugWorkshopState(this));
		this->setInitialState(MAIN_MENU_CLASSIC_LAYOUT_STATE_ID);
	}

	this->setInputManagerEnabled();
	this->logic.onStatesListInitFinished();

	Game::initialize();
}
