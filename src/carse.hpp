/*
 * carse.hpp
 *
 *  Created on: 5 de dez de 2016
 *      Author: hydren
 */

#ifndef CARSE_HPP_
#define CARSE_HPP_
#include <ciso646>

#include "game_logic.hpp"

#include "fgeal/extra/game.hpp"

#include "futil/language.hpp"

// Versioning
#define CARSE_MAJOR_VERSION 0
#define CARSE_MINOR_VERSION 6
#define CARSE_REVIS_VERSION 3
#define CARSE_DEV_VERSION

// Turn 'macro' into a string literal without expanding macro definitions (however, if invoked from a macro, macro arguments are expanded).
#define STRINGIZE(macro) #macro

// Turn 'macro' into a string literal after macro-expanding it.
#define STRINGIFY(macro) STRINGIZE(macro)

#ifdef CARSE_DEV_VERSION
	#define CARSE_VERSION_STR (STRINGIFY(CARSE_MAJOR_VERSION) "." STRINGIFY(CARSE_MINOR_VERSION) "." STRINGIFY(CARSE_REVIS_VERSION) "-dev")
#else
	#define CARSE_VERSION_STR (STRINGIFY(CARSE_MAJOR_VERSION) "." STRINGIFY(CARSE_MINOR_VERSION) "." STRINGIFY(CARSE_REVIS_VERSION))
#endif

#define CARSE_VERSION ((CARSE_MAJOR_VERSION << 24) | (CARSE_MINOR_VERSION << 16) | (CARSE_REVIS_VERSION << 8))

extern const std::string CARSE_VERSION_STRING;

class Carse extends public fgeal::Game
{
	public:
	GameLogic& logic;

	/** Wrapper to resources shared between states. */
	struct SharedResources
	{
		fgeal::Sound sndCursorMove, sndCursorIn, sndCursorOut;

		SharedResources();
	} *sharedResources;

	enum StateID
	{
		RACE_STATE_ID,
		MAIN_MENU_SIMPLE_LIST_STATE_ID,
		MAIN_MENU_CLASSIC_LAYOUT_STATE_ID,
		OPTIONS_MENU_STATE_ID,
		VEHICLE_SELECTION_SIMPLE_LIST_STATE_ID,
		VEHICLE_SELECTION_SHOWROOM_LAYOUT_STATE_ID,
		COURSE_SELECTION_STATE_ID,
		COURSE_EDITOR_STATE_ID,
		RADIO_SELECTION_STATE_ID,
		RACE_RESULTS_STATE_ID,
		SIDE_DRAG_RACE_STATE_ID,
		DEBUG_WORKSHOP_STATE_ID
	};

	Carse();
	~Carse();
	virtual void initialize();
};

#endif /* CARSE_HPP_ */
