/*
 * course.cpp
 *
 *  Created on: 29 de mar de 2017
 *      Author: carlosfaruolo
 */

#include "course.hpp"

#include "game_logic.hpp"

#include "futil/random.h"

#include <cstdlib>
#include <cstring>

using std::string;
using fgeal::Image;
using fgeal::Color;
using fgeal::Vector2D;
using fgeal::Rectangle;
using futil::random_between_decimal;

Pseudo3DCourse::Pseudo3DCourse()
: spec(100, 1000)
{}

// #################### Pseudo3D Course Spec. methods #####################################################

void Pseudo3DCourse::Spec::loadFromFile(const string& filename, bool readSegments)
{
	this->parseProperties(filename);
	if(readSegments) this->loadSegments(segmentFilename);
}

void Pseudo3DCourse::Spec::loadRoadStyleFromFile(const string& filename)
{
	this->parseRoadStyleProperties(filename);
}

void Pseudo3DCourse::Spec::loadLandscapeStyleFromFile(const string& filename)
{
	this->parseLandscapeStyleProperties(filename);
}

void Pseudo3DCourse::Spec::saveToFile(const string& filename)
{
	const string specFilename = filename + ".properties", segmentsFilename = filename + ".csv";
	this->storeProperties(specFilename, segmentsFilename);
	this->saveSegments(segmentsFilename);
}

// ========================================================================================================================

static Pseudo3DCourse::Spec createDefaultRoadStyle()
{
	Pseudo3DCourse::Spec spec;
	spec.presetRoadStyleName = "default";
	spec.colorRoadPrimary = Color::create(64, 80, 80);
	spec.colorRoadSecondary = Color::create(40, 64, 64);
	spec.colorHumblePrimary = Color::create(200, 200, 200);
	spec.colorHumbleSecondary = Color::create(152, 0, 0);
	spec.colorLaneMarking = Color::create(200, 200, 200);
	return spec;
}

// needed to put this in a static method to avoid racing conditions when static initialization occurs
const Pseudo3DCourse::Spec& Pseudo3DCourse::Spec::getDefaultRoadStyle()
{
	static const Pseudo3DCourse::Spec spec = createDefaultRoadStyle();
	return spec;
}

static Pseudo3DCourse::Spec createDefaultLandscapeStyle()
{
	Pseudo3DCourse::Spec spec;
	spec.presetLandscapeStyleName = "default";
	spec.colorOffRoadPrimary = Color::create(0, 112, 0);
	spec.colorOffRoadSecondary = Color::create(0, 88, 0);
	spec.colorLandscape = Color::create(209, 241, 234);
	spec.landscapeFilename = GameLogic::LANDSCAPE_BG_FOLDER+"default.jpg";
	spec.ornamentSpriteSpecs.resize(3);
	spec.ornaments.resize(3);
	spec.ornamentSpriteSpecs[0].spriteFilename = GameLogic::PROP_ASSETS_FOLDER+"bush.png";
	spec.ornamentSpriteSpecs[0].spriteScale = 1.0;
	spec.ornaments[0].blocking = false;
	spec.ornaments[0].width = 0;
	spec.ornamentSpriteSpecs[1].spriteFilename = GameLogic::PROP_ASSETS_FOLDER+"tree.png";
	spec.ornamentSpriteSpecs[1].spriteScale = 1.0;
	spec.ornaments[1].blocking = true;
	spec.ornaments[1].width = 1.0;
	spec.ornamentSpriteSpecs[2].spriteFilename = GameLogic::PROP_ASSETS_FOLDER+"redbarn.png";
	spec.ornamentSpriteSpecs[2].spriteScale = 1.0;
	spec.ornaments[2].blocking = true;
	spec.ornaments[2].width = 26.5;
	return spec;
}

// needed to put this in a static method to avoid racing conditions when static initialization occurs
const Pseudo3DCourse::Spec& Pseudo3DCourse::Spec::getDefaultLandscapeStyle()
{
	static const Pseudo3DCourse::Spec spec = createDefaultLandscapeStyle();
	return spec;
}

const float Pseudo3DCourse::COURSE_POSITION_FACTOR = 500;

// ====================== built-in generators =============================================================================

//static
Pseudo3DCourse::Spec Pseudo3DCourse::Spec::createDebug()
{
	Pseudo3DCourse::Spec spec(200, 3000);
	for(unsigned i = 0; i < 1600; i++)  // generating hardcoded course
	{
		CourseSpec::Segment line;
		line.z = i*spec.roadSegmentLength;
		if(i > 300 && i < 500) line.curve = 0.3;
		if(i > 500 && i < 700) line.curve = -0.3;
		if(i > 900 && i < 1300) line.curve = -2.2;
		if(i > 750) line.y = sin(i/30.0)*1500;
		if(i % 17 == 0) { line.decoration = 1; }
		spec.lines.push_back(line);
	}

	// ornament id 0
	spec.ornaments.resize(1);
	spec.ornaments[0].blocking = false;
	spec.ornaments[0].width = 1.5f;
	spec.ornamentSpriteSpecs.resize(spec.ornaments.size());
	spec.ornamentSpriteSpecs[0].spriteFilename = GameLogic::PROP_ASSETS_FOLDER+"bush.png";
	spec.ornamentSpriteSpecs[0].spriteScale = 1.0f;

	spec.decorations.resize(2);  // null decoration (0) plus one decoration
	spec.decorations[1].resize(2);  // 2 ornaments on decoration #1
	spec.decorations[1][0].spec = spec.decorations[1][1].spec = 0;  // both ornaments are id 0
	spec.decorations[1][0].x = -10.0;
	spec.decorations[1][1].x =  10.0;

	spec.landscapeFilename = GameLogic::LANDSCAPE_BG_FOLDER+"default.jpg";
	spec.colorRoadPrimary =      Color::create( 64, 80, 80);
	spec.colorRoadSecondary =    Color::create( 40, 64, 64);
	spec.colorOffRoadPrimary =   Color::create(  0,112,  0);
	spec.colorOffRoadSecondary = Color::create(  0, 88, 80);
	spec.colorHumblePrimary =    Color::create(200,200,200);
	spec.colorHumbleSecondary =  Color::create(152,  0,  0);
	spec.colorLaneMarking =      Color::create(200,200,200);

	spec.colorLandscape = Color::create(209, 241, 234);
	spec.colorHorizon = spec.colorOffRoadPrimary;

	spec.musicFilename = "assets/music/music_sample.ogg";

	return spec;
}

//static
Pseudo3DCourse::Spec Pseudo3DCourse::Spec::createRandom(float segmentLength, float roadWidth, const RandomGenerationParameters& params)
{
	Spec spec(segmentLength, roadWidth);

	// TODO make these prop settings parametrizable
	spec.ornaments.resize(3);
	spec.ornaments[0].blocking = false;
	spec.ornaments[1].blocking = true;
	spec.ornaments[2].blocking = true;
	spec.ornamentSpriteSpecs.resize(spec.ornaments.size());

	// default colors
	spec.colorRoadPrimary =      Color::create(32, 32, 32);
	spec.colorRoadSecondary =    Color::create(64, 64, 64);
	spec.colorOffRoadPrimary =   Color::create(96, 96, 96);
	spec.colorOffRoadSecondary = Color::create(128, 128, 128);
	spec.colorHumblePrimary =    Color::create(255, 255, 255);
	spec.colorHumbleSecondary =  Color::create(240, 240, 240);

	spec.colorLandscape = Color::create(0, 255, 255);
	spec.colorHorizon = spec.colorOffRoadPrimary;

	spec.colorLaneMarking = Color::create(240, 240, 240);
	spec.laneCount = params.randomizeLaneCount? rand()%4 : 0;
	spec.weatherEffect = static_cast<WeatherEffect>(params.randomizeWeatherEffect? rand() % Spec::WEATHER_COUNT : 0);

	spec.lines.resize(params.segmentCount);

	// generating random course
	float currentCurve = 0, currentSlopeScale = 0;
	unsigned currentSlopeStart = 0, currentSlopeCycle = 1;
	for(unsigned i = 0; i < params.segmentCount; i++)
	{
		Segment& line = spec.lines[i];
		line.z = i*segmentLength;

		if(currentCurve == 0)
		{
			if(rand() % 500 == 0)
				currentCurve = random_between_decimal(-5*params.curveness, 5*params.curveness);
			else if(rand() % 50 == 0)
				currentCurve = random_between_decimal(-params.curveness, params.curveness);
		}
		else if(currentCurve != 0 and rand() % 100 == 0)
			currentCurve = 0;

		line.curve = currentCurve;

		if(currentSlopeScale == 0)
		{
			if(rand() % 500 == 0)
			{
				currentSlopeScale = random_between_decimal(0.1 * segmentLength, 1500);
				currentSlopeCycle = 20 * currentSlopeScale / segmentLength;
				currentSlopeStart = i;
			}
		}
		else if(currentSlopeScale != 0 and (i - currentSlopeStart) % currentSlopeCycle == 0 and rand()%2 == 0)
			currentSlopeScale = 0;

		line.y = currentSlopeScale != 0? currentSlopeScale * sin(M_PI * (i - currentSlopeStart)/(float) currentSlopeCycle) : 0;

		if(params.randomizeProps)
		{
			if(rand() % 10 == 0)
			{
				line.decoration = spec.decorations.size();
				spec.decorations.resize(spec.decorations.size()+1);

				spec.decorations.back().resize(1);
				spec.decorations.back()[0].spec = 0;
				spec.decorations.back()[0].x = (rand()%2==0? -1 : 1) * random_between_decimal(7.5, 15);
			}
			else if(rand() % 100 == 0)
			{
				line.decoration = spec.decorations.size();
				spec.decorations.resize(spec.decorations.size()+1);

				spec.decorations.back().resize(1);
				spec.decorations.back()[0].spec = 1;
				spec.decorations.back()[0].x = (rand()%2==0? -1 : 1) * random_between_decimal(7.5, 10);
			}
			else if(rand() % 1000 == 0)
			{
				line.decoration = spec.decorations.size();
				spec.decorations.resize(spec.decorations.size()+1);

				spec.decorations.back().resize(1);
				spec.decorations.back()[0].spec = 2;
				spec.decorations.back()[0].x = (rand()%2==0? -1 : 1) * random_between_decimal(10, 15);
			}
		}
	}
	return spec;
}
