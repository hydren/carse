/*
 * course.hpp
 *
 *  Created on: 29 de mar de 2017
 *      Author: carlosfaruolo
 */

#ifndef PSEUDO3D_COURSE_HPP_
#define PSEUDO3D_COURSE_HPP_
#include <ciso646>

#include "vehicle.hpp"

#include "course_gfx.hpp"

#include "fgeal/fgeal.hpp"

#include <vector>

struct Pseudo3DCourse
{
	struct Spec extends Pseudo3DCourseAnimation::Spec
	{
		std::string filename, segmentFilename;

		std::string name, author, credits, comments;
		std::string previewFilename;

		std::string presetLandscapeStyleName, presetRoadStyleName;

		std::string musicFilename;

		Spec(float segmentLength=0, float roadWidth=0)
		: Pseudo3DCourseAnimation::Spec(segmentLength, roadWidth) {}

		inline std::string toString() const { return not name.empty()? name : not filename.empty()? filename : "<unnamed>"; }
		inline operator std::string() const { return this->toString(); }

		/* Loads data from the given filename, parse its course spec data and store in this object.
		 * If 'readSegments' is false, this method will skip reading course segment data (which is the slowest part).
		 * Note that this option was meant to quickly read data useful to peek at, since it doesn't read all data needed to run a race. */
		void loadFromFile(const std::string& filename, bool readSegments=true);

		/* Loads data from the given filename, parse its course spec data related to road style only and store in this object. */
		void loadRoadStyleFromFile(const std::string& filename);

		/* Loads data from the given filename, parse its course spec data related to landscape style only and store in this object. */
		void loadLandscapeStyleFromFile(const std::string& filename);

		/* Saves this course spec. to the given filename. */
		void saveToFile(const std::string& filename);

		/* Generates a debug course spec. */
		static Spec createDebug();

		struct RandomGenerationParameters
		{
			float curveness;  // affects how curvy the result is; the higher this value is, more likely curvier segments occurs
			unsigned segmentCount;  // the amount of segments to generate
			bool randomizeProps;  // indicates wether to generate random props or not
			bool randomizeLaneCount;  // indicates whether to randomize the number of lanes (0-3)
			bool randomizeWeatherEffect;  // indicates whether to replace weathet effect with a randomly chosen one
		};

		/* Generates a random course spec using the given parameters. Note: course length will be equal to (segment lentgh x segment count) */
		static Spec createRandom(float segmentLength, float roadWidth, const RandomGenerationParameters& params);

		inline void copyRoadStyle(const Spec& spec)
		{
			presetRoadStyleName = spec.presetRoadStyleName;
			colorRoadPrimary = spec.colorRoadPrimary;
			colorRoadSecondary = spec.colorRoadSecondary;
			colorHumblePrimary = spec.colorHumblePrimary;
			colorHumbleSecondary = spec.colorHumbleSecondary;
			colorLaneMarking = spec.colorLaneMarking;
		}

		inline void copyLandscapeStyle(const Spec& spec)
		{
			presetLandscapeStyleName = spec.presetLandscapeStyleName;
			colorOffRoadPrimary = spec.colorOffRoadPrimary;
			colorOffRoadSecondary = spec.colorOffRoadSecondary;
			colorLandscape = spec.colorLandscape;
			colorHorizon = spec.colorHorizon;
			landscapeFilename = spec.landscapeFilename;
			ornamentSpriteSpecs = spec.ornamentSpriteSpecs;
			ornaments = spec.ornaments;
		}

		static inline std::string toString(Pseudo3DCourseAnimation::Spec::WeatherEffect weather)
		{
			if(weather == WEATHER_NONE)
				return "none";
			else if(weather == WEATHER_RAIN)
				return "rainy";
			else if(weather == WEATHER_RAIN_BLUE)
				return "rainy (bluish)";
			else if(weather == WEATHER_SNOW)
				return "snowy";
			else
				return "???";
		}

		static const Spec& getDefaultRoadStyle();
		static const Spec& getDefaultLandscapeStyle();

		private:
		void parseRoadStyleProperties(const std::string& filename);
		void parseLandscapeStyleProperties(const std::string& filename);
		void parseProperties(const std::string& filename);
		void loadSegments(const std::string& filename);
		void storeProperties(const std::string& specFile, const std::string& segmentsFile);
		void saveSegments(const std::string& filename);
	};

	Spec spec;
	Pseudo3DCourseAnimation animation;

	// value used to convert physics position to course segment position
	const static float COURSE_POSITION_FACTOR;

	Pseudo3DCourse();

	inline void loadSpec(const Spec& s) { spec = s; animation.loadSpec(spec); }
};

#endif /* PSEUDO3D_COURSE_HPP_ */
