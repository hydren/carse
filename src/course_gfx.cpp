/*
 * course_gfx.cpp
 *
 *  Created on: 23 de set de 2019
 *      Author: carlosfaruolo
 */

#include "course_gfx.hpp"

#include "vehicle.hpp"

#include "futil/random.h"

#include <algorithm>
#include <cstdlib>
#include <cmath>

using std::string;
using fgeal::Image;
using fgeal::Color;
using fgeal::Vector2D;
using fgeal::Rectangle;
using fgeal::Graphics;
using futil::random_between_decimal;

static inline float fractional_part(float value) { return value - (int) value; }

const float Pseudo3DCourseAnimation::DEFAULT_CAMERA_HEIGHT = 1500, Pseudo3DCourseAnimation::INTERIOR_CAMERA_HEIGHT = 900;

Pseudo3DCourseAnimation::Pseudo3DCourseAnimation()
: spec(100, 1000), sprites(), landscapeImage(null),
  drawAreaOffset(), drawAreaWidth(), drawAreaHeight(), drawDistance(1),
  cameraDepth(1.0), cameraHeight(DEFAULT_CAMERA_HEIGHT), fogging(), pseudoFogging(), lightFiltering(), headlightsYawAngle(), isHeadlightsEffectEnabled(false), isHeadlightsEffectFirstPerson(false), pseudoFogggingAvoidBackground(true),
  isVehicleSpriteLeftOcclusionEnabled(true), cameraBehavior(),
  lengthScale(1)
{}

Pseudo3DCourseAnimation::~Pseudo3DCourseAnimation()
{
	freeAssetsData();
}

void Pseudo3DCourseAnimation::freeAssetsData()
{
	if(not sprites.empty())
	{
		for(unsigned i = 0; i < sprites.size(); i++)
			delete sprites[i];
		sprites.clear();
	}

	if(landscapeImage != null)
	{
		delete landscapeImage;
		landscapeImage = null;
	}
}

void Pseudo3DCourseAnimation::loadSpec(const Spec& s)
{
	freeAssetsData();

	// set new spec and reset some values
	spec = s;

	// resize cache when needed
	if(coordCache.size() != spec.lines.size())
		coordCache.resize(spec.lines.size(), ScreenCoord());

	// pre-compute segment orientation
	float sumangle = 0;
	for(unsigned i = 0; i < spec.lines.size(); i++)
	{
		const float deltaAngle = asin(spec.lines[i].curve/spec.roadSegmentLength);
		sumangle += deltaAngle;
		coordCache[i].orientation = sumangle/(2*M_PI);
		while(coordCache[i].orientation < 0)
			coordCache[i].orientation += 1;
		if(coordCache[i].orientation >= 1)
			coordCache[i].orientation = coordCache[i].orientation - static_cast<int>(coordCache[i].orientation);
	}

	// load assets' data
	for(unsigned i = 0; i < spec.ornamentSpriteSpecs.size(); i++)
		if(not spec.ornamentSpriteSpecs[i].spriteFilename.empty())
			sprites.push_back(new Image(spec.ornamentSpriteSpecs[i].spriteFilename));
		else
			sprites.push_back(null);

	landscapeImage = spec.landscapeFilename.empty()? null : new Image(spec.landscapeFilename);
}

static const float HORIZON_DISTANCE = 7.14,  // == 7.14km
                   BACKGROUND_VERTICAL_PARALLAX_FACTOR = 0.509375;

void Pseudo3DCourseAnimation::draw(float pos, float posX, float slopeAngle)
{
	if(spec.lines.empty() or spec.roadSegmentLength == 0)
		return;

	if(pos < 0)
		pos = 0;

	bool isCameraHeightRelativeToGround = (cameraBehavior == CAMERA_BEHAVIOR_TYPE_B or cameraBehavior == CAMERA_BEHAVIOR_TYPE_C);
	const unsigned N = spec.lines.size(), fromIndex = pos/spec.roadSegmentLength;
	const float segmentProportion = 1 - fractional_part(pos/spec.roadSegmentLength), slopeAngleHeight = cameraBehavior == CAMERA_BEHAVIOR_TYPE_C? tan(slopeAngle) * 0.5 * lengthScale : 0,
				camHeight = cameraHeight + (isCameraHeightRelativeToGround? (segmentProportion * spec.lines[fromIndex%N].y + (1 - segmentProportion) * spec.lines[(fromIndex+1)%N].y) : 0);

	if(landscapeImage != null)
	{
		const float backgroundWidth =  2.0f * drawAreaWidth, landscapeImageScaleX = backgroundWidth / landscapeImage->getWidth(),
					backgroundHeight = 0.2f * drawAreaHeight, landscapeImageScaleY = backgroundHeight / landscapeImage->getHeight(),
					verticalBackgroundParallax = cameraBehavior == CAMERA_BEHAVIOR_TYPE_A? 0
												:cameraBehavior == CAMERA_BEHAVIOR_TYPE_B? -camHeight/(200*HORIZON_DISTANCE)
												:slopeAngleHeight * drawAreaHeight * 0.0022f,  // aprox. (4/3)/600; probably related to bg's image height
					parallaxAbsoluteX = -backgroundWidth * coordCache[fromIndex%N].orientation,
					parallaxAbsoluteY = verticalBackgroundParallax - backgroundHeight + BACKGROUND_VERTICAL_PARALLAX_FACTOR * drawAreaHeight;

		Graphics::drawFilledRectangle(drawAreaOffset.x, drawAreaOffset.y, drawAreaWidth, drawAreaHeight, spec.colorLandscape);
		Graphics::drawFilledRectangle(drawAreaOffset.x, drawAreaOffset.y + std::max(parallaxAbsoluteY + backgroundHeight, 0.f), drawAreaWidth, drawAreaHeight, spec.colorHorizon);

		const float bgx = fmod(parallaxAbsoluteX, backgroundWidth) - (parallaxAbsoluteX > 0? backgroundWidth : 0);
		if((1 + bgx/backgroundWidth) * landscapeImage->getWidth() >= 1 and -parallaxAbsoluteY < landscapeImage->getHeight() * landscapeImageScaleY)
			landscapeImage->drawScaledRegion(drawAreaOffset.x, drawAreaOffset.y + std::max(parallaxAbsoluteY, 0.f), landscapeImageScaleX, landscapeImageScaleY, Image::FLIP_NONE,
											(-bgx/backgroundWidth) * landscapeImage->getWidth(), std::max(-parallaxAbsoluteY/landscapeImageScaleY, 0.f),
											(1 + bgx/backgroundWidth) * landscapeImage->getWidth(), landscapeImage->getHeight() - std::max(-parallaxAbsoluteY/landscapeImageScaleY, 0.f));
		if(parallaxAbsoluteY >= 0)
			landscapeImage->drawScaled(drawAreaOffset.x + bgx + backgroundWidth, drawAreaOffset.y + parallaxAbsoluteY, landscapeImageScaleX, landscapeImageScaleY);

		else if(-parallaxAbsoluteY < landscapeImage->getHeight() * landscapeImageScaleY)
			landscapeImage->drawScaledRegion(drawAreaOffset.x + bgx + backgroundWidth, drawAreaOffset.y, landscapeImageScaleX, landscapeImageScaleY, Image::FLIP_NONE,
											 0, std::max(-parallaxAbsoluteY/landscapeImageScaleY, 0.f),
											 landscapeImage->getWidth(), landscapeImage->getHeight() - std::max(-parallaxAbsoluteY/landscapeImageScaleY, 0.f));
	}

	const float virtualDrawAreaWidth = drawAreaHeight * 4 / 3.f, aspectRatioOffset = (drawAreaWidth - virtualDrawAreaWidth)/2;
	float x = 0, dx = 0, maxY = drawAreaHeight;
	for(unsigned n = fromIndex; n < fromIndex + drawDistance; n++)
	{
		const CourseSpec::Segment& segment = spec.lines[n%N];

		// project from "world" to "screen" coordinates
		const float camX = posX - x,
					camY = camHeight + slopeAngleHeight * (n - fromIndex + segmentProportion),
					camZ = pos - (n >= N? N*spec.roadSegmentLength : 0);

		// FIXME since segment.x is always zero, camX is actually the one which controls the horizontal shift; it should be segment.x, much like segment.y controls the vertical shift
		ScreenCoord& sc = coordCache[n%N];
		sc.scale = cameraDepth / (segment.z - camZ);
		sc.X = (1 + sc.scale*(segment.x - camX)) * virtualDrawAreaWidth/2;
		sc.Y = (1 - sc.scale*(segment.y - camY)) * drawAreaHeight/2;
		sc.W = sc.scale * spec.roadWidth * virtualDrawAreaWidth/2;

		// update curve
		x += dx;
		dx += segment.curve;

		sc.clip=maxY;

		if(n == fromIndex or sc.Y > maxY)
			continue;

		maxY = sc.Y;

		const bool oddn = (n/3)%2;
		const ScreenCoord& psc = coordCache[(n-1)%N];  // previous "screen" coordinate

		Graphics::drawFilledQuadrangle(drawAreaOffset.x,                 drawAreaOffset.y + psc.Y,
		                               drawAreaOffset.x,                 drawAreaOffset.y +  sc.Y,
									   drawAreaOffset.x + drawAreaWidth, drawAreaOffset.y +  sc.Y,
									   drawAreaOffset.x + drawAreaWidth, drawAreaOffset.y + psc.Y,
									   oddn? spec.colorOffRoadPrimary : spec.colorOffRoadSecondary);

		const float exWidthFactor = 1 + 2*spec.humbleWidth/spec.roadWidth, scWex = sc.W * exWidthFactor, pscWex = psc.W * exWidthFactor;
		if(aspectRatioOffset + std::min(sc.X + scWex, psc.X + pscWex) > 0)
		{
			Graphics::drawFilledQuadrangle(drawAreaOffset.x + std::max(0.f, aspectRatioOffset + psc.X - pscWex), drawAreaOffset.y + psc.Y,
			                               drawAreaOffset.x + std::max(0.f, aspectRatioOffset +  sc.X -  scWex), drawAreaOffset.y +  sc.Y,
										   drawAreaOffset.x +               aspectRatioOffset +  sc.X +  scWex,  drawAreaOffset.y +  sc.Y,
										   drawAreaOffset.x +               aspectRatioOffset + psc.X + pscWex,  drawAreaOffset.y + psc.Y,
										   oddn? spec.colorHumblePrimary : spec.colorHumbleSecondary);
			if(aspectRatioOffset + psc.X - pscWex < 0)
			{
				Graphics::drawFilledTriangle(drawAreaOffset.x,                                                   drawAreaOffset.y + psc.Y - (aspectRatioOffset + psc.X - pscWex) * (sc.Y - psc.Y) / ((aspectRatioOffset + sc.X - scWex) - (aspectRatioOffset + psc.X - pscWex)),
						                     drawAreaOffset.x,                                                   drawAreaOffset.y + psc.Y,
                                             drawAreaOffset.x + std::max(0.f, aspectRatioOffset + sc.X - scWex), drawAreaOffset.y +  sc.Y,
											 oddn? spec.colorHumblePrimary : spec.colorHumbleSecondary);
			}
			if(aspectRatioOffset + std::min(sc.X + sc.W, psc.X + psc.W) > 0)
			{
				Graphics::drawFilledQuadrangle(drawAreaOffset.x + std::max(0.f, aspectRatioOffset + psc.X - psc.W), drawAreaOffset.y + psc.Y,
											   drawAreaOffset.x + std::max(0.f, aspectRatioOffset +  sc.X -  sc.W), drawAreaOffset.y +  sc.Y,
											   drawAreaOffset.x +               aspectRatioOffset +  sc.X +  sc.W,  drawAreaOffset.y +  sc.Y,
											   drawAreaOffset.x +               aspectRatioOffset + psc.X + psc.W,  drawAreaOffset.y + psc.Y,
											   n % N == 0? Color::WHITE : oddn? spec.colorRoadPrimary : spec.colorRoadSecondary);
				if(aspectRatioOffset + psc.X - psc.W < 0)
				{
					Graphics::drawFilledTriangle(drawAreaOffset.x,                                                  drawAreaOffset.y + psc.Y - (aspectRatioOffset + psc.X - psc.W) * (sc.Y - psc.Y) / ((aspectRatioOffset + sc.X - sc.W) - (aspectRatioOffset + psc.X - psc.W)),
							                     drawAreaOffset.x,                                                  drawAreaOffset.y + psc.Y,
	                                             drawAreaOffset.x + std::max(0.f, aspectRatioOffset + sc.X - sc.W), drawAreaOffset.y +  sc.Y,
												 n % N == 0? Color::WHITE : oddn? spec.colorRoadPrimary : spec.colorRoadSecondary);
				}
				if((n/5)%3>1 or not spec.laneMarkingDashed) for(unsigned i = 1; i < spec.laneCount; i++)
				{
					const float ls = 2 * sc.W/(spec.laneCount), pls = 2 * psc.W/(spec.laneCount),
								lhwf = 0.5f * spec.laneMarkingWidth/spec.roadWidth, lhw = sc.W * lhwf, plhw = psc.W * lhwf;

					if(aspectRatioOffset + std::min(sc.X - sc.W + ls*i + lhw, psc.X - psc.W + pls*i + plhw) > 0)
					{
						Graphics::drawFilledQuadrangle(drawAreaOffset.x + std::max(0.f, aspectRatioOffset + psc.X - psc.W + pls*i - plhw), drawAreaOffset.y + psc.Y,
													   drawAreaOffset.x + std::max(0.f, aspectRatioOffset +  sc.X -  sc.W +  ls*i -  lhw), drawAreaOffset.y +  sc.Y,
													   drawAreaOffset.x +               aspectRatioOffset +  sc.X -  sc.W +  ls*i +  lhw,  drawAreaOffset.y +  sc.Y,
													   drawAreaOffset.x +               aspectRatioOffset + psc.X - psc.W + pls*i + plhw,  drawAreaOffset.y + psc.Y,
													   spec.colorLaneMarking);
						if(aspectRatioOffset + psc.X - psc.W + pls*i - plhw < 0)
						{
							Graphics::drawFilledTriangle(drawAreaOffset.x,                                                                 drawAreaOffset.y + psc.Y - (aspectRatioOffset + psc.X - psc.W + pls*i - plhw) * (sc.Y - psc.Y) / ((aspectRatioOffset + sc.X - sc.W + ls*i - lhw) - (aspectRatioOffset + psc.X - psc.W + pls*i - plhw)),
														 drawAreaOffset.x,                                                                 drawAreaOffset.y + psc.Y,
														 drawAreaOffset.x + std::max(0.f, aspectRatioOffset + sc.X - sc.W + ls*i - lhw),   drawAreaOffset.y +  sc.Y,
														 spec.colorLaneMarking);
						}
						// TODO create another call to filled triangle to fix a missing "under" lane painting
					}
				}
			}
		}
	}

	// variables for pseudo-fog drawing
	bool pseudoFogFirstDraw = true;
	unsigned pseudoFogStepsToDraw = 5;
	float pseudoFogCurrentH = 0;
	const float fscY = coordCache[(fromIndex - 1 + drawDistance)%N].clip;

	for(unsigned n = fromIndex - 1 + drawDistance; n > fromIndex + 1; n--)
	{
		const CourseSpec::Segment& segment = spec.lines[n%N];
		const ScreenCoord& sc = coordCache[n%N];  // get cached "screen" coordinate

		if(segment.decoration) for(unsigned i = 0; i < spec.decorations[segment.decoration].size(); i++)
		{
			const CourseSpec::Ornament& ornament = spec.decorations[segment.decoration][i];
			Image& ornamentSprite = *sprites[ornament.spec];
			const float scale = (10 * sc.scale * virtualDrawAreaWidth/2) * spec.ornamentSpriteSpecs[ornament.spec].spriteScale,
						destW = ornamentSprite.getWidth() * scale,
						destH = ornamentSprite.getHeight()* scale,
						destX = sc.X - destW/2 + sc.scale * (virtualDrawAreaWidth/2) * lengthScale * ornament.x,
						destY = sc.Y - destH,
						clipH = std::max(destY + destH - sc.clip, 0.f),
						clipHTop = std::max(-destY, 0.f),
						sh = ornamentSprite.getHeight() * (1 - clipH/destH - clipHTop/destH),
						clipW = std::max(-aspectRatioOffset-destX, 0.f),
						sw = ornamentSprite.getWidth() * (1 - clipW/destW);

			if(not (clipH >= destH or destW > 2*virtualDrawAreaWidth or destH > 2*drawAreaHeight or sh <= 1 or clipW >= destW or sw <= 1))
				ornamentSprite.drawScaledRegion(drawAreaOffset.x + aspectRatioOffset + destX + clipW,
												drawAreaOffset.y + destY + clipHTop,
												scale, scale, Image::FLIP_NONE,
												ornamentSprite.getWidth() - sw, ornamentSprite.getHeight() * (clipHTop/destH), sw, sh);
		}

		for(unsigned i = 0; i < vehicles.size() and vehicles[i] != null; i++)
		{
			const Pseudo3DVehicle& vehicle = *vehicles[i];
			if(static_cast<unsigned>(vehicle.position * lengthScale / spec.roadSegmentLength) % N == n % N)
			{
				const float segProp = 1 - fractional_part(vehicle.position * lengthScale / spec.roadSegmentLength),
							vehiclePositionInterpolatedGroundHeight = spec.lines[n%N].y*segProp + spec.lines[(n+1)%N].y*(1-segProp);
				const ScreenCoord& nsc = coordCache[(n+1)%N],  // get next cached "screen" coordinate
				                   isc = { sc.X     * segProp + nsc.X     * (1 - segProp),  // interpolated screen coordinates
				                		   sc.Y     * segProp + nsc.Y     * (1 - segProp),
				                           sc.W     * segProp + nsc.W     * (1 - segProp),
				                           sc.scale * segProp + nsc.scale * (1 - segProp) };

				const float scale = (3000.f * isc.scale * virtualDrawAreaWidth/2) * 1.55f,
							destX = isc.X + isc.scale * (virtualDrawAreaWidth/2) * (1 + 0.10731f * 1.55f * 3000.f) * vehicle.horizontalPosition,
							destY = isc.Y + isc.scale * (drawAreaHeight/2) * (vehiclePositionInterpolatedGroundHeight - vehicle.verticalPosition),
							destW = vehicle.getAnimationWidth() * scale,
							destH = vehicle.getAnimationHeight() * scale,
							destA = atan2(virtualDrawAreaWidth/2 - destX, destY),
							clipH = std::max(destY - sc.clip, 0.f);

				if(not (clipH >= destH or destW > 2*virtualDrawAreaWidth or destH > 2*drawAreaHeight or (isVehicleSpriteLeftOcclusionEnabled and aspectRatioOffset + destX - 0.5f * destW < 0)))
					vehicle.draw(drawAreaOffset.x + aspectRatioOffset + destX, drawAreaOffset.y + destY, destA, scale, (1 - clipH/destH));
			}
		}

		if(fogging != 0 and n % 3 == 0)  // volumetric fogging; looks nicer than pseudo-fogging, but unacceptable slow on non-hardware-accelerated setups
			Graphics::drawFilledRectangle(drawAreaOffset.x, drawAreaOffset.y, drawAreaWidth, sc.Y, (fogging > 0? Color::WHITE : Color::BLACK).getTransparent(std::abs(255*fogging)));

		if(pseudoFogging != 0)
		{
			const float roadAreaH = drawAreaHeight - fscY;
			const Color fogColor = pseudoFogging > 0? Color::WHITE : Color::BLACK;
			if(pseudoFogFirstDraw)
			{
				if(not pseudoFogggingAvoidBackground)
					Graphics::drawFilledRectangle(drawAreaOffset.x, drawAreaOffset.y, drawAreaWidth, fscY, fogColor.getTransparent(std::abs(255*pseudoFogging)));
				pseudoFogFirstDraw = false;
				pseudoFogCurrentH = fscY;
			}
			if(pseudoFogStepsToDraw > 0)
			{
				if(sc.Y >= pseudoFogCurrentH)
				{
					const float fogNextH = fscY + roadAreaH/std::pow(2.0, (int)(pseudoFogStepsToDraw--)-1), fogDistanceFactor = std::exp(-2*(pseudoFogCurrentH - fscY)/roadAreaH);
					Graphics::drawFilledRectangle(drawAreaOffset.x, drawAreaOffset.y + pseudoFogCurrentH, drawAreaWidth, drawAreaOffset.y + fogNextH-pseudoFogCurrentH-1, fogColor.getTransparent(std::abs(255*pseudoFogging*fogDistanceFactor)));
					pseudoFogCurrentH = fogNextH;
				}
			}
		}
	}

	switch(spec.weatherEffect)
	{
		case Spec::WEATHER_RAIN:
		case Spec::WEATHER_RAIN_BLUE:
		{
			const float RAIN_SPEED = 5.0, cycle = fractional_part(fgeal::uptime()*RAIN_SPEED), roffx[8] = { 0, 0.3, -0.2, 0.1, -0.3, 0.4, 0.1, -0.2 };
			const unsigned DROPS_PER_COLUMN = 6, COLUMN_COUNT = 6*drawAreaWidth/(float)drawAreaHeight, RAIN_DEPTH = 4, DROP_LENGTH = drawAreaHeight/75,
			               columnSpacing = drawAreaWidth/COLUMN_COUNT, streamSpacing = drawAreaHeight/DROPS_PER_COLUMN, fallOffset = streamSpacing*cycle;

			for(unsigned z = RAIN_DEPTH; z > 0; z--)
				for(unsigned i = 0; i < COLUMN_COUNT; i++)
					for(unsigned j = 0; j < DROPS_PER_COLUMN; j++)
						Graphics::drawThickLine(drawAreaOffset.x + (i + 0.5f + roffx[(z+i)%8])*columnSpacing, drawAreaOffset.y + ((streamSpacing-DROP_LENGTH)/2 + (j-0.5f)*streamSpacing + fallOffset)*((z+1)/2.f-RAIN_DEPTH)/(1-(int)RAIN_DEPTH),
												drawAreaOffset.x + (i + 0.5f + roffx[(z+i)%8])*columnSpacing, drawAreaOffset.y + ( streamSpacing/2              + (j-0.5f)*streamSpacing + fallOffset)*((z+1)/2.f-RAIN_DEPTH)/(1-(int)RAIN_DEPTH),
												std::max(drawAreaHeight/(240.f*z), 1.f), spec.weatherEffect == Spec::WEATHER_RAIN? Color::GREY : Color::BLUE),
						Graphics::drawThickLine(drawAreaOffset.x + (i + 0.5f + roffx[(z+i)%8])*columnSpacing, drawAreaOffset.y + ( streamSpacing/2              + (j-0.5f)*streamSpacing + fallOffset)*((z+1)/2.f-RAIN_DEPTH)/(1-(int)RAIN_DEPTH),
												drawAreaOffset.x + (i + 0.5f + roffx[(z+i)%8])*columnSpacing, drawAreaOffset.y + ((streamSpacing+DROP_LENGTH)/2 + (j-0.5f)*streamSpacing + fallOffset)*((z+1)/2.f-RAIN_DEPTH)/(1-(int)RAIN_DEPTH),
												std::max(drawAreaHeight/(240.f*z), 1.f), spec.weatherEffect == Spec::WEATHER_RAIN? Color::LIGHT_GREY : Color::AZURE);
			break;
		}
		case Spec::WEATHER_SNOW:
		{
			const float SNOW_SPEED = 4.0, cycle = fractional_part(fgeal::uptime()*SNOW_SPEED), soffx[8] = { 0, 0.2, -0.3, 0.3, -0.1, 0.1, 0.4, -0.2 };
			const unsigned FLAKES_PER_COLUMN = 6, COLUMN_COUNT = 6*drawAreaWidth/(float)drawAreaHeight, SNOW_DEPTH = 4, FLAKE_SIZE = drawAreaHeight/75,
			               columnSpacing = drawAreaWidth/COLUMN_COUNT, streamSpacing = drawAreaHeight/FLAKES_PER_COLUMN, fallOffset = streamSpacing*cycle;

			for(unsigned z = SNOW_DEPTH; z > 0; z--)
				for(unsigned i = 0; i < COLUMN_COUNT; i++)
					for(unsigned j = 0; j < FLAKES_PER_COLUMN; j++)
						if(j % 4 == 0)
							Graphics::drawThickLine(drawAreaOffset.x + (i + 0.5f + soffx[(z+i)%8])*columnSpacing, drawAreaOffset.y + ((streamSpacing-FLAKE_SIZE)/2 + (j-0.5f)*streamSpacing + fallOffset)*((z+1)/2.f-SNOW_DEPTH)/(1-(int)SNOW_DEPTH),
													drawAreaOffset.x + (i + 0.5f + soffx[(z+i)%8])*columnSpacing, drawAreaOffset.y + ((streamSpacing+FLAKE_SIZE)/2 + (j-0.5f)*streamSpacing + fallOffset)*((z+1)/2.f-SNOW_DEPTH)/(1-(int)SNOW_DEPTH),
													std::max(drawAreaHeight/(240.f*z), 1.f), Color::WHITE);
						else if(j % 4 == 2)
							Graphics::drawThickLine(drawAreaOffset.x + (i + 0.5f + soffx[(z+i)%8])*columnSpacing-FLAKE_SIZE/2, drawAreaOffset.y + ((streamSpacing-FLAKE_SIZE)/2 + (j-0.5f)*streamSpacing + fallOffset)*((z+1)/2.f-SNOW_DEPTH)/(1-(int)SNOW_DEPTH),
													drawAreaOffset.x + (i + 0.5f + soffx[(z+i)%8])*columnSpacing+FLAKE_SIZE/2, drawAreaOffset.y + ((streamSpacing+FLAKE_SIZE)/2 + (j-0.5f)*streamSpacing + fallOffset)*((z+1)/2.f-SNOW_DEPTH)/(1-(int)SNOW_DEPTH),
													std::max(drawAreaHeight/(240.f*z), 1.f), Color::WHITE);
						else if(j % 4 == 3)
							Graphics::drawThickLine(drawAreaOffset.x + (i + 0.5f + soffx[(z+i)%8])*columnSpacing+FLAKE_SIZE/2, drawAreaOffset.y + ((streamSpacing-FLAKE_SIZE)/2 + (j-0.5f)*streamSpacing + fallOffset)*((z+1)/2.f-SNOW_DEPTH)/(1-(int)SNOW_DEPTH),
													drawAreaOffset.x + (i + 0.5f + soffx[(z+i)%8])*columnSpacing-FLAKE_SIZE/2, drawAreaOffset.y + ((streamSpacing+FLAKE_SIZE)/2 + (j-0.5f)*streamSpacing + fallOffset)*((z+1)/2.f-SNOW_DEPTH)/(1-(int)SNOW_DEPTH),
													std::max(drawAreaHeight/(240.f*z), 1.f), Color::WHITE);
						else
							Graphics::drawFilledCircle(drawAreaOffset.x + (i + 0.5f + soffx[(z+i)%8])*columnSpacing, drawAreaOffset.y + (streamSpacing/2 + (j-0.5f)*streamSpacing + fallOffset)*((z+1)/2.f-SNOW_DEPTH)/(1-(int)SNOW_DEPTH),
													   (FLAKE_SIZE/2)*((z+1)/2.f-SNOW_DEPTH)/(1-(int)SNOW_DEPTH), Color::WHITE);
			break;
		}
		default:case Spec::WEATHER_NONE: break;
	}

	if(lightFiltering != 0)
	{
		const Color color = lightFiltering > 0? Color::create(255, 255, 255, 255*std::pow(lightFiltering, 0.25f)) : Color::create(0, 0, 0, 255*std::pow(-lightFiltering, 0.25f));  //, innerColor = color.getTransparent(color.a/2);
		if(isHeadlightsEffectEnabled)  // FIXME this shows headlight lighting over player sprite instead of under
		{
			const fgeal::Rectangle normalLightingArea = { drawAreaOffset.x + (std::tan(0.25f*headlightsYawAngle)+0.333f)*virtualDrawAreaWidth + aspectRatioOffset, drawAreaOffset.y + 0.525f*drawAreaHeight, 0.333f*virtualDrawAreaWidth, 0.175f*drawAreaHeight},
					               cockpitLightingArea = { drawAreaOffset.x + 0.1f*virtualDrawAreaWidth + aspectRatioOffset, drawAreaOffset.y + 0.375f*drawAreaHeight, 0.8f*virtualDrawAreaWidth, 0.333f*drawAreaHeight},
								   lightingArea = isHeadlightsEffectFirstPerson? cockpitLightingArea : normalLightingArea;
			Graphics::drawFilledRectangle(drawAreaOffset.x, drawAreaOffset.y, drawAreaWidth, lightingArea.y - 1, color);
			Graphics::drawFilledRectangle(drawAreaOffset.x, lightingArea.y + lightingArea.h + 1, drawAreaWidth, drawAreaHeight - (lightingArea.y + lightingArea.h + 1), color);
			Graphics::drawFilledRectangle(drawAreaOffset.x, lightingArea.y, lightingArea.x - drawAreaOffset.x - 1, lightingArea.h, color);
			Graphics::drawFilledRectangle(lightingArea.x + lightingArea.w + 1, lightingArea.y, drawAreaWidth - (lightingArea.x + lightingArea.w), lightingArea.h, color);

			// rouding the borders of the illuminated area (FIXME this presents some line artifacts due to minor inconsistencies between backend adapters' primitive calls)
			const float hs = lightingArea.h/3;
			Graphics::drawFilledTriangle(lightingArea.x, lightingArea.y, lightingArea.x + hs, lightingArea.y, lightingArea.x + hs/4, lightingArea.y + hs/4, color);
			Graphics::drawFilledTriangle(lightingArea.x, lightingArea.y, lightingArea.x, lightingArea.y + hs, lightingArea.x + hs/4, lightingArea.y + hs/4, color);
			Graphics::drawFilledTriangle(lightingArea.x, lightingArea.y + lightingArea.h, lightingArea.x + hs, lightingArea.y + lightingArea.h, lightingArea.x + hs/4, lightingArea.y + lightingArea.h - hs/4, color);
			Graphics::drawFilledTriangle(lightingArea.x, lightingArea.y + lightingArea.h, lightingArea.x, lightingArea.y + lightingArea.h - hs, lightingArea.x + hs/4, lightingArea.y + lightingArea.h - hs/4, color);

			Graphics::drawFilledTriangle(lightingArea.x + lightingArea.w, lightingArea.y, lightingArea.x + lightingArea.w - hs, lightingArea.y, lightingArea.x + lightingArea.w - hs/4, lightingArea.y + hs/4, color);
			Graphics::drawFilledTriangle(lightingArea.x + lightingArea.w, lightingArea.y, lightingArea.x + lightingArea.w, lightingArea.y + hs, lightingArea.x + lightingArea.w - hs/4, lightingArea.y + hs/4, color);
			Graphics::drawFilledTriangle(lightingArea.x + lightingArea.w, lightingArea.y + lightingArea.h, lightingArea.x + lightingArea.w - hs, lightingArea.y + lightingArea.h, lightingArea.x + lightingArea.w - hs/4, lightingArea.y + lightingArea.h - hs/4, color);
			Graphics::drawFilledTriangle(lightingArea.x + lightingArea.w, lightingArea.y + lightingArea.h, lightingArea.x + lightingArea.w, lightingArea.y + lightingArea.h - hs, lightingArea.x + lightingArea.w - hs/4, lightingArea.y + lightingArea.h - hs/4, color);

			// inner area border (outside penumbra)
			//const float borderRatio = 0.075f * lightingArea.h;
			//const fgeal::Rectangle innerLightingArea = { lightingArea.x + borderRatio, lightingArea.y + borderRatio, lightingArea.w - 2*borderRatio, lightingArea.h - 2*borderRatio };
			//Graphics::drawFilledRectangle(lightingArea.x, lightingArea.y, lightingArea.w, innerLightingArea.y - lightingArea.y - 1, innerColor);
			//Graphics::drawFilledRectangle(lightingArea.x, innerLightingArea.y + innerLightingArea.h + 1, lightingArea.w, lightingArea.h - (innerLightingArea.y - lightingArea.y + innerLightingArea.h + 1), innerColor);
			//Graphics::drawFilledRectangle(lightingArea.x, innerLightingArea.y, innerLightingArea.x - lightingArea.x - 1, innerLightingArea.h, innerColor);
			//Graphics::drawFilledRectangle(innerLightingArea.x + innerLightingArea.w + 1, innerLightingArea.y, lightingArea.w - (innerLightingArea.x - lightingArea.x + innerLightingArea.w), innerLightingArea.h, innerColor);
		}
		else Graphics::drawFilledRectangle(drawAreaOffset.x, drawAreaOffset.y, drawAreaWidth, drawAreaHeight, color);
	}
}
