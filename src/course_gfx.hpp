/*
 * course_gfx.hpp
 *
 *  Created on: 23 de set de 2019
 *      Author: carlosfaruolo
 */

#ifndef COURSE_GFX_HPP_
#define COURSE_GFX_HPP_

#include "fgeal/fgeal.hpp"

#include <vector>

/** an object that describes a course physically and logically (but not graphically) */
struct CourseSpec
{
	/** an object that describes a single road segment */
	struct Segment
	{
		float x, y, z;  // 3d center of line (delta coordinates)
		float curve;  // FIXME this "curve" field completely renders the "x" field useless
		float slope; //TODO make this slope field to control y-variation

		unsigned decoration;  // an identifier of an object describing decorative elements in this segment; 0 means no decoration

		Segment() : x(), y(), z(), curve(), slope(), decoration() {}
	};

	/** the list of segments of this course (size should be equal to roadSegmentCount)  */
	std::vector<Segment> lines;

	/** an object that describes a static element of decoration */
	struct Ornament
	{
		float x, y;  // the position of this ornament on the road segment (TODO implement y-positioning of ornaments)
		unsigned spec;  // an identifier of a object with data about this ornament, that is shared with others

		/** an object that stores common data about ornaments, meant to be shared */
		struct Spec
		{
			bool blocking;  // indicates whether this ornament blocks vehicles when colliding
			float width, height;  // the ornament's physical dimensions (used for collision detection)

			Spec(bool blocking=false, float width=1.0f, float height=1.0f)
			: blocking(blocking), width(width), height(height) {}
		};
	};

	/** lists of ornaments used to decorate segments; first index specifies a list of ornaments for one segment, second index denotes each ornament on this list */
	std::vector< std::vector<Ornament> > decorations;

	/** a list of shared data for ornaments that may be used on this course */
	std::vector<Ornament::Spec> ornaments;

	/** the number of road segments */
	unsigned roadSegmentCount;

	/** the length of each road segment */
	float roadSegmentLength;

	// TODO specify the fields below in a per-segment basis, instead of being "course-global"

	/** the physical width of the road */
	float roadWidth;

	/** the road rolling resistance and friction coefficient (relative to a common tire) */
	float roadRollingResistance, roadFrictionCoefficient;

	/** the off-road area rolling resistance and friction coefficient (relative to a common tire) */
	float offRoadRollingResistance, offRoadFrictionCoefficient;

	/** the width of road humble */
	float humbleWidth;

	/** number of lanes, if any */
	unsigned laneCount;

	/** the width of lane marking, if any */
	float laneMarkingWidth;

	/** flag indicating whether lane marking should be dashed or continuous, if any */
	bool laneMarkingDashed;

	CourseSpec(float segmentLength, float roadWidth)
	: lines(), decorations(1), ornaments(), roadSegmentCount(), roadSegmentLength(segmentLength),
	  roadWidth(roadWidth), roadRollingResistance(0.01), roadFrictionCoefficient(0.9),
	  offRoadRollingResistance(0.1), offRoadFrictionCoefficient(0.5),
	  humbleWidth(roadWidth/10),
	  laneCount(1), laneMarkingWidth(roadWidth/15), laneMarkingDashed(true)
	{}

	inline float getHeightAt(float pos)
	{
		const int index = pos/roadSegmentLength;
		const float segmentProportion = 1 - (pos/roadSegmentLength - index);
		return segmentProportion * lines[index % lines.size()].y + (1 - segmentProportion) * lines[(index + 1) % lines.size()].y;
	}
};

// fwd. declared
class Pseudo3DVehicle;

struct Pseudo3DCourseAnimation
{
	struct OrnamentSpriteSpec
	{
		std::string spriteFilename;
		float spriteScale;
		// TODO support pseudo-3D non-sprite ornament animation
	};

	struct Spec extends CourseSpec
	{
		std::string landscapeFilename;
		fgeal::Color colorLandscape, colorHorizon,
					 colorRoadPrimary, colorRoadSecondary,
					 colorOffRoadPrimary, colorOffRoadSecondary,
					 colorHumblePrimary, colorHumbleSecondary,
					 colorLaneMarking;

		enum WeatherEffect
		{
			WEATHER_NONE,
			WEATHER_RAIN,
			WEATHER_RAIN_BLUE,

			WEATHER_SNOW,

			WEATHER_COUNT  // for counting purposes...
		} weatherEffect;

		std::vector<OrnamentSpriteSpec> ornamentSpriteSpecs;

		Spec(float segmentLength, float roadWidth)
		: CourseSpec(segmentLength, roadWidth), weatherEffect() {}
	};

	struct ScreenCoord
	{
		float X, Y, W, scale, clip, orientation;
	};

	Spec spec;

	std::vector<fgeal::Image*> sprites;  // TODO use Sprite class to enable animated sprites
	fgeal::Image* landscapeImage;  // TODO replace Image with a Sprite and maybe replace both with a vector of sprites for layered composition

	// drawing parameters
	fgeal::Vector2D drawAreaOffset;
	int drawAreaWidth, drawAreaHeight;
	unsigned drawDistance;
	float cameraDepth, cameraHeight;

	// (experimental)
	float fogging;  // applies distance fog; leave 0 to disable or values up to 1.0 (max) to choose intensity; negative values (down to -1.0) applies darkness ("black fog")
	float pseudoFogging;  // applies a faster distance fog, which applies only to the road; values behave like normal fogging above.
	float lightFiltering;  // applies a layer of white (for positive values up to 1.0) or black (for negative values down to -1.0) transparency; leave 0 to disable
	float headlightsYawAngle;  // if isHeadlightsEffectEnabled is true, leans the exclusion zone bellow to the left (-1.0) or right (1.0), to simulate headlights lightning
	bool isHeadlightsEffectEnabled;  // enforces a exclusion zone to the above (fogging and light filtering) effects, to simulate headlights lightning
	bool isHeadlightsEffectFirstPerson;  // if true, the above headlights lightning will be fixed, otherwise it will lean according to the headlightsYawAngle variable
	bool pseudoFogggingAvoidBackground;  // if true, pseudo-fogging will not be applied to course background (useful for backgrounds that already had some pre-treatment)

	// controls whether to hide leftwards out-of-bounds vehicle sprites (in other words, disabled said workaroung if not needed); enabled by default.
	bool isVehicleSpriteLeftOcclusionEnabled;

	enum CameraBehavior
	{
		CAMERA_BEHAVIOR_TYPE_A,
		CAMERA_BEHAVIOR_TYPE_B,
		CAMERA_BEHAVIOR_TYPE_C,

		CAMERA_BEHAVIOR_TYPE_COUNT  // for counting purposes...
	} cameraBehavior;

	// screen coordinates cache
	std::vector<ScreenCoord> coordCache;

	// A list of vehicles (pointers) to be displayed
	std::vector<const Pseudo3DVehicle*> vehicles;

	// Scale factor of the position of objects (props, vehicles).
	float lengthScale;

	// default recommended camera height
	static const float DEFAULT_CAMERA_HEIGHT;

	// recommended camera height from a vehicle interior perspective
	static const float INTERIOR_CAMERA_HEIGHT;

	Pseudo3DCourseAnimation();
	~Pseudo3DCourseAnimation();

	void loadSpec(const Spec&);

	void draw(float zpos=0, float xpos=0, float slopeAngle=0);

	void freeAssetsData();
};

#endif /* COURSE_GFX_HPP_ */
