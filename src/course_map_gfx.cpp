/*
 * course_map_gfx.cpp
 *
 *  Created on: 23 de set de 2019
 *      Author: carlosfaruolo
 */

#include "course_map_gfx.hpp"

#include "psimpl/psimpl.h"

#include <iterator>

using fgeal::Point;
using fgeal::Color;
using fgeal::Graphics;
using std::vector;

inline static void rotatePoint(Point& p, const Point& center, float angle)
{
	const float s = sin(angle),
			c = cos(angle);

	// translate point back to origin:
	p.x -= center.x;
	p.y -= center.y;

	// rotate point
	float xnew = p.x * c - p.y * s;
	float ynew = p.x * s + p.y * c;

	// translate point back:
	p.x = xnew + center.x;
	p.y = ynew + center.y;
}

Pseudo3DCourseMap::Pseudo3DCourseMap()
: spec(CourseSpec(0,0)), bounds(), offset(), scale(), color(), secondaryColor(Color::_TRANSPARENT),
  plottedPositions(),
  segmentHighlightColor(), isSegmentHighlighted(), segmentHighlightingEnabled(),
  geometryOtimizationEnabled(), roadContrastColorEnabled(), usethickLinePrimitive()
{}

Pseudo3DCourseMap::Pseudo3DCourseMap(const CourseSpec& s)
: spec(s), bounds(), offset(), scale(), color(), secondaryColor(Color::_TRANSPARENT),
  plottedPositions(),
  segmentHighlightColor(), isSegmentHighlighted(), segmentHighlightingEnabled(),
  geometryOtimizationEnabled(), roadContrastColorEnabled(), usethickLinePrimitive()
{}

void Pseudo3DCourseMap::compile()
{
	Point p1 = offset;
	float angle = 0;

	// if no scale set, set one automatically to fit minimap bounds
	if(scale.isZero())
	{
		Point pmin = Point(), pmax = Point();
		for(unsigned i = 0; i < spec.lines.size(); i++)
		{
			Point p2 = p1;
			p2.x += spec.lines[i].curve;
			p2.y += sqrt(pow(spec.roadSegmentLength, 2) - pow(spec.lines[i].curve, 2));
			angle += asin(spec.lines[i].curve/spec.roadSegmentLength);
			rotatePoint(p2, p1, angle);
			if(p2.x < pmin.x)
				pmin.x = p2.x;
			else if(p2.x > pmax.x)
				pmax.x = p2.x;

			if(p2.y < pmin.y)
				pmin.y = p2.y;
			else if(p2.y > pmax.y)
				pmax.y = p2.y;

			p1 = p2;
		}

		const float deltaX = pmax.x - pmin.x, deltaY = pmax.y - pmin.y,
				newscale = (deltaX > deltaY? 0.9f*bounds.w/deltaX :
						deltaY > deltaX? 0.9f*bounds.h/deltaY : 1.f);

		scale.x = scale.y = newscale;
		offset.x = -pmin.x + 0.5f*(bounds.w/newscale - deltaX);
		offset.y = -pmin.y + 0.5f*(bounds.h/newscale - deltaY);
	}

	vector<float> points;
	points.resize(2*(spec.lines.size()+1));

	p1 = offset; angle = 0;
	for(unsigned i = 0; i < spec.lines.size(); i++)
	{
		Point p2 = p1;
		p2.x += spec.lines[i].curve;
		p2.y += sqrt(pow(spec.roadSegmentLength, 2) - pow(spec.lines[i].curve, 2));
		angle += asin(spec.lines[i].curve/spec.roadSegmentLength);
		rotatePoint(p2, p1, angle);
		if(i == 0)
		{
			points[0] = p1.x*scale.x;
			points[1] = p1.y*scale.y;
		}
		points[2*(i+1)] =   p2.x*scale.x;
		points[2*(i+1)+1] = p2.y*scale.y;

		p1 = p2;
	}

	cache.clear();
	cacheLength.clear();
	isSegmentHighlighted.clear();
	if(geometryOtimizationEnabled and not segmentHighlightingEnabled)
	{
		vector<float> simplifiedPoints;
		psimpl::simplify_douglas_peucker <2> (points.begin (), points.end (), 1.5f, std::back_inserter(simplifiedPoints));

		cache.resize(simplifiedPoints.size()/2);
		for(unsigned i = 0; i < cache.size(); i++)
		{
			cache[i].x = simplifiedPoints[2*i];
			cache[i].y = simplifiedPoints[2*i+1];
		}
	}
	else
	{
		cache.resize(points.size()/2);
		for(unsigned i = 0; i < cache.size(); i++)
		{
			cache[i].x = points[2*i];
			cache[i].y = points[2*i+1];
		}
		if(segmentHighlightingEnabled)
			isSegmentHighlighted.resize(cache.size(), false);
	}

	cacheLength.resize(cache.size());
	cacheLength[0] = 0;
	for(unsigned i = 1, j = 1; i < cache.size(); i++)
	{
		cacheLength[i] = cacheLength[i-1];
		for(unsigned k = j; k < spec.lines.size(); k++)
		{
			cacheLength[i] += spec.roadSegmentLength;
			if(points[2*k] == cache[i].x and points[2*k+1] == cache[i].y)
			{
				j = k+1;
				break;
			}
		}
	}
}

void Pseudo3DCourseMap::draw()
{
	if(spec.lines.empty())
		return;
	else if(cache.empty())
		this->compile();

	if(secondaryColor == Color::_TRANSPARENT)
		secondaryColor = roadContrastColorEnabled? Color::create(255-color.r, 255-color.g, 255-color.b) : color;

	for(unsigned i = 1; i < cache.size(); i++)
	{
		const Point& p1 = cache[i-1], &p2 = cache[i];
		if(p1.x > 0 and p1.x < bounds.w and p1.y > 0 and p1.y < bounds.h and p2.x > 0 and p2.x < bounds.w and p2.y > 0 and p2.y < bounds.h)
		{
			const Color& col = segmentHighlightingEnabled and not geometryOtimizationEnabled and isSegmentHighlighted[i]? segmentHighlightColor: (i % 2)? color : secondaryColor;
			if(usethickLinePrimitive)
			{
				if(spec.roadWidth * scale.x > 1)
					Graphics::drawThickLine(bounds.x + p1.x, bounds.y + p1.y, bounds.x + p2.x, bounds.y + p2.y, spec.roadWidth * scale.x, col);
				else
					Graphics::drawLine(bounds.x + p1.x, bounds.y + p1.y, bounds.x + p2.x, bounds.y + p2.y, col);
			}
			else
			{
				const float dx = p1.x - p2.x, dy = p1.y - p2.y, length = std::sqrt(dx * dx + dy * dy);
				if(length > 0)
				{
					const float offsetx = 0.5f * spec.roadWidth * scale.x * dy / length, offsety = 0.5f * spec.roadWidth * scale.x * dx / length;
					Graphics::drawFilledQuadrangle(bounds.x + p1.x - offsetx, bounds.y + p1.y + offsety, bounds.x + p1.x + offsetx, bounds.y + p1.y - offsety,
					                               bounds.x + p2.x + offsetx, bounds.y + p2.y - offsety, bounds.x + p2.x - offsetx, bounds.y + p2.y + offsety, col);
				}
			}
		}
	}

	for(unsigned k = 0; k < plottedPositions.size(); k++)
	for(unsigned i = 1; i < cacheLength.size(); i++)
	if(plottedPositions[k].size > 0 and plottedPositions[k].value < cacheLength[i])
	{
		const PlottedPosition& plottedPosition = plottedPositions[k];
		const float segDiff = (plottedPosition.value - cacheLength[i-1])/(cacheLength[i] - cacheLength[i-1]),
					size = std::max(plottedPosition.minimumScaledSize, plottedPosition.size * 0.5f * spec.roadWidth * scale.x);
		const Point plotCoord = {bounds.x + cache[i].x*segDiff + cache[i-1].x*(1-segDiff), bounds.y + cache[i].y*segDiff + cache[i-1].y*(1-segDiff)};

		if(plotCoord.x > bounds.x and plotCoord.x < bounds.x + bounds.w and plotCoord.y > bounds.y and plotCoord.y < bounds.y + bounds.h)
		{
			switch(plottedPosition.shape)
			{
				default:case PlottedPosition::SHAPE_CIRCLE:
					Graphics::drawFilledCircle(plotCoord, size, plottedPosition.fillColor);
					if(plottedPosition.borderEnabled)
						Graphics::drawCircle(plotCoord, size, plottedPosition.borderColor);
					break;
				case PlottedPosition::SHAPE_SQUARE:
					Graphics::drawFilledRectangle(plotCoord.x - 0.5*size, plotCoord.y - 0.5*size,
												  size, size,
												  plottedPosition.fillColor);
					if(plottedPosition.borderEnabled)
						Graphics::drawRectangle(plotCoord.x - 0.5*size, plotCoord.y - 0.5*size,
												size, size,
												plottedPosition.borderColor);
					break;
				case PlottedPosition::SHAPE_BAR:
					Graphics::drawFilledRectangle(plotCoord.x - 0.5*size, plotCoord.y - 0.16*size,
												  size, 0.32*size,
												  plottedPosition.fillColor);
					if(plottedPosition.borderEnabled)
						Graphics::drawRectangle(plotCoord.x - 0.5*size, plotCoord.y - 0.16*size,
												size, 0.32*size,
												plottedPosition.borderColor);
					break;
				case PlottedPosition::SHAPE_TRIANGLE:
					Graphics::drawFilledTriangle(plotCoord.x, plotCoord.y - size,
												 plotCoord.x - 0.866*size, plotCoord.y + 0.25*size,
												 plotCoord.x + 0.866*size, plotCoord.y + 0.25*size,
												 plottedPosition.fillColor);
					if(plottedPosition.borderEnabled)
						Graphics::drawTriangle(plotCoord.x, plotCoord.y - size,
											   plotCoord.x - 0.866*size, plotCoord.y + 0.25*size,
											   plotCoord.x + 0.866*size, plotCoord.y + 0.25*size,
											   plottedPosition.borderColor);
					break;
			}
		}
		break;
	}
}
