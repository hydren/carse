/*
 * course_map_gfx.hpp
 *
 *  Created on: 06/05/2024
 *      Author: hydren
 */

#ifndef COURSE_MAP_GFX_HPP_
#define COURSE_MAP_GFX_HPP_

#include "course_gfx.hpp"

struct Pseudo3DCourseMap
{
	CourseSpec spec;

	fgeal::Rectangle bounds;
	fgeal::Vector2D offset, scale;

	fgeal::Color color, secondaryColor;

	struct PlottedPosition
	{
		fgeal::Color fillColor, borderColor;
		float value, size, minimumScaledSize;   // size is relative to road width, minimumScaledSize is not
		enum Shape { SHAPE_CIRCLE, SHAPE_SQUARE, SHAPE_BAR, SHAPE_TRIANGLE } shape;
		bool borderEnabled;
	};
	std::vector<PlottedPosition> plottedPositions;

	fgeal::Color segmentHighlightColor;
	std::vector<bool> isSegmentHighlighted;
	bool segmentHighlightingEnabled;

	bool geometryOtimizationEnabled, roadContrastColorEnabled, usethickLinePrimitive;

	Pseudo3DCourseMap();
	Pseudo3DCourseMap(const CourseSpec& spec);

	void compile();

	void draw();

	private:
	std::vector<fgeal::Point> cache;
	std::vector<float> cacheLength;
};

#endif /* COURSE_MAP_GFX_HPP_ */
