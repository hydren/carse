/*
 * course_spec_parser.cpp
 *
 *  Created on: 26 de fev de 2018
 *      Author: carlosfaruolo
 */

#include "course.hpp"

#include "carse.hpp"

#include "util/filename_context.hpp"

#include "futil/properties.hpp"
#include "futil/string_actions.hpp"
#include "futil/string_split.hpp"
#include "futil/collection_actions.hpp"

#include <stdexcept>
#include <iostream>
#include <fstream>

using std::string;
using std::vector;
using std::cout;
using std::endl;
using fgeal::Color;
using fgeal::Display;
using fgeal::Image;
using futil::Properties;
using futil::split;
using futil::trim;
using futil::to_lower;
using futil::starts_with;

static const unsigned DEFAULT_SPRITE_COUNT = 32;

namespace  // static
{
	inline Color parseColor(const string& str)
	{
		return Color::parseCStr(str.c_str());
	}

	inline Pseudo3DCourseAnimation::Spec::WeatherEffect parseWeather(const string& str)
	{
		for(unsigned i = 0; i < Pseudo3DCourseAnimation::Spec::WEATHER_COUNT; i++)
			if(Pseudo3DCourse::Spec::toString(static_cast<Pseudo3DCourseAnimation::Spec::WeatherEffect>(i)) == str)
				return static_cast<Pseudo3DCourseAnimation::Spec::WeatherEffect>(i);

		return Pseudo3DCourseAnimation::Spec::WEATHER_NONE;
	}
}

// -------------------------------------------------------------------------------------------------------------------------------------
void Pseudo3DCourse::Spec::parseRoadStyleProperties(const string& filename)
{
	Properties prop;
	prop.load(filename);
	const Pseudo3DCourse::Spec& defaultRoadStyle = Pseudo3DCourse::Spec::getDefaultRoadStyle();
	colorRoadPrimary = prop.getParsedAllowDefault<Color, parseColor>("road_color_primary", defaultRoadStyle.colorRoadPrimary);
	colorRoadSecondary = prop.getParsedAllowDefault<Color, parseColor>("road_color_secondary", defaultRoadStyle.colorRoadSecondary);
	colorHumblePrimary = prop.getParsedAllowDefault<Color, parseColor>("humble_color_primary", defaultRoadStyle.colorHumblePrimary);
	colorHumbleSecondary = prop.getParsedAllowDefault<Color, parseColor>("humble_color_secondary", defaultRoadStyle.colorHumbleSecondary);
	colorLaneMarking = prop.getParsedAllowDefault<Color, parseColor>("lane_marking_color", defaultRoadStyle.colorLaneMarking);
}

void Pseudo3DCourse::Spec::parseLandscapeStyleProperties(const string& filename)
{
	const string baseDir = filename.substr(0, filename.find_last_of("/\\")+1);
	Properties properties;
	properties.load(filename);
	const Pseudo3DCourse::Spec& defaultLandscapeStyle = Pseudo3DCourse::Spec::getDefaultLandscapeStyle();
	colorOffRoadPrimary = properties.getParsedAllowDefault<Color, parseColor>("offroad_color_primary", defaultLandscapeStyle.colorOffRoadPrimary);
	colorOffRoadSecondary = properties.getParsedAllowDefault<Color, parseColor>("offroad_color_secondary", defaultLandscapeStyle.colorOffRoadSecondary);
	colorLandscape = properties.getParsedAllowDefault<Color, parseColor>("landscape_color", defaultLandscapeStyle.colorLandscape);
	colorHorizon = properties.getParsedAllowDefault<Color, parseColor>("horizon_color", colorOffRoadPrimary);

	landscapeFilename = getContextualizedFilename(properties.get("landscape_image"), baseDir, GameLogic::LANDSCAPE_BG_FOLDER);
	if(landscapeFilename.empty())
	{
		cout << "warning: image file specified in \"landscape_image\" entry could not be found" << ", specified by \"" << filename << "\". using default instead..." << endl;
		landscapeFilename = defaultLandscapeStyle.landscapeFilename;
	}

	const int propMaxIndex = properties.getParsedCStrAllowDefault<int, atoi>("prop_max_id", DEFAULT_SPRITE_COUNT);
	for(int id = 0; id <= propMaxIndex; id++)
	{
		string specifiedSpriteFilename = properties.get("prop" + futil::to_string(id) + "_sprite"), spriteFilename;
		if(not specifiedSpriteFilename.empty())
		{
			spriteFilename = getContextualizedFilename(specifiedSpriteFilename, baseDir, GameLogic::PROP_ASSETS_FOLDER);
			if(spriteFilename.empty())
				cout << "warning: could not load sprite for prop ID #" << id << ": missing file \"" << specifiedSpriteFilename << "\". Prop sprite will be left unspecified!" << endl;
		}

		if(id + 1 > (int) ornamentSpriteSpecs.size())
		{
			ornamentSpriteSpecs.resize(ornamentSpriteSpecs.size() + 1);
			ornamentSpriteSpecs.back().spriteFilename = spriteFilename;
			ornamentSpriteSpecs.back().spriteScale = 1.f;
		}
		else if(not spriteFilename.empty())
			ornamentSpriteSpecs[id].spriteFilename = spriteFilename;

		if(id + 1 > (int) ornaments.size())
			ornaments.resize(ornaments.size() + 1);

		const string blockingFlagStr = to_lower(trim(properties.get("prop" + futil::to_string(id) + "_blocking")));
		if(not blockingFlagStr.empty())
			ornaments[id].blocking = (blockingFlagStr == "true");

		ornaments[id].width = properties.getParsedCStrAllowDefault<double, atof>("prop" + futil::to_string(id) + "_blocking_width", 1);
	}
}

void Pseudo3DCourse::Spec::parseProperties(const string& filename)
{
	GameLogic& logic = GameLogic::getInstance();
	const string baseDir = filename.substr(0, filename.find_last_of("/\\")+1);
	string key;

	*this = Spec(0, 0);  // resets all data in this object

	Properties prop;
	prop.load(filename);

	roadSegmentLength = prop.getParsedCStrAllowDefault<double, atof>("segment_length", 200);  // this may become non-customizable
	roadWidth = prop.getParsedCStrAllowDefault<double, atof>("road_width", 3000);
	roadRollingResistance = prop.getParsedCStrAllowDefault<double, atof>("road_rolling_resistance", logic.getSurfaceTypeRollingResistance("default"));
	roadFrictionCoefficient = prop.getParsedCStrAllowDefault<double, atof>("road_friction_coefficient", logic.getSurfaceTypeFrictionCoefficient("default"));
	offRoadRollingResistance = prop.getParsedCStrAllowDefault<double, atof>("offroad_rolling_resistance", logic.getSurfaceTypeRollingResistance("default_offroad"));
	offRoadFrictionCoefficient = prop.getParsedCStrAllowDefault<double, atof>("offroad_friction_coefficient", logic.getSurfaceTypeFrictionCoefficient("default_offroad"));
	humbleWidth = prop.getParsedCStrAllowDefault<double, atof>("humble_width", roadWidth/10);
	laneMarkingWidth = prop.getParsedCStrAllowDefault<double, atof>("lane_marking_width", roadWidth/15);
	laneCount = prop.getParsedCStrAllowDefault<int, atoi>("lane_count", 0);

	name = prop.get("name");
	author = prop.get("author");
	credits = prop.get("credits");
	comments = prop.get("comments");
	this->filename = filename;

	musicFilename = prop.get("music");
	if(not musicFilename.empty() and not fgeal::filesystem::isFilenameArchive(musicFilename))
	{
		cout << "warning: missing music file \"" << musicFilename << "\". ignoring...\n";
		musicFilename.clear();
	}

	previewFilename = prop.get("portrait");
	if(not previewFilename.empty() and not fgeal::filesystem::isFilenameArchive(previewFilename))
	{
		cout << "warning: missing preview image file \"" << previewFilename << "\". ignoring...\n";
		previewFilename.clear();
	}

	weatherEffect = prop.getParsedAllowDefault<Pseudo3DCourseAnimation::Spec::WeatherEffect, parseWeather>("weather", Pseudo3DCourseAnimation::Spec::WEATHER_NONE);

	key = "preset_road_style";
	if(prop.containsKey(key) and not prop.get(key).empty() and prop.get(key) != "custom")
		copyRoadStyle(logic.getPresetRoadStyle(prop.get(key)));  // attempts to set a preset style and, if not found or unspecified, set the default
	else
		parseRoadStyleProperties(filename);  // reads road style from course spec

	key = "preset_landscape_style";
	if(prop.containsKey(key) and not prop.get(key).empty() and prop.get(key) != "custom")
		copyLandscapeStyle(logic.getPresetLandscapeStyle(prop.get(key)));  // attempts to set a preset style and, if not found or unspecified, set the default
	else
		parseLandscapeStyleProperties(filename);  // reads landscape style from course spec

	key = "road_surface_type";
	if(prop.containsKey(key))
	{
		const string surfaceType = futil::to_lower(futil::trim(prop.get(key)));
		if(logic.existSurfaceType(surfaceType))
		{
			roadRollingResistance = logic.getSurfaceTypeRollingResistance(surfaceType);
			roadFrictionCoefficient = logic.getSurfaceTypeFrictionCoefficient(surfaceType);
		}
		else cout << "unknown surface type \"" << surfaceType << "\". ignoring...\n";
	}

	key = "offroad_surface_type";
	if(prop.containsKey(key))
	{
		const string surfaceType = futil::to_lower(futil::trim(prop.get(key)));
		if(logic.existSurfaceType(surfaceType))
		{
			offRoadRollingResistance = logic.getSurfaceTypeRollingResistance(surfaceType);
			offRoadFrictionCoefficient = logic.getSurfaceTypeFrictionCoefficient(surfaceType);
		}
		else cout << "unknown surface type \"" << surfaceType << "\". ignoring...\n";
	}

	key = "lane_marking_dashed";
	if(prop.containsKey(key))
		laneMarkingDashed = futil::to_lower(futil::trim(prop.get(key))) == "true";

	roadSegmentCount = prop.getParsedCStrAllowDefault<int, atoi>("segment_count", 6400);
	if(prop.containsKey("course_length") and not prop.get("course_length").empty())
	{
		roadSegmentCount = prop.getParsedCStrAllowDefault<double, atof>("course_length", (double) roadSegmentCount);
		cout << "warning: usage of \"course_length\" property is deprecated; use \"segment_count\" property instead." << endl;
	}

	const string specifiedSegmentFilename = prop.retrieve("segment_file", "Missing segment file for course!");
	segmentFilename = getContextualizedFilename(specifiedSegmentFilename, baseDir);

	std::ifstream stream(segmentFilename.c_str());
	if(not stream.is_open())
		throw std::runtime_error("Course description file could not be opened: \"" + specifiedSegmentFilename + "\", specified by \"" + filename + "\"");
}

void Pseudo3DCourse::Spec::loadSegments(const string& segmentFilename)
{
	std::ifstream stream(segmentFilename.c_str());
	if(not stream.is_open())
		throw std::runtime_error("Course description file could not be opened: \"" + segmentFilename + "\", specified by \"" + filename + "\"");

	lines.resize(roadSegmentCount);
	for(unsigned i = 0; i < lines.size(); i++)
	{
		CourseSpec::Segment& line = lines[i];
		line.z = i*roadSegmentLength;

		string str;
		do{
			if(stream.good())
			{
				str = trim(str);
				getline(stream, str);
			}
			else
			{
				str.clear();  // if no more input, signal no data by clearing str
				break;
			}
		}
		while(str.empty() or starts_with(str, "#") or starts_with(str, "!")); // ignore empty lines or commented out ones

		vector<string> tokens = split(str, ',');

		line.x = atof(tokens[0].c_str());

		if(tokens.size() >= 2)
			line.y = atof(tokens[1].c_str());

		if(tokens.size() >= 3)
			line.curve = atof(tokens[2].c_str());

		if(tokens.size() >= 4)
			line.slope = atof(tokens[3].c_str());

		if(tokens.size() >= 6)
		{
			const int ornamentId = atoi(tokens[4].c_str());
			if(ornamentId > -1)
			{
				if(ornamentId >= (int) ornaments.size() or ornamentSpriteSpecs[ornamentId].spriteFilename.empty())
					throw std::logic_error("Course indicates usage of an unspecified prop ID (#" + futil::to_string(line.decoration) + "), specified by \"" + segmentFilename+"\"");

				line.decoration = decorations.size();
				decorations.resize(decorations.size()+1);
				decorations.back().resize(1);
				decorations.back()[0].spec = ornamentId;
				decorations.back()[0].x = atof(tokens[5].c_str());
			}
		}

		if(tokens.size() == 5 or tokens.size() > 6)
			std::cout << "warning: line " << i << " had an unexpected number of parameters (" << tokens.size() << ") - some of them we'll be ignored (specified by \"" << segmentFilename << "\")" << std::endl;
	}

	stream.close();
}

void Pseudo3DCourse::Spec::storeProperties(const string& filename, const string& segmentsFilename)
{
	Properties prop;
	prop.put("name", name);

	if(not author.empty())
		prop.put("author", author);

	if(not credits.empty())
		prop.put("credits", credits);

	if(not comments.empty())
		prop.put("comments", comments);

	prop.put("segment_file", segmentsFilename);
	prop.put("segment_length", futil::to_string((int)roadSegmentLength));
	prop.put("road_width", futil::to_string((int)roadWidth));
	prop.put("segment_count", futil::to_string(roadSegmentCount));

	if(humbleWidth != roadWidth/10)
		prop.put("humble_width", futil::to_string(humbleWidth));

	if(laneMarkingWidth != roadWidth/15)
		prop.put("humble_width", futil::to_string(laneMarkingWidth));

	if(laneCount > 0)
		prop.put("lane_count", futil::to_string(laneCount));

	if(not musicFilename.empty())
		prop.put("music", musicFilename);

	if(weatherEffect != WEATHER_NONE)
		prop.put("weather", Pseudo3DCourse::Spec::toString(weatherEffect));

	if(not presetRoadStyleName.empty())
		prop.put("preset_road_style", presetRoadStyleName);
	else
	{
		prop.put("road_color_primary", colorRoadPrimary.toRgbString());
		prop.put("road_color_secondary", colorRoadSecondary.toRgbString());
		prop.put("humble_color_primary", colorHumblePrimary.toRgbString());
		prop.put("humble_color_secondary", colorHumbleSecondary.toRgbString());
		prop.put("lane_marking_color", colorLaneMarking.toRgbString());
	}

	if(not presetLandscapeStyleName.empty())
		prop.put("preset_landscape_style", presetLandscapeStyleName);
	else
	{
		prop.put("offroad_color_primary", colorOffRoadPrimary.toRgbString());
		prop.put("offroad_color_secondary", colorOffRoadSecondary.toRgbString());
		prop.put("landscape_color", colorLandscape.toRgbString());
		prop.put("horizon_color", colorHorizon.toRgbString());

		if(not landscapeFilename.empty())
			prop.put("landscape_image", landscapeFilename);

		for(unsigned i = 0; i < ornamentSpriteSpecs.size(); i++)
		{
			prop.put("prop"+futil::to_string(i)+"_sprite", ornamentSpriteSpecs[i].spriteFilename);
			prop.put("prop"+futil::to_string(i)+"_blocking", futil::to_string(ornaments[i].blocking));
		}

		if(ornamentSpriteSpecs.size() > DEFAULT_SPRITE_COUNT)
			prop.put("prop_max_id", futil::to_string(ornamentSpriteSpecs.size()-1));
	}

	prop.storeUpdate(filename);
}

void Pseudo3DCourse::Spec::saveSegments(const string& filename)
{
	std::ofstream stream(filename.c_str());

	if(not stream.is_open())
		throw std::runtime_error("Course description file could not be saved: \"" + filename + "\"");

	stream << "#segment file created by carse editor\n";

	for(unsigned i = 0; i < lines.size() and stream.good(); i++)
	{
		const Segment& line = lines[i];
		stream << line.x << ',' << line.y << ',' << line.curve << ',' << line.slope;
		if(line.decoration)
			stream << ',' << decorations[line.decoration].back().spec << ',' << decorations[line.decoration].back().x;
		stream << endl;
	}

	stream.close();
}
