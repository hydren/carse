/*
 * engine_sound.cpp
 *
 *  Created on: 12 de abr de 2017
 *      Author: carlosfaruolo
 */

#include "engine_sound.hpp"

#include <cmath>

using std::string;
using std::vector;
using fgeal::Sound;

EngineSoundSimulator::~EngineSoundSimulator()
{
	freeAssetsData();
}

void EngineSoundSimulator::setProfile(const EngineSoundProfile& profile, short maxRpm)
{
	freeAssetsData();
	this->profile = profile;  // copies the profile (preserves the original intact)
	this->simulatedMaximumRpm = maxRpm;
}

void EngineSoundSimulator::loadAssetsData()
{
	// loads sound data
	for(unsigned i = 0; i < profile.ranges.size(); i++)
		this->soundData.push_back(new Sound(profile.ranges[i].soundFilename));

	assetsAreShared = false;
}

void EngineSoundSimulator::useAssetsDataFrom(const EngineSoundSimulator& sim)
{
	this->soundData = sim.soundData;
	assetsAreShared = true;
}

void EngineSoundSimulator::freeAssetsData()
{
	if(not assetsAreShared)
		for(unsigned i = 0; i < soundData.size(); i++)
			delete soundData[i];  // cleanup

	this->soundData.clear();
}

unsigned EngineSoundSimulator::getRangeIndex(float rpm)
{
	unsigned rangeIndex = 0;
	for(unsigned i = 0; i < soundData.size(); i++)
		if(rpm > profile.ranges[i].startRpm)
			rangeIndex = i;

	return rangeIndex;
}

vector<Sound*>& EngineSoundSimulator::getSoundData()
{
	return soundData;
}

vector<EngineSoundProfile::RangeProfile>& EngineSoundSimulator::getProfileRanges()
{
	return profile.ranges;
}

void EngineSoundSimulator::play()
{
	if(not profile.ranges.empty())
		update(profile.ranges[0].startRpm+1);  //XXX this +1 may be unneccessary
}

void EngineSoundSimulator::update(float currentRpm)
{
	const unsigned soundCount = soundData.size();
	if(soundCount > 0 and currentRpm > 0) // its no use if there is no engine sound or rpm is too low
	{
		// establish the current range index
		const unsigned currentRangeIndex = this->getRangeIndex(currentRpm);

		// some aliases
		const EngineSoundProfile::RangeProfile& currentRange = profile.ranges[currentRangeIndex];
		Sound& currentSound = *soundData[currentRangeIndex];

		currentSound.setVolume(volume * currentRange.soundVolumeFactor);

		if(profile.allowRpmPitching)
			currentSound.setPlaybackSpeed(currentRpm / currentRange.depictedRpm, true);

		if(not currentSound.isPlaying())
		{
			currentSound.stop();  // just to avoid some glitches; theoretically, this call is spurious since the sound is supposedly "not playing"
			currentSound.loop();
		}

		const float currentRangeEndRpm = currentRangeIndex < soundCount - 1? profile.ranges[currentRangeIndex + 1].startRpm : simulatedMaximumRpm,
					currentRangeSize = currentRangeEndRpm - currentRange.startRpm,
					currentRangePosition = (currentRpm - currentRange.startRpm) / currentRangeSize;

		for(unsigned i = 0; i < soundData.size(); i++)
		{
			// some aliases
			const EngineSoundProfile::RangeProfile& range = profile.ranges[i];
			Sound& rangeSound = *soundData[i];

			// preceding range
			if(i == currentRangeIndex - 1 and currentRangePosition < 0.25f)  // only perform if current RPM is within 0-25% of current range
			{
				// rangeSound.setVolume(range.soundVolumeFactor * volume * (1.f - 4.f * currentRangePosition));  // linear fade out
				rangeSound.setVolume(range.soundVolumeFactor * volume * std::sqrt(1.f - 16.f * std::pow(currentRangePosition, 2.f)));  // quadratic fade out

				if(profile.allowRpmPitching)
					rangeSound.setPlaybackSpeed(currentRpm / range.depictedRpm, true);

				if(not rangeSound.isPlaying())
				{
					rangeSound.stop();  // just to avoid some glitches; theoretically, this call is spurious since the sound is supposedly "not playing"
					rangeSound.loop();
				}
			}

			// succeeding range
			else if(i == currentRangeIndex + 1 and currentRangePosition > 0.75f)  // only perform if current RPM is within 75-100% of current range
			{
//				rangeSound.setVolume(range.soundVolumeFactor * volume * (-3.f + 4.f * currentRangePosition));  // linear fade in
				rangeSound.setVolume(range.soundVolumeFactor * volume * std::sqrt(1.f - std::pow(4.f * (currentRangePosition) - 4.f, 2.f)));  // quadratic fade in

				if(profile.allowRpmPitching)
					rangeSound.setPlaybackSpeed(currentRpm / range.depictedRpm, true);

				if(not rangeSound.isPlaying())
				{
					rangeSound.stop();  // just to avoid some glitches; theoretically, this call is spurious since the sound is supposedly "not playing"
					rangeSound.loop();
				}
			}

			// other non-current ranges
			else if(i != currentRangeIndex)
				rangeSound.stop();
		}
	}
}

void EngineSoundSimulator::halt()
{
	for(unsigned i = 0; i < soundData.size(); i++)
		soundData[i]->stop();
}

void EngineSoundSimulator::setVolume(float vol)
{
	this->volume = (vol < 0.f? 0.f : vol > 1.f? 1.f : vol);
}

float EngineSoundSimulator::getVolume() const
{
	return this->volume;
}
