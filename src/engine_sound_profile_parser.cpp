/*
 * engine_sound_profile_parser.cpp
 *
 *  Created on: 19 de fev de 2022
 *      Author: carlosfaruolo
 */

#include "engine_sound.hpp"
#include "game_logic.hpp"
#include "util/filename_context.hpp"

#include "futil/string_actions.hpp"
#include "futil/snprintf.h"

#include <iostream>
#include <algorithm>

using std::string;
using std::vector;
using std::cout;
using std::endl;
using futil::Properties;

EngineSoundProfile GameLogic::createEngineSoundProfileFromFile(const string& filename)
{
	EngineSoundProfile profile;
	Properties prop;
	prop.load(filename);

	const string baseDir = filename.substr(0, filename.find_last_of("/\\")+1);
	const short maxRpm = prop.getParsedCStrAllowDefault<int, atoi>("engine_maximum_rpm", 7000);
	profile.allowRpmPitching = true;

	const string baseKey = "sound";
	if(prop.containsKey(baseKey))
	{
		if(prop.get(baseKey) == "none")
		{
			profile.ranges.clear();
		}
		else if(prop.get(baseKey) == "custom")
		{
			string key = baseKey + "_rpm_pitching";
			if(prop.containsKey(key) and not prop.get(key).empty() and prop.get(key) != "default")
			{
				string value = futil::trim(prop.get(key));
				if(value == "false" or value == "no")
					profile.allowRpmPitching = false;
			}

			key = baseKey + "_count";
			unsigned soundCount = prop.getParsedCStrAllowDefault<int, atoi>(key, 16);

			for(unsigned i = 0; i < soundCount; i++)
			{
				const string subBaseKey = baseKey + futil::to_string(i);
				if(prop.containsKey(subBaseKey))
				{
					const string sndFilename = getContextualizedFilename(prop.get(subBaseKey), baseDir, GameLogic::VEHICLES_FOLDER+"/", GameLogic::PRESET_ENGINE_SOUND_PROFILES_FOLDER+"/");
					if(sndFilename.empty())
						cout << "warning: sound file \"" << prop.get(subBaseKey) << "\" could not be found!"  // TODO use default sound?
						<< " (specified by \"" << filename << "\")" << endl;

					// now try to read _rpm property
					key = subBaseKey + "_rpm";
					short rpm = -1;
					if(prop.containsKey(key))
						rpm = atoi(prop.get(key).c_str());

					// if rpm < 0, either rpm wasn't specified, or was intentionally left -1 (or other negative number)
					if(rpm < 0)
					{
						if(i == 0) rpm = 0;
						else       rpm = (maxRpm - profile.ranges.rbegin()->startRpm)/2;
					}

					key = subBaseKey + "_depicted_rpm";
					short depictedRpm = -1;
					if(prop.containsKey(key))
						depictedRpm = atoi(prop.get(key).c_str());

					key = subBaseKey + "_pitch_factor";
					if(prop.containsKey(key))
					{
						const double pitchFactor = atof(prop.get(key).c_str());
						if(pitchFactor > 0)
							depictedRpm = rpm*pitchFactor;
					}

					if(depictedRpm < 0)
						depictedRpm = rpm;

					if(depictedRpm == 0)
						depictedRpm = 1;  // to avoid division by zero

					key = subBaseKey + "_volume_factor";
					float volumeFactor = 1.f;
					if(prop.containsKey(key))
						volumeFactor = atof(prop.get(key).c_str());

					// save filename and settings for given rpm
					const EngineSoundProfile::RangeProfile range = {rpm, depictedRpm, volumeFactor, sndFilename};
					profile.ranges.push_back(range);
				}
				else cout << "warning: missing expected entry \"" << subBaseKey << "\" (specified by \"" << filename << "\")" << endl;
			}

			struct RangeProfileCompare { static bool function(const EngineSoundProfile::RangeProfile& p1, const EngineSoundProfile::RangeProfile& p2) { return p1.startRpm < p2.startRpm; } };
			std::stable_sort(profile.ranges.begin(), profile.ranges.end(), RangeProfileCompare::function);
		}
		else
			throw std::logic_error("properties specify a preset profile instead of a custom one");
	}

	return profile;
}

vector<string> EngineSoundSimulator::getDebugInfo(float currentRpm)
{
	char buffer[64];
	std::vector<string> lines(soundData.size());
	const unsigned currentRangeIndex = (currentRpm != -1? this->getRangeIndex(currentRpm) : -1);  // overflow intended
	for(unsigned i = 0; i < soundData.size(); i++)
	{
		const char guardChar = (soundData[i]->isPlaying()? (currentRangeIndex==i? '[' : '(') : ' '), guardChar2 = guardChar == '[' ? ']' : guardChar == '(' ? ')' : guardChar;
		futil::snprintf(buffer, 64, "%cs%u%c vol: %2.2f pitch: %2.2f", guardChar, i, guardChar2, soundData[i]->getVolume(), soundData[i]->getPlaybackSpeed());
		lines[i] = buffer;
	}
	return lines;
}
