/*
 * game_logic.cpp
 *
 *  Created on: 1 de dez de 2017
 *      Author: carlosfaruolo
 */

#include "game_logic.hpp"

#include "carse.hpp"

#include "main_args.hpp"

#include "util/filename_context.hpp"

#include "futil/random.h"
#include "futil/string_extra_operators.hpp"
#include "futil/string_parse.hpp"
#include "futil/string_actions.hpp"
#include "futil/string_split.hpp"
#include "futil/collection_actions.hpp"

#include <iostream>
#include <fstream>
#include <algorithm>
#include <set>
#include <cmath>

using std::string;
using std::vector;
using std::set;
using std::map;
using std::cout;
using std::endl;
using futil::ends_with;
using futil::Properties;
using fgeal::AdapterException;
using namespace fgeal::filesystem;

const string
	GameLogic::USER_CONFIG_FOLDER = getResolvedUserConfigFolder(),
	GameLogic::COURSES_FOLDER = "data/courses",
	GameLogic::VEHICLES_FOLDER = "data/vehicles",
	GameLogic::TRAFFIC_FOLDER = "data/traffic",
	GameLogic::DASHBOARDS_FOLDER = "assets/hud/dashboards",
	GameLogic::MUSIC_FOLDER = "assets/music",
	GameLogic::PROP_ASSETS_FOLDER = "assets/props/",
	GameLogic::LANDSCAPE_BG_FOLDER = "assets/landscapes/",
	GameLogic::PRESET_ENGINE_SOUND_PROFILES_FOLDER = "assets/sound/engine",
	GameLogic::PRESET_COURSE_ROAD_STYLES_FOLDER = "data/styles/road",
	GameLogic::PRESET_COURSE_LANDSCAPE_STYLES_FOLDER = "data/styles/landscape";

const float
	GameLogic::PHYSICS_GRAVITY_ACCELERATION_DEFAULT = 9.8066, // standard gravity (actual value varies with altitude, from 9.7639 to 9.8337)
	GameLogic::PHYSICS_AIR_DENSITY_DEFAULT = 1.2041,  // air density at sea level, 20ºC (68ºF) (but actually varies significantly with altitude, temperature and humidity)
	GameLogic::PHYSICS_MPS_TO_MPH = 2.236936,  // m/s to mph conversion factor
	GameLogic::PHYSICS_MPS_TO_KPH = 3.6,  // m/s to km/h conversion factor
	GameLogic::PHYSICS_HP_TO_KW = 0.745699,
	GameLogic::PHYSICS_HP_TO_PS = 1.013870,
	GameLogic::PHYSICS_NM_TO_LBFT = 0.737562,
	GameLogic::PHYSICS_NM_TO_KGFM = 0.101972,
	GameLogic::PHYSICS_MAXIMUM_DELTA_TIME = 0.01,
	GameLogic::PHYSICS_BURNOUT_SLIP_RATIO = 0.2,  // 20%
	GameLogic::PHYSICS_PEDAL_POSITION_SPEED = 10.0;

const Pseudo3DRaceState::RaceType DEFAULT_RACE_TYPE = Pseudo3DRaceState::RACE_TYPE_PRACTICE;

// fwd. declared
static void searchFiles(vector<string>& possibleFilenames, const string& folder, const string& extension, bool includeUserDir);
static vector<string> searchFiles(const string& folder, const string& extension, bool includeUserDir=false);

GameLogic::NextMatchParameters::NextMatchParameters()
: isRandomCourseSpec(true), isDebugCourseSpec(false), courseSpec(0, 0), race(), players(1)
{
	race.raceType = DEFAULT_RACE_TYPE;  // set default race type
	race.isLapped = false;  // set non-lapped by default
	race.lapCountGoal = 2;    // set default lap count
	race.playerCount = 1;  // by default, one player
	race.cpuOpponentCount = 0;  // by default, no opponents
	race.opponentVehicleCategory = "all";  // by default, all vehicles
	race.trafficDensity = 0;  // by default, no traffic
	race.simulationType = Mechanics::SIMULATION_TYPE_SLIPLESS;  // by default, slipless simulation
	race.enableJumpSimulation = false;  // by default, disable jump simulation
	race.disableSteeringSpeedLimit = true;  // by default, disable steering speed limit
	race.powerBoostCount = 0;  // by default, no power boosts available
	race.isWeatherEffectEnforced = false;  // by default, do not force weather effects
	race.forcedWeatherEffect = Pseudo3DCourseAnimation::Spec::WEATHER_NONE;    // by default, no weather effects enforced
	race.cameraBehavior = Pseudo3DCourseAnimation::CAMERA_BEHAVIOR_TYPE_B;  // set default camera behavior
	race.isCockpitView = false;
	race.isImperialUnit = false;
	race.useCachedDialGauge = false;
	race.musicOption = -1;
	dashboardLayoutFilename = "current";
	players[0].alternateSpriteIndex = -1;
	players[0].enableAutomaticShifting = true;
}

GameLogic* GameLogic::instance = null;

// logic constructor, booooooring!
GameLogic::GameLogic()
: raceOnlyMode(), masterVolume(0.9f), uiUnits(),
  currentMainMenuStateId(Carse::MAIN_MENU_CLASSIC_LAYOUT_STATE_ID),
  nextMatch()
{
	uiUnits.power = UI_UNIT_POWER_HP;
	uiUnits.torque = UI_UNIT_TORQUE_NM;
	uiUnits.isImperialDistance = uiUnits.isImperialWeight = false;
}

void GameLogic::initialize()
{
	cout << "user config folder is at " << USER_CONFIG_FOLDER << endl;
	this->loadPresetEngineSoundProfiles();
	this->loadPresetSurfaceTypes();
	this->loadPresetCourseStyles();
	this->loadCourses();
	this->loadVehicles();
	this->loadTrafficVehicles();

	cout << endl << "reading dashboard configs..." << endl;
	searchFiles(dashboardConfigFilenames, GameLogic::DASHBOARDS_FOLDER, ".properties", true);

	nextMatch.dashboardLayoutFilename = dashboardConfigFilenames[0];

	cout << endl << "reading music files..." << endl;
	searchFiles(musicFilenames, GameLogic::MUSIC_FOLDER, ".ogg", true);

	// remove "music_sample.ogg" from music list
	for(unsigned i = 0, done = 0; i < musicFilenames.size() and not done; i++)
		if(futil::ends_with(musicFilenames[i], "music_sample.ogg"))
			musicFilenames.erase(musicFilenames.begin() + i), done = 1;

	fontsInfo.load("assets/fonts/fonts.properties");

	vehicleCategories.push_back("all");
	vehicleCategories.push_back("same as players");
	vehicleCategories.push_back("same class");
	vehicleCategories.push_back("cars");
	vehicleCategories.push_back("bikes");
	std::ifstream stream("data/vehicles/misc_categories.txt");
	if(stream.is_open())
	{
		cout << endl << "reading misc. vehicle categories..." << endl;
		while(stream.good())
		{
			string str;
			getline(stream, str);
			str = futil::trim(str);
			if(not str.empty() and not futil::starts_with(str, "#") and not futil::starts_with(str, "!"))
				vehicleCategories.push_back(str);
		}
		stream.close();
	}
	else cout << "could not read misc. vehicle categories, issues may occur." << endl;

}

void GameLogic::onStatesListInitFinished()
{
	if(raceOnlyMode)
	{
		if(CmdArgs.debugMode.isSet())
		{
			nextMatch.race.raceType = Pseudo3DRaceState::RACE_TYPE_DEBUG;
			nextMatch.isDebugCourseSpec = true;
		}
		else
		{
			nextMatch.isDebugCourseSpec = false;
			nextMatch.isRandomCourseSpec = CmdArgs.randomCourse.isSet();

			if(not nextMatch.isRandomCourseSpec)
			{
				if(CmdArgs.courseIndex.isSet())
				{
					if(CmdArgs.courseIndex.getValue() < courses.size())
						nextMatch.courseSpec.loadFromFile(courses[CmdArgs.courseIndex.getValue()].filename);
					else
					{
						nextMatch.courseSpec.loadFromFile(courses.back().filename);
						if(CmdArgs.courseIndex.isSet())
							cout << "warning: specified course index is out of bounds! using last valid index instead..." << endl;
					}
				}
				else nextMatch.isRandomCourseSpec = true;
			}

			if(CmdArgs.raceType.isSet() and CmdArgs.raceType.getValue() < Pseudo3DRaceState::RACE_TYPE_COUNT)
				nextMatch.race.raceType = static_cast<Pseudo3DRaceState::RaceType>(CmdArgs.raceType.getValue());
			else
			{
				nextMatch.race.raceType = DEFAULT_RACE_TYPE;
				if(CmdArgs.raceType.isSet())
					cout << "warning: specified race type index is out of bounds! using default race type instead..." << endl;
			}

			if(nextMatch.race.isLapped)
				nextMatch.race.lapCountGoal = CmdArgs.lapCount.getValue();
		}

		if(CmdArgs.playerCount.isSet())
			nextMatch.race.playerCount = CmdArgs.playerCount.getValue();

		if(CmdArgs.vehicleIndex.getValue() < vehicles.size())
			nextMatch.players[0].vehicleSpec = vehicles[CmdArgs.vehicleIndex.getValue()];
		else
		{
			nextMatch.players[0].vehicleSpec = vehicles.back();
			cout << "warning: specified player vehicle index is out of bounds! using another valid index instead..." << endl;
		}

		if(CmdArgs.vehicleAltSpriteIndex.getValue() >= -1 and CmdArgs.vehicleAltSpriteIndex.getValue() < (int) nextMatch.players[0].vehicleSpec.alternateSprites.size())
			nextMatch.players[0].alternateSpriteIndex = CmdArgs.vehicleAltSpriteIndex.getValue();
		else
		{
			nextMatch.players[0].alternateSpriteIndex = nextMatch.players[0].vehicleSpec.alternateSprites.size()-1;
			cout << "warning: specified player vehicle alternate sprite index is out of bounds! using another valid index instead..." << endl;
		}

		if(nextMatch.players.size() < nextMatch.race.playerCount)  // copy vehicle selection for remaining players
			nextMatch.players.resize(nextMatch.race.playerCount, nextMatch.players.back());

		if(CmdArgs.vehicleManualShifting.isSet())
			nextMatch.players[0].enableAutomaticShifting = false;

		if(CmdArgs.extraPlayersVehicles.isSet())
		{
			vector<string> extraPlayersArgs = futil::split(CmdArgs.extraPlayersVehicles.getValue(), ',');
			for(unsigned i = 0, k = 1; i < extraPlayersArgs.size() and k < nextMatch.players.size(); i++, k++)
			{
				extraPlayersArgs[i] = futil::trim(extraPlayersArgs[i]);
				if(not extraPlayersArgs[i].empty())
				{
					if(futil::ends_with(extraPlayersArgs[i], "M"))
					{
						nextMatch.players[k].enableAutomaticShifting = false;
						extraPlayersArgs[i].resize(extraPlayersArgs[i].length()-1);
					}
					if(not extraPlayersArgs[i].empty())
					{
						const vector<string> extraPlayerArgsTokens = futil::split(extraPlayersArgs[i], 'A');
						const string extraPlayerVehicleArgToken = extraPlayerArgsTokens[0],
									 extraPlayerVehicleAltSpriteArgToken = extraPlayerArgsTokens.size() > 1? extraPlayerArgsTokens[1] : "";
						if(futil::parseable<unsigned>(extraPlayerVehicleArgToken))
						{
							const unsigned index = futil::parse<unsigned>(extraPlayerVehicleArgToken);
							if(index < vehicles.size())
								nextMatch.players[k].vehicleSpec = vehicles[index];
							else
							{
								nextMatch.players[k].vehicleSpec = vehicles.back();
								cout << "warning: specified player " << k << " vehicle index is out of bounds! using another valid index instead..." << endl;
							}
						}
						if(futil::parseable<int>(extraPlayerVehicleAltSpriteArgToken))
						{
							const int altSpriteIndex = futil::parse<int>(extraPlayerVehicleAltSpriteArgToken);
							if(altSpriteIndex >= -1 and altSpriteIndex < (int) nextMatch.players[k].vehicleSpec.alternateSprites.size())
								nextMatch.players[k].alternateSpriteIndex = altSpriteIndex;
							else
							{
								nextMatch.players[k].alternateSpriteIndex = nextMatch.players[k].vehicleSpec.alternateSprites.size()-1;
								cout << "warning: specified player " << k << " vehicle alternate sprite index is out of bounds! using another valid index instead..." << endl;
							}
						}
					}
				}
			}
		}

		if(CmdArgs.simulationType.getValue() < Mechanics::SIMULATION_TYPE_COUNT)
			nextMatch.race.simulationType = static_cast<Mechanics::SimulationType>(CmdArgs.simulationType.getValue());
		else
		{
			nextMatch.race.simulationType = Mechanics::SIMULATION_TYPE_SLIPLESS;
			cout << "warning: specified simulation type is out of bounds! using default type instead..." << endl;
		}

		if(CmdArgs.hudType.getValue() < getDashboardLayouts().size())
			nextMatch.dashboardLayoutFilename = getDashboardLayouts()[CmdArgs.hudType.getValue()];
		else
		{
			nextMatch.dashboardLayoutFilename = getDashboardLayouts()[0];
			cout << "warning: specified HUD layout is out of bounds! using default type instead..." << endl;
		}

		if(CmdArgs.opponentCount.isSet())
		{
			nextMatch.race.cpuOpponentCount = CmdArgs.opponentCount.getValue();
			nextMatch.race.opponentVehicleCategory = "same class";
		}

		if(CmdArgs.trafficDensity.isSet())
			nextMatch.race.trafficDensity = CmdArgs.trafficDensity.getValue() * 0.05;

		if(CmdArgs.imperialUnit.isSet())
			nextMatch.race.isImperialUnit = true;

		if(CmdArgs.cameraType.isSet())
			nextMatch.race.cameraBehavior = static_cast<Pseudo3DCourseAnimation::CameraBehavior>(CmdArgs.cameraType.getValue() % Pseudo3DCourseAnimation::CAMERA_BEHAVIOR_TYPE_COUNT);
	}
	else
	{
		nextMatch.isRandomCourseSpec = true;  // set random course as default
		nextMatch.players[0].vehicleSpec = vehicles[0];  // set default vehicle
	}
}

void GameLogic::passJoystickAxisToKeyboardArrows(fgeal::Game::State* const state, unsigned axis, float value)
{
	if(std::abs(value) >= 0.5f)
	{
		if(axis % 2)  // odd axes
			state->onKeyPressed(value < 0? fgeal::Keyboard::KEY_ARROW_UP : fgeal::Keyboard::KEY_ARROW_DOWN);
		else  // even axes
			state->onKeyPressed(value < 0? fgeal::Keyboard::KEY_ARROW_LEFT : fgeal::Keyboard::KEY_ARROW_RIGHT);
	}
}

void GameLogic::passJoystickButtonsToEnterReturnKeys(fgeal::Game::State* const state, unsigned button)
{
	if(button % 2)  // odd buttons
		state->onKeyPressed(fgeal::Keyboard::KEY_ESCAPE);
	else  // even buttons
		state->onKeyPressed(fgeal::Keyboard::KEY_ENTER);
}

string GameLogic::getFontPath(const std::string& id)
{
	string path = fontsInfo.get(id);

	for(unsigned loopCount = 0; futil::starts_with(path, "@") and loopCount < 100; loopCount++)
		path = fontsInfo.get(path.substr(1));

	if(path.empty())
		path = fontsInfo.get("@default");

	return "assets/fonts/" + path;
}

// needed to futil::parse be able to used by getParsed()
namespace { struct parseUnsigned { static unsigned function(const string& str) { return futil::parse<unsigned>(str); } }; }

unsigned GameLogic::getFontSize(const std::string& id, const string& modifier)
{
	return fontsInfo.getParsed<unsigned, parseUnsigned::function>(id + "_size" + (modifier.empty()? "" : "_" + modifier), 12);
}

const EngineSoundProfile& GameLogic::getPresetEngineSoundProfile(const std::string& presetName) const
{
	if(presetEngineSoundProfiles.find(presetName) != presetEngineSoundProfiles.end())
		return presetEngineSoundProfiles.find(presetName)->second;
	else
		return presetEngineSoundProfiles.find("default")->second;
}

const Pseudo3DCourse::Spec& GameLogic::getPresetLandscapeStyle(const std::string& presetName) const
{
	if(presetLandscapeStyles.find(presetName) != presetLandscapeStyles.end())
		return presetLandscapeStyles.find(presetName)->second;
	else
		return presetLandscapeStyles.find("default")->second;
}

const Pseudo3DCourse::Spec& GameLogic::getPresetRoadStyle(const std::string& presetName) const
{
	if(presetRoadStyles.find(presetName) != presetRoadStyles.end())
		return presetRoadStyles.find(presetName)->second;
	else
		return presetRoadStyles.find("default")->second;
}

bool GameLogic::existSurfaceType(const string& typeName)
{
	return presetSurfaceTypes.find(typeName) != presetSurfaceTypes.end();
}

float GameLogic::getSurfaceTypeRollingResistance(const string& typeName)
{
	if(presetSurfaceTypes.find(typeName) != presetSurfaceTypes.end())
		return presetSurfaceTypes.find(typeName)->second.rollingResistance;
	else
		return presetSurfaceTypes.find("default")->second.rollingResistance;
}

float GameLogic::getSurfaceTypeFrictionCoefficient(const string& typeName)
{
	if(presetSurfaceTypes.find(typeName) != presetSurfaceTypes.end())
		return presetSurfaceTypes.find(typeName)->second.frictionCoefficient;
	else
		return presetSurfaceTypes.find("default")->second.frictionCoefficient;
}

vector<string> GameLogic::getPresetRoadStylesNames() const
{
	vector<string> names(presetRoadStyles.size()); int i = 0;
	for(map<string, Pseudo3DCourse::Spec>::const_iterator it = presetRoadStyles.begin(); it != presetRoadStyles.end(); ++it)
		names[i++] = it->first;
	return names;
}

vector<string> GameLogic::getPresetLandscapeStylesNames() const
{
	vector<string> names(presetLandscapeStyles.size()); int i = 0;
	for(map<string, Pseudo3DCourse::Spec>::const_iterator it = presetLandscapeStyles.begin(); it != presetLandscapeStyles.end(); ++it)
		names[i++] = it->first;
	return names;
}

const Pseudo3DCourse::Spec& GameLogic::getRandomPresetLandscapeStyle() const
{
	map<string, Pseudo3DCourse::Spec>::const_iterator it = presetLandscapeStyles.begin();
	std::advance(it, futil::random_between(0, presetLandscapeStyles.size()));
	return it->second;  //@suppress("Returning the address of a local variable")
}

const Pseudo3DCourse::Spec& GameLogic::getRandomPresetRoadStyle() const
{
	map<string, Pseudo3DCourse::Spec>::const_iterator it = presetRoadStyles.begin();
	std::advance(it, futil::random_between(0, presetRoadStyles.size()));
	return it->second;  //@suppress("Returning the address of a local variable")
}

inline static float getVehicleSpecRating(const Pseudo3DVehicle::Spec& spec)
{
	return spec.engine.maximumPower/spec.body.mass;  // using power-to-weight ratio as estimation
}

vector<const Pseudo3DVehicle::Spec*> GameLogic::getEligibleCpuOpponentVehicleSpecs() const
{
	vector<const Pseudo3DVehicle::Spec*> opponentVehicleSpecs;

	if(nextMatch.race.opponentVehicleCategory.empty() or nextMatch.race.opponentVehicleCategory == "all")
		const_foreach(const Pseudo3DVehicle::Spec&, spec, vector<Pseudo3DVehicle::Spec>, vehicles)
			opponentVehicleSpecs.push_back(&spec);

	else if(nextMatch.race.opponentVehicleCategory == "cars")
	{
		const_foreach(const Pseudo3DVehicle::Spec&, spec, vector<Pseudo3DVehicle::Spec>, vehicles)
			if(spec.body.vehicleType == Mechanics::TYPE_CAR)
				opponentVehicleSpecs.push_back(&spec);
	}
	else if(nextMatch.race.opponentVehicleCategory == "bikes")
	{
		const_foreach(const Pseudo3DVehicle::Spec&, spec, vector<Pseudo3DVehicle::Spec>, vehicles)
			if(spec.body.vehicleType == Mechanics::TYPE_BIKE)
				opponentVehicleSpecs.push_back(&spec);
	}
	else if(nextMatch.race.opponentVehicleCategory == "same class")
	{
		float meanPlayerVehicleRating = 0;
		set<Mechanics::VehicleType> playersTypes;
		set<string> playersCategories;
		const_foreach(const GameLogic::NextMatchParameters::PlayerParameters&, choice, vector<GameLogic::NextMatchParameters::PlayerParameters>, nextMatch.players)
		{
			meanPlayerVehicleRating += getVehicleSpecRating(choice.vehicleSpec);
			playersTypes.insert(choice.vehicleSpec.body.vehicleType);
			playersCategories.insert(choice.vehicleSpec.categories.begin(), choice.vehicleSpec.categories.end());
		}
		meanPlayerVehicleRating /= nextMatch.players.size();

		const_foreach(const Pseudo3DVehicle::Spec&, spec, vector<Pseudo3DVehicle::Spec>, vehicles)
		{
			const float vehicleSpecRating = getVehicleSpecRating(spec);
			if(vehicleSpecRating/meanPlayerVehicleRating > 0.97f and vehicleSpecRating/meanPlayerVehicleRating < 1.03f and futil::contains_element(playersTypes, spec.body.vehicleType))
			{
				if(playersCategories.empty() and spec.categories.empty())
					opponentVehicleSpecs.push_back(&spec);
				else
					for(unsigned i = 0, done = 0; i < spec.categories.size() and not done; i++)
						if(futil::contains_element(playersCategories, spec.categories[i]))
							opponentVehicleSpecs.push_back(&spec), done = true;
			}
		}
	}
	else if(nextMatch.race.opponentVehicleCategory == "same as players")
		for(unsigned i = 0; i < std::max((unsigned) 1, std::min(nextMatch.race.playerCount, (unsigned) nextMatch.players.size())); i++)
			opponentVehicleSpecs.push_back(&nextMatch.players[i].vehicleSpec);
	else
		const_foreach(const Pseudo3DVehicle::Spec&, spec, vector<Pseudo3DVehicle::Spec>, vehicles)
			if(futil::contains_element(spec.categories, nextMatch.race.opponentVehicleCategory))
				opponentVehicleSpecs.push_back(&spec);

	if(opponentVehicleSpecs.empty())
		const_foreach(const Pseudo3DVehicle::Spec&, spec, vector<Pseudo3DVehicle::Spec>, vehicles)
		opponentVehicleSpecs.push_back(&spec);

	return opponentVehicleSpecs;
}

void GameLogic::updateCourseList()
{
	courses.clear();
	this->loadCourses();
}

void GameLogic::updateVehicleList()
{
	vehicles.clear();
	this->loadVehicles();
}

void GameLogic::setNextCourseDebug()
{
	nextMatch.courseSpec = Pseudo3DCourse::Spec::createDebug();
	nextMatch.race.raceType = Pseudo3DRaceState::RACE_TYPE_DEBUG;
}

//===============================================================================================================================================

/// stores (by appending) a list of files inside the given folder and inside its subfolders (but not recursively)
static void searchFiles(vector<string>& possibleFilenames, const string& folder, const string& extension, bool includeUserDir)
{
	try {
		const vector<string> folderFilenames = getFilenamesWithinDirectory(folder);
		const_foreach(const string&, filename, vector<string>, folderFilenames) try
		{
			if(isFilenameArchive(filename) and (extension.empty() or ends_with(filename, extension)))
				possibleFilenames.push_back(filename);

			else if(isFilenameDirectory(filename)) try
			{
				const vector<string> subfolderFilenames = getFilenamesWithinDirectory(filename);
				const_foreach(const string&, subfolderFilename, vector<string>, subfolderFilenames) try
				{
					if(isFilenameArchive(subfolderFilename) and (extension.empty() or ends_with(subfolderFilename, extension)))
						possibleFilenames.push_back(subfolderFilename);
				}
				catch (AdapterException& e) { cout << "failed to analyze file \"" << subfolderFilename << "\"\n   reason: " << e.what() << endl; }
			}
			catch (AdapterException& e) { cout << "failed to open folder \"" << filename << "\"\n   reason: " << e.what() << endl; }
		}
		catch (AdapterException& e) { cout << "failed to analyze file \"" << filename << "\"\n   reason: " << e.what() << endl; }
	}
	catch (AdapterException& e) { cout << "failed to open folder \"" << folder << "\"\n   reason: " << e.what() << endl; }

	if(includeUserDir)
	{
		const string userFolder = GameLogic::USER_CONFIG_FOLDER + '/' + folder;
		if(isFilenameDirectory(userFolder))
			searchFiles(possibleFilenames, userFolder, extension, false);
	}
}

/// returns a list of files inside the given folder and inside its subfolders (but not recursively)
static inline vector<string> searchFiles(const string& folder, const string& extension, bool includeUserDir)
{
	vector<string> v; searchFiles(v, folder, extension, includeUserDir); return v;
}

void GameLogic::loadPresetEngineSoundProfiles()
{
	cout << endl << "reading preset engine sound profiles..." << endl;
	vector<string> pendingPresetFiles;
	const vector<string> presetFiles(searchFiles(GameLogic::PRESET_ENGINE_SOUND_PROFILES_FOLDER, ".properties", true));
	const_foreach(const string&, filename, vector<string>, presetFiles)
	{
		Properties prop;
		prop.load(filename);

		const string filenameWithoutPath = filename.substr(filename.find_last_of("/\\")+1),
					 filenameWithoutExtension = filenameWithoutPath.substr(0, filenameWithoutPath.find_last_of(".")),
					 presetName = filenameWithoutExtension;

		if(prop.containsKey("sound") and not prop.get("sound").empty())
		{
			if(prop.get("sound") == "custom") try
			{
				presetEngineSoundProfiles[presetName] = createEngineSoundProfileFromFile(filename);
				cout << "read \"" << presetName << "\" from file \"" << filename << "\"" << endl;
			}
			catch(const std::exception& e)
			{
				cout << "failed to read \"" << filename << "\"\n   reason: " << e.what() << endl;
			}
			else
			{
				pendingPresetFiles.push_back(filename);
				cout << "found alias \"" << presetName << "\" in file \"" << filename << "\"" << endl;
			}
		}
	}

	cout << "resolving preset engine sound profile aliases..." << endl;
	unsigned previousCount = pendingPresetFiles.size();
	while(not pendingPresetFiles.empty())
	{
		for(unsigned i = 0; i < pendingPresetFiles.size(); i++)
		{
			string filename = pendingPresetFiles[i];
			Properties prop;
			prop.load(filename);

			const string
				basePresetName = prop.get("sound"),
				filenameWithoutPath = filename.substr(filename.find_last_of("/\\")+1),
				filenameWithoutExtension = filenameWithoutPath.substr(0, filenameWithoutPath.find_last_of(".")),
				presetName = filenameWithoutExtension;

			if(presetEngineSoundProfiles.find(basePresetName) != presetEngineSoundProfiles.end())
			{
				presetEngineSoundProfiles[presetName] = presetEngineSoundProfiles[basePresetName];
				cout << "copied profile \"" << presetName << "\" from \"" << basePresetName << "\"" << endl;
				pendingPresetFiles.erase(pendingPresetFiles.begin() + i);
				i--;
			}
		}
		if(pendingPresetFiles.size() == previousCount)
		{
			cout << "circular dependency or unresolved reference detected when loading preset engine sound profiles. skipping resolution." << endl;
			cout << "the following preset engine sound profiles could not be loaded: " << endl;
			for(unsigned i = 0; i < pendingPresetFiles.size(); i++)
				cout << pendingPresetFiles[i] << endl;
			break;
		}
		else previousCount = pendingPresetFiles.size();
	}
}

void GameLogic::loadCourses()
{
	cout << endl << "reading courses specs..." << endl;
	const vector<string> courseFiles(searchFiles(GameLogic::COURSES_FOLDER, ".properties", true));
	const_foreach(const string&, filename, vector<string>, courseFiles)
	{
		courses.resize(courses.size()+1);
		try {
			courses.back().loadFromFile(filename, false);
			cout << "read \"" << filename << "\"" << endl;
		}
		catch(const std::exception& e)
		{
			cout << "failed to read \"" << filename << "\"\n   reason: " << e.what() << endl;
			courses.resize(courses.size()-1);
		}
	}
}

void GameLogic::loadVehicles()
{
	cout << endl << "reading vehicles specs..." << endl;
	const vector<string> vehiclesFilenames(searchFiles(GameLogic::VEHICLES_FOLDER, ".properties", true));
	const_foreach(const string&, filename, vector<string>, vehiclesFilenames)
	{
		Properties prop;
		prop.load(filename);
		if(prop.containsKey("definition") and prop.get("definition") == "vehicle")
		{
			vehicles.resize(vehicles.size()+1);
			try {
				vehicles.back().loadFromFile(filename);
				cout << "read \"" << filename << "\"" << endl;
			}
			catch(const std::exception& e)
			{
				cout << "failed to read \"" << filename << "\"\n   reason: " << e.what() << endl;
				vehicles.resize(vehicles.size()-1);
			}
		}
	}
}

void GameLogic::loadTrafficVehicles()
{
	cout << endl << "reading traffic vehicles specs..." << endl;
	const vector<string> trafficVehiclesFilenames(searchFiles(GameLogic::TRAFFIC_FOLDER, ".properties", true));
	const_foreach(const string&, filename, vector<string>, trafficVehiclesFilenames)
	{
		Properties prop;
		prop.load(filename);
		if(prop.containsKey("definition") and prop.get("definition") == "vehicle")
		{
			trafficVehicles.resize(trafficVehicles.size()+1);
			try {
				trafficVehicles.back().loadFromFile(filename);
				trafficVehicles.back().soundProfile = EngineSoundProfile();  // force no sound for traffic FIXME remove this line and deal with engine sound sharing properly between traffic vehicles
				cout << "read \"" << filename << "\"" << endl;
			}
			catch(const std::exception& e)
			{
				cout << "failed to read \"" << filename << "\"\n   reason: " << e.what() << endl;
				trafficVehicles.resize(trafficVehicles.size()-1);
			}
		}
	}
}

void GameLogic::loadPresetSurfaceTypes()
{
	cout << "\nreading default surface types...\n";
	Properties prop;
	prop.load("data/default_surfaces.properties");

	const float count = prop.getParsedCStr<int, atoi>("surface_types");
	for(unsigned i = 0; i <= count; i++)
	{
		const string prefix = string("surface_type") + i;
		if(prop.containsKey(prefix))
		{
			presetSurfaceTypes[prop.get(prefix)].rollingResistance = prop.getParsedCStr<double, atof>(prefix + "_rolling_resistance");
			presetSurfaceTypes[prop.get(prefix)].frictionCoefficient = prop.getParsedCStr<double, atof>(prefix + "_friction_coefficient");
			cout << "read surface type " << prop.get(prefix) << '\n';
		}
	}

	if(prop.containsKey("default_surface_type") and existSurfaceType(prop.get("default_surface_type")))
		presetSurfaceTypes["default"] = presetSurfaceTypes[prop.get("default_surface_type")];
	else
		presetSurfaceTypes["default"] = presetSurfaceTypes.begin()->second;

	if(prop.containsKey("default_offroad_surface_type") and existSurfaceType(prop.get("default_offroad_surface_type")))
		presetSurfaceTypes["default_offroad"] = presetSurfaceTypes[prop.get("default_offroad_surface_type")];
	else
		presetSurfaceTypes["default_offroad"] = presetSurfaceTypes.begin()->second;
}

void GameLogic::loadPresetCourseStyles()
{
	cout << endl << "reading preset course styles..." << endl;
	vector<string> pendingLandscapePresetFiles;
	const vector<string> landscapePresetFiles(searchFiles(GameLogic::PRESET_COURSE_LANDSCAPE_STYLES_FOLDER, ".properties", true));
	const_foreach(const string&, filename, vector<string>, landscapePresetFiles)
	{
		Properties prop;
		prop.load(filename);

		const string filenameWithoutPath = filename.substr(filename.find_last_of("/\\")+1),
		             filenameWithoutExtension = filenameWithoutPath.substr(0, filenameWithoutPath.find_last_of(".")),
		             presetName = filenameWithoutExtension;

		if(prop.containsKey("preset_landscape_style") and not prop.get("preset_landscape_style").empty())
		{
			if(prop.get("preset_landscape_style") == "custom") try
			{
				presetLandscapeStyles[presetName].loadLandscapeStyleFromFile(filename);
				presetLandscapeStyles[presetName].presetLandscapeStyleName = presetName;
				cout << "read \"" << presetName << "\" from file \"" << filename << "\"" << endl;
			}
			catch(const std::exception& e)
			{
				cout << "failed to read \"" << filename << "\"\n   reason: " << e.what() << endl;
			}
			else
			{
				pendingLandscapePresetFiles.push_back(filename);
				cout << "found alias \"" << presetName << "\" in file \"" << filename << "\"" << endl;
			}
		}
	}

	vector<string> pendingRoadStylePresetFiles;
	const vector<string> roadStylePresetFiles(searchFiles(GameLogic::PRESET_COURSE_ROAD_STYLES_FOLDER, ".properties", true));
	const_foreach(const string&, filename, vector<string>, roadStylePresetFiles)
	{
		Properties prop;
		prop.load(filename);

		const string filenameWithoutPath = filename.substr(filename.find_last_of("/\\")+1),
		             filenameWithoutExtension = filenameWithoutPath.substr(0, filenameWithoutPath.find_last_of(".")),
		             presetName = filenameWithoutExtension;

		if(prop.containsKey("preset_road_style") and not prop.get("preset_road_style").empty())
		{
			if(prop.get("preset_road_style") == "custom") try
			{
				presetRoadStyles[presetName].loadRoadStyleFromFile(filename);
				presetRoadStyles[presetName].presetRoadStyleName = presetName;
				cout << "read \"" << presetName << "\" from file \"" << filename << "\"" << endl;
			}
			catch(const std::exception& e)
			{
				cout << "failed to read \"" << filename << "\"\n   reason: " << e.what() << endl;
			}
			else
			{
				pendingRoadStylePresetFiles.push_back(filename);
				cout << "found alias \"" << presetName << "\" in file \"" << filename << "\"" << endl;
			}

		}
	}

	unsigned previousCount = pendingRoadStylePresetFiles.size();
	while(not pendingRoadStylePresetFiles.empty())
	{
		for(unsigned i = 0; i < pendingRoadStylePresetFiles.size(); i++)
		{
			string filename = pendingRoadStylePresetFiles[i];
			Properties prop;
			prop.load(filename);

			const string
				basePresetName = prop.get("preset_road_style"),
				filenameWithoutPath = filename.substr(filename.find_last_of("/\\")+1),
				filenameWithoutExtension = filenameWithoutPath.substr(0, filenameWithoutPath.find_last_of(".")),
				presetName = filenameWithoutExtension;

			if(presetRoadStyles.find(basePresetName) != presetRoadStyles.end())
			{
				presetRoadStyles[presetName] = presetRoadStyles[basePresetName];
				cout << "copied road style profile \"" << presetName << "\" from \"" << basePresetName << "\"" << endl;
				pendingRoadStylePresetFiles.erase(pendingRoadStylePresetFiles.begin() + i);
				i--;
			}
		}
		if(pendingRoadStylePresetFiles.size() == previousCount)
		{
			cout << "circular dependency or unresolved reference detected when loading preset road style. skipping resolution." << endl;
			cout << "the following preset road style could not be loaded: " << endl;
			for(unsigned i = 0; i < pendingRoadStylePresetFiles.size(); i++)
				cout << pendingRoadStylePresetFiles[i] << endl;
			break;
		}
		else previousCount = pendingRoadStylePresetFiles.size();
	}
	previousCount = pendingLandscapePresetFiles.size();
	while(not pendingLandscapePresetFiles.empty())
	{
		for(unsigned i = 0; i < pendingLandscapePresetFiles.size(); i++)
		{
			string filename = pendingLandscapePresetFiles[i];
			Properties prop;
			prop.load(filename);

			const string
				basePresetName = prop.get("preset_landscape_style"),
				filenameWithoutPath = filename.substr(filename.find_last_of("/\\")+1),
				filenameWithoutExtension = filenameWithoutPath.substr(0, filenameWithoutPath.find_last_of(".")),
				presetName = filenameWithoutExtension;

			if(presetLandscapeStyles.find(basePresetName) != presetLandscapeStyles.end())
			{
				presetLandscapeStyles[presetName] = presetLandscapeStyles[basePresetName];
				cout << "copied landscape profile \"" << presetName << "\" from \"" << basePresetName << "\"" << endl;
				pendingLandscapePresetFiles.erase(pendingLandscapePresetFiles.begin() + i);
				i--;
			}
		}
		if(pendingLandscapePresetFiles.size() == previousCount)
		{
			cout << "circular dependency or unresolved reference detected when loading preset landscape style. skipping resolution." << endl;
			cout << "the following preset landscape style could not be loaded: " << endl;
			for(unsigned i = 0; i < pendingLandscapePresetFiles.size(); i++)
				cout << pendingLandscapePresetFiles[i] << endl;
			break;
		}
		else previousCount = pendingLandscapePresetFiles.size();
	}
}
