/*
 * game_logic.hpp
 *
 *  Created on: 1 de jul de 2019
 *      Author: carlos.faruolo
 */

#ifndef GAME_LOGIC_HPP_
#define GAME_LOGIC_HPP_
#include <ciso646>

#include "gamestates/pseudo3d_race_state.hpp"

#include "course.hpp"
#include "vehicle.hpp"

#include "engine_sound.hpp"
#include "vehicle_mechanics.hpp"

#include "fgeal/fgeal.hpp"

#include "futil/language.hpp"
#include "futil/properties.hpp"

#include <map>
#include <vector>

// fwd. declared
class Carse;

/** Class to wrap together all between-states game logic. */
class GameLogic
{
	friend class Carse;
	static GameLogic* instance;

	std::map<std::string, EngineSoundProfile> presetEngineSoundProfiles;
	std::vector<Pseudo3DCourse::Spec> courses;
	std::vector<Pseudo3DVehicle::Spec> vehicles, trafficVehicles;
	std::map<std::string, Pseudo3DCourse::Spec> presetLandscapeStyles;
	std::map<std::string, Pseudo3DCourse::Spec> presetRoadStyles;
	std::vector<std::string> musicFilenames, vehicleCategories, dashboardConfigFilenames;

	struct SurfaceType { float rollingResistance, frictionCoefficient; };
	std::map<std::string, SurfaceType> presetSurfaceTypes;

	futil::Properties fontsInfo;

	GameLogic();

	// intended to run on startup
	void initialize();

	// intended to run on startup, after initializing all states
	void onStatesListInitFinished();

	// intended to run on startup, loads all engine sound presets in assets/sound/engine/
	void loadPresetEngineSoundProfiles();

	// intended to run on startup, loads all courses in the data/courses folder
	void loadCourses();

	// intended to run on startup, loads all vehicles in the data/vehicles folder
	void loadVehicles();

	// intended to run on startup, loads all traffic vehicles in the data/traffic folder
	void loadTrafficVehicles();

	// intended to run on startup, loads all default surface types (templated) in data/default_surfaces.properties
	void loadPresetSurfaceTypes();

	// intended to run on startup, loads all course style presets in data/courses/styles/
	void loadPresetCourseStyles();

	// Creates a hardcoded debug course and sets as the next one
	void setNextCourseDebug();

	public:
	bool raceOnlyMode;

	float masterVolume;

	enum PowerUnit { UI_UNIT_POWER_HP, UI_UNIT_POWER_PS, UI_UNIT_POWER_KW, UI_UNIT_POWER_COUNT };
	enum TorqueUnit { UI_UNIT_TORQUE_LBFT, UI_UNIT_TORQUE_KGFM, UI_UNIT_TORQUE_NM, UI_UNIT_TORQUE_COUNT };
	enum EngineDisplacementUnit { UI_UNIT_ENGDISP_LITRE, UI_UNIT_ENGDISP_CC, UI_UNIT_ENGDISP_CUIN, UI_UNIT_ENGDISP_CID, UI_UNIT_ENGDISP_COUNT };
	struct UnitSet
	{
		PowerUnit power;
		TorqueUnit torque;
		EngineDisplacementUnit engineDisplacement;
		bool isImperialWeight, isImperialDistance;
	}
	uiUnits;

	int currentMainMenuStateId;

	struct NextMatchParameters  // parameters for next match
	{
		bool isRandomCourseSpec, isDebugCourseSpec;
		Pseudo3DCourse::Spec courseSpec;
		Pseudo3DRaceState::Parameters race;
		std::string dashboardLayoutFilename;

		struct PlayerParameters
		{
			Pseudo3DVehicle::Spec vehicleSpec;
			int alternateSpriteIndex;
			bool enableAutomaticShifting;
		};
		std::vector<PlayerParameters> players;

		NextMatchParameters();
	}
	nextMatch;

	// special locations
	const static std::string
		USER_CONFIG_FOLDER,
		COURSES_FOLDER,
		VEHICLES_FOLDER,
		TRAFFIC_FOLDER,
		DASHBOARDS_FOLDER,
		MUSIC_FOLDER,
		PROP_ASSETS_FOLDER,
		LANDSCAPE_BG_FOLDER,
		PRESET_ENGINE_SOUND_PROFILES_FOLDER,
		PRESET_COURSE_LANDSCAPE_STYLES_FOLDER,
		PRESET_COURSE_ROAD_STYLES_FOLDER;

	// shared physics-related constants
	const static float
		PHYSICS_GRAVITY_ACCELERATION_DEFAULT,
		PHYSICS_AIR_DENSITY_DEFAULT,
		PHYSICS_MPS_TO_MPH,
		PHYSICS_MPS_TO_KPH,
		PHYSICS_HP_TO_KW,
		PHYSICS_HP_TO_PS,
		PHYSICS_NM_TO_LBFT,
		PHYSICS_NM_TO_KGFM,
		PHYSICS_MAXIMUM_DELTA_TIME,
		PHYSICS_BURNOUT_SLIP_RATIO,
		PHYSICS_PEDAL_POSITION_SPEED;

	inline static GameLogic& getInstance()
	{
		if(instance == null)
			instance = new GameLogic();

		return *instance;
	}

	static void passJoystickAxisToKeyboardArrows(fgeal::Game::State* const state, unsigned axis, float value);

	static void passJoystickButtonsToEnterReturnKeys(fgeal::Game::State* const state, unsigned button);

	/* Returns a font path specified in fonts.properties */
	std::string getFontPath(const std::string& id);

	/* Returns a font size specified in fonts.properties */
	unsigned getFontSize(const std::string& id, const std::string& modifier=std::string());

	#define gameFontParams(id)              game.logic.getFontPath(id), game.logic.getFontSize(id)
	#define gameFontParamsMod(id, modifier) game.logic.getFontPath(id), game.logic.getFontSize(id, modifier)

	/* Creates a engine sound profile by loading and parsing the data in the given filename. */
	static EngineSoundProfile createEngineSoundProfileFromFile(const std::string& filename);

	// Returns the correct preset according to the specified preset-name from one of the built-in presets; if the given preset name is not present on the map, returns the default preset
	const EngineSoundProfile& getPresetEngineSoundProfile(const std::string& presetName) const;

	// Returns the correct preset according to the specified preset-name from one of the built-in presets; if the given preset name is not present on the map, returns the default preset
	const Pseudo3DCourse::Spec& getPresetRoadStyle(const std::string& presetName) const;

	// Returns a random preset from one of the built-in presets
	const Pseudo3DCourse::Spec& getRandomPresetRoadStyle() const;

	// Returns a list of available preset styles names
	std::vector<std::string> getPresetRoadStylesNames() const;

	// Returns the correct preset according to the specified preset-name from one of the built-in presets; if the given preset name is not present on the map, returns the default preset
	const Pseudo3DCourse::Spec& getPresetLandscapeStyle(const std::string& presetName) const;

	// Returns a random preset from one of the built-in presets
	const Pseudo3DCourse::Spec& getRandomPresetLandscapeStyle() const;

	// Returns a list of available preset styles names
	std::vector<std::string> getPresetLandscapeStylesNames() const;

	bool existSurfaceType(const std::string& surfaceName);
	float getSurfaceTypeRollingResistance(const std::string& surfaceName);
	float getSurfaceTypeFrictionCoefficient(const std::string& surfaceName);

	// Returns a list of eligible vehicle specs for cpu opponent vehicles
	std::vector<const Pseudo3DVehicle::Spec*> getEligibleCpuOpponentVehicleSpecs() const;

	// Clear the course list and reloads it from files
	void updateCourseList();

	// Clear the vehicle list and reloads it from files
	void updateVehicleList();

	inline const std::vector<Pseudo3DCourse::Spec>& getCourseList() { return courses; }

	inline const std::vector<Pseudo3DVehicle::Spec>& getVehicleList() { return vehicles; }

	inline const std::vector<Pseudo3DVehicle::Spec>& getTrafficVehicleList() { return trafficVehicles; }

	inline const std::vector<std::string>& getDashboardLayouts() { return dashboardConfigFilenames; }

	inline const std::vector<std::string>& getMusicFilenames() { return musicFilenames; }

	inline const std::vector<std::string>& getRegisteredVehicleCategories() { return vehicleCategories; }
};

#endif /* GAME_LOGIC_HPP_ */
