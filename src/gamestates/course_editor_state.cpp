/*
 * course_editor_state.cpp
 *
 *  Created on: 10 de jul de 2018
 *      Author: carlosfaruolo
 */

#include "course_editor_state.hpp"

#include "carse.hpp"

#include "util/sizing.hpp"

#include "futil/string_extra_operators.hpp"
#include "futil/string_actions.hpp"
#include "futil/string_parse.hpp"
#include "futil/random.h"
#include "futil/collection_actions.hpp"
#include "futil/math_constants.h"
#include "futil/snprintf.h"
#include "futil/round.h"

#include <cstdio>
#include <algorithm>
#include <cassert>

using fgeal::Vector2D;
using fgeal::Point;
using fgeal::Rectangle;
using fgeal::Color;
using fgeal::Graphics;
using fgeal::Display;
using fgeal::Image;
using fgeal::Font;
using fgeal::Keyboard;
using fgeal::Mouse;
using fgeal::Button;
using fgeal::Label;
using fgeal::Joystick;
using std::vector;
using std::string;
using futil::ends_with;
using futil::Properties;
using futil::random_between_decimal;

// these guys help giving semantics to menu indexes.
enum SettingsMenuIndex
{
	MENU_NONE,
	MENU_NAME,
	MENU_LANE_COUNT,
	MENU_ROAD_WIDTH,
	MENU_ROAD_STYLE,
	MENU_LANDSCAPE,
	MENU_WEATHER,
	MENU_COUNT
};
enum GenerateMenuIndex
{
	GEN_MENU_SEGMENTS,
	GEN_MENU_CURVENESS,
	GEN_MENU_RANDOM_LANDSCAPE,
	GEN_MENU_RANDOM_ROADSTYLE,
	GEN_MENU_RANDOM_PROPS,
	GEN_MENU_COUNT
};

int CourseEditorState::getId() { return Carse::COURSE_EDITOR_STATE_ID; }

CourseEditorState::CourseEditorState(Carse* game)
: State(*game), game(*game), lastDisplaySize(), focus(),
  font(null), fontSmall(null), fontTitle(null), fontButton(null),
  sndCursorMove(null), sndCursorIn(null), sndCursorOut(null), imgPointer(null),
  courseSegmentCursor(), previewCameraPosition(), lastAddedAmount(), lastAddedPosition(),
  generationParameters(), propGenerationAction(), randomizeRoadStyle(), randomizeLandscapeStyle(),
  isCoursePreviewVisible(),
  courseStyleBackupSpec()
{}

CourseEditorState::~CourseEditorState()
{
	if(font != null) delete font;
	if(fontSmall != null) delete fontSmall;
	if(fontTitle != null) delete fontTitle;
	if(fontButton != null) delete fontButton;
	if(imgPointer != null) delete imgPointer;
	foreach(PresetButton&, btn, vector<PresetButton>, presetButtons) if(btn.icon != null) delete btn.icon;
}

void CourseEditorState::initialize()
{
	font = new Font(gameFontParams("ui_course_editor"));
	fontSmall = new Font(gameFontParams("ui_course_editor"));
	fontTitle = new Font(gameFontParams("ui_course_editor_title"));
	fontButton = new Font(gameFontParams("ui_button"));
	imgPointer = new Image("assets/ui/pointer.png");

	mainTitle.text.setFont(fontTitle);
	mainTitle.text.setContent("Course Editor");
	mainTitle.text.setColor(Color::WHITE);
	mainTitle.shape = fgeal::PlainComponent::SHAPE_ROUNDED_RECTANGULAR;
	mainTitle.backgroundColor = Color::create(32, 32, 32);

	course.animation.drawDistance = 300;
	course.animation.cameraDepth = 0.84;
	course.animation.lengthScale = Pseudo3DCourse::COURSE_POSITION_FACTOR;
	course.animation.cameraBehavior = Pseudo3DCourseAnimation::CAMERA_BEHAVIOR_TYPE_B;

	map.plottedPositions.resize(2);
	map.plottedPositions[0].fillColor = Color::YELLOW;
	map.plottedPositions[0].borderEnabled = true;
	map.plottedPositions[0].borderColor = Color::BLACK;
	map.plottedPositions[0].shape = Pseudo3DCourseMap::PlottedPosition::SHAPE_BAR;
	map.plottedPositions[0].value = 0;
	map.plottedPositions[1].fillColor = Color::RED.getBrighter();
	map.plottedPositions[1].borderEnabled = true;
	map.plottedPositions[1].borderColor = Color::MAROON;
	map.plottedPositions[1].shape = Pseudo3DCourseMap::PlottedPosition::SHAPE_TRIANGLE;
	map.segmentHighlightingEnabled = true;
	map.segmentHighlightColor = Color::ORANGE;
	mapPanel.addComponent(map);

	mapVisibilityButton.shape = Button::SHAPE_ROUNDED_RECTANGULAR;
	mapVisibilityButton.backgroundColor = Color::RED;
	mapVisibilityButton.borderColor = Color::BLACK;
	mapVisibilityButton.borderThickness = 1.1;
	mapVisibilityButton.highlightColor = Color::RED;
	mapVisibilityButton.highlightPeriod = 0.15;
	mapVisibilityButton.text.setFont(fontButton);
	mapVisibilityButton.text.setContent("Map");
	mapVisibilityButton.text.setColor(Color::WHITE);

	coursePreviewVisibilityButton = mapVisibilityButton;
	coursePreviewVisibilityButton.text.setContent("Preview");
	coursePreviewVisibilityButton.backgroundColor = Color::MAROON;
	coursePreviewVisibilityButton.text.setColor(Color::GREY);

	toolsPanel.backgroundColor = Color::DARK_GREY;

	toolsTabbedPane.backgroundDisabled = toolsTabbedPane.borderDisabled = true;
	toolsTabbedPane.text.setFont(fontButton);
	toolsPanel.addComponent(toolsTabbedPane);

	presetsTabPanel.backgroundColor = Color::GREY;
	toolsTabbedPane.addTab(presetsTabPanel, "Presets");

	PresetButton presetButton;
	presetButton.shape = Button::SHAPE_SQUIRCULAR;
	presetButton.backgroundColor = Color::LIGHT_GREY;
	presetButton.borderColor = Color::DARK_GREY;
	presetButton.borderThickness = 1.1;
	presetButton.highlightColor = Color::RED;
	presetButton.highlightPeriod = 0.15;
	presetButton.text.setFont(fontButton);

	struct CustomSegmentGeneration { static unsigned function(CourseSpec& spec, unsigned position, unsigned segmentAmountToAdd, float totalAngle, float slopeHeightFactor, bool curvedSlope=false)
	{
		assert(spec.lines.empty() or position < spec.lines.size());  // just to be safe

		vector<CourseSpec::Segment> generatedSegments(segmentAmountToAdd);
		const CourseSpec::Segment borderSegment = spec.lines.empty()? CourseSpec::Segment() : spec.lines[position];
		const float curve = totalAngle == 0? 0 : spec.roadSegmentLength * totalAngle / segmentAmountToAdd;

		for(unsigned i = 0; i < segmentAmountToAdd; i++)
		{
			const CourseSpec::Segment& previousSegment = i > 0? generatedSegments[i-1] : borderSegment;
			CourseSpec::Segment& segment = generatedSegments[i];
			segment.curve = curve;
			segment.y = previousSegment.y;
			if(curvedSlope)
			{
				segment.y += M_PI_2 * sin(i * M_PI / segmentAmountToAdd) * slopeHeightFactor * spec.roadSegmentLength;

				if(i == segmentAmountToAdd - 1)  // last one added
					segment.y = futil::round(segment.y);
			}
			else segment.y += slopeHeightFactor * spec.roadSegmentLength;
		}

		const unsigned insertionIndex = spec.lines.empty()? 0 : position + 1;
		spec.lines.insert(spec.lines.begin() + insertionIndex, generatedSegments.begin(), generatedSegments.end());

		// rewrite all z positions correctly
		for(unsigned i = insertionIndex; i < spec.lines.size(); i++)
			spec.lines[i].z = i * spec.roadSegmentLength;

		return segmentAmountToAdd;
	}};

	// used to avoid error accumulation
	#define SIN10_APPROX 0.171875  // = (11/64) ~= sin(9.897)
	#define SIN15_APPROX 0.25      // =  (1/4)  ~= sin(14.48)  // gives simpler values
	#define SIN20_APPROX 0.34375   // = (11/32) ~= sin(20.11)
	#define SIN25_APPROX 0.421875  // = (27/64) ~= sin(24.95)
	#define SIN30        0.5       // =  (1/2)  == sin(30)  // gives the simplest values, but a little too steep

	#define SEGMENT_AMOUNT_SHORT 100
	#define SEGMENT_AMOUNT_LONG (3.0*SEGMENT_AMOUNT_SHORT)
	#define SEGMENT_AMOUNT_MINI (0.5*SEGMENT_AMOUNT_SHORT)

	#define PRESET_BTN(uniqueNameId, descr, iconFilename, amount, totalYawAngle, slopeHeightFactor, useCurvedSlope)\
		presetButton.description = descr;\
		presetButton.icon = new fgeal::Image("assets/ui/course-editor/" iconFilename);\
		struct Generator##uniqueNameId { static unsigned function(CourseSpec& spec, unsigned pos)\
		{ return CustomSegmentGeneration::function(spec, pos, amount, totalYawAngle, slopeHeightFactor, useCurvedSlope); }};\
		presetButton.addNewSegmentsCallback = Generator##uniqueNameId::function;\
		presetButtons.push_back(presetButton)
	{
		PRESET_BTN(0, "Mini straight", "cep-sm.png", SEGMENT_AMOUNT_MINI, 0, 0, false);
		PRESET_BTN(1, "Short straight", "cep-ss.png", SEGMENT_AMOUNT_SHORT, 0, 0, false);
		PRESET_BTN(2, "Long straight", "cep-sl.png", SEGMENT_AMOUNT_LONG, 0, 0, false);
		PRESET_BTN(3, "Mini 90-degree left curve", "cep-c90lm.png", SEGMENT_AMOUNT_MINI, -M_PI_2, 0, false);
		PRESET_BTN(4, "Short 90-degree left curve", "cep-c90ls.png", SEGMENT_AMOUNT_SHORT, -M_PI_2, 0, false);
		PRESET_BTN(5, "Long 90-degree left curve", "cep-c90ll.png", SEGMENT_AMOUNT_LONG, -M_PI_2, 0, false);
		PRESET_BTN(6, "Mini 90-degree right curve", "cep-c90rm.png", SEGMENT_AMOUNT_MINI, M_PI_2, 0, false);
		PRESET_BTN(7, "Short 90-degree right curve", "cep-c90rs.png", SEGMENT_AMOUNT_SHORT, M_PI_2, 0, false);
		PRESET_BTN(8, "Long 90-degree right curve", "cep-c90rl.png", SEGMENT_AMOUNT_LONG, M_PI_2, 0, false);
		PRESET_BTN(9, "Short 45-degree left curve", "cep-c45ls.png", SEGMENT_AMOUNT_SHORT, -M_PI_4, 0, false);
		PRESET_BTN(10, "Long 45-degree left curve", "cep-c45ll.png", SEGMENT_AMOUNT_LONG, -M_PI_4, 0, false);
		PRESET_BTN(11, "Short 15-degree slope up straight", "cep-s20us.png", SEGMENT_AMOUNT_SHORT, 0, SIN15_APPROX, false);
		PRESET_BTN(12, "Short 45-degree right curve", "cep-c45rs.png", SEGMENT_AMOUNT_SHORT, M_PI_4, 0, false);
		PRESET_BTN(13, "Long 45-degree right curve", "cep-c45rl.png", SEGMENT_AMOUNT_LONG, M_PI_4, 0, false);
		PRESET_BTN(14, "Short 15-degree slope down straight", "cep-s20ds.png", SEGMENT_AMOUNT_SHORT, 0, -SIN15_APPROX, false);
		PRESET_BTN(15, "Long 15-degree curved slope up 45-degree left curve", "cep-c45ll.png", SEGMENT_AMOUNT_LONG, -M_PI_4, SIN15_APPROX, true);
		PRESET_BTN(16, "Long 15-degree curved slope up 45-degree right curve", "cep-c45rl.png", SEGMENT_AMOUNT_LONG, M_PI_4, SIN15_APPROX, true);
		PRESET_BTN(17, "Long 15-degree curved slope up straight", "cep-s20us.png", SEGMENT_AMOUNT_LONG, 0, SIN15_APPROX, true);
		PRESET_BTN(18, "Long 15-degree curved slope down 45-degree left curve", "cep-c45ll.png", SEGMENT_AMOUNT_LONG, -M_PI_4, -SIN15_APPROX, true);
		PRESET_BTN(19, "Long 15-degree curved slope down 45-degree right curve", "cep-c45rl.png", SEGMENT_AMOUNT_LONG, M_PI_4, -SIN15_APPROX, true);
		PRESET_BTN(20, "Long 15-degree curved slope down straight", "cep-s20ds.png", SEGMENT_AMOUNT_LONG, 0, -SIN15_APPROX, true);
	}

	for(unsigned i = 0; i < presetButtons.size(); i++)
		presetsTabPanel.addHighlightableComponent(presetButtons[i]);

	propertiesTabPanel.backgroundColor = Color::GREY;
	toolsTabbedPane.addTab(propertiesTabPanel, "Config");

	propertiesMenu.setFont(font);
	propertiesMenu.setColor(Color::BLACK);
	propertiesMenu.backgroundColor = Color::_TRANSPARENT;
	propertiesMenu.borderColor = Color::_TRANSPARENT;
	propertiesMenu.focusedEntryBgColor = propertiesTabPanel.backgroundColor;
	propertiesMenu.focusedEntryFontColor = Color::BLACK;
	propertiesMenu.addEntry("_________________");
	propertiesMenu.addEntry("Change name");
	propertiesMenu.addEntry("Lane count");
	propertiesMenu.addEntry("Road width");
	propertiesMenu.addEntry("Road style");
	propertiesMenu.addEntry("Landscape");
	propertiesMenu.addEntry("Weather");
	propertiesTabPanel.addComponent(propertiesMenu);

	newButton.shape = Button::SHAPE_RECTANGULAR;
	newButton.backgroundColor = Color::GREY;
	newButton.borderDisabled = true;
	newButton.borderColor = Color::DARK_GREY;
	newButton.borderThickness = 1.1;
	newButton.highlightColor = Color::RED;
	newButton.highlightPeriod = 0.15;
	newButton.text.setFont(fontButton);
	newButton.text.setContent("New");
	toolsPanel.addHighlightableComponent(newButton);

	loadButton = newButton;
	loadButton.text.setContent("Load");
	toolsPanel.addHighlightableComponent(loadButton);

	saveButton = newButton;
	saveButton.text.setContent("Save");
	toolsPanel.addHighlightableComponent(saveButton);

	undoButton = newButton;
	undoButton.text.setContent("Undo");
	toolsPanel.addHighlightableComponent(undoButton);

	eraseButton = newButton;
	eraseButton.text.setContent("Erase");
	toolsPanel.addHighlightableComponent(eraseButton);

	exitButton = newButton;
	exitButton.text.setContent("Exit");
	toolsPanel.addHighlightableComponent(exitButton);

	generateButton = newButton;
	generateButton.text.setContent("Generate");
	toolsPanel.addHighlightableComponent(generateButton);

	infoButton = newButton;
	infoButton.text.setContent("Info");
	toolsPanel.addHighlightableComponent(infoButton);

	loadDialog.shape = Label::SHAPE_CONST_ROUNDED_RECTANGULAR;
	loadDialog.backgroundColor = Color::GREY;
	loadDialog.borderColor = Color::DARK_GREY;
	loadDialog.borderThickness = 1.1;
	loadDialog.borderDisabled = false;

	deleteDialog = textInputDialog = inputDialog = menuInputDialog = loadDialog;

	loadDialogLabel.text.setFont(font);
	loadDialogLabel.text.setColor(Color::WHITE);
	loadDialogLabel.textHorizontalAlignment = Label::ALIGN_LEADING;
	loadDialogLabel.text.setContent("Select course file:");
	loadDialog.addComponent(loadDialogLabel);

	fileMenu.setFont(font);
	fileMenu.setColor(Color::GREEN);
	loadDialog.addComponent(fileMenu);

	loadDialogSelectButton = newButton;
	loadDialogSelectButton.backgroundColor = Color::LIGHT_GREY;
	loadDialogSelectButton.text.setContent("Select");
	loadDialog.addHighlightableComponent(loadDialogSelectButton);

	loadDialogCancelButton = loadDialogSelectButton;
	loadDialogCancelButton.text.setContent("Cancel");
	loadDialog.addHighlightableComponent(loadDialogCancelButton);

	loadDialogEraseButton = loadDialogSelectButton;
	loadDialogEraseButton.text.setContent("Delete");
	loadDialogEraseButton.text.setColor(Color::MAROON);
	loadDialog.addHighlightableComponent(loadDialogEraseButton);

	loadDialogUpButton.shape = Button::SHAPE_SQUIRCULAR;
	loadDialogUpButton.backgroundColor = Color::LIGHT_GREY;
	loadDialogUpButton.borderDisabled = true;
	loadDialogUpButton.highlightColor = Color::WHITE;
	loadDialogUpButton.arrowColor = Color::WHITE;
	loadDialogUpButton.arrowOrientation = fgeal::ArrowIconButton::ARROW_UP;
	loadDialog.addHighlightableComponent(loadDialogUpButton);

	loadDialogDownButton = loadDialogUpButton;
	loadDialogDownButton.arrowOrientation = fgeal::ArrowIconButton::ARROW_DOWN;
	loadDialog.addHighlightableComponent(loadDialogDownButton);

	deleteDialog.borderColor = Color::RED;

	deleteDialogLabel.text.setFont(font);
	deleteDialogLabel.text.setContent("Are you sure you want to delete the course file?");
	deleteDialogLabel.text.setColor(~Color::SALMON);
	deleteDialog.addComponent(deleteDialogLabel);

	deleteDialogConfirmButton = loadDialogSelectButton;
	deleteDialogConfirmButton.text.setContent("Yes");
	deleteDialog.addHighlightableComponent(deleteDialogConfirmButton);

	deleteDialogCancelButton = loadDialogSelectButton;
	deleteDialogCancelButton.text.setContent("No");
	deleteDialog.addHighlightableComponent(deleteDialogCancelButton);

	textInputDialogLabel = loadDialogLabel;
	textInputDialog.addComponent(textInputDialogLabel);

	textInputDialogTextField.text.setFont(fontSmall);
	textInputDialogTextField.backgroundColor = Color::BLACK;
	textInputDialogTextField.backgroundColor.a = 127;
	textInputDialogTextField.text.setColor(Color::WHITE);
	textInputDialogTextField.borderDisabled = true;
	textInputDialogTextField.borderColor = Color::CREAM;
	textInputDialogTextField.overflowBehavior = fgeal::TextField::OVERFLOW_ABBREVIATE_TRAILING;
	textInputDialog.addComponent(textInputDialogTextField);

	textInputDialogConfirmButton = loadDialogSelectButton;
	textInputDialogConfirmButton.text.setContent("Apply");
	textInputDialog.addHighlightableComponent(textInputDialogConfirmButton);

	textInputDialogCancelButton = loadDialogCancelButton;
	textInputDialog.addHighlightableComponent(textInputDialogCancelButton);

	inputDialogLabel = textInputDialogLabel;
	inputDialog.addComponent(inputDialogLabel);

	inputDialogTextField = textInputDialogTextField;
	inputDialog.addComponent(inputDialogTextField);

	inputDialogConfirmButton = textInputDialogConfirmButton;
	inputDialog.addHighlightableComponent(inputDialogConfirmButton);

	inputDialogCancelButton = textInputDialogCancelButton;
	inputDialog.addHighlightableComponent(inputDialogCancelButton);

	inputDialogIncButton = loadDialogSelectButton;
	inputDialogIncButton.text.setContent("+");
	inputDialog.addHighlightableComponent(inputDialogIncButton);

	inputDialogDecButton = loadDialogSelectButton;
	inputDialogDecButton.text.setContent("-");
	inputDialog.addHighlightableComponent(inputDialogDecButton);

	menuInputDialogLabel = inputDialogLabel;
	menuInputDialog.addComponent(menuInputDialogLabel);

	menuInput.setFont(fontSmall);
	menuInput.setColor(Color::LIGHT_GREY);
	menuInput.title.setContent("");
	menuInputDialog.addComponent(menuInput);

	menuInputDialogConfirmButton = inputDialogConfirmButton;
	menuInputDialog.addHighlightableComponent(menuInputDialogConfirmButton);

	menuInputDialogCancelButton = inputDialogCancelButton;
	menuInputDialog.addHighlightableComponent(menuInputDialogCancelButton);

	menuInputDialogUpButton = loadDialogUpButton;
	menuInputDialogUpButton.arrowOrientation = fgeal::ArrowIconButton::ARROW_UP;
	menuInputDialog.addHighlightableComponent(menuInputDialogUpButton);

	menuInputDialogDownButton = menuInputDialogUpButton;
	menuInputDialogDownButton.arrowOrientation = fgeal::ArrowIconButton::ARROW_DOWN;
	menuInputDialog.addHighlightableComponent(menuInputDialogDownButton);

	viewToolbar.backgroundColor = Color::GREY.getTransparent(192);

	scaleIndicatorLabel.text.setFont(fontSmall);
	scaleIndicatorLabel.text.setColor(Color::WHITE.getTransparent());
	scaleIndicatorLabel.textHorizontalAlignment = Label::ALIGN_LEADING;
	viewToolbar.addComponent(scaleIndicatorLabel);

	zoomUpButton.shape = Button::SHAPE_RECTANGULAR;
	zoomUpButton.backgroundColor = Color::LIGHT_GREY;
	zoomUpButton.borderColor = Color::DARK_GREY;
	zoomUpButton.borderThickness = 1.1;
	zoomUpButton.highlightColor = Color::RED;
	zoomUpButton.highlightPeriod = 0.15;
	zoomUpButton.text.setFont(fontSmall);
	zoomUpButton.text.setContent("+");
	viewToolbar.addHighlightableComponent(zoomUpButton);

	zoomDownButton = zoomUpButton;
	zoomDownButton.text.setContent("-");
	viewToolbar.addHighlightableComponent(zoomDownButton);

	zoomResetButton = zoomDownButton;
	zoomResetButton.text.setContent("1");
	viewToolbar.addHighlightableComponent(zoomResetButton);

	viewToolbar.addComponent(scaleIndicatorLabel);

	statusBar.backgroundColor = Color::create(64, 64, 64);
	statusBar.text.setFont(fontSmall);
	statusBar.text.setColor(Color::WHITE);
	statusBar.textHorizontalAlignment = Label::ALIGN_LEADING;

	messageDialog.shape = Label::SHAPE_CONST_ROUNDED_RECTANGULAR;
	messageDialog.backgroundColor = Color::GREY;
	messageDialog.borderThickness = 1;
	messageDialog.borderDisabled = false;
	messageDialog.borderColor = Color::BLACK;

	messageDialogTitle.text.setFont(font);
	messageDialogTitle.text.setContent("An error has ocurred");
	messageDialogTitle.textHorizontalAlignment = Label::ALIGN_LEADING;
	messageDialog.addComponent(messageDialogTitle);

	messageDialogLabel.text.setFont(fontSmall);
	messageDialogLabel.text.setColor(Color::RED);
	messageDialogLabel.textHorizontalAlignment = Label::ALIGN_LEADING;
	messageDialog.addComponent(messageDialogLabel);

	messageDialogLabel2 = messageDialogLabel;
	messageDialogLabel2.text.setColor(Color::BLACK);
	messageDialog.addComponent(messageDialogLabel2);

	messageDialogConfirmButton = loadDialogSelectButton;
	messageDialogConfirmButton.text.setContent("Ok");
	messageDialog.addHighlightableComponent(messageDialogConfirmButton);

	// loan some shared resources
	sndCursorMove = &game.sharedResources->sndCursorMove;
	sndCursorIn   = &game.sharedResources->sndCursorIn;
	sndCursorOut  = &game.sharedResources->sndCursorOut;
}

void CourseEditorState::onEnter()
{
	Display& display = game.getDisplay();
	const unsigned dw = display.getWidth(), dh = display.getHeight(), vdw = dh * 4/3.f;
	const float widgetSpacing = 0.02*dh;

	// reload fonts if display size changed
	if(lastDisplaySize.x != dw or lastDisplaySize.y != dh)
	{
		FontSizer fs(dh);
		font->setSize(fs(game.logic.getFontSize("ui_course_editor")));
		fontSmall->setSize(fs(game.logic.getFontSize("ui_course_editor", "small")));
		fontTitle->setSize(fs(game.logic.getFontSize("ui_course_editor_title")));
		fontButton->setSize(fs(game.logic.getFontSize("ui_button", "small")));
		lastDisplaySize.x = dw;
		lastDisplaySize.y = dh;
	}

	reloadFileList();

	// reset geometry

	toolsPanel.bounds.w = 0.25*vdw;

	mapPanel.bounds.x = 0;
	mapPanel.bounds.y = 0;
	mapPanel.bounds.w = dw - toolsPanel.bounds.w;
	mapPanel.bounds.h = 0.97*dh;

	map.bounds = mapPanel.bounds;
	map.plottedPositions[1].size = (map.plottedPositions[0].size = dh/150.f)/2;
	map.plottedPositions[1].minimumScaledSize = map.plottedPositions[0].minimumScaledSize = dh/90.f;

	courseViewBounds = mapPanel.bounds;
	course.animation.drawAreaWidth = courseViewBounds.w;
	course.animation.drawAreaHeight = courseViewBounds.h;
	previewCameraPosition = courseSegmentCursor = 0;

	coursePreviewVisibilityButton.padding.x = coursePreviewVisibilityButton.padding.y = widgetSpacing/3;
	coursePreviewVisibilityButton.pack();
	mapVisibilityButton.bounds = coursePreviewVisibilityButton.bounds;

	mapVisibilityButton.bounds.x = 0.01*dw;
	mapVisibilityButton.bounds.y = 0.01*dh;

	coursePreviewVisibilityButton.bounds.x = mapVisibilityButton.bounds.x + mapVisibilityButton.bounds.w + 0.75*widgetSpacing;
	coursePreviewVisibilityButton.bounds.y = mapVisibilityButton.bounds.y;

	toolsPanel.bounds.x = mapPanel.bounds.w;
	toolsPanel.bounds.y = 0.035*dh;
	toolsPanel.bounds.h = 0.945*dh;

	mainTitle.padding.x = mainTitle.padding.y = widgetSpacing/2;
	mainTitle.pack();
	mainTitle.bounds.x = toolsPanel.bounds.x + (toolsPanel.bounds.w - mainTitle.bounds.w)/2;
	mainTitle.bounds.y = 0;

	toolsTabbedPane.bounds = toolsPanel.bounds;
	toolsTabbedPane.bounds.x += widgetSpacing;
	toolsTabbedPane.bounds.y += widgetSpacing;
	toolsTabbedPane.bounds.w -= 2*widgetSpacing;
	toolsTabbedPane.bounds.h -= 7*widgetSpacing + font->getTextHeight() + 0.1*dh;
	toolsTabbedPane.pack();

	presetsTabPanel.bounds = toolsTabbedPane.bounds;
	presetsTabPanel.bounds.y += 0.9*toolsTabbedPane.text.getFont()->getTextHeight();

	float presetButtonSpacing = widgetSpacing/3;
	for(unsigned i = 0; i < presetButtons.size(); i++)
	{
		presetButtons[i].bounds.w = presetButtons[i].bounds.h = 4.4 * widgetSpacing;
		presetButtons[i].bounds.x = presetsTabPanel.bounds.x + presetButtonSpacing + (i % 3) * (presetButtons[0].bounds.w + presetButtonSpacing);
		presetButtons[i].bounds.y = presetsTabPanel.bounds.y + presetButtonSpacing + (i / 3) * (presetButtons[0].bounds.h + presetButtonSpacing);
		presetButtons[i].iconScale.x = 0.8 * presetButtons[i].bounds.w / presetButtons[i].icon->getWidth();
		presetButtons[i].iconScale.y = 0.8 * presetButtons[i].bounds.h / presetButtons[i].icon->getHeight();
		presetButtons[i].highlightSpacing = 0.1*widgetSpacing;
	}

	propertiesTabPanel.bounds = presetsTabPanel.bounds;

	propertiesMenu.bounds = propertiesTabPanel.bounds;
	propertiesMenu.bounds.x += widgetSpacing/2;
	propertiesMenu.bounds.w -= widgetSpacing;
	propertiesMenu.bounds.h -= widgetSpacing;

	newButton.bounds.x = presetsTabPanel.bounds.x;
	newButton.bounds.y = presetsTabPanel.bounds.y + presetsTabPanel.bounds.h + widgetSpacing;
	newButton.bounds.w = 0.085*dh;
	newButton.bounds.h = 0.05*dh;
	newButton.highlightSpacing = 0.4*widgetSpacing;
	newButton.padding.x = newButton.padding.y = 0.1*font->getTextHeight();

	loadButton.bounds = newButton.bounds;
	loadButton.bounds.x += newButton.bounds.w + widgetSpacing;
	loadButton.highlightSpacing = newButton.highlightSpacing;

	saveButton.bounds = loadButton.bounds;
	saveButton.bounds.x += loadButton.bounds.w + widgetSpacing;
	saveButton.highlightSpacing = loadButton.highlightSpacing;

	undoButton.bounds = newButton.bounds;
	undoButton.bounds.y += newButton.bounds.h + widgetSpacing;
	undoButton.highlightSpacing = newButton.highlightSpacing;

	eraseButton.bounds = undoButton.bounds;
	eraseButton.bounds.x += undoButton.bounds.w + widgetSpacing;
	eraseButton.highlightSpacing = undoButton.highlightSpacing;

	infoButton.bounds = eraseButton.bounds;
	infoButton.bounds.x += eraseButton.bounds.w + widgetSpacing;
	infoButton.highlightSpacing = eraseButton.highlightSpacing;

	generateButton.bounds = undoButton.bounds;
	generateButton.bounds.w *= 2.25;
	generateButton.bounds.y += undoButton.bounds.h + widgetSpacing;
	generateButton.highlightSpacing = undoButton.highlightSpacing;

	exitButton.bounds = infoButton.bounds;
	exitButton.bounds.y += infoButton.bounds.h + widgetSpacing;
	exitButton.highlightSpacing = infoButton.highlightSpacing;

	loadDialog.bounds.x = 0.10*dw;
	loadDialog.bounds.y = 0.20*dh;
	loadDialog.bounds.w = 0.80*dw;
	loadDialog.bounds.h = 0.60*dh;

	loadDialogLabel.pack();
	loadDialogLabel.bounds.x = loadDialog.bounds.x + widgetSpacing;
	loadDialogLabel.bounds.y = loadDialog.bounds.y + widgetSpacing;

	fileMenu.bounds.x = loadDialog.bounds.x + widgetSpacing;
	fileMenu.bounds.y = loadDialogLabel.bounds.y + loadDialogLabel.bounds.h + widgetSpacing/2;
	fileMenu.bounds.w = loadDialog.bounds.w - widgetSpacing*2;
	fileMenu.bounds.h = loadDialog.bounds.h - widgetSpacing*5 - font->getTextHeight();
	fileMenu.updateDrawableText();

	loadDialogSelectButton.padding = newButton.padding;
	loadDialogSelectButton.pack();
	loadDialogSelectButton.bounds.x = loadDialog.bounds.x + (loadDialog.bounds.w - loadDialogSelectButton.bounds.w)/3.f;
	loadDialogSelectButton.bounds.y = loadDialog.bounds.y + loadDialog.bounds.h - 1.2*loadDialogSelectButton.bounds.h;
	loadDialogSelectButton.highlightSpacing = newButton.highlightSpacing;

	loadDialogCancelButton.padding = loadDialogSelectButton.padding;
	loadDialogCancelButton.pack();
	loadDialogCancelButton.bounds.y = loadDialogSelectButton.bounds.y;
	loadDialogCancelButton.bounds.x = loadDialogSelectButton.bounds.x + loadDialogSelectButton.bounds.w + widgetSpacing;
	loadDialogCancelButton.highlightSpacing = newButton.highlightSpacing;

	loadDialogEraseButton.padding = loadDialogCancelButton.padding;
	loadDialogEraseButton.pack();
	loadDialogEraseButton.bounds.y = loadDialogCancelButton.bounds.y;
	loadDialogEraseButton.bounds.x = loadDialogCancelButton.bounds.x + loadDialogCancelButton.bounds.w + 2*widgetSpacing;
	loadDialogEraseButton.highlightSpacing = newButton.highlightSpacing;

	loadDialogUpButton.bounds.w = 1.5*widgetSpacing;
	loadDialogUpButton.bounds.h = widgetSpacing;
	loadDialogUpButton.bounds.x = fileMenu.bounds.x + fileMenu.bounds.w - loadDialogUpButton.bounds.w;
	loadDialogUpButton.bounds.y = fileMenu.bounds.y - loadDialogUpButton.bounds.h - widgetSpacing/2;

	loadDialogDownButton.bounds = loadDialogUpButton.bounds;
	loadDialogDownButton.bounds.y = loadDialogSelectButton.bounds.y;

	deleteDialog.bounds.x = 0.125f * dw;
	deleteDialog.bounds.y = 0.425f * dh;
	deleteDialog.bounds.w = 0.750f * dw;
	deleteDialog.bounds.h = 0.150f * dh;

	deleteDialogLabel.pack();
	deleteDialogLabel.bounds.x = deleteDialog.bounds.x + widgetSpacing;
	deleteDialogLabel.bounds.y = deleteDialog.bounds.y + widgetSpacing;

	deleteDialogConfirmButton.bounds = loadDialogSelectButton.bounds;
	deleteDialogConfirmButton.bounds.y = deleteDialog.bounds.y + deleteDialog.bounds.h - deleteDialogConfirmButton.bounds.h - widgetSpacing;
	deleteDialogConfirmButton.highlightSpacing = newButton.highlightSpacing;

	deleteDialogCancelButton.bounds = loadDialogCancelButton.bounds;
	deleteDialogCancelButton.bounds.y = deleteDialogConfirmButton.bounds.y;
	deleteDialogCancelButton.highlightSpacing = newButton.highlightSpacing;

	inputDialog.bounds.w = 0.25*dw;
	inputDialog.bounds.h = 0.20*dh;
	inputDialog.bounds.x = 0.45*(dw - inputDialog.bounds.w);
	inputDialog.bounds.y = 0.50*(dh - inputDialog.bounds.h);

	inputDialogTextField.bounds.x = inputDialog.bounds.x + widgetSpacing;
	inputDialogTextField.bounds.y = inputDialog.bounds.y + widgetSpacing + font->getTextHeight();
	inputDialogTextField.bounds.w = inputDialog.bounds.w - 4*widgetSpacing;
	inputDialogTextField.bounds.h = 1.1*font->getTextHeight();
	inputDialogTextField.content.clear();

	inputDialogLabel.pack();
	inputDialogLabel.bounds.x = inputDialog.bounds.x + widgetSpacing;
	inputDialogLabel.bounds.y = inputDialog.bounds.y + widgetSpacing;

	inputDialogConfirmButton.bounds = loadDialogSelectButton.bounds;
	inputDialogConfirmButton.bounds.y = inputDialog.bounds.y + inputDialog.bounds.h - inputDialogConfirmButton.bounds.h - widgetSpacing;
	inputDialogConfirmButton.highlightSpacing = newButton.highlightSpacing;

	inputDialogCancelButton.bounds = loadDialogCancelButton.bounds;
	inputDialogCancelButton.bounds.y = inputDialogConfirmButton.bounds.y;
	inputDialogCancelButton.highlightSpacing = newButton.highlightSpacing;

	inputDialogIncButton.bounds.w = widgetSpacing*2;
	inputDialogIncButton.bounds.h = widgetSpacing;
	inputDialogIncButton.bounds.y = inputDialogTextField.bounds.y;
	inputDialogIncButton.bounds.x = inputDialog.bounds.x + inputDialog.bounds.w - 3*widgetSpacing;

	inputDialogDecButton.bounds = inputDialogIncButton.bounds;
	inputDialogDecButton.bounds.y += widgetSpacing;

	textInputDialog.bounds.w = 0.6*dw;
	textInputDialog.bounds.h = 0.2*dh;
	textInputDialog.bounds.x = 0.5*(dw - textInputDialog.bounds.w);
	textInputDialog.bounds.y = 0.5*(dh - textInputDialog.bounds.h);

	textInputDialogTextField.bounds.x = textInputDialog.bounds.x + widgetSpacing;
	textInputDialogTextField.bounds.y = textInputDialog.bounds.y + widgetSpacing + font->getTextHeight();
	textInputDialogTextField.bounds.w = textInputDialog.bounds.w - 2*widgetSpacing;
	textInputDialogTextField.bounds.h = 1.1*font->getTextHeight();
	textInputDialogTextField.content.clear();
	textInputDialogTextField.caretPosition = 0;
	textInputDialogTextField.borderDisabled = false;

	textInputDialogLabel.pack();
	textInputDialogLabel.bounds.x = textInputDialog.bounds.x + widgetSpacing;
	textInputDialogLabel.bounds.y = textInputDialog.bounds.y + widgetSpacing;

	textInputDialogConfirmButton.bounds = loadDialogSelectButton.bounds;
	textInputDialogConfirmButton.bounds.y = textInputDialog.bounds.y + textInputDialog.bounds.h - textInputDialogConfirmButton.bounds.h - widgetSpacing;
	textInputDialogConfirmButton.highlightSpacing = newButton.highlightSpacing;

	textInputDialogCancelButton.bounds = loadDialogCancelButton.bounds;
	textInputDialogCancelButton.bounds.y = textInputDialogConfirmButton.bounds.y;
	textInputDialogCancelButton.highlightSpacing = newButton.highlightSpacing;

	menuInputDialog.bounds.w = 0.30*dw;
	menuInputDialog.bounds.h = 0.55*dh;
	menuInputDialog.bounds.x = (dw - menuInputDialog.bounds.w)/2;
	menuInputDialog.bounds.y = (dh - menuInputDialog.bounds.h)/2;

	menuInputDialogLabel.pack();
	menuInputDialogLabel.bounds.x = menuInputDialog.bounds.x + widgetSpacing;
	menuInputDialogLabel.bounds.y = menuInputDialog.bounds.y + widgetSpacing;

	menuInput.bounds.x = menuInputDialog.bounds.x + widgetSpacing;
	menuInput.bounds.y = menuInputDialogLabel.bounds.y + 2*widgetSpacing;
	menuInput.bounds.w = menuInputDialog.bounds.w - widgetSpacing*2;
	menuInput.bounds.h = menuInputDialog.bounds.h - menuInputDialogLabel.bounds.h - font->getTextHeight() - widgetSpacing*2.25;

	menuInputDialogConfirmButton.padding = newButton.padding;
	menuInputDialogConfirmButton.pack();
	menuInputDialogConfirmButton.bounds.x = menuInputDialog.bounds.x + (menuInputDialog.bounds.w - menuInputDialogConfirmButton.bounds.w)/4.f;
	menuInputDialogConfirmButton.bounds.y = menuInputDialog.bounds.y + menuInputDialog.bounds.h - 1.2*menuInputDialogConfirmButton.bounds.h;
	menuInputDialogConfirmButton.highlightSpacing = newButton.highlightSpacing;

	menuInputDialogCancelButton.padding = newButton.padding;
	menuInputDialogCancelButton.pack();
	menuInputDialogCancelButton.bounds.y = menuInputDialogConfirmButton.bounds.y;
	menuInputDialogCancelButton.bounds.x = menuInputDialogConfirmButton.bounds.x + menuInputDialogConfirmButton.bounds.w + widgetSpacing;
	menuInputDialogCancelButton.highlightSpacing = newButton.highlightSpacing;

	menuInputDialogUpButton.bounds.w = 2*widgetSpacing;
	menuInputDialogUpButton.bounds.h = widgetSpacing;
	menuInputDialogUpButton.bounds.x = menuInput.bounds.x + menuInput.bounds.w - menuInputDialogUpButton.bounds.w;
	menuInputDialogUpButton.bounds.y = menuInput.bounds.y - menuInputDialogUpButton.bounds.h - widgetSpacing/2;

	menuInputDialogDownButton.bounds = menuInputDialogUpButton.bounds;
	menuInputDialogDownButton.bounds.y = menuInputDialogConfirmButton.bounds.y;

	viewToolbar.bounds.x = mapPanel.bounds.x;
	viewToolbar.bounds.w = mapPanel.bounds.w;
	viewToolbar.bounds.h = 0.04*dh;
	viewToolbar.bounds.y = mapPanel.bounds.y + mapPanel.bounds.h - viewToolbar.bounds.h;

	zoomUpButton.bounds.w = zoomUpButton.bounds.h = 0.75 * viewToolbar.bounds.h;
	zoomUpButton.bounds.x = toolsPanel.bounds.x - zoomUpButton.bounds.w - widgetSpacing;
	zoomUpButton.bounds.y = viewToolbar.bounds.y + (viewToolbar.bounds.h - zoomUpButton.bounds.h)/3;
	zoomUpButton.highlightSpacing = 0.4*(viewToolbar.bounds.h - zoomUpButton.bounds.h);

	zoomDownButton.bounds = zoomUpButton.bounds;
	zoomDownButton.bounds.x -= zoomDownButton.bounds.w + widgetSpacing;
	zoomDownButton.highlightColor = Color::RED;
	zoomDownButton.highlightSpacing = zoomUpButton.highlightSpacing;

	zoomResetButton.bounds = zoomDownButton.bounds;
	zoomResetButton.bounds.x -= zoomResetButton.bounds.w + widgetSpacing;
	zoomResetButton.highlightColor = Color::RED;
	zoomResetButton.highlightSpacing = zoomDownButton.highlightSpacing;

	scaleIndicatorLabel.text.setContent("zoom: 000.00%");
	scaleIndicatorLabel.pack();
	scaleIndicatorLabel.text.setContent("");
	scaleIndicatorLabel.bounds.x = 0;
	scaleIndicatorLabel.bounds.y = viewToolbar.bounds.y + widgetSpacing/4;

	statusBar.bounds.x = 0;
	statusBar.bounds.y = 0.97*dh;
	statusBar.bounds.w = dw;
	statusBar.bounds.h = 0.03*dh;

	messageDialog.bounds.w = dw/2;
	messageDialog.bounds.h = dh/4;
	messageDialog.bounds.x = (dw - messageDialog.bounds.w)/2;
	messageDialog.bounds.y = (dh - messageDialog.bounds.h)/2;

	messageDialogTitle.bounds = messageDialog.bounds;
	messageDialogTitle.bounds.x += widgetSpacing;
	messageDialogTitle.bounds.y += widgetSpacing;
	messageDialogTitle.pack();

	messageDialogLabel.bounds = messageDialogTitle.bounds;
	messageDialogLabel.bounds.y += 2.5*widgetSpacing;
	messageDialogLabel.pack();

	messageDialogLabel2.bounds = messageDialogLabel.bounds;
	messageDialogLabel2.bounds.y += messageDialogLabel.bounds.h;

	messageDialogConfirmButton.bounds.w = messageDialog.bounds.w/4;
	messageDialogConfirmButton.bounds.h = font->getTextHeight() * 1.1;
	messageDialogConfirmButton.bounds.x = messageDialog.bounds.x + (messageDialog.bounds.w - messageDialogConfirmButton.bounds.w)/2;
	messageDialogConfirmButton.bounds.y = messageDialog.bounds.y + messageDialog.bounds.h - messageDialogConfirmButton.bounds.h - widgetSpacing;

	// initial values

	focus = ON_EDITOR;
	lastAddedAmount = lastAddedPosition = 0;
	toolsTabbedPane.setActiveTab(presetsTabPanel);
	isCoursePreviewVisible = false;
	mapVisibilityButton.backgroundColor = Color::RED;
	mapVisibilityButton.text.setColor(Color::WHITE);
	coursePreviewVisibilityButton.backgroundColor = Color::MAROON;
	coursePreviewVisibilityButton.text.setColor(Color::GREY);
	resetGenerationParameters();

	gamepadPointerPosition = Point::NULL_VECTOR;
	course.spec = Pseudo3DCourse::Spec(200, 3000);
 	course.spec.copyRoadStyle(game.logic.getPresetRoadStyle("default"));
	course.spec.copyLandscapeStyle(game.logic.getPresetLandscapeStyle("default"));
	this->loadCourseSpec(course.spec);
	this->resetMapView(true);
}

void CourseEditorState::onLeave()
{}

void CourseEditorState::render()
{
	// Course preview
	if(isCoursePreviewVisible)
	{
		Graphics::drawFilledRectangle(courseViewBounds, course.spec.colorLandscape);
		course.animation.draw(previewCameraPosition*course.animation.spec.roadSegmentLength, 0.5*course.animation.drawAreaWidth);
//		game.sharedResources->fontDev.drawText("Preview", courseViewBounds.x, courseViewBounds.y, Color::RED);
	}
	else
		mapPanel.draw();

	mapVisibilityButton.draw();
	coursePreviewVisibilityButton.draw();

	Graphics::drawFilledRectangle(toolsPanel.bounds.x, 0, toolsPanel.bounds.w, toolsPanel.bounds.w, toolsPanel.backgroundColor);
	toolsPanel.hoveringDisabled = (focus != ON_EDITOR);
	toolsPanel.draw();
	mainTitle.draw();

	// load file dialog
	if(focus == ON_FILE_MENU or focus == ON_DELETE_DIALOG)
		loadDialog.draw();

	// delete file dialog
	if(focus == ON_DELETE_DIALOG)
		deleteDialog.draw();

	// input dialog
	if(focus == ON_LANE_COUNT_DIALOG or focus == ON_ROAD_WIDTH_DIALOG)
		inputDialog.draw();

	// text input dialog
	if(focus == ON_SAVE_DIALOG or focus == ON_NAME_TEXTFIELD)
		textInputDialog.draw();

	// generic menu input dialog
	if(focus == ON_LANDSCAPE_STYLE_DIALOG or focus == ON_ROAD_STYLE_DIALOG or focus == ON_WEATHER_DIALOG or focus == ON_GENERATE_DIALOG)
		menuInputDialog.draw();

	// error dialog
	if(focus == ON_ERROR_DIALOG or focus == ON_INFO_DIALOG)
		messageDialog.draw();

	// view toolbar
	if(not isCoursePreviewVisible)
	{
		eraseButton.highlightColor = Keyboard::isKeyPressed(Keyboard::KEY_LEFT_SHIFT)? ~Color::CELESTE:
									 Keyboard::isKeyPressed(Keyboard::KEY_LEFT_CONTROL)? Color::MAGENTA: Color::RED;
		viewToolbar.hoveringDisabled = (focus != ON_EDITOR);
		viewToolbar.draw();
	}

	// status bar
	statusBar.draw();

	if(not gamepadPointerPosition.isZero())
		imgPointer->draw(gamepadPointerPosition);
}

void CourseEditorState::update(float delta)
{
	if(focus == ON_EDITOR)
	{
		const Point oldOffset = map.offset;
		const unsigned segmentCursorDisplacement = Keyboard::isKeyPressed(Keyboard::KEY_LEFT_SHIFT)? 4: Keyboard::isKeyPressed(Keyboard::KEY_LEFT_CONTROL)? 2: 1;

		if(Keyboard::isKeyPressed(Keyboard::KEY_ARROW_UP))
		{
			if(isCoursePreviewVisible)
				previewCameraPosition += segmentCursorDisplacement;
			else
				map.offset.y -= 100*delta/map.scale.y;
		}

		if(Keyboard::isKeyPressed(Keyboard::KEY_ARROW_DOWN))
		{
			if(isCoursePreviewVisible)
			{
				if(previewCameraPosition >= segmentCursorDisplacement)
					previewCameraPosition -= segmentCursorDisplacement;
				else
					previewCameraPosition = (map.spec.lines.size() + previewCameraPosition) - segmentCursorDisplacement;
			}
			else
				map.offset.y += 100*delta/map.scale.y;
		}

		if(Keyboard::isKeyPressed(Keyboard::KEY_ARROW_LEFT))
			map.offset.x -= 100*delta/map.scale.x;

		if(Keyboard::isKeyPressed(Keyboard::KEY_ARROW_RIGHT))
			map.offset.x += 100*delta/map.scale.x;

		const Vector2D oldScale = map.scale;

		if(Keyboard::isKeyPressed(Keyboard::KEY_EQUALS) or Keyboard::isKeyPressed(Keyboard::KEY_NUMPAD_ADDITION))
			map.scale *= 1+delta;

		if(Keyboard::isKeyPressed(Keyboard::KEY_MINUS) or Keyboard::isKeyPressed(Keyboard::KEY_NUMPAD_SUBTRACTION))
			map.scale *= 1-delta;

		if(map.scale.x != oldScale.x)
			map.offset.x *= oldScale.x/map.scale.x;

		if(map.scale.y != oldScale.y)
			map.offset.y *= oldScale.y/map.scale.y;

		if(map.scale != oldScale or scaleIndicatorLabel.text.getContent().empty() or map.offset != oldOffset)
		{
			onScaleChange();
			map.compile();
			if(not map.isSegmentHighlighted.empty() and not map.spec.lines.empty())
				map.isSegmentHighlighted[courseSegmentCursor % map.spec.lines.size()] = true;
		}

		if(Keyboard::isKeyPressed(Keyboard::KEY_NUMPAD_8))
		{
			map.isSegmentHighlighted[courseSegmentCursor % map.spec.lines.size()] = false;
			courseSegmentCursor += segmentCursorDisplacement;
			map.isSegmentHighlighted[courseSegmentCursor % map.spec.lines.size()] = true;
		}

		if(Keyboard::isKeyPressed(Keyboard::KEY_NUMPAD_2))
		{
			map.isSegmentHighlighted[courseSegmentCursor % map.spec.lines.size()] = false;
			if(courseSegmentCursor >= segmentCursorDisplacement) courseSegmentCursor -= segmentCursorDisplacement;
			else courseSegmentCursor = (map.spec.lines.size() + courseSegmentCursor) - segmentCursorDisplacement;
			map.isSegmentHighlighted[courseSegmentCursor % map.spec.lines.size()] = true;
		}

		if(Keyboard::isKeyPressed(Keyboard::KEY_NUMPAD_6))
			previewCameraPosition += segmentCursorDisplacement;

		if(Keyboard::isKeyPressed(Keyboard::KEY_NUMPAD_4))
		{
			if(previewCameraPosition >= segmentCursorDisplacement)
				previewCameraPosition -= segmentCursorDisplacement;
			else
				previewCameraPosition = (map.spec.lines.size() + previewCameraPosition) - segmentCursorDisplacement;
		}

		if(Joystick::getCount() > 0)
		{
			if(Joystick::getAxisPosition(0, 0) > 0.5f)
				gamepadPointerPosition.x += mapPanel.bounds.w * 0.5f *delta;
			else if(Joystick::getAxisPosition(0, 0) < -0.5f)
				gamepadPointerPosition.x -= mapPanel.bounds.w * 0.5f *delta;

			if(Joystick::getAxisPosition(0, 1) > 0.5f)
				gamepadPointerPosition.y += mapPanel.bounds.h * 0.5f *delta;
			else if(Joystick::getAxisPosition(0, 1) < -0.5f)
				gamepadPointerPosition.y -= mapPanel.bounds.h * 0.5f *delta;

			if(gamepadPointerPosition.x < 0)
				gamepadPointerPosition.x = 0;
			else if(gamepadPointerPosition.x > toolsPanel.bounds.x + toolsPanel.bounds.w)
				gamepadPointerPosition.x = toolsPanel.bounds.x + toolsPanel.bounds.w;

			if(gamepadPointerPosition.y < 0)
				gamepadPointerPosition.y = 0;
			else if(gamepadPointerPosition.y > statusBar.bounds.y)
				gamepadPointerPosition.y = statusBar.bounds.y;
		}

		static Point lastMousePosition;
		const Point mousePosition = Mouse::getPosition();
		if(statusBar.text.getContent().empty() or mousePosition != lastMousePosition)
		{
			lastMousePosition = mousePosition;

			if(not statusBar.text.getContent().empty())
				statusBar.text.setContent("");

			if(toolsTabbedPane.isActiveTab(presetsTabPanel))
			{
				for(unsigned i = 0; i < presetButtons.size(); i++)
				{
					if(presetButtons[i].bounds.contains(mousePosition))
					{
						statusBar.text.setContent(presetButtons[i].description);
						break;
					}
				}
			}
			if(generateButton.bounds.contains(mousePosition)) statusBar.text.setContent("Generate random course with random props, landscale and road style");
			else if(infoButton.bounds.contains(mousePosition)) statusBar.text.setContent("Shows general information about course");
			else if(eraseButton.bounds.contains(mousePosition)) statusBar.text.setContent("Erase last segment (hold Ctrl to erase 10; hold Shift to erase 100)");
			else if(undoButton.bounds.contains(mousePosition)) statusBar.text.setContent("Erase last added preset");
			else if(exitButton.bounds.contains(mousePosition)) statusBar.text.setContent("Leave course editor");
			else if(newButton.bounds.contains(mousePosition)) statusBar.text.setContent("Clear canvas to start over");
			else if(loadButton.bounds.contains(mousePosition)) statusBar.text.setContent("Opens a dialog to load a course from file");
			else if(saveButton.bounds.contains(mousePosition)) statusBar.text.setContent("Opens a dialog to save a course to a file");
			else if(map.bounds.contains(mousePosition)) statusBar.text.setContent(string("Segment cursor pos: ") + (courseSegmentCursor+1) + " / Preview camera pos: " + (previewCameraPosition+1));
		}
	}

	if(not map.spec.lines.empty())
	{
		courseSegmentCursor %= map.spec.lines.size();
		previewCameraPosition %= map.spec.lines.size();
		map.plottedPositions[1].value = previewCameraPosition * course.spec.roadSegmentLength;
		map.plottedPositions[1].fillColor.a = map.plottedPositions[1].borderColor.a = previewCameraPosition == 0? 0 : 128;
	}
}

void CourseEditorState::onKeyPressed(Keyboard::Key key)
{
	if(focus == ON_EDITOR)
	{
		if(key == Keyboard::KEY_ESCAPE)
		{
			sndCursorOut->play();
			game.enterState(game.logic.currentMainMenuStateId);
		}
		else if(key == Keyboard::KEY_SPACE)
		{
			isCoursePreviewVisible = !isCoursePreviewVisible;
			sndCursorMove->play();
			mapVisibilityButton.backgroundColor = isCoursePreviewVisible? Color::MAROON : Color::RED;
			mapVisibilityButton.text.setColor(isCoursePreviewVisible? Color::GREY : Color::WHITE);
			coursePreviewVisibilityButton.backgroundColor = isCoursePreviewVisible? Color::RED : Color::MAROON;
			coursePreviewVisibilityButton.text.setColor(isCoursePreviewVisible? Color::WHITE : Color::GREY);
		}
		else if(key == Keyboard::KEY_NUMPAD_7)
		{
			map.isSegmentHighlighted[courseSegmentCursor % map.spec.lines.size()] = false;
			courseSegmentCursor++;
			map.isSegmentHighlighted[courseSegmentCursor % map.spec.lines.size()] = true;
		}

		else if(key == Keyboard::KEY_NUMPAD_1)
		{
			map.isSegmentHighlighted[courseSegmentCursor % map.spec.lines.size()] = false;
			courseSegmentCursor--;
			map.isSegmentHighlighted[courseSegmentCursor % map.spec.lines.size()] = true;
		}
	}
	else if(focus == ON_PROPERTIES_MENU)
	{
		if(key == Keyboard::KEY_ESCAPE)
		{
			sndCursorOut->play();
			propertiesMenu.focusedEntryBgColor = propertiesTabPanel.backgroundColor;
			propertiesMenu.focusedEntryFontColor = Color::BLACK;
			focus = ON_EDITOR;
		}
		else if(key == Keyboard::KEY_ARROW_UP and propertiesMenu.getSelectedIndex() > 1)
		{
			sndCursorMove->play();
			propertiesMenu.moveCursorUp();
		}
		else if(key == Keyboard::KEY_ARROW_DOWN)
		{
			sndCursorMove->play();
			propertiesMenu.moveCursorDown();
		}
		else switch(propertiesMenu.getSelectedIndex())
		{
			case MENU_NAME:
			{
				if(key == Keyboard::KEY_ENTER)
				{
					sndCursorIn->play();
					focus = ON_NAME_TEXTFIELD;
					textInputDialogLabel.text.setContent("Enter a name for the course:");
					textInputDialogTextField.content = course.spec.name;
					textInputDialogTextField.updateDrawableText();
					textInputDialogTextField.caretPosition = 0;
				}
				break;
			}
			case MENU_LANE_COUNT:
			{
				if(key == Keyboard::KEY_ENTER)
				{
					sndCursorIn->play();
					focus = ON_LANE_COUNT_DIALOG;
					inputDialogLabel.text.setContent("Lane count:");
					inputDialogTextField.content = course.spec.laneCount < 2? "none" : futil::to_string(course.spec.laneCount);
					inputDialogTextField.updateDrawableText();
				}
				else if(key == Keyboard::KEY_ARROW_RIGHT or key == Keyboard::KEY_ARROW_LEFT)
				{
					sndCursorMove->play();
					if(key == Keyboard::KEY_ARROW_RIGHT)
						course.spec.laneCount++;
					else if(course.spec.laneCount > 1)
						course.spec.laneCount--;
					course.animation.spec.laneCount = course.spec.laneCount;
				}
				break;
			}
			case MENU_ROAD_WIDTH:
			{
				if(key == Keyboard::KEY_ENTER)
				{
					sndCursorIn->play();
					focus = ON_ROAD_WIDTH_DIALOG;
					inputDialogLabel.text.setContent("Road width:");
					inputDialogTextField.content = futil::to_string(course.spec.roadWidth);
					inputDialogTextField.updateDrawableText();
				}
				else if(key == Keyboard::KEY_ARROW_RIGHT or key == Keyboard::KEY_ARROW_LEFT)
				{
					sndCursorMove->play();
					if(key == Keyboard::KEY_ARROW_RIGHT)
						course.spec.roadWidth += 100;
					else if(course.spec.roadWidth > 100)
						course.spec.roadWidth -= 100;
					course.animation.spec.roadWidth = course.spec.roadWidth;
				}
				break;
			}
			case MENU_ROAD_STYLE:
			{
				if(key == Keyboard::KEY_ENTER or key == Keyboard::KEY_ARROW_RIGHT)
				{
					sndCursorIn->play();
					focus = ON_ROAD_STYLE_DIALOG;
					menuInputDialogLabel.text.setContent("Road style");
					menuInput.clearEntries();
					menuInput.addEntry("custom");
					const vector<string> names = game.logic.getPresetRoadStylesNames();
					const_foreach(const string&, name, vector<string>, names)
						menuInput.addEntry(name);

					int presetRoadStyleIndex = -1;
					if(not course.spec.presetRoadStyleName.empty())
						presetRoadStyleIndex = futil::index_of(game.logic.getPresetRoadStylesNames(), course.spec.presetRoadStyleName);

					menuInput.setSelectedIndex(presetRoadStyleIndex == -1? 0 : presetRoadStyleIndex + 1);
					menuInput.updateDrawableText();

					courseStyleBackupSpec.copyRoadStyle(course.spec);
				}
				break;
			}
			case MENU_LANDSCAPE:
			{
				if(key == Keyboard::KEY_ENTER or key == Keyboard::KEY_ARROW_RIGHT)
				{
					sndCursorIn->play();
					focus = ON_LANDSCAPE_STYLE_DIALOG;
					menuInputDialogLabel.text.setContent("Landscape style");
					menuInput.clearEntries();
					menuInput.addEntry("custom");
					const vector<string> names = game.logic.getPresetLandscapeStylesNames();
					const_foreach(const string&, name, vector<string>, names)
						menuInput.addEntry(name);

					int presetLandscapeStyleIndex = -1;
					if(not course.spec.presetLandscapeStyleName.empty())
						presetLandscapeStyleIndex = futil::index_of(game.logic.getPresetLandscapeStylesNames(), course.spec.presetLandscapeStyleName);

					menuInput.setSelectedIndex(presetLandscapeStyleIndex == -1? 0 : presetLandscapeStyleIndex + 1);
					menuInput.updateDrawableText();

					courseStyleBackupSpec.copyLandscapeStyle(course.spec);
				}
				break;
			}
			case MENU_WEATHER:
			{
				if(key == Keyboard::KEY_ENTER)
				{
					sndCursorIn->play();
					focus = ON_WEATHER_DIALOG;
					menuInputDialogLabel.text.setContent("Weather effect");
					menuInput.clearEntries();
					for(unsigned i = 0; i < Pseudo3DCourse::Spec::WEATHER_COUNT; i++)
						menuInput.addEntry(Pseudo3DCourse::Spec::toString(static_cast<Pseudo3DCourse::Spec::WeatherEffect>(i)));
					menuInput.setSelectedIndex(static_cast<unsigned>(course.spec.weatherEffect));
					menuInput.updateDrawableText();
				}
				else if(key == Keyboard::KEY_ARROW_LEFT or key == Keyboard::KEY_ARROW_RIGHT)
				{
					sndCursorMove->play();
					int weatherOption = course.spec.weatherEffect;
					if(key == Keyboard::KEY_ARROW_LEFT)
						if(weatherOption > 0)
							weatherOption--;
						else
							weatherOption = Pseudo3DCourseAnimation::Spec::WEATHER_COUNT-1;
					else
						if(weatherOption < Pseudo3DCourseAnimation::Spec::WEATHER_COUNT-1)
							weatherOption++;
						else
							weatherOption = 0;

					course.spec.weatherEffect = static_cast<Pseudo3DCourseAnimation::Spec::WeatherEffect>(weatherOption);
					course.animation.spec.weatherEffect = course.spec.weatherEffect;
//					weatherTextField.text.setContent(Pseudo3DCourse::Spec::toString(course.spec.weatherEffect);
				}
				break;
			}
			default:break;
		}
	}
	else if(focus == ON_FILE_MENU)
	{
		if(key == Keyboard::KEY_ESCAPE)
		{
			sndCursorOut->play();
			focus = ON_EDITOR;
		}

		if(key == Keyboard::KEY_ARROW_UP)
		{
			sndCursorMove->play();
			fileMenu.moveCursorUp();
		}

		if(key == Keyboard::KEY_ARROW_DOWN)
		{
			sndCursorMove->play();
			fileMenu.moveCursorDown();
		}

		if(key == Keyboard::KEY_ENTER)
		{
			sndCursorIn->play();
			Pseudo3DCourse::Spec spec;
			spec.loadFromFile(fileMenu.getSelectedEntry().label);
			this->loadCourseSpec(spec);
			this->resetMapView();
			focus = ON_EDITOR;
		}
	}
	else if(focus == ON_DELETE_DIALOG)
	{
		if(key == Keyboard::KEY_ESCAPE)
		{
			sndCursorOut->play();
			focus = ON_FILE_MENU;
		}
	}
	else if(focus == ON_SAVE_DIALOG)
	{
		if(key == Keyboard::KEY_ESCAPE)
		{
			sndCursorOut->play();
			textInputDialogConfirmButton.text.setContent("Apply");
			focus = ON_EDITOR;
		}
		else if(key == Keyboard::KEY_ENTER)
		{
			sndCursorIn->play();
			Graphics::drawFilledRectangle(game.getDisplay().getWidth()/3, 3*game.getDisplay().getHeight()/7, game.getDisplay().getWidth()/3, game.getDisplay().getHeight()/7, Color::BLUE);
			font->drawText("  Please wait...", game.getDisplay().getWidth()/3,  game.getDisplay().getHeight()/2, Color::YELLOW);
			game.getDisplay().refresh();

			course.spec.filename = textInputDialogTextField.content;
			if(course.spec.name.empty())
				course.spec.name = textInputDialogTextField.content;
			course.spec.comments = "Generated using carse v" + CARSE_VERSION_STRING;
			course.spec.roadSegmentCount = course.spec.lines.size();

			try { course.spec.saveToFile(GameLogic::USER_CONFIG_FOLDER+"/"+GameLogic::COURSES_FOLDER+"/"+textInputDialogTextField.content); }
			catch(const std::exception& e) { sndCursorOut->play(); messageDialogLabel.text.setContent(e.what()); focus = ON_ERROR_DIALOG; return; }

			game.logic.updateCourseList();
			reloadFileList();
			textInputDialogConfirmButton.text.setContent("Apply");
			focus = ON_EDITOR;
		}
		else
		{
			textInputDialogTextField.onKeyPressed(key);
			sndCursorMove->play();
		}
	}
	else if(focus == ON_NAME_TEXTFIELD)
	{
		if(key == Keyboard::KEY_ESCAPE)
		{
			sndCursorOut->play();
			focus = ON_PROPERTIES_MENU;
		}
		else if(key == Keyboard::KEY_ENTER)
		{
			sndCursorIn->play();
			focus = ON_PROPERTIES_MENU;
			course.spec.name = textInputDialogTextField.content;
		}
		else
		{
			textInputDialogTextField.onKeyPressed(key);
			sndCursorMove->play();
		}
	}

	else if(focus == ON_LANE_COUNT_DIALOG)
	{
		if(key == Keyboard::KEY_ESCAPE)
		{
			sndCursorOut->play();
			course.animation.spec.laneCount = course.spec.laneCount;  // reset anim
			focus = ON_PROPERTIES_MENU;
		}
		else if(key == Keyboard::KEY_ENTER)
		{
			sndCursorIn->play();
			if(inputDialogTextField.content == "none" or futil::parseable<unsigned>(inputDialogTextField.content))
			{
				course.spec.laneCount = inputDialogTextField.content == "none"? 1 : futil::parse<unsigned>(inputDialogTextField.content);
				course.animation.spec.laneCount = course.spec.laneCount;
				focus = ON_PROPERTIES_MENU;
			}
			else
			{
				course.animation.spec.laneCount = course.spec.laneCount;  // reset anim
				messageDialogLabel.text.setContent("Lane count must be a positive integer.");
				focus = ON_ERROR_DIALOG;
			}
		}
		else if(key == Keyboard::KEY_ARROW_UP or key == Keyboard::KEY_ARROW_DOWN)
		{
			sndCursorMove->play();
			unsigned value = inputDialogTextField.content == "none"? 1 : futil::parse<unsigned>(inputDialogTextField.content);
			if(key == Keyboard::KEY_ARROW_UP)
				value++;
			else if(value > 1)
				value--;
			inputDialogTextField.content = (value == 1? "none" : futil::to_string(value));
			inputDialogTextField.updateDrawableText();
			course.animation.spec.laneCount = value; // preview
		}
	}

	else if(focus == ON_ROAD_WIDTH_DIALOG)
	{
		if(key == Keyboard::KEY_ESCAPE)
		{
			sndCursorOut->play();
			course.animation.spec.roadWidth = course.spec.roadWidth;  // reset anim
			focus = ON_PROPERTIES_MENU;
		}
		else if(key == Keyboard::KEY_ENTER)
		{
			sndCursorIn->play();
			if(futil::parseable<float>(inputDialogTextField.content))
			{
				course.spec.roadWidth = futil::parse<float>(inputDialogTextField.content);
				course.animation.spec.roadWidth = course.spec.roadWidth;
				focus = ON_PROPERTIES_MENU;
			}
			else
			{
				course.animation.spec.roadWidth = course.spec.roadWidth;  // reset anim
				messageDialogLabel.text.setContent("Road width must be a real number.");
				focus = ON_ERROR_DIALOG;
			}
		}
		else if(key == Keyboard::KEY_ARROW_UP or key == Keyboard::KEY_ARROW_DOWN)
		{
			sndCursorMove->play();
			float value = futil::parse<float>(inputDialogTextField.content);
			if(key == Keyboard::KEY_ARROW_UP)
				value += 100;
			else if(value > 100)
				value -= 100;
			inputDialogTextField.content = futil::to_string(value);
			inputDialogTextField.updateDrawableText();
			course.animation.spec.roadWidth = value; // preview
		}
	}

	else if(focus == ON_ROAD_STYLE_DIALOG)
	{
		if(key == Keyboard::KEY_ESCAPE)
		{
			sndCursorOut->play();
			course.spec.copyRoadStyle(courseStyleBackupSpec);  // reset previous style
			course.loadSpec(course.spec);  // reset anim sprites
			map.color = course.spec.colorRoadPrimary != Color::BLACK? course.spec.colorRoadPrimary : Color::create(112, 112, 112);
			map.secondaryColor = course.spec.colorRoadSecondary != Color::BLACK? course.spec.colorRoadSecondary : Color::create(255-map.color.r, 255-map.color.g, 255-map.color.b);
			focus = ON_PROPERTIES_MENU;
		}
		else if(key == Keyboard::KEY_ENTER)
		{
			sndCursorIn->play();
			focus = ON_PROPERTIES_MENU;
		}
		else if(key == Keyboard::KEY_ARROW_UP or key == Keyboard::KEY_ARROW_DOWN)
		{
			sndCursorMove->play();
			if(key == Keyboard::KEY_ARROW_UP)
				menuInput.moveCursorUp();
			else
				menuInput.moveCursorDown();

			if(menuInput.getSelectedIndex() == 0)  // custom road style
			{
				course.spec.copyRoadStyle(courseStyleBackupSpec);  // reset previous style (so to no screw previous custom style)
				course.spec.presetRoadStyleName.clear();  // no preset name means custom
			}
			else
				course.spec.copyRoadStyle(game.logic.getPresetRoadStyle(game.logic.getPresetRoadStylesNames()[menuInput.getSelectedIndex()-1]));
			course.loadSpec(course.spec);  // reset anim sprites
			map.color = course.spec.colorRoadPrimary != Color::BLACK? course.spec.colorRoadPrimary : Color::create(112, 112, 112);
			map.secondaryColor = course.spec.colorRoadSecondary != Color::BLACK? course.spec.colorRoadSecondary : Color::create(255-map.color.r, 255-map.color.g, 255-map.color.b);
		}
	}

	else if(focus == ON_LANDSCAPE_STYLE_DIALOG)
	{
		if(key == Keyboard::KEY_ESCAPE)
		{
			sndCursorOut->play();
			course.spec.copyLandscapeStyle(courseStyleBackupSpec);  // reset previous style
			course.loadSpec(course.spec);  // reset anim sprites
			mapPanel.backgroundColor = course.spec.colorOffRoadPrimary != Color::BLACK? course.spec.colorOffRoadPrimary : Color::DARK_GREEN;
			focus = ON_PROPERTIES_MENU;
		}
		else if(key == Keyboard::KEY_ENTER)
		{
			sndCursorIn->play();
			focus = ON_PROPERTIES_MENU;
		}
		else if(key == Keyboard::KEY_ARROW_UP or key == Keyboard::KEY_ARROW_DOWN)
		{
			sndCursorMove->play();
			if(key == Keyboard::KEY_ARROW_UP)
				menuInput.moveCursorUp();
			else
				menuInput.moveCursorDown();

			if(menuInput.getSelectedIndex() == 0)  // custom landscape style
			{
				course.spec.copyLandscapeStyle(courseStyleBackupSpec);  // reset previous style (so to no screw previous custom style)
				course.spec.presetLandscapeStyleName.clear();  // no preset name means custom
			}
			else
				course.spec.copyLandscapeStyle(game.logic.getPresetLandscapeStyle(game.logic.getPresetLandscapeStylesNames()[menuInput.getSelectedIndex()-1]));
			course.loadSpec(course.spec);  // reset anim sprites
			mapPanel.backgroundColor = course.spec.colorOffRoadPrimary != Color::BLACK? course.spec.colorOffRoadPrimary : Color::DARK_GREEN;
		}
	}

	else if(focus == ON_WEATHER_DIALOG)
	{
		if(key == Keyboard::KEY_ESCAPE)
		{
			sndCursorOut->play();
			course.animation.spec.weatherEffect = course.spec.weatherEffect; // reset anim
			focus = ON_PROPERTIES_MENU;
		}
		else if(key == Keyboard::KEY_ENTER)
		{
			sndCursorIn->play();
			course.spec.weatherEffect = static_cast<Pseudo3DCourseAnimation::Spec::WeatherEffect>(menuInput.getSelectedIndex());
			focus = ON_PROPERTIES_MENU;
		}
		else if(key == Keyboard::KEY_ARROW_UP or key == Keyboard::KEY_ARROW_DOWN)
		{
			sndCursorMove->play();
			if(key == Keyboard::KEY_ARROW_UP)
				menuInput.moveCursorUp();
			else
				menuInput.moveCursorDown();
			course.animation.spec.weatherEffect = static_cast<Pseudo3DCourseAnimation::Spec::WeatherEffect>(menuInput.getSelectedIndex());  // preview
		}
	}

	else if(focus == ON_GENERATE_DIALOG)
	{
		if(key == Keyboard::KEY_ESCAPE)
		{
			sndCursorOut->play();
			menuInputDialogConfirmButton.text.setContent("Apply");
			focus = ON_EDITOR;
		}
		else if(key == Keyboard::KEY_ENTER)
		{
			sndCursorIn->play();
			const Pseudo3DCourse::Spec tmpSpec = Pseudo3DCourse::Spec::createRandom(200, course.spec.roadWidth, generationParameters);
			const unsigned previousSegmentCount = course.spec.lines.size(), previousDecorationCount = course.spec.decorations.size();
			course.spec.lines.reserve(course.spec.lines.size() + tmpSpec.lines.size());
			course.spec.lines.insert(course.spec.lines.end(), tmpSpec.lines.begin(), tmpSpec.lines.end());
			course.spec.decorations.reserve(course.spec.decorations.size() + tmpSpec.decorations.size());
			course.spec.decorations.insert(course.spec.decorations.end(), tmpSpec.decorations.begin(),tmpSpec.decorations.end());
			for(unsigned i = previousSegmentCount; i < course.spec.lines.size(); i++)
			{
				course.spec.lines[i].z += previousSegmentCount*course.spec.roadSegmentLength;
				if(course.spec.lines[i].decoration != 0)
					course.spec.lines[i].decoration += previousDecorationCount;
			}

			if(randomizeRoadStyle)
			{
				const vector<string> roadStyleList = game.logic.getPresetRoadStylesNames();
				course.spec.copyRoadStyle(game.logic.getPresetRoadStyle(roadStyleList[futil::random_between(0, roadStyleList.size())]));
			}

			if(randomizeLandscapeStyle)
			{
				const vector<string> landscapeStyleList = game.logic.getPresetLandscapeStylesNames();
				course.spec.copyLandscapeStyle(game.logic.getPresetLandscapeStyle(landscapeStyleList[futil::random_between(0, landscapeStyleList.size())]));
			}

			if(propGenerationAction == PROPS_ACTION_RANDOMIZE_ALL)
			{
				course.spec.decorations.resize(1);
				for(unsigned i = 0; i < course.spec.lines.size(); i++)
				{
					CourseSpec::Segment& line = course.spec.lines[i];
					if(course.spec.ornaments.size() > 0 and rand() % 10 == 0)
					{
						line.decoration = course.spec.decorations.size();
						course.spec.decorations.resize(course.spec.decorations.size()+1);

						course.spec.decorations.back().resize(1);
						course.spec.decorations.back()[0].spec = 0;
						course.spec.decorations.back()[0].x = (rand()%2==0? -1 : 1) * random_between_decimal(7.5, 15);
					}
					else if(course.spec.ornaments.size() > 1 and rand() % 100 == 0)
					{
						line.decoration = course.spec.decorations.size();
						course.spec.decorations.resize(course.spec.decorations.size()+1);

						course.spec.decorations.back().resize(1);
						course.spec.decorations.back()[0].spec = 1;
						course.spec.decorations.back()[0].x = (rand()%2==0? -1 : 1) * random_between_decimal(7.5, 10);
					}
					else if(course.spec.ornaments.size() > 2 and rand() % 1000 == 0)
					{
						line.decoration = course.spec.decorations.size();
						course.spec.decorations.resize(course.spec.decorations.size()+1);

						course.spec.decorations.back().resize(1);
						course.spec.decorations.back()[0].spec = 2;
						course.spec.decorations.back()[0].x = (rand()%2==0? -1 : 1) * random_between_decimal(10, 15);
					}
					else line.decoration = 0;
				}
			}

			else if(propGenerationAction == PROPS_ACTION_CLEAR_ALL)
			{
				course.spec.decorations.resize(1);
				for(unsigned i = 0; i < course.spec.lines.size(); i++)
					course.spec.lines[i].decoration = 0;
			}

			lastAddedAmount = generationParameters.segmentCount;
			lastAddedPosition = courseSegmentCursor;
			courseSegmentCursor = std::min(courseSegmentCursor + lastAddedAmount, (unsigned) course.spec.lines.size() - 1);
			this->loadCourseSpec(course.spec);

			this->resetMapView();
			map.compile();
			this->onScaleChange();

			menuInputDialogConfirmButton.text.setContent("Apply");
			focus = ON_EDITOR;
		}
		else if(key == Keyboard::KEY_ARROW_UP or key == Keyboard::KEY_ARROW_DOWN)
		{
			sndCursorMove->play();
			if(key == Keyboard::KEY_ARROW_UP)
				menuInput.moveCursorUp();
			else
				menuInput.moveCursorDown();
		}
		else if(key == Keyboard::KEY_ARROW_LEFT or key == Keyboard::KEY_ARROW_RIGHT)
		{
			sndCursorMove->play();
			const bool  isLeft = (key == Keyboard::KEY_ARROW_LEFT),
						isCtrl = Keyboard::isKeyPressed(Keyboard::KEY_LEFT_CONTROL),
						isShift = Keyboard::isKeyPressed(Keyboard::KEY_LEFT_SHIFT);

			if(menuInput.getSelectedIndex() == GEN_MENU_SEGMENTS)
			{
				const int delta = (isLeft? -1 : 1) * (isCtrl and isShift? 1000 : isShift? 500 : isCtrl? 100 : 1);
				generationParameters.segmentCount = std::max(0, delta + (int) generationParameters.segmentCount);
			}
			else if(menuInput.getSelectedIndex() == GEN_MENU_CURVENESS)
				generationParameters.curveness += (isLeft? -1.f : 1.f) * (isCtrl and isShift? 1.f : isShift? 0.5f : isCtrl? 0.1f : 0.05f);

			else if(menuInput.getSelectedIndex() == GEN_MENU_RANDOM_LANDSCAPE)
				randomizeLandscapeStyle = !randomizeLandscapeStyle;

			else if(menuInput.getSelectedIndex() == GEN_MENU_RANDOM_ROADSTYLE)
				randomizeRoadStyle = !randomizeRoadStyle;

			else if(menuInput.getSelectedIndex() == GEN_MENU_RANDOM_PROPS)
			{
				propGenerationAction = static_cast<PropGenerationAction>((propGenerationAction + (isLeft? -1 : 1)) % PROPS_ACTION_COUNT);
				generationParameters.randomizeProps = (propGenerationAction == PROPS_ACTION_RANDOMIZE_ADDED);
			}
			updateGenerationMenuLabels();
		}
	}

	else if(focus == ON_ERROR_DIALOG or focus == ON_INFO_DIALOG)
	{
		if(key == Keyboard::KEY_ESCAPE)
		{
			sndCursorOut->play();
			focus = ON_EDITOR;
		}
		if(key == Keyboard::KEY_ENTER)
		{
			sndCursorIn->play();
			focus = ON_EDITOR;
		}
	}
}

void CourseEditorState::onMouseButtonPressed(Mouse::Button button, int x, int y)
{
	if(button == Mouse::BUTTON_LEFT)
	{
		if(focus == ON_EDITOR)
		{
			if(toolsTabbedPane.setActiveTabByButtonPosition(x, y))
				sndCursorMove->play();

			if(toolsTabbedPane.isActiveTab(presetsTabPanel))
			{
				for(unsigned i = 0; i < presetButtons.size(); i++)
				{
					if(presetButtons[i].bounds.contains(x, y))
					{
						lastAddedAmount = (presetButtons[i].addNewSegmentsCallback)(course.spec, courseSegmentCursor);
						lastAddedPosition = courseSegmentCursor;
						courseSegmentCursor = std::min(courseSegmentCursor + lastAddedAmount, (unsigned) course.spec.lines.size() - 1);
						this->loadCourseSpec(course.spec);
						break;
					}
				}
			}
			else if(toolsTabbedPane.isActiveTab(propertiesTabPanel))
			{
				if(propertiesMenu.bounds.contains(x, y))
				{
					const unsigned index = propertiesMenu.getIndexAtLocation(x, y);
					if(index > 0)
					{
						sndCursorIn->play();
						focus = ON_PROPERTIES_MENU;
						propertiesMenu.focusedEntryBgColor = Color::RED;
						propertiesMenu.focusedEntryFontColor = Color::WHITE;
						if(index != propertiesMenu.getSelectedIndex())
							propertiesMenu.setSelectedIndex(index);
					}
				}
			}

			if(newButton.bounds.contains(x, y))
			{
				sndCursorIn->play();
				lastAddedAmount = lastAddedPosition = previewCameraPosition = courseSegmentCursor = 0;
				course.spec = Pseudo3DCourse::Spec(200, 3000);
			 	course.spec.copyRoadStyle(game.logic.getPresetRoadStyle("default"));
				course.spec.copyLandscapeStyle(game.logic.getPresetLandscapeStyle("default"));
				this->loadCourseSpec(course.spec);
				this->resetMapView(true);
			}
			else if(loadButton.bounds.contains(x, y))
			{
				sndCursorIn->play();
				focus = ON_FILE_MENU;
			}
			else if(saveButton.bounds.contains(x, y))
			{
				sndCursorIn->play();
				focus = ON_SAVE_DIALOG;
				textInputDialogLabel.text.setContent("Enter a filename for the course:");
				textInputDialogConfirmButton.text.setContent("Save");
				string filename = course.spec.filename.substr(course.spec.filename.find_last_of("/\\")+1);  // remove base dir
				textInputDialogTextField.content = filename.substr(0, filename.find_last_of("."));  // remove extension
				textInputDialogTextField.updateDrawableText();
				textInputDialogTextField.caretPosition = 0;
			}
			else if(generateButton.bounds.contains(x, y))
			{
				sndCursorIn->play();
				if(Keyboard::isKeyPressed(Keyboard::KEY_LEFT_CONTROL))
				{
					focus = ON_GENERATE_DIALOG;
					onKeyPressed(Keyboard::KEY_ENTER);
				}
				else
				{
					menuInput.clearEntries();
					for(unsigned i = 0; i < GEN_MENU_COUNT; i++)
						menuInput.addEntry(string());
					updateGenerationMenuLabels();
					menuInputDialogLabel.text.setContent("Generate menu:");
					menuInputDialogConfirmButton.text.setContent("Run");
					focus = ON_GENERATE_DIALOG;
				}
			}
			else if(exitButton.bounds.contains(x, y))
			{
				sndCursorOut->play();
				game.enterState(game.logic.currentMainMenuStateId);
			}
			else if(eraseButton.bounds.contains(x, y))
			{
				unsigned amountToRemove = std::min(Keyboard::isKeyPressed(Keyboard::KEY_LEFT_SHIFT)?  100:
												   Keyboard::isKeyPressed(Keyboard::KEY_LEFT_CONTROL)? 10: 1, (int) course.spec.lines.size());
				if(amountToRemove > 0 and courseSegmentCursor > 0)
				{
					sndCursorIn->play();
					if(amountToRemove > courseSegmentCursor)  // cap amount to remove to avoid deleting out of bounds
						amountToRemove = courseSegmentCursor;
					course.spec.lines.erase(course.spec.lines.begin() + courseSegmentCursor - amountToRemove,
											course.spec.lines.begin() + courseSegmentCursor);
					courseSegmentCursor -= amountToRemove;

					// rewrite all z positions correctly
					for(unsigned i = courseSegmentCursor; i < course.spec.lines.size(); i++)
						course.spec.lines[i].z = i * course.spec.roadSegmentLength;

					this->loadCourseSpec(course.spec);
					lastAddedAmount = lastAddedPosition = 0;
				}
			}
			else if(undoButton.bounds.contains(x, y))
			{
				if(lastAddedAmount > 0)
				{
					sndCursorIn->play();
					course.spec.lines.erase(course.spec.lines.begin() + lastAddedPosition,
											course.spec.lines.begin() + lastAddedPosition + lastAddedAmount);

					// rewrite all z positions correctly
					for(unsigned i = lastAddedPosition; i < course.spec.lines.size(); i++)
						course.spec.lines[i].z = i * course.spec.roadSegmentLength;

					if(courseSegmentCursor >= lastAddedPosition)
					{
						if(courseSegmentCursor >= lastAddedPosition + lastAddedAmount)
							courseSegmentCursor -= lastAddedAmount;
						else if(lastAddedPosition > 0)
							courseSegmentCursor = lastAddedPosition - 1;
						else
							courseSegmentCursor = 0;
					}
					this->loadCourseSpec(course.spec);
					lastAddedAmount = lastAddedPosition = 0;
				}
			}
			else if(infoButton.bounds.contains(x, y))
			{
				sndCursorIn->play();
				focus = ON_INFO_DIALOG;
				messageDialogTitle.text.setContent("Course information");
				messageDialogLabel.text.setColor(Color::BLACK);
				if(course.spec.lines.empty())
					messageDialogLabel.text.setContent("Nothing");
				else
				{
					const float courseLength = course.spec.lines.size()*course.spec.roadSegmentLength/(1000*course.animation.lengthScale),
								startHeight = course.spec.lines.front().y, endHeight = course.spec.lines.back().y;
					messageDialogLabel.text.setContent("Course length: " + futil::to_string(courseLength) + "Km (" + futil::to_string(course.spec.lines.size()) + " segments). ");
					messageDialogLabel2.text.setContent(string("Start/end height ") + (startHeight == endHeight? "match (" + futil::to_string(startHeight) : "do not match (" + futil::to_string(startHeight) + " =/= " + futil::to_string(endHeight)) + ")");
				}
			}
			else if(mapVisibilityButton.bounds.contains(x, y))
			{
				isCoursePreviewVisible = true;
				onKeyPressed(Keyboard::KEY_SPACE);
			}
			else if(coursePreviewVisibilityButton.bounds.contains(x, y))
			{
				isCoursePreviewVisible = false;
				onKeyPressed(Keyboard::KEY_SPACE);
			}
			else if(not isCoursePreviewVisible and (zoomUpButton.bounds.contains(x, y) or zoomDownButton.bounds.contains(x, y) or zoomResetButton.bounds.contains(x, y)))
			{
				const Vector2D oldScale = map.scale;

				if(zoomUpButton.bounds.contains(x, y))
					map.scale *= 1.25;
				if(zoomDownButton.bounds.contains(x, y))
					map.scale *= 0.80;
				if(zoomResetButton.bounds.contains(x, y))
					map.scale.x = map.scale.y = 0.01;

				map.offset.x *= oldScale.x/map.scale.x;
				map.offset.y *= oldScale.y/map.scale.y;

				sndCursorMove->play();
				onScaleChange();
				map.compile();
			}
		}
		else if(focus == ON_PROPERTIES_MENU)
		{
			if(propertiesMenu.bounds.contains(x, y))
			{
				const unsigned index = propertiesMenu.getIndexAtLocation(x, y);
				if(index == propertiesMenu.getSelectedIndex())
					onKeyPressed(Keyboard::KEY_ENTER);
				else
				{
					sndCursorIn->play();
					propertiesMenu.setSelectedIndex(index);
				}
			}
			else
				onKeyPressed(Keyboard::KEY_ESCAPE);
		}
		else if(focus == ON_FILE_MENU)
		{
			if(loadDialogUpButton.bounds.contains(x, y))
			{
				sndCursorMove->play();
				fileMenu.moveCursorUp();
			}

			else if(loadDialogDownButton.bounds.contains(x, y))
			{
				sndCursorMove->play();
				fileMenu.moveCursorDown();
			}

			else if(fileMenu.bounds.contains(x, y))
			{
				sndCursorMove->play();
				fileMenu.setSelectedIndexByLocation(x, y);
			}

			if(loadDialogSelectButton.bounds.contains(x, y))
			{
				sndCursorIn->play();
				Pseudo3DCourse::Spec spec;
				spec.loadFromFile(fileMenu.getSelectedEntry().label);
				this->loadCourseSpec(spec);
				this->resetMapView();
				map.compile();
				this->onScaleChange();
				focus = ON_EDITOR;
			}

			if(loadDialogCancelButton.bounds.contains(x, y))
			{
				sndCursorIn->play();
				focus = ON_EDITOR;
			}

			if(loadDialogEraseButton.bounds.contains(x, y))
			{
				sndCursorIn->play();
				focus = ON_DELETE_DIALOG;
			}
		}
		else if(focus == ON_DELETE_DIALOG)
		{
			if(deleteDialogConfirmButton.bounds.contains(x, y) and not fileMenu.getEntries().empty())
			{
				sndCursorIn->play();
				Graphics::drawFilledRectangle(game.getDisplay().getWidth()/3, 3*game.getDisplay().getHeight()/7, game.getDisplay().getWidth()/3, game.getDisplay().getHeight()/7, Color::BLUE);
				font->drawText("  Please wait...", game.getDisplay().getWidth()/3,  game.getDisplay().getHeight()/2, Color::YELLOW);
				game.getDisplay().refresh();

				std::remove(fileMenu.getSelectedEntry().label.c_str());
				game.logic.updateCourseList();
				reloadFileList();
				focus = ON_FILE_MENU;
			}

			if(deleteDialogCancelButton.bounds.contains(x, y))
			{
				sndCursorIn->play();
				focus = ON_FILE_MENU;
			}
		}
		else if(focus == ON_SAVE_DIALOG)
		{
			if(textInputDialogConfirmButton.bounds.contains(x, y))
				onKeyPressed(Keyboard::KEY_ENTER);

			if(textInputDialogCancelButton.bounds.contains(x, y))
				onKeyPressed(Keyboard::KEY_ESCAPE);
		}
		else if(focus == ON_NAME_TEXTFIELD)
		{
			if(textInputDialogConfirmButton.bounds.contains(x, y))
				onKeyPressed(Keyboard::KEY_ENTER);

			else if(textInputDialogCancelButton.bounds.contains(x, y))
				onKeyPressed(Keyboard::KEY_ESCAPE);
		}
		else if(focus == ON_LANE_COUNT_DIALOG or focus == ON_ROAD_WIDTH_DIALOG)
		{
			if(inputDialogConfirmButton.bounds.contains(x, y))
				onKeyPressed(Keyboard::KEY_ENTER);

			else if(inputDialogCancelButton.bounds.contains(x, y))
				onKeyPressed(Keyboard::KEY_ESCAPE);

			else if(inputDialogIncButton.bounds.contains(x, y))
				onKeyPressed(Keyboard::KEY_ARROW_UP);

			else if(inputDialogDecButton.bounds.contains(x, y))
				onKeyPressed(Keyboard::KEY_ARROW_DOWN);
		}
		else if(focus == ON_ROAD_STYLE_DIALOG or focus == ON_LANDSCAPE_STYLE_DIALOG or focus == ON_WEATHER_DIALOG or focus == ON_GENERATE_DIALOG)
		{
			if(menuInputDialogConfirmButton.bounds.contains(x, y))
				onKeyPressed(Keyboard::KEY_ENTER);

			else if(menuInputDialogCancelButton.bounds.contains(x, y))
				onKeyPressed(Keyboard::KEY_ESCAPE);

			else if(menuInputDialogUpButton.bounds.contains(x, y))
				onKeyPressed(Keyboard::KEY_ARROW_UP);

			else if(menuInputDialogDownButton.bounds.contains(x, y))
				onKeyPressed(Keyboard::KEY_ARROW_DOWN);

			else if(menuInput.bounds.contains(x, y))
			{
				// minor hack to delegate handling to onKeyPressed()
				menuInput.setSelectedIndexByLocation(x, y);
				if(menuInput.getSelectedIndex() > 0)
				{
					menuInput.moveCursorUp();
					onKeyPressed(Keyboard::KEY_ARROW_DOWN);
				}
				else
				{
					menuInput.moveCursorDown();
					onKeyPressed(Keyboard::KEY_ARROW_UP);
				}
			}
		}
		else if(focus == ON_ERROR_DIALOG or focus == ON_INFO_DIALOG)
		{
			if(messageDialogConfirmButton.bounds.contains(x, y))
			{
				sndCursorIn->play();
				if(focus == ON_INFO_DIALOG)
				{
					messageDialogLabel.text.setColor(Color::RED);
					messageDialogLabel2.text.setContent("");
					messageDialogTitle.text.setContent("An error has ocurred");
				}
				focus = ON_EDITOR;
			}
		}
	}
}

void CourseEditorState::onMouseWheelMoved(int amount)
{
	const Point mousePos = Mouse::getPosition();
	if(focus == ON_EDITOR)
	{
		if(map.bounds.contains(mousePos))
		{
			const Vector2D oldScale = map.scale;

			if(amount > 0)
				map.scale *= 1.25;
			else
				map.scale *= 0.80;

			map.offset.x *= oldScale.x/map.scale.x;
			map.offset.y *= oldScale.y/map.scale.y;

			onScaleChange();
			map.compile();
		}
	}
	else if(focus == ON_FILE_MENU)
	{
		if(fileMenu.bounds.contains(mousePos))
		{
			while(amount != 0)
			{
				if(amount > 0)
				{
					fileMenu.moveCursorUp();
					amount--;
				}
				else
				{
					fileMenu.moveCursorDown();
					amount++;
				}
			}
		}
	}
}

void CourseEditorState::onJoystickAxisMoved(unsigned joystick, unsigned axis, float value)
{
	if(focus != ON_EDITOR)
		GameLogic::passJoystickAxisToKeyboardArrows(this, axis, value);
}

void CourseEditorState::onJoystickButtonPressed(unsigned joystick, unsigned button)
{
	if(button % 2)  // odd buttons
		this->onKeyPressed(fgeal::Keyboard::KEY_ESCAPE);
	else  // even buttons
		this->onMouseButtonPressed(Mouse::BUTTON_LEFT, gamepadPointerPosition.x, gamepadPointerPosition.y);
}

void CourseEditorState::reloadFileList()
{
	fileMenu.clearEntries();
	for(unsigned i = 0; i < game.logic.getCourseList().size(); i++)
		fileMenu.addEntry(game.logic.getCourseList()[i].filename);
}

void CourseEditorState::loadCourseSpec(const Pseudo3DCourse::Spec& spec)
{
	course.loadSpec(spec);
	map.spec = course.spec;
	scaleIndicatorLabel.text.setContent("");
	mapPanel.backgroundColor = spec.colorOffRoadPrimary != Color::BLACK? spec.colorOffRoadPrimary : Color::DARK_GREEN;
	map.color = spec.colorRoadPrimary != Color::BLACK? spec.colorRoadPrimary : Color::create(112, 112, 112);
	map.secondaryColor = spec.colorRoadSecondary != Color::BLACK? spec.colorRoadSecondary : Color::create(255-map.color.r, 255-map.color.g, 255-map.color.b);
}

void CourseEditorState::resetMapView(bool forEmptyMap)
{
	if(forEmptyMap)
	{
		map.scale.x = map.scale.y = 0.005f;
		map.offset.x = 0.5*mapPanel.bounds.w/map.scale.x;
		map.offset.y = 0.5*mapPanel.bounds.h/map.scale.y;
	}
	else
	{
		map.scale = Vector2D::NULL_VECTOR;
		map.offset.x = 0.5*mapPanel.bounds.w;
		map.offset.y = 0.5*mapPanel.bounds.h;
	}
}

void CourseEditorState::onScaleChange()
{
	char tmp[8];
	futil::snprintf(tmp, sizeof(tmp)/sizeof(*tmp), "%.2f", (10000*map.scale.x));
	scaleIndicatorLabel.text.setContent(string("Zoom: ").append(tmp).append("%"));
	map.plottedPositions.front().fillColor.a = std::max(map.scale.x, map.scale.y) > 0.005? 128 : 255;
}

void CourseEditorState::resetGenerationParameters()
{
	generationParameters.curveness = 1.5;
	generationParameters.segmentCount = 6400;
	randomizeLandscapeStyle = true;
	randomizeRoadStyle = true;
	propGenerationAction = PROPS_ACTION_RANDOMIZE_ADDED;
	generationParameters.randomizeProps = true;
}

void CourseEditorState::updateGenerationMenuLabels()
{
	menuInput.getEntryAt(GEN_MENU_SEGMENTS).label = "Segments: " + futil::to_string(generationParameters.segmentCount);
	menuInput.getEntryAt(GEN_MENU_CURVENESS).label = "Curveness: " + futil::to_string(generationParameters.curveness);
	menuInput.getEntryAt(GEN_MENU_RANDOM_LANDSCAPE).label = "Landscape: " + string(randomizeLandscapeStyle? "randomize" : "keep");
	menuInput.getEntryAt(GEN_MENU_RANDOM_ROADSTYLE).label = "Road style: " + string(randomizeRoadStyle? "randomize" : "keep");
	menuInput.getEntryAt(GEN_MENU_RANDOM_PROPS).label = "Props: ";
	switch(propGenerationAction)
	{
		case PROPS_ACTION_NONE:  menuInput.getEntryAt(GEN_MENU_RANDOM_PROPS).label += "keep"; break;
		case PROPS_ACTION_RANDOMIZE_ADDED:  menuInput.getEntryAt(GEN_MENU_RANDOM_PROPS).label += "randomize added"; break;
		case PROPS_ACTION_RANDOMIZE_ALL:  menuInput.getEntryAt(GEN_MENU_RANDOM_PROPS).label += "randomize all"; break;
		case PROPS_ACTION_CLEAR_ALL:  menuInput.getEntryAt(GEN_MENU_RANDOM_PROPS).label += "clear all"; break;
		default: break;
	}
	menuInput.updateDrawableText();
}
