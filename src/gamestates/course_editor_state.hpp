/*
 * course_editor_state.hpp
 *
 *  Created on: 10 de jul de 2018
 *      Author: carlosfaruolo
 */

#ifndef COURSE_EDITOR_STATE_HPP_
#define COURSE_EDITOR_STATE_HPP_
#include <ciso646>

#include "course.hpp"
#include "course_map_gfx.hpp"

#include "fgeal/fgeal.hpp"
#include "fgeal/extra/game.hpp"
#include "fgeal/extra/gui.hpp"

#include "futil/language.hpp"

#include <vector>

// fwd. declared
class Carse;

class CourseEditorState extends public fgeal::Game::State
{
	Carse& game;

	fgeal::Vector2D lastDisplaySize;

	// used to store where is the current focus
	enum StateFocus
	{
		ON_EDITOR,
		ON_FILE_MENU,
		ON_SAVE_DIALOG,
		ON_DELETE_DIALOG,
		ON_NAME_TEXTFIELD,
		ON_LANE_COUNT_DIALOG,
		ON_ROAD_WIDTH_DIALOG,
		ON_LANDSCAPE_STYLE_DIALOG,
		ON_ROAD_STYLE_DIALOG,
		ON_WEATHER_DIALOG,
		ON_GENERATE_DIALOG,
		ON_ERROR_DIALOG,
		ON_INFO_DIALOG,
		ON_PROPERTIES_MENU
	}
	focus;

	// resources
	fgeal::Font* font, *fontSmall, *fontTitle, *fontButton;
	fgeal::Sound* sndCursorMove, *sndCursorIn, *sndCursorOut;
	fgeal::Image* imgPointer;

	// the course being edited and preview widget
	Pseudo3DCourse course;
	unsigned courseSegmentCursor, previewCameraPosition;

	// editor logic variables
	unsigned lastAddedAmount, lastAddedPosition;
	Pseudo3DCourse::Spec::RandomGenerationParameters generationParameters;
	enum PropGenerationAction { PROPS_ACTION_NONE, PROPS_ACTION_RANDOMIZE_ADDED, PROPS_ACTION_RANDOMIZE_ALL, PROPS_ACTION_CLEAR_ALL, PROPS_ACTION_COUNT } propGenerationAction;
	bool randomizeRoadStyle, randomizeLandscapeStyle;
	fgeal::Point gamepadPointerPosition;

	// general stuff
	fgeal::Label mainTitle;

	// map being displayed
	fgeal::Panel mapPanel;
	Pseudo3DCourseMap map;

	// the area being used to draw a course preview
	fgeal::Rectangle courseViewBounds;
	bool isCoursePreviewVisible;

	// buttons that control visibility of map or course preview
	fgeal::Button mapVisibilityButton, coursePreviewVisibilityButton;

	// tools panel stuff
	fgeal::Panel toolsPanel;
	fgeal::TabbedPane toolsTabbedPane;
	fgeal::Button newButton, loadButton, saveButton, eraseButton, undoButton, exitButton, generateButton, infoButton;

	// presets tab stuff
	fgeal::Panel presetsTabPanel;
	struct PresetButton extends fgeal::IconButton
	{
		const char* description;
		unsigned (*addNewSegmentsCallback)(CourseSpec&, unsigned);  // adds segments to given course spec and returns the amount of segments added
	};
	std::vector<PresetButton> presetButtons;

	// properties tab stuff
	fgeal::Panel propertiesTabPanel;
	fgeal::Menu propertiesMenu;
	Pseudo3DCourse::Spec courseStyleBackupSpec;

	// load dialog
	fgeal::Panel loadDialog;
	fgeal::Label loadDialogLabel;
	fgeal::Menu fileMenu;
	fgeal::Button loadDialogSelectButton, loadDialogCancelButton, loadDialogEraseButton;
	fgeal::ArrowIconButton loadDialogUpButton, loadDialogDownButton;

	// delete dialog
	fgeal::Panel deleteDialog;
	fgeal::Label deleteDialogLabel;
	fgeal::Button deleteDialogConfirmButton, deleteDialogCancelButton;

	// input dialog
	fgeal::Panel inputDialog;
	fgeal::Label inputDialogLabel;
	fgeal::TextField inputDialogTextField;
	fgeal::Button inputDialogConfirmButton, inputDialogCancelButton, inputDialogIncButton, inputDialogDecButton;

	// text input dialog
	fgeal::Panel textInputDialog;
	fgeal::Label textInputDialogLabel;
	fgeal::TextField textInputDialogTextField;
	fgeal::Button textInputDialogConfirmButton, textInputDialogCancelButton;

	// menu input dialog
	fgeal::Panel menuInputDialog;
	fgeal::Label menuInputDialogLabel;
	fgeal::Menu menuInput;
	fgeal::Button menuInputDialogConfirmButton, menuInputDialogCancelButton;
	fgeal::ArrowIconButton menuInputDialogUpButton, menuInputDialogDownButton;

	// view toolbar
	fgeal::Panel viewToolbar;
	fgeal::Label scaleIndicatorLabel;
	fgeal::Button zoomUpButton, zoomDownButton, zoomResetButton;

	// status bar
	fgeal::Label statusBar;

	// message dialog
	fgeal::Panel messageDialog;
	fgeal::Label messageDialogTitle, messageDialogLabel, messageDialogLabel2;
	fgeal::Button messageDialogConfirmButton;

	public:
	virtual int getId();

	CourseEditorState(Carse* game);
	~CourseEditorState();

	virtual void initialize();
	virtual void onEnter();
	virtual void onLeave();

	virtual void onKeyPressed(fgeal::Keyboard::Key k);
	virtual void onMouseButtonPressed(fgeal::Mouse::Button button, int x, int y);
	virtual void onMouseWheelMoved(int change);
	virtual void onJoystickAxisMoved(unsigned, unsigned, float);
	virtual void onJoystickButtonPressed(unsigned, unsigned);

	virtual void render();
	virtual void update(float delta);

	private:
	void reloadFileList();
	void loadWeatherMenuEntries();
	void loadCourseSpec(const Pseudo3DCourse::Spec&);
	void resetMapView(bool forEmptyMap=false);
	void resetGenerationParameters();
	void updateGenerationMenuLabels();
	void onScaleChange();
};

#endif /* COURSE_EDITOR_STATE_HPP_ */
