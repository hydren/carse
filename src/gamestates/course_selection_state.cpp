/*
 * course_selection_state.cpp
 *
 *  Created on: 23 de mai de 2017
 *      Author: carlosfaruolo
 */

#include "course_selection_state.hpp"

#include "carse.hpp"

#include "pseudo3d_race_state.hpp"

#include "util/sizing.hpp"

#include "futil/string_actions.hpp"
#include "futil/snprintf.h"

#include <cmath>

using fgeal::Display;
using fgeal::Event;
using fgeal::EventQueue;
using fgeal::Keyboard;
using fgeal::Mouse;
using fgeal::Font;
using fgeal::Color;
using fgeal::Image;
using fgeal::Graphics;
using fgeal::Sound;
using fgeal::Rectangle;
using fgeal::Button;
using fgeal::Point;
using fgeal::Vector2D;
using fgeal::Menu;
using std::vector;
using std::string;
using futil::to_string;

// these guys help giving semantics to menu indexes.
enum SettingsMenuIndex
{
	SETTINGS_RACE_TYPE,
	SETTINGS_COURSE_FORMAT,
	SETTINGS_LAPS,
	SETTINGS_PLAYER_COUNT,
	SETTINGS_CPU_OPPONENT_COUNT,
	SETTINGS_CPU_OPPONENT_CLASS,
	SETTINGS_TRAFFIC_DENSITY,
	SETTINGS_POWER_BOOST,
	SETTINGS_WEATHER_FX,
	SETTINGS_MENU_COUNT
};

static const string TITLE_TEXT = "Choose a course";

const static bool DEBUG_COURSE_VISIBLE = true;
const static unsigned MENU_COURSE_OFFSET = DEBUG_COURSE_VISIBLE? 2 : 1;

int CourseSelectionState::getId() { return Carse::COURSE_SELECTION_STATE_ID; }

CourseSelectionState::CourseSelectionState(Carse* game)
: State(*game), game(*game), lastDisplaySize(),
  sndCursorMove(null), sndCursorIn(null), sndCursorOut(null),
  backgroundImage(null), fontMain(null),
  imgRandom(null), imgCircuit(null), imgCurrentCourse(null), fontInfo(null), fontMenus(null), fontButtons(null),
  courseMapViewer(Pseudo3DCourse::Spec(0,0)),
  backButton(), selectButton(),
  raceParams(),
  focusChangeProgress(), keydownTimestamp(),
  isFocusOnCourseList(), isRandomCourseSelected(), isDebugCourseSelected(), isMenuSettingsVisible(),
  isCourseMapVisible()
{}

CourseSelectionState::~CourseSelectionState()
{
	if(backgroundImage != null) delete backgroundImage;
	if(imgRandom != null) delete imgRandom;
	if(imgCircuit != null) delete imgCircuit;
	if(imgCurrentCourse != null) delete imgCurrentCourse;
	if(fontMain != null) delete fontMain;
	if(fontInfo != null) delete fontInfo;
	if(fontButtons != null) delete fontButtons;
	if(fontMenus != null) delete fontMenus;
}

void CourseSelectionState::initialize()
{
	backgroundImage = new Image("assets/ui/course-selection-bg.jpg");
	imgRandom = new Image("assets/ui/portrait-random.png");
	imgCircuit = new Image("assets/ui/portrait-circuit.png");

	fontMain = new Font(gameFontParams("ui_course_selection_title"));
	fontInfo = new Font(gameFontParams("ui_course_selection_info"));
	fontMenus = new Font(gameFontParams("ui_course_selection_items"));
	fontButtons = new Font(gameFontParams("ui_button"));

	menuCourse.setFont(fontMenus);
	menuCourse.setColor(Color::RED);
	menuCourse.backgroundColor = Color::create(0, 0, 0, 128);
	menuCourse.borderColor = Color::create(0, 0, 0, 192);
	menuCourse.focusedEntryFontColor = Color::WHITE;
	menuCourse.cursorWrapAroundEnabled = true;
	menuCourse.addEntry("<Random course>");
	if(DEBUG_COURSE_VISIBLE)
		menuCourse.addEntry("<Debug course>");

	// agreeing with game logic
	menuCourse.setSelectedIndex(0);
	isRandomCourseSelected = true;
	isDebugCourseSelected = false;
	isCourseMapVisible = false;

	menuCourseArrowUp.shape = Button::SHAPE_ROUNDED_RECTANGULAR;
	menuCourseArrowUp.backgroundColor = Color::RED;
	menuCourseArrowUp.borderColor = Color::MAROON;
	menuCourseArrowUp.highlightColor = Color::MAROON;
	menuCourseArrowUp.arrowColor = Color::MAROON;
	menuCourseArrowUp.arrowOrientation = fgeal::ArrowIconButton::ARROW_UP;

	menuCourseArrowDown = menuCourseArrowUp;
	menuCourseArrowDown.arrowOrientation = fgeal::ArrowIconButton::ARROW_DOWN;

	menuSettings.title.setContent("Race settings");
	menuSettings.title.setColor(Color::WHITE);
	menuSettings.setFont(fontMenus);
	menuSettings.setColor(Color::RED);
	menuSettings.backgroundColor = Color::BLACK.getTransparent(224);
	menuSettings.borderColor = menuCourse.borderColor;
	menuSettings.focusedEntryFontColor = menuCourse.focusedEntryFontColor;
	menuSettings.entryOverflowBehavior = fgeal::TextField::OVERFLOW_ABBREVIATE_LEADING;
	menuSettings.cursorWrapAroundEnabled = false;
	for(int i = 0; i < SETTINGS_MENU_COUNT; i++)
		menuSettings.addEntry(string());

	raceParams = game.logic.nextMatch.race;
	updateMenuSettingsLabels();

	courseMapViewer.geometryOtimizationEnabled = true;
	courseMapViewer.usethickLinePrimitive = true;
	courseMapViewer.color = Color::WHITE;
	courseMapViewer.plottedPositions.resize(1);
	courseMapViewer.plottedPositions[0].shape = Pseudo3DCourseMap::PlottedPosition::SHAPE_BAR;
	courseMapViewer.plottedPositions[0].fillColor = Color::YELLOW;
	courseMapViewer.plottedPositions[0].borderColor = Color::BLACK;
	courseMapViewer.plottedPositions[0].borderEnabled = true;
	courseMapViewer.plottedPositions[0].value = 0;

	backButton.backgroundColor = menuSettings.backgroundColor;
	backButton.shape = Button::SHAPE_ROUNDED_RECTANGULAR;
	backButton.text.setColor(Color::WHITE);
	backButton.text.setContent(" Back ");
	backButton.text.setFont(fontButtons);

	selectButton = backButton;
	selectButton.text.setContent(" Select ");

	// loan some shared resources
	sndCursorMove = &game.sharedResources->sndCursorMove;
	sndCursorIn   = &game.sharedResources->sndCursorIn;
	sndCursorOut  = &game.sharedResources->sndCursorOut;
}

void CourseSelectionState::onEnter()
{
	Display& display = game.getDisplay();
	const float dw = game.getDisplay().getWidth(), dh = game.getDisplay().getHeight(), spacing = 0.01*dh;

	// reload fonts if display size changed
	if(lastDisplaySize.x != dw or lastDisplaySize.y != dh)
	{
		const FontSizer fs(display.getHeight());
		fontMain->setSize(fs(game.logic.getFontSize("ui_course_selection_title")));
		fontInfo->setSize(fs(game.logic.getFontSize("ui_course_selection_info")));
		fontMenus->setSize(fs(game.logic.getFontSize("ui_course_selection_items")));
		fontButtons->setSize(fs(game.logic.getFontSize("ui_button")));
		lastDisplaySize.x = dw;
		lastDisplaySize.y = dh;
	}

	paneBounds.h = dh * 0.75f;
	paneBounds.w = paneBounds.h * 1.45f;
	paneBounds.x = (dw - paneBounds.w)/2;
	paneBounds.y = (dh - paneBounds.h) * 0.6125f;

	courseMapViewer.bounds.x = paneBounds.x + spacing;
	courseMapViewer.bounds.y = paneBounds.y + spacing;
	courseMapViewer.bounds.h = courseMapViewer.bounds.w = (paneBounds.w - 2*spacing)/3;
	courseMapViewer.plottedPositions[0].size = dh/100.f;
	courseMapViewer.plottedPositions[0].minimumScaledSize = dh/60.f;
	isCourseMapVisible = true;  // TODO make a timer to enable this after selection change

	portraitImgBounds.x = paneBounds.x + spacing;
	portraitImgBounds.y = courseMapViewer.bounds.y + courseMapViewer.bounds.h + 2*spacing;
	portraitImgBounds.w = portraitImgBounds.h = courseMapViewer.bounds.h;

	menuCourse.bounds.x = portraitImgBounds.x + portraitImgBounds.w + spacing;
	menuCourse.bounds.y = portraitImgBounds.y;
	menuCourse.bounds.w = paneBounds.w - 2*spacing - portraitImgBounds.w;
	menuCourse.bounds.h = paneBounds.h - courseMapViewer.bounds.h - 4*spacing;

	// clear course selection
	menuCourse.setSelectedIndex(0);

	// clear course list
	while(menuCourse.getEntries().size() > MENU_COURSE_OFFSET)
		menuCourse.removeEntry(MENU_COURSE_OFFSET);

	const vector<Pseudo3DCourse::Spec>& courses = game.logic.getCourseList();
	for(unsigned i = 0; i < courses.size(); i++)
		menuCourse.addEntry(courses[i].toString());

	isRandomCourseSelected = game.logic.nextMatch.isRandomCourseSpec;
	isDebugCourseSelected = game.logic.nextMatch.isDebugCourseSpec;

	if(not isRandomCourseSelected and not isDebugCourseSelected)
	{
		for(unsigned i = 0; i < courses.size(); i++)
		if(courses[i].toString() == game.logic.nextMatch.courseSpec.toString())  // FIXME lame comparison...
		{
			menuCourse.setSelectedIndex(i + MENU_COURSE_OFFSET);
			break;
		}
		// selected course not listed (shouldn't happen, but better safe than sorry...)
		if(menuCourse.getSelectedIndex() == 0)
			isRandomCourseSelected = true;
	}
	else
		menuCourse.setSelectedIndex(DEBUG_COURSE_VISIBLE and isDebugCourseSelected? 1 : 0);

	onMenuCourseIndexChange();
	menuCourse.updateDrawableText();

	menuCourseArrowUp.highlightSpacing = 0.0025*dh;
	menuCourseArrowUp.bounds.w = menuCourseArrowUp.bounds.h = 3*spacing;
	menuCourseArrowUp.bounds.x = menuCourse.bounds.x + menuCourse.bounds.w - menuCourseArrowUp.bounds.w - menuCourseArrowUp.highlightSpacing;
	menuCourseArrowUp.bounds.y = menuCourse.bounds.y + 0.005*dh;

	menuCourseArrowDown.bounds = menuCourseArrowUp.bounds;
	menuCourseArrowDown.bounds.y = menuCourse.bounds.y + menuCourse.bounds.h - menuCourseArrowDown.bounds.h - 0.005*dh;
	menuCourseArrowDown.highlightSpacing = menuCourseArrowUp.highlightSpacing;

	menuSettings.bounds.h = 0.4*dh;
	menuSettings.bounds.w = 1.5*menuSettings.bounds.h;
	menuSettings.bounds.x = (dw - menuSettings.bounds.w)/2;
	menuSettings.bounds.y = (dh - menuSettings.bounds.h)*(2/3.f);
	menuSettings.borderThickness = 0.01*dh;
	updateMenuSettingsLabels();

	selectButton.padding.x = selectButton.padding.y = 0.002*dw;
	selectButton.pack();
	selectButton.bounds.x = 3*dw/5 - selectButton.bounds.w/2;
	selectButton.bounds.y = 0.935*dh;

	backButton.bounds = selectButton.bounds;
	backButton.bounds.x = 2*dw/5 - backButton.bounds.w/2;
	backButton.bounds.y = 0.935*dh;

	raceParams = game.logic.nextMatch.race;
	if(raceParams.cpuOpponentCount < 1 and raceParams.playerCount < 2)
		raceParams.cpuOpponentCount = 1;
	isFocusOnCourseList = true;
	isMenuSettingsVisible = false;
	focusChangeProgress = 0;
	keydownTimestamp = 0;
}

void CourseSelectionState::onLeave() {}

void CourseSelectionState::onMenuCourseIndexChange()
{
	isRandomCourseSelected = (menuCourse.getSelectedIndex() == 0);
	isDebugCourseSelected = (DEBUG_COURSE_VISIBLE and menuCourse.getSelectedIndex() == 1);

	if(menuCourse.getSelectedIndex() >= MENU_COURSE_OFFSET)
	{
		const Pseudo3DCourse::Spec& spec = game.logic.getCourseList()[menuCourse.getSelectedIndex() - MENU_COURSE_OFFSET];

		if(isCourseMapVisible)
		{
			Pseudo3DCourse::Spec tmpSpec;
			tmpSpec.loadFromFile(spec.filename);
			courseMapViewer.spec = tmpSpec;
			courseMapViewer.scale.scale(0);
			courseMapViewer.offset.scale(0);
			courseMapViewer.compile();
		}

		if(imgCurrentCourse != null)
			delete imgCurrentCourse;

		if(fgeal::filesystem::isFilenameArchive(spec.previewFilename))  // FIXME use get contextualized filename function here
			imgCurrentCourse = new Image(spec.previewFilename);
		else
			imgCurrentCourse = null;
	}
	else if(courseMapViewer.spec.roadSegmentLength != 0)  // XXX kludged way to check if zero-initialized spec is in place
	{
		courseMapViewer.spec = Pseudo3DCourse::Spec(0,0);
		courseMapViewer.compile();
		if(imgCurrentCourse != null)
		{
			delete imgCurrentCourse;
			imgCurrentCourse = null;
		}
	}
}

void CourseSelectionState::render()
{
	Display& display = game.getDisplay();
	const fgeal::Point mousePosition = Mouse::getPosition();
	const unsigned dw = display.getWidth(), dh = display.getHeight();
	const float spacing = 0.01*dh;
	const bool blinkCycle = cos(20*fgeal::uptime()) > 0;

	display.clear();

	// draw bg
	backgroundImage->drawScaled(0, 0, scaledToSize(backgroundImage, display));

	// draw title
	fontMain->drawText(TITLE_TEXT, (1/32.f)*dw, (1/36.f)*dh, Color::WHITE);
	Graphics::drawLine((1/32.f)*dw, fontMain->getTextHeight()+(1/36.f)*dh, (31/32.f)*dw, fontMain->getTextHeight()+(1/36.f)*dh, Color::WHITE);

	// draw panel bg
	Graphics::drawFilledRectangle(paneBounds, Color::create(0,0,0, 96));

	// draw info
	const Point infoPos = { courseMapViewer.bounds.x + courseMapViewer.bounds.w + spacing, courseMapViewer.bounds.y };
	fontInfo->drawText(menuCourse.getSelectedEntry().label, infoPos, Color::WHITE);
	if(menuCourse.getSelectedIndex() >= MENU_COURSE_OFFSET)
	{
		const Pseudo3DCourse::Spec& course = game.logic.getCourseList()[menuCourse.getSelectedIndex() - MENU_COURSE_OFFSET];
		char tmp[32]; futil::snprintf(tmp, sizeof(tmp)/sizeof(*tmp), "%#.*f", 2, course.roadSegmentCount*course.roadSegmentLength/((game.logic.uiUnits.isImperialDistance? 1609.34 : 1000)*Pseudo3DCourse::COURSE_POSITION_FACTOR));
		const string txtLength = "Length: " + string(tmp) + (game.logic.uiUnits.isImperialDistance? "mi" : "Km");
		fontInfo->drawText(txtLength, infoPos.x, infoPos.y + fontInfo->getTextHeight(), Color::WHITE);
	}

	// portrait frame
	Graphics::drawFilledRectangle(courseMapViewer.bounds, Color::DARK_GREY);

	if(isCourseMapVisible)
	{
		// draw course outline
		if(menuCourse.getSelectedIndex() >= MENU_COURSE_OFFSET)
			courseMapViewer.draw();
	}

	// draw picture portrait
	Image* const image = menuCourse.getSelectedIndex() == 0? imgRandom : imgCurrentCourse != null? imgCurrentCourse : imgCircuit;
	image->drawScaled(portraitImgBounds.x, portraitImgBounds.y, scaledToRect(image, portraitImgBounds));

	menuCourse.draw();
	if(not isMenuSettingsVisible and menuCourse.bounds.contains(mousePosition) and blinkCycle)
		Graphics::drawRectangle(menuCourse.bounds.getSpacedOutline(spacing), Color::RED);

	// draw race settings
	if(isMenuSettingsVisible)
	{
		menuSettings.draw();

		if(menuSettings.bounds.contains(mousePosition) and blinkCycle)
			Graphics::drawRectangle(menuSettings.bounds.getSpacedOutline(spacing), Color::RED);
	}
	else if(focusChangeProgress > 0.05f)
		Graphics::drawFilledRectangle(menuSettings.bounds.x+menuSettings.bounds.w*(1-focusChangeProgress)*0.5f,
									  menuSettings.bounds.y+menuSettings.bounds.h*(1-focusChangeProgress)*0.5f,
									  menuSettings.bounds.w*focusChangeProgress, menuSettings.bounds.h*focusChangeProgress, menuSettings.backgroundColor);

	if(isFocusOnCourseList)
	{
		menuCourseArrowUp.highlighted = blinkCycle and menuCourseArrowUp.bounds.contains(mousePosition);
		menuCourseArrowUp.draw();

		menuCourseArrowDown.highlighted = blinkCycle and menuCourseArrowDown.bounds.contains(mousePosition);
		menuCourseArrowDown.draw();
	}

	backButton.draw();
	if(blinkCycle and backButton.bounds.contains(mousePosition))
		Graphics::drawRectangle(backButton.bounds.getSpacedOutline(0.005*dh), Color::RED);

	selectButton.draw();
	if(blinkCycle and selectButton.bounds.contains(mousePosition))
		Graphics::drawRectangle(selectButton.bounds.getSpacedOutline(0.005*dh), Color::RED);
}

void CourseSelectionState::update(float delta)
{
	if((isFocusOnCourseList and focusChangeProgress > 0.05f) or (not isFocusOnCourseList and focusChangeProgress < 0.95f))
	{
		focusChangeProgress -= delta * (isFocusOnCourseList? 8.f : -8.f);

		if(isFocusOnCourseList)
			isMenuSettingsVisible = false;
		else if(focusChangeProgress >= 0.95f)
			isMenuSettingsVisible = true;
	}

	static float lastCursorChangeTimestamp = 0;
	if(isFocusOnCourseList and fgeal::uptime() - keydownTimestamp > 0.6f and fgeal::uptime() - lastCursorChangeTimestamp > 0.1f)
	{
		if(Keyboard::isKeyPressed(Keyboard::KEY_ARROW_UP) or (Mouse::isButtonPressed(Mouse::BUTTON_LEFT) and menuCourseArrowUp.bounds.contains(Mouse::getPosition())))
		{
			menuCourse.moveCursorUp();
			onMenuCourseIndexChange();
			sndCursorMove->play();
			lastCursorChangeTimestamp = fgeal::uptime();
		}

		else if(Keyboard::isKeyPressed(Keyboard::KEY_ARROW_DOWN) or (Mouse::isButtonPressed(Mouse::BUTTON_LEFT) and menuCourseArrowUp.bounds.contains(Mouse::getPosition())))
		{
			menuCourse.moveCursorDown();
			onMenuCourseIndexChange();
			sndCursorMove->play();
			lastCursorChangeTimestamp = fgeal::uptime();
		}
	}
}

void CourseSelectionState::updateMenuSettingsLabels()
{
	menuSettings.getEntryAt(SETTINGS_RACE_TYPE).label = "Race Type: ";
	switch(raceParams.raceType)
	{
		case Pseudo3DRaceState::RACE_TYPE_DEBUG:
			menuSettings.getEntryAt(SETTINGS_RACE_TYPE).label += "Debug";
			break;
		case Pseudo3DRaceState::RACE_TYPE_PRACTICE:
			menuSettings.getEntryAt(SETTINGS_RACE_TYPE).label += "Practice";
			break;
		case Pseudo3DRaceState::RACE_TYPE_TIME_TRIAL:
			menuSettings.getEntryAt(SETTINGS_RACE_TYPE).label += "Time Trial";
			break;
		case Pseudo3DRaceState::RACE_TYPE_STANDARD_RACE:
			menuSettings.getEntryAt(SETTINGS_RACE_TYPE).label += "Standard race";
			break;
		default:
			menuSettings.getEntryAt(SETTINGS_RACE_TYPE).label += "???";
			break;
	}

	menuSettings.getEntryAt(SETTINGS_COURSE_FORMAT).label.assign("Course format: ") += raceParams.isLapped? "Lapped" : "Point-to-point";

	menuSettings.getEntryAt(SETTINGS_LAPS).label = "Laps: ";
	if(raceParams.isLapped)
	{
		menuSettings.getEntryAt(SETTINGS_LAPS).enabled = true;
		menuSettings.getEntryAt(SETTINGS_LAPS).label += to_string(raceParams.lapCountGoal);
	}
	else
	{
		menuSettings.getEntryAt(SETTINGS_LAPS).enabled = false;
		menuSettings.getEntryAt(SETTINGS_LAPS).label += "--";
	}

	menuSettings.getEntryAt(SETTINGS_PLAYER_COUNT).enabled = (raceParams.raceType == Pseudo3DRaceState::RACE_TYPE_STANDARD_RACE or raceParams.raceType == Pseudo3DRaceState::RACE_TYPE_DEBUG);
	menuSettings.getEntryAt(SETTINGS_PLAYER_COUNT).label = "Players: " + to_string(raceParams.playerCount);

	menuSettings.getEntryAt(SETTINGS_CPU_OPPONENT_COUNT).label = "CPU opponents (experim.): ";
	if(raceParams.raceType == Pseudo3DRaceState::RACE_TYPE_STANDARD_RACE or raceParams.raceType == Pseudo3DRaceState::RACE_TYPE_DEBUG)
	{
		menuSettings.getEntryAt(SETTINGS_CPU_OPPONENT_COUNT).enabled = true;
		menuSettings.getEntryAt(SETTINGS_CPU_OPPONENT_COUNT).label += (raceParams.cpuOpponentCount == 0? "none" : to_string(raceParams.cpuOpponentCount));
	}
	else
	{
		menuSettings.getEntryAt(SETTINGS_CPU_OPPONENT_COUNT).enabled = false;
		menuSettings.getEntryAt(SETTINGS_CPU_OPPONENT_COUNT).label += "--";
	}

	menuSettings.getEntryAt(SETTINGS_CPU_OPPONENT_CLASS).label = "CPU vehicles: ";
	if(menuSettings.getEntryAt(SETTINGS_CPU_OPPONENT_COUNT).enabled)
	{
		menuSettings.getEntryAt(SETTINGS_CPU_OPPONENT_CLASS).enabled = true;
		menuSettings.getEntryAt(SETTINGS_CPU_OPPONENT_CLASS).label += raceParams.opponentVehicleCategory;
	}
	else
	{
		menuSettings.getEntryAt(SETTINGS_CPU_OPPONENT_CLASS).enabled = false;
		menuSettings.getEntryAt(SETTINGS_CPU_OPPONENT_CLASS).label += "--";
	}

	menuSettings.getEntryAt(SETTINGS_TRAFFIC_DENSITY).label = "Traffic (experim.): " + (raceParams.trafficDensity == 0? "none" : to_string(raceParams.trafficDensity*100) + "%");

	menuSettings.getEntryAt(SETTINGS_POWER_BOOST).label = "Power boosts: " + (raceParams.powerBoostCount > 0 ? to_string(raceParams.powerBoostCount) : "none");

	menuSettings.getEntryAt(SETTINGS_WEATHER_FX).label = "Weather effects: " + (raceParams.isWeatherEffectEnforced? Pseudo3DCourse::Spec::toString(raceParams.forcedWeatherEffect) : "default");

	menuSettings.updateDrawableText();
}

void CourseSelectionState::onKeyPressed(Keyboard::Key key)
{
	const bool isCursorUp = (key == Keyboard::KEY_ARROW_UP), isCursorLeft = (key == Keyboard::KEY_ARROW_LEFT);
	if(isFocusOnCourseList)
	{
		switch(key)
		{
			default: break;
			case Keyboard::KEY_ESCAPE:
			{
				sndCursorOut->play();
				game.enterState(game.logic.currentMainMenuStateId);
				break;
			}
			case Keyboard::KEY_ENTER:
			{
				sndCursorIn->play();
				isFocusOnCourseList = false;
				break;
			}
			case Keyboard::KEY_ARROW_UP:
			case Keyboard::KEY_ARROW_DOWN:
			{
				sndCursorMove->play();
				if(isCursorUp)
					menuCourse.moveCursorUp();
				else
					menuCourse.moveCursorDown();
				onMenuCourseIndexChange();
				keydownTimestamp = fgeal::uptime();
				break;
			}
		}
	}
	else  // on race settings
	{
		switch(key)
		{
			default: break;
			case Keyboard::KEY_ESCAPE:
			{
				sndCursorOut->play();
				isFocusOnCourseList = true;
				break;
			}
			case Keyboard::KEY_ENTER:
			{
				sndCursorIn->play();
				if(raceParams.raceType != Pseudo3DRaceState::RACE_TYPE_STANDARD_RACE and raceParams.raceType != Pseudo3DRaceState::RACE_TYPE_DEBUG)
					raceParams.playerCount = 1;
				game.logic.nextMatch.race = raceParams;
				game.logic.nextMatch.isDebugCourseSpec = isDebugCourseSelected;
				game.logic.nextMatch.isRandomCourseSpec = isRandomCourseSelected;
				if(not isDebugCourseSelected and not isRandomCourseSelected)
					game.logic.nextMatch.courseSpec.loadFromFile(game.logic.getCourseList()[(menuCourse.getSelectedIndex() - MENU_COURSE_OFFSET)].filename);
				if(game.logic.nextMatch.players.size() < game.logic.nextMatch.race.playerCount)
					game.logic.nextMatch.players.resize(game.logic.nextMatch.race.playerCount, game.logic.nextMatch.players.back());
				game.enterState(game.logic.currentMainMenuStateId);
				break;
			}
			case Keyboard::KEY_ARROW_UP:
			case Keyboard::KEY_ARROW_DOWN:
			{
				sndCursorMove->play();
				if(isCursorUp)
					menuSettings.moveCursorUp();
				else
					menuSettings.moveCursorDown();
				break;
			}
			case Keyboard::KEY_ARROW_LEFT:
			case Keyboard::KEY_ARROW_RIGHT:
			{
				sndCursorMove->play();
				switch(menuSettings.getSelectedIndex())
				{
					default: break;
					case SETTINGS_RACE_TYPE:
					{
						if(isCursorLeft)
							if(raceParams.raceType == 0)
								raceParams.raceType =  static_cast<Pseudo3DRaceState::RaceType>(Pseudo3DRaceState::RACE_TYPE_COUNT-1);
							else
								raceParams.raceType =  static_cast<Pseudo3DRaceState::RaceType>(raceParams.raceType-1);
						else
							if(raceParams.raceType == Pseudo3DRaceState::RACE_TYPE_COUNT-1)
								raceParams.raceType =  static_cast<Pseudo3DRaceState::RaceType>(0);
							else
								raceParams.raceType =  static_cast<Pseudo3DRaceState::RaceType>(raceParams.raceType+1);
						break;
					}
					case SETTINGS_COURSE_FORMAT:
					{
						raceParams.isLapped = !raceParams.isLapped;
						break;
					}
					case SETTINGS_LAPS:
					{
						// if not a loop type race, do nothing
						if(not menuSettings.getEntryAt(SETTINGS_LAPS).enabled)
							break;

						if(isCursorLeft)
						{
							if(raceParams.lapCountGoal > 2)
								raceParams.lapCountGoal--;
						}
						else
							raceParams.lapCountGoal++;

						break;
					}
					case SETTINGS_PLAYER_COUNT:
					{
						// if not an against opposition type race, do nothing
						if(not menuSettings.getEntryAt(SETTINGS_PLAYER_COUNT).enabled)
							break;

						if(isCursorLeft)
						{
							if(raceParams.playerCount > 1 and raceParams.cpuOpponentCount + raceParams.playerCount > 2)
								raceParams.playerCount--;

							else if(raceParams.playerCount > 0 and raceParams.raceType == Pseudo3DRaceState::RACE_TYPE_DEBUG)
								raceParams.playerCount--;
						}
						else
							raceParams.playerCount++;

						break;
					}
					case SETTINGS_CPU_OPPONENT_COUNT:
					{
						if(menuSettings.getEntryAt(SETTINGS_CPU_OPPONENT_COUNT).enabled)
						{
							if(isCursorLeft)
							{
								if(raceParams.cpuOpponentCount > 0 and raceParams.cpuOpponentCount + raceParams.playerCount > 2)
									raceParams.cpuOpponentCount--;

								else if(raceParams.cpuOpponentCount > 0 and raceParams.raceType == Pseudo3DRaceState::RACE_TYPE_DEBUG)
									raceParams.cpuOpponentCount--;
							}
							else
								raceParams.cpuOpponentCount++;
						}
						break;
					}
					case SETTINGS_CPU_OPPONENT_CLASS:
					{
						if(menuSettings.getEntryAt(SETTINGS_CPU_OPPONENT_CLASS).enabled)
						{
							const vector<string>& categories = game.logic.getRegisteredVehicleCategories();
							for(unsigned i = 0; i < categories.size(); i++)
							if(raceParams.opponentVehicleCategory == categories[i])
							{
								raceParams.opponentVehicleCategory = categories[isCursorLeft? ((i > 0)? i-1 : categories.size()-1) : ((i < categories.size()-1)? i+1 : 0)];
								break;
							}
						}
						break;
					}
					case SETTINGS_TRAFFIC_DENSITY:
					{
						if(isCursorLeft)
						{
							raceParams.trafficDensity -= raceParams.trafficDensity <= 0.10f? 0.01f : 0.10f;
							if(raceParams.trafficDensity < 0.f)
								raceParams.trafficDensity = 0.f;
						}
						else
							raceParams.trafficDensity += raceParams.trafficDensity < 0.099f? 0.01f : 0.10f;  // using "crooked" decimals as a epsilon for comparison

						break;
					}

					case SETTINGS_POWER_BOOST:
					{
						if(not isCursorLeft)
							raceParams.powerBoostCount++;
						else if(raceParams.powerBoostCount > 0)
							raceParams.powerBoostCount--;
						break;
					}

					case SETTINGS_WEATHER_FX:
					{
						if(raceParams.isWeatherEffectEnforced)
						{
							if((isCursorLeft and raceParams.forcedWeatherEffect == 0) or (not isCursorLeft and raceParams.forcedWeatherEffect == Pseudo3DCourseAnimation::Spec::WEATHER_COUNT-1))
							{
								raceParams.isWeatherEffectEnforced = false;
								raceParams.forcedWeatherEffect = static_cast<Pseudo3DCourseAnimation::Spec::WeatherEffect>(0);
							}
							else
							{
								raceParams.forcedWeatherEffect = static_cast<Pseudo3DCourseAnimation::Spec::WeatherEffect>(raceParams.forcedWeatherEffect + (isCursorLeft? -1 : 1));
							}
						}
						else
						{
							raceParams.isWeatherEffectEnforced = true;
							if(isCursorLeft)
								raceParams.forcedWeatherEffect = static_cast<Pseudo3DCourseAnimation::Spec::WeatherEffect>(Pseudo3DCourseAnimation::Spec::WEATHER_COUNT-1);
							else
								raceParams.forcedWeatherEffect = static_cast<Pseudo3DCourseAnimation::Spec::WeatherEffect>(0);
						}
					}
				}
				updateMenuSettingsLabels();
			}
		}
	}
}

void CourseSelectionState::onMouseButtonPressed(Mouse::Button button, int x, int y)
{
	if(button == Mouse::BUTTON_LEFT)
	{
		if(selectButton.bounds.contains(x, y))
			onKeyPressed(Keyboard::KEY_ENTER);

		else if(backButton.bounds.contains(x, y))
			onKeyPressed(Keyboard::KEY_ESCAPE);

		else if(isFocusOnCourseList)
		{
			if(menuCourseArrowUp.bounds.contains(x, y))
				onKeyPressed(Keyboard::KEY_ARROW_UP);

			else if(menuCourseArrowDown.bounds.contains(x, y))
				onKeyPressed(Keyboard::KEY_ARROW_DOWN);

			else if(menuCourse.bounds.contains(x, y))
			{
				sndCursorMove->play();
				menuCourse.setSelectedIndexByLocation(x, y);
				onMenuCourseIndexChange();
			}

			else if(menuSettings.bounds.contains(x, y))
			{
				sndCursorMove->play();
				isFocusOnCourseList = false;
			}
		}
		else if(menuCourse.bounds.contains(x, y))
			onKeyPressed(Keyboard::KEY_ESCAPE);
	}

	if(not isFocusOnCourseList)
	{
		if(menuSettings.bounds.contains(x, y))
		{
			if(menuSettings.getIndexAtLocation(x, y) == menuSettings.getSelectedIndex())
			{
				sndCursorIn->play();
				onKeyPressed(button == Mouse::BUTTON_LEFT? Keyboard::KEY_ARROW_RIGHT : Keyboard::KEY_ARROW_LEFT);
			}
			else
			{
				sndCursorMove->play();
				menuSettings.setSelectedIndexByLocation(x, y);
			}
		}
	}
}

void CourseSelectionState::onMouseWheelMoved(int amount)
{
	if(isFocusOnCourseList and menuCourse.bounds.contains(Mouse::getPosition())) while(amount != 0)
	{
		if(amount > 0)
		{
			menuCourse.moveCursorUp();
			amount--;
		}
		else
		{
			menuCourse.moveCursorDown();
			amount++;
		}
		onMenuCourseIndexChange();
	}
}

void CourseSelectionState::onJoystickAxisMoved(unsigned joystick, unsigned axis, float value)
{
	GameLogic::passJoystickAxisToKeyboardArrows(this, axis, value);
}

void CourseSelectionState::onJoystickButtonPressed(unsigned joystick, unsigned button)
{
	GameLogic::passJoystickButtonsToEnterReturnKeys(this, button);
}
