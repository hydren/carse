/*
 * course_selection_state.hpp
 *
 *  Created on: 23 de mai de 2017
 *      Author: carlosfaruolo
 */

#ifndef COURSE_SELECTION_STATE_HPP_
#define COURSE_SELECTION_STATE_HPP_
#include <ciso646>

#include "pseudo3d_race_state.hpp"

#include "course_gfx.hpp"

#include "fgeal/fgeal.hpp"
#include "fgeal/extra/game.hpp"
#include "fgeal/extra/gui.hpp"

#include "futil/language.hpp"

#include <vector>

// fwd. declared
class Carse;

class CourseSelectionState extends public fgeal::Game::State
{
	Carse& game;

	fgeal::Vector2D lastDisplaySize;

	fgeal::Sound* sndCursorMove, *sndCursorIn, *sndCursorOut;

	fgeal::Image* backgroundImage;

	fgeal::Font* fontMain;

	fgeal::Rectangle paneBounds;

	fgeal::Rectangle portraitImgBounds;
	fgeal::Image* imgRandom, *imgCircuit, *imgCurrentCourse;

	fgeal::Font* fontInfo, *fontMenus, *fontButtons;

	Pseudo3DCourseMap courseMapViewer;

	fgeal::Menu menuCourse;
	fgeal::ArrowIconButton menuCourseArrowUp, menuCourseArrowDown;

	fgeal::Menu menuSettings;

	fgeal::Button backButton, selectButton;

	Pseudo3DRaceState::Parameters raceParams;

	float focusChangeProgress, keydownTimestamp;
	bool isFocusOnCourseList, isRandomCourseSelected, isDebugCourseSelected, isMenuSettingsVisible;

	bool isCourseMapVisible;

	public:
	virtual int getId();

	CourseSelectionState(Carse* game);
	~CourseSelectionState();

	virtual void initialize();
	virtual void onEnter();
	virtual void onLeave();

	virtual void onKeyPressed(fgeal::Keyboard::Key k);
	virtual void onMouseButtonPressed(fgeal::Mouse::Button button, int x, int y);
	virtual void onMouseWheelMoved(int change);
	virtual void onJoystickAxisMoved(unsigned, unsigned, float);
	virtual void onJoystickButtonPressed(unsigned, unsigned);

	virtual void render();
	virtual void update(float delta);

	inline bool& wasRandomCourseSelected() { return isRandomCourseSelected; }

	private:
	void updateMenuSettingsLabels();
	void onMenuCourseIndexChange();
};

#endif /* COURSE_SELECTION_STATE_HPP_ */
