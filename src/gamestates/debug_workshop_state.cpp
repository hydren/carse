/*
 * debug_workshop_state.cpp
 *
 *  Created on: 4 de set de 2023
 *      Author: hydren
 */

#include "debug_workshop_state.hpp"

#include "carse.hpp"
#include "fgeal/extra/sprite.hpp"
#include "fgeal/extra/primitives.hpp"
#include "util/sizing.hpp"

using fgeal::Image;
using fgeal::Sprite;
using fgeal::Keyboard;
using fgeal::Graphics;
using fgeal::Color;
using fgeal::Vector2D;
using fgeal::Sound;
using std::vector;
using std::string;
namespace primitives = fgeal::primitives;

int DebugWorkshopState::getId(){ return Carse::DEBUG_WORKSHOP_STATE_ID; }

struct DebugWorkshopState::Assets
{
	Image imgVehicle, imgWheel;
	Assets()
	: imgVehicle("assets/sprites/car-side-default.png"),
	  imgWheel("assets/sprites/wheel.png")
	{}
};

DebugWorkshopState::DebugWorkshopState(Carse* game)
: State(*game), game(*game), body(Engine(), Mechanics::Spec::EXAMPLE_CAR),
  frontWheelsAngle(), rearWheelsAngle(), engineSound(),
  isRpmLocked(),
  assets(null)
{}

void DebugWorkshopState::initialize()
{
	assets = new Assets(); // @suppress("Symbol is not resolved")
	rpmDialGauge.min = 0;
	rpmDialGauge.graduationLevel = 3;
	rpmDialGauge.graduationPrimarySize = 1000;
	rpmDialGauge.graduationPrimaryLineSize = 0.5;
	rpmDialGauge.graduationValueScale = 0.001;
	rpmDialGauge.graduationSecondarySize = 0.5 * rpmDialGauge.graduationPrimarySize;
	rpmDialGauge.graduationSecondaryLineSize = 0.55;
	rpmDialGauge.graduationTertiarySize = 0.1 * rpmDialGauge.graduationPrimarySize;
	rpmDialGauge.graduationTertiaryLineSize = 0.3;
	rpmDialGauge.needleLinePrimitiveInstead = false;
}

DebugWorkshopState::~DebugWorkshopState()
{
	if(assets != null) delete assets;
}

void DebugWorkshopState::onEnter()
{
	const float dw = game.getDisplay().getWidth(), dh = game.getDisplay().getHeight();

	const Pseudo3DVehicle::Spec& testSpec = game.logic.nextMatch.players[0].vehicleSpec;
	body.spec = testSpec.body;
	body.simulationType = Mechanics::SIMULATION_TYPE_SLIPLESS;
	body.automaticShiftingEnabled = true;
	body.engine.spec = testSpec.engine;
	engineSound.setProfile(testSpec.soundProfile, body.engine.spec.maxRpm);
	engineSound.loadAssetsData();
	engineSound.setVolume(game.logic.masterVolume);
	body.reset();
	engineSound.play();

	rpmDialGauge.max = 1000.f * static_cast<int>((body.engine.spec.maxRpm+950.f)/1000.f);
	rpmDialGauge.text.drawCharFunctionSize = dh/64;
	rpmDialGauge.borderThickness = 0.005 * dh;
	rpmDialGauge.boltRadius = 0.025 * dh;
	rpmDialGauge.needleThickness = 0.005 * dh;
	rpmDialGauge.bounds.w = rpmDialGauge.bounds.h = dh * 0.25f;
	rpmDialGauge.bounds.x = dw * 0.95f - rpmDialGauge.bounds.w;
	rpmDialGauge.bounds.y = dh * 0.95f - rpmDialGauge.bounds.h;
	rpmDialGauge.compile();

	isRpmLocked = false;
}

void DebugWorkshopState::onLeave()
{
	engineSound.halt();
}

void DebugWorkshopState::render()
{
	const float dw = game.getDisplay().getWidth(), dh = game.getDisplay().getHeight(),
				vs = 0.2f*dh/assets->imgVehicle.getHeight(),
				ws = 62*vs/assets->imgWheel.getWidth();

	const Vector2D imgVehiclePos = { 0.05f*dw, 0.5f*dh }, imgVehicleScale = {vs, vs},
			       imgWheelPos1 = {imgVehiclePos.x +  84*vs, imgVehiclePos.y + 115*vs},
				   imgWheelPos2 = {imgVehiclePos.x + 386*vs, imgWheelPos1.y},
				   wheelScale = {ws, ws};

	game.getDisplay().clear();

	Graphics::drawFilledCircleSector(imgWheelPos1.x-0.02f*dw, 0.71f*dh, 0.02f*dh, 0, M_PI, Color::DARK_GREY);
	Graphics::drawFilledCircleSector(imgWheelPos1.x+0.02f*dw, 0.71f*dh, 0.02f*dh, 0, M_PI, Color::DARK_GREY);
	Graphics::drawFilledCircleSector(imgWheelPos2.x-0.02f*dw, 0.71f*dh, 0.02f*dh, 0, M_PI, Color::DARK_GREY);
	Graphics::drawFilledCircleSector(imgWheelPos2.x+0.02f*dw, 0.71f*dh, 0.02f*dh, 0, M_PI, Color::DARK_GREY);

	Graphics::drawFilledRectangle(0.05f*dw, 0.70f*dh, 0.42f*dw, 0.01f*dh, Color::GREY);
	Graphics::drawFilledRectangle(0,        0.71f*dh, 0.50f*dw, 0.02f*dh, Color::DARK_GREY);

	assets->imgVehicle.drawScaled(imgVehiclePos, imgVehicleScale);
	assets->imgWheel.drawScaledRotated(imgWheelPos1, wheelScale, rearWheelsAngle);
	assets->imgWheel.drawScaledRotated(imgWheelPos2, wheelScale, frontWheelsAngle);

	rpmDialGauge.draw(body.engine.rpm);

	primitives::drawDigits(std::abs(body.speed*3.6f), dw/32, dh/32, 10, body.speed >= 0? Color::GREEN : Color::RED);
	primitives::drawDigits(body.engine.gear, dw/32, 2*dh/32, 10, body.automaticShiftingEnabled? Color::BLUE : Color::GREEN);
	primitives::drawDigits(body.engine.rpm, dw/32, 3*dh/32, 10, Color::GREEN);
	if(isRpmLocked)
		primitives::drawString("engine speed locked", dw/32, 4*dh/32, 10, 8, Color::RED);

	const vector<string> engineSoundDegugInfo = engineSound.getDebugInfo(body.engine.rpm);
	for(unsigned i = 0; i < engineSoundDegugInfo.size(); i++)
		primitives::drawString(engineSoundDegugInfo[i], dw * 0.5f, dh * 0.1f + i * 16, 12, 9, Color::WHITE);
}

void DebugWorkshopState::handlePhysics(float delta)
{
	body.updatePowertrain(delta);

	// update position

	if(body.spec.driveWheelType == Mechanics::DRIVE_WHEEL_FRONT or body.spec.driveWheelType == Mechanics::DRIVE_WHEEL_ALL)
		frontWheelsAngle -= body.wheelAngularSpeed*delta;

	if(body.spec.driveWheelType == Mechanics::DRIVE_WHEEL_REAR or body.spec.driveWheelType == Mechanics::DRIVE_WHEEL_ALL)
		rearWheelsAngle -= body.wheelAngularSpeed*delta;

	while(frontWheelsAngle < -2*M_PI) frontWheelsAngle += 2*M_PI;
	while(rearWheelsAngle < -2*M_PI) rearWheelsAngle += 2*M_PI;
}

void DebugWorkshopState::update(float delta)
{
	body.engine.throttlePosition = Keyboard::isKeyPressed(Keyboard::KEY_ARROW_UP)? 1 : 0;
	body.brakePedalPosition = Keyboard::isKeyPressed(Keyboard::KEY_ARROW_DOWN)? 1 : 0;

	const float prevRpm = body.engine.rpm, prevSpeed = body.speed;
	float physicsDelta = delta;
	while(physicsDelta > GameLogic::PHYSICS_MAXIMUM_DELTA_TIME)  // process delta too large in smaller delta steps
	{
		handlePhysics(GameLogic::PHYSICS_MAXIMUM_DELTA_TIME);
		physicsDelta -= GameLogic::PHYSICS_MAXIMUM_DELTA_TIME;
	}
	handlePhysics(physicsDelta);
	if(isRpmLocked)
	{
		body.engine.rpm = prevRpm;
		body.speed = prevSpeed;
	}
	engineSound.update(body.engine.rpm);
}

void DebugWorkshopState::onKeyPressed(Keyboard::Key key)
{
	switch(key)
	{
		case Keyboard::KEY_ESCAPE:
			game.enterState(game.logic.currentMainMenuStateId);
			break;

		case Keyboard::KEY_LEFT_SHIFT:
			body.shiftGear(body.engine.gear+1);
			break;

		case Keyboard::KEY_LEFT_CONTROL:
			body.shiftGear(body.engine.gear-1);
			break;

		case Keyboard::KEY_T:
			body.automaticShiftingEnabled = !body.automaticShiftingEnabled;
			break;

		case Keyboard::KEY_L:
			isRpmLocked = !isRpmLocked;
			break;

		case Keyboard::KEY_ARROW_UP:
			if(isRpmLocked) body.engine.rpm += 50;
			break;

		case Keyboard::KEY_ARROW_DOWN:
			if(isRpmLocked) body.engine.rpm -= 50;
			break;

		case Keyboard::KEY_PAGE_UP:
		case Keyboard::KEY_PAGE_DOWN:
			for(unsigned i = 0; i < engineSound.getSoundData().size(); i++)
				if(Keyboard::isKeyPressed(static_cast<Keyboard::Key>(Keyboard::KEY_NUMPAD_0 + i)))
				{
					const float delta = (key == Keyboard::KEY_PAGE_UP? 1.0 : -1.0);
					if(Keyboard::isKeyPressed(Keyboard::KEY_NUMPAD_DIVISION))
						engineSound.getProfileRanges()[i].startRpm += delta * 100;
					else if(Keyboard::isKeyPressed(Keyboard::KEY_NUMPAD_MULTIPLICATION))
						engineSound.getProfileRanges()[i].soundVolumeFactor += delta * 0.05;
					else
						engineSound.getProfileRanges()[i].depictedRpm += delta * 100;
				}
			break;
		default:
			break;
	}
}
