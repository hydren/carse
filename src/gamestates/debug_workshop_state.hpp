/*
 * debug_workshop_state.hpp
 *
 *  Created on: 4 de set de 2023
 *      Author: hydren
 */

#ifndef GAMESTATES_DEBUG_WORKSHOP_STATE_HPP_
#define GAMESTATES_DEBUG_WORKSHOP_STATE_HPP_
#include <ciso646>

#include "vehicle_mechanics.hpp"
#include "engine_sound.hpp"

#include "fgeal/fgeal.hpp"
#include "fgeal/extra/game.hpp"
#include "race_hud.hpp"

// fwd. declared
class Carse;

class DebugWorkshopState extends public fgeal::Game::State
{
	Carse& game;

	// physics simulation
	Mechanics body;
	float frontWheelsAngle, rearWheelsAngle;

	// sound effect simulation
	EngineSoundSimulator engineSound;

	Hud::DialGauge<float> rpmDialGauge;

	bool isRpmLocked;

	struct Assets;
	Assets *assets;

	public:
	virtual int getId();

	DebugWorkshopState(Carse* game);
	~DebugWorkshopState();

	virtual void initialize();

	virtual void onEnter();
	virtual void onLeave();

	void handlePhysics(float delta);
	virtual void update(float delta);
	virtual void render();

	virtual void onKeyPressed(fgeal::Keyboard::Key);
};

#endif /* GAMESTATES_DEBUG_WORKSHOP_STATE_HPP_ */
