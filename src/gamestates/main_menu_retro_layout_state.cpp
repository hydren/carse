/*
 * main_menu_retro_layout_state.cpp
 *
 *  Created on: 2 de ago de 2018
 *      Author: carlosfaruolo
 */

#include "main_menu_retro_layout_state.hpp"

#include "carse.hpp"

#include "course_selection_state.hpp"

#include "util/sizing.hpp"

#include <cmath>

using std::string;
using fgeal::Display;
using fgeal::Image;
using fgeal::Sprite;
using fgeal::Font;
using fgeal::Color;
using fgeal::Rectangle;
using fgeal::Point;
using fgeal::Keyboard;
using fgeal::Mouse;

int MainMenuRetroLayoutState::getId() { return Carse::MAIN_MENU_CLASSIC_LAYOUT_STATE_ID; }

MainMenuRetroLayoutState::MainMenuRetroLayoutState(Carse* game)
: State(*game), game(*game), lastDisplaySize(),
  imgBackground(null),
  fntTitle(null), fntMain(null),
  sndCursorMove(null), sndCursorIn(null), sndCursorOut(null)
{}

MainMenuRetroLayoutState::~MainMenuRetroLayoutState()
{
	if(imgBackground != null) delete imgBackground;
	if(fntTitle != null) delete fntTitle;
	if(fntMain != null) delete fntMain;
}

MainMenuRetroLayoutState::SlotMenuItem::~SlotMenuItem()
{
	if(icon != null)
		delete icon;
}

void MainMenuRetroLayoutState::initialize()
{
	fntMain = new Font(gameFontParams("ui_main_menu_items"));

	strTitle = "Carse Project";
	fntTitle = new Font(gameFontParams("ui_main_menu_title"));

	slotMenuItemRace.borderColor = Color::DARK_GREY;
	slotMenuItemRace.borderDisabled = false;
	slotMenuItemRace.backgroundColor = Color::LIGHT_GREY;
	slotMenuItemRace.highlightColor = Color::RED;
	slotMenuItemRace.highlightPeriod = 0.15;
	slotMenuItemRace.text.setFont(fntMain);
	slotMenuItemRace.text.setContent("Race!");
	slotMenuItemRace.textVerticalAlignment = fgeal::Label::ALIGN_LEADING;
	slotMenuItemRace.icon = new Sprite("assets/ui/portrait-race.png");

	slotMenuItemVehicle = slotMenuItemRace;
	slotMenuItemVehicle.text.setContent("Vehicle");
	slotMenuItemVehicle.textVerticalAlignment = fgeal::Label::ALIGN_LEADING;
	slotMenuItemVehicle.icon = null;

	slotMenuItemCourse = slotMenuItemRace;
	slotMenuItemCourse.text.setContent("Course");
	slotMenuItemCourse.textVerticalAlignment = fgeal::Label::ALIGN_LEADING;
	slotMenuItemCourse.icon = null;

	slotMenuItemEditor = slotMenuItemRace;
	slotMenuItemEditor.text.setContent("Editor");
	slotMenuItemEditor.textVerticalAlignment = fgeal::Label::ALIGN_LEADING;
	slotMenuItemEditor.icon = new Sprite("assets/ui/portrait-course-editor.png");

	slotMenuItemSettings = slotMenuItemRace;
	slotMenuItemSettings.text.setContent("Options");
	slotMenuItemSettings.textVerticalAlignment = fgeal::Label::ALIGN_LEADING;
	slotMenuItemSettings.icon = new Sprite("assets/ui/portrait-settings.png");

	slotMenuItemExit = slotMenuItemRace;
	slotMenuItemExit.text.setContent("Exit");
	slotMenuItemExit.textVerticalAlignment = fgeal::Label::ALIGN_LEADING;
	slotMenuItemExit.icon = new Sprite("assets/ui/portrait-exit.png");

	imgBackground = new Image("assets/ui/dashboard-bg.jpg");

	// set initial selected slot
	slotMenuItemRace.highlighted = true;

	// loan some shared resources
	sndCursorMove = &game.sharedResources->sndCursorMove;
	sndCursorIn   = &game.sharedResources->sndCursorIn;
	sndCursorOut  = &game.sharedResources->sndCursorOut;
}

void MainMenuRetroLayoutState::onEnter()
{
	Display& display = game.getDisplay();
	const float dw = display.getWidth(),
				dh = display.getHeight(),
				titleHeaderHeight = 0.2 * dw,
				marginX = dw * 0.01, marginY = dh * 0.01;

	// reload fonts if display size changed
	if(lastDisplaySize.x != dw or lastDisplaySize.y != dh)
	{
		const FontSizer fs(display.getHeight());
		fntTitle->setSize(fs(game.logic.getFontSize("ui_main_menu_title")));
		fntMain->setSize(fs(game.logic.getFontSize("ui_main_menu_items")));
		lastDisplaySize.x = dw;
		lastDisplaySize.y = dh;
	}

	const float slotWidth  = slotMenuItemRace.bounds.w = (dh/3 - marginX) * 5/4.f,
				slotHeight = slotMenuItemRace.bounds.h = (dh - titleHeaderHeight - 3*marginY)/2.0f;

	slotMenuItemRace.bounds.x = (dw - slotMenuItemRace.bounds.w)/2;
	slotMenuItemRace.bounds.y = titleHeaderHeight + marginY;
	slotMenuItemRace.borderThickness = std::max(dw, dh) * 0.01f;
	slotMenuItemRace.packIcon(slotWidth * 0.02f, slotHeight * 0.02f);

	slotMenuItemVehicle.bounds = slotMenuItemRace.bounds;
	slotMenuItemVehicle.bounds.x -= slotMenuItemVehicle.bounds.w + marginX;
	slotMenuItemVehicle.borderThickness = slotMenuItemRace.borderThickness;

	if(slotMenuItemVehicle.icon != null)
		delete slotMenuItemVehicle.icon;

	const Pseudo3DVehicleAnimation::Spec& vspec = (
		game.logic.nextMatch.players[0].alternateSpriteIndex == -1? game.logic.nextMatch.players[0].vehicleSpec.sprite
		                                                          : game.logic.nextMatch.players[0].vehicleSpec.alternateSprites[game.logic.nextMatch.players[0].alternateSpriteIndex]
	);
	slotMenuItemVehicle.icon = new Sprite(vspec.sheetFilename, vspec.frameWidth, vspec.frameHeight, -1, 1);
	slotMenuItemVehicle.icon->scale.x = vspec.scale.x * display.getHeight() * 0.0049f;
	slotMenuItemVehicle.icon->scale.y = vspec.scale.y * display.getHeight() * 0.0049f;
	slotMenuItemVehicle.icon->referencePixelX = -(slotMenuItemVehicle.bounds.w - slotMenuItemVehicle.icon->width * slotMenuItemVehicle.icon->scale.x)/2;
	slotMenuItemVehicle.icon->referencePixelY = -(0.9 * slotMenuItemVehicle.bounds.h - slotMenuItemVehicle.borderThickness - (slotMenuItemVehicle.icon->height - vspec.contactOffset) * slotMenuItemVehicle.icon->scale.y);

	slotMenuItemCourse.bounds = slotMenuItemRace.bounds;
	slotMenuItemCourse.bounds.y += slotMenuItemRace.bounds.h + marginY;
	slotMenuItemCourse.borderThickness = slotMenuItemRace.borderThickness;

	if(slotMenuItemCourse.icon != null)
		delete slotMenuItemCourse.icon;

	if(static_cast<CourseSelectionState*>(game.getState(Carse::COURSE_SELECTION_STATE_ID))->wasRandomCourseSelected())
		slotMenuItemCourse.icon = new Sprite("assets/ui/portrait-random.png");
	else if(game.logic.nextMatch.courseSpec.previewFilename.empty())
		slotMenuItemCourse.icon = new Sprite("assets/ui/portrait-circuit.png");
	else
		slotMenuItemCourse.icon = new Sprite(game.logic.nextMatch.courseSpec.previewFilename);
	slotMenuItemCourse.packIcon(slotWidth/4, slotHeight/4);
	slotMenuItemCourse.icon->referencePixelX = -0.125f * slotMenuItemCourse.bounds.w * (1 - 0.75f/slotMenuItemCourse.icon->width);
	slotMenuItemCourse.icon->referencePixelY = -0.175f * slotMenuItemCourse.bounds.h * (1 - 0.75f/(slotMenuItemCourse.icon->height));

	slotMenuItemEditor.bounds = slotMenuItemVehicle.bounds;
	slotMenuItemEditor.bounds.y += slotMenuItemVehicle.bounds.h + marginY;
	slotMenuItemEditor.borderThickness = slotMenuItemRace.borderThickness;
	slotMenuItemEditor.packIcon(slotWidth/4, slotHeight/4);
	slotMenuItemEditor.icon->referencePixelX = -0.125f * slotMenuItemEditor.bounds.w * (1 - 0.75f/slotMenuItemEditor.icon->width);
	slotMenuItemEditor.icon->referencePixelY = -0.175f * slotMenuItemEditor.bounds.h * (1 - 0.75f/slotMenuItemEditor.icon->height);

	slotMenuItemSettings.bounds = slotMenuItemRace.bounds;
	slotMenuItemSettings.bounds.x += slotMenuItemRace.bounds.w + marginX;
	slotMenuItemSettings.borderThickness = slotMenuItemRace.borderThickness;
	slotMenuItemSettings.packIcon(slotWidth/4, slotHeight/4);
	slotMenuItemSettings.icon->referencePixelX = -0.125f * slotMenuItemSettings.bounds.w * (1 - 0.75f/slotMenuItemSettings.icon->width);
	slotMenuItemSettings.icon->referencePixelY = -0.175f * slotMenuItemSettings.bounds.h * (1 - 0.75f/slotMenuItemSettings.icon->height);

	slotMenuItemExit.bounds = slotMenuItemSettings.bounds;
	slotMenuItemExit.bounds.y += slotMenuItemSettings.bounds.h + marginY;
	slotMenuItemExit.borderThickness = slotMenuItemRace.borderThickness;
	slotMenuItemExit.packIcon(slotWidth/4, slotHeight/4);
	slotMenuItemExit.icon->referencePixelX = -0.125f * slotMenuItemExit.bounds.w * (1 - 0.75f/slotMenuItemExit.icon->width);
	slotMenuItemExit.icon->referencePixelY = -0.175f * slotMenuItemExit.bounds.h * (1 - 0.75f/slotMenuItemExit.icon->height);

	handleMouseReturnIfSelected(slotMenuItemRace.bounds.x + 1, slotMenuItemRace.bounds.y + 1);
}

void MainMenuRetroLayoutState::onLeave()
{}

void MainMenuRetroLayoutState::SlotMenuItem::packIcon(float paddingX, float paddingY)
{
	if(icon != null and paddingX < bounds.w and paddingY < bounds.h)
	{
		icon->scale.x = (bounds.w - paddingX) / icon->width;
		icon->scale.y = (bounds.h - paddingY) / icon->height;
	}
}

void MainMenuRetroLayoutState::SlotMenuItem::draw()
{
	backgroundColor = highlighted? Color::LIGHT_GREY : Color::GREY;
	const Color textColor = highlighted? ((static_cast<int>(fgeal::uptime()/highlightPeriod) % 2 == 0)? highlightColor : highlightColor.getDarker()) : Color::WHITE;
	if(textColor != text.getColor())
		text.setColor(textColor);
	Button::draw();
	icon->draw(bounds.x, bounds.y);
}

void MainMenuRetroLayoutState::render()
{
	Display& display = game.getDisplay();
	display.clear();
	imgBackground->drawScaled(0, 0, scaledToSize(imgBackground, display));

	slotMenuItemRace.draw();
	slotMenuItemVehicle.draw();
	slotMenuItemCourse.draw();
	slotMenuItemEditor.draw();
	slotMenuItemSettings.draw();
	slotMenuItemExit.draw();

	fntTitle->drawText(strTitle, 0.5*(display.getWidth() - fntTitle->getTextWidth(strTitle)), 0.11*(display.getHeight() - fntTitle->getTextHeight()), Color::BLACK);
	fntTitle->drawText(strTitle, 0.5*(display.getWidth() - fntTitle->getTextWidth(strTitle)), 0.10*(display.getHeight() - fntTitle->getTextHeight()), Color::WHITE);
}

void MainMenuRetroLayoutState::update(float delta) {}

void MainMenuRetroLayoutState::onKeyPressed(Keyboard::Key key)
{
	if(key == Keyboard::KEY_1)
	{
		game.logic.currentMainMenuStateId = Carse::MAIN_MENU_SIMPLE_LIST_STATE_ID;
		game.enterState(game.logic.currentMainMenuStateId);
		sndCursorOut->play();
	}

	else if(key == Keyboard::KEY_ESCAPE)
		game.running = false;

	else
	{
		// cursor snd
		if(key == Keyboard::KEY_ENTER)
			sndCursorIn->play();
		else if(key == Keyboard::KEY_ARROW_UP or key == Keyboard::KEY_ARROW_DOWN or key == Keyboard::KEY_ARROW_LEFT or key == Keyboard::KEY_ARROW_RIGHT)
			sndCursorMove->play();

		// cursor logic
		if(slotMenuItemRace.highlighted)
		{
			if(key == Keyboard::KEY_ENTER)
				game.enterState(Carse::RACE_STATE_ID);

			else if(key == Keyboard::KEY_ARROW_UP or key == Keyboard::KEY_ARROW_DOWN)
				slotMenuItemCourse.highlighted = !(slotMenuItemRace.highlighted = false);

			else if(key == Keyboard::KEY_ARROW_LEFT)
				slotMenuItemVehicle.highlighted = !(slotMenuItemRace.highlighted = false);

			else if(key == Keyboard::KEY_ARROW_RIGHT)
				slotMenuItemSettings.highlighted = !(slotMenuItemRace.highlighted = false);
		}

		else if(slotMenuItemVehicle.highlighted)
		{
			if(key == Keyboard::KEY_ENTER)
				game.enterState(Carse::VEHICLE_SELECTION_SHOWROOM_LAYOUT_STATE_ID);

			else if(key == Keyboard::KEY_ARROW_UP or key == Keyboard::KEY_ARROW_DOWN)
				slotMenuItemEditor.highlighted = !(slotMenuItemVehicle.highlighted = false);

			else if(key == Keyboard::KEY_ARROW_LEFT)
				slotMenuItemSettings.highlighted = !(slotMenuItemVehicle.highlighted = false);

			else if(key == Keyboard::KEY_ARROW_RIGHT)
				slotMenuItemRace.highlighted = !(slotMenuItemVehicle.highlighted = false);
		}

		else if(slotMenuItemEditor.highlighted)
		{
			if(key == Keyboard::KEY_ENTER)
				game.enterState(Carse::COURSE_EDITOR_STATE_ID);

			else if(key == Keyboard::KEY_ARROW_UP or key == Keyboard::KEY_ARROW_DOWN)
				slotMenuItemVehicle.highlighted = !(slotMenuItemEditor.highlighted = false);

			else if(key == Keyboard::KEY_ARROW_LEFT)
				slotMenuItemExit.highlighted = !(slotMenuItemEditor.highlighted = false);

			else if(key == Keyboard::KEY_ARROW_RIGHT)
				slotMenuItemCourse.highlighted = !(slotMenuItemEditor.highlighted = false);
		}

		else if(slotMenuItemCourse.highlighted)
		{
			if(key == Keyboard::KEY_ENTER)
				game.enterState(Carse::COURSE_SELECTION_STATE_ID);

			else if(key == Keyboard::KEY_ARROW_UP or key == Keyboard::KEY_ARROW_DOWN)
				slotMenuItemRace.highlighted = !(slotMenuItemCourse.highlighted = false);

			else if(key == Keyboard::KEY_ARROW_LEFT)
				slotMenuItemEditor.highlighted = !(slotMenuItemCourse.highlighted = false);

			else if(key == Keyboard::KEY_ARROW_RIGHT)
				slotMenuItemExit.highlighted = !(slotMenuItemCourse.highlighted = false);
		}

		else if(slotMenuItemSettings.highlighted)
		{
			if(key == Keyboard::KEY_ENTER)
				game.enterState(Carse::OPTIONS_MENU_STATE_ID);

			else if(key == Keyboard::KEY_ARROW_UP or key == Keyboard::KEY_ARROW_DOWN)
				slotMenuItemExit.highlighted = !(slotMenuItemSettings.highlighted = false);

			else if(key == Keyboard::KEY_ARROW_LEFT)
				slotMenuItemRace.highlighted = !(slotMenuItemSettings.highlighted = false);

			else if(key == Keyboard::KEY_ARROW_RIGHT)
				slotMenuItemVehicle.highlighted = !(slotMenuItemSettings.highlighted = false);
		}

		else if(slotMenuItemExit.highlighted)
		{
			if(key == Keyboard::KEY_ENTER)
				game.running = false;

			else if(key == Keyboard::KEY_ARROW_UP or key == Keyboard::KEY_ARROW_DOWN)
				slotMenuItemSettings.highlighted = !(slotMenuItemExit.highlighted = false);

			else if(key == Keyboard::KEY_ARROW_LEFT)
				slotMenuItemCourse.highlighted = !(slotMenuItemExit.highlighted = false);

			else if(key == Keyboard::KEY_ARROW_RIGHT)
				slotMenuItemEditor.highlighted = !(slotMenuItemExit.highlighted = false);
		}
	}
}

bool MainMenuRetroLayoutState::handleMouseReturnIfSelected(int x, int y)
{
	SlotMenuItem* selected = null;

	if(slotMenuItemRace.bounds.contains(x, y))
		selected = &slotMenuItemRace;

	else if(slotMenuItemVehicle.bounds.contains(x, y))
		selected = &slotMenuItemVehicle;

	else if(slotMenuItemCourse.bounds.contains(x, y))
		selected = &slotMenuItemCourse;

	else if(slotMenuItemEditor.bounds.contains(x, y))
		selected = &slotMenuItemEditor;

	else if(slotMenuItemSettings.bounds.contains(x, y))
		selected = &slotMenuItemSettings;

	else if(slotMenuItemExit.bounds.contains(x, y))
		selected = &slotMenuItemExit;

	if(selected != null)
	{
		slotMenuItemRace.highlighted = slotMenuItemVehicle.highlighted = slotMenuItemCourse.highlighted =
				slotMenuItemEditor.highlighted = slotMenuItemSettings.highlighted = slotMenuItemExit.highlighted = false;

		selected->highlighted = true;
	}

	return selected != null;
}

void MainMenuRetroLayoutState::onMouseButtonPressed(Mouse::Button button, int x, int y)
{
	if(button == Mouse::BUTTON_LEFT)
	{
		if(handleMouseReturnIfSelected(x, y) == true)
			onKeyPressed(Keyboard::KEY_ENTER);
	}
}

void MainMenuRetroLayoutState::onMouseMoved(int x, int y)
{
	handleMouseReturnIfSelected(x, y);
}

void MainMenuRetroLayoutState::onJoystickAxisMoved(unsigned joystick, unsigned axis, float value)
{
	GameLogic::passJoystickAxisToKeyboardArrows(this, axis, value);
}

void MainMenuRetroLayoutState::onJoystickButtonPressed(unsigned joystick, unsigned button)
{
	if(button == 0)
		this->onKeyPressed(Keyboard::KEY_1);
	else
		GameLogic::passJoystickButtonsToEnterReturnKeys(this, button);
}
