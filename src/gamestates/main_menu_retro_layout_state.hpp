/*
 * main_menu_retro_layout_state.hpp
 *
 *  Created on: 2 de ago de 2018
 *      Author: carlosfaruolo
 */

#ifndef MAIN_MENU_RETRO_LAYOUT_STATE_HPP_
#define MAIN_MENU_RETRO_LAYOUT_STATE_HPP_
#include <ciso646>

#include "fgeal/fgeal.hpp"
#include "fgeal/extra/game.hpp"
#include "fgeal/extra/sprite.hpp"
#include "fgeal/extra/gui.hpp"

#include "futil/language.hpp"

// fwd. declared
class Carse;

class MainMenuRetroLayoutState extends public fgeal::Game::State
{
	Carse& game;

	fgeal::Vector2D lastDisplaySize;

	std::string strTitle;

	// the background image
	fgeal::Image* imgBackground;

	// fonts
	fgeal::Font* fntTitle, *fntMain;

	struct SlotMenuItem extends fgeal::Button
	{
		fgeal::Sprite* icon;

		SlotMenuItem() : fgeal::Button(), icon(null) {}
		~SlotMenuItem();

		void packIcon(float paddingX, float paddingY);
		inline void packIcon(float padding) { packIcon(padding, padding); }
		void draw();
	}
	slotMenuItemRace, slotMenuItemVehicle, slotMenuItemCourse, slotMenuItemEditor, slotMenuItemSettings, slotMenuItemExit;

	fgeal::Sound* sndCursorMove, *sndCursorIn, *sndCursorOut;

	bool handleMouseReturnIfSelected(int mx, int my);

	public:
	MainMenuRetroLayoutState(Carse* game);
	~MainMenuRetroLayoutState();

	virtual int getId();

	virtual void initialize();
	virtual void onEnter();
	virtual void onLeave();

	virtual void onKeyPressed(fgeal::Keyboard::Key);
	virtual void onMouseButtonPressed(fgeal::Mouse::Button, int, int);
	virtual void onMouseMoved(int, int);
	virtual void onJoystickAxisMoved(unsigned, unsigned, float);
	virtual void onJoystickButtonPressed(unsigned, unsigned);

	virtual void render();
	virtual void update(float delta);
};

#endif /* MAIN_MENU_RETRO_LAYOUT_STATE_HPP_ */
