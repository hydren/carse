/*
 * main_menu_simple_list_state.cpp
 *
 *  Created on: 2 de ago de 2018
 *      Author: carlosfaruolo
 */

#include "main_menu_simple_list_state.hpp"

#include "carse.hpp"

#include "util/sizing.hpp"

using fgeal::Display;
using fgeal::Image;
using fgeal::Menu;
using fgeal::Font;
using fgeal::Color;
using fgeal::Keyboard;
using fgeal::Mouse;
using std::string;

int MainMenuSimpleListState::getId() { return Carse::MAIN_MENU_SIMPLE_LIST_STATE_ID; }

MainMenuSimpleListState::MainMenuSimpleListState(Carse* game)
: State(*game), game(*game), lastDisplaySize(),
  imgBackground(null), fntTitle(null), fntMenu(null),
  sndCursorMove(null), sndCursorIn(null), sndCursorOut(null)
{}

MainMenuSimpleListState::~MainMenuSimpleListState()
{
	if(imgBackground != null) delete imgBackground;
	if(fntTitle != null) delete fntTitle;
	if(fntMenu != null) delete fntMenu;
}

void MainMenuSimpleListState::initialize()
{
	fntMenu = new Font(gameFontParams("ui_main_menu_items"));
	menu.setFont(fntMenu);
	menu.setColor(Color::WHITE);
	menu.backgroundColor = Color::BLACK.getTransparent(192);
	menu.borderColor = Color::_TRANSPARENT;
	menu.focusedEntryFontColor = Color::NAVY;
	menu.addEntry("Start race");
	menu.addEntry("Vehicle selection");
	menu.addEntry("Course selection");
	menu.addEntry("Editor");
	menu.addEntry("Options");
	menu.addEntry("Exit");

	imgBackground = new Image("assets/ui/title-bg.png");

	// loan some shared resources
	sndCursorMove = &game.sharedResources->sndCursorMove;
	sndCursorIn   = &game.sharedResources->sndCursorIn;
	sndCursorOut  = &game.sharedResources->sndCursorOut;

	fntTitle = new Font(gameFontParams("ui_main_menu_title"));
	strTitle = "Carse Project";
}

void MainMenuSimpleListState::onEnter()
{
	Display& display = game.getDisplay();

	// reload fonts if display size changed
	if(lastDisplaySize.x != display.getWidth() or lastDisplaySize.y != display.getHeight())
	{
		const FontSizer fs(display.getHeight());
		fntTitle->setSize(fs(game.logic.getFontSize("ui_main_menu_title")));
		fntMenu->setSize(fs(game.logic.getFontSize("ui_main_menu_items")));
		lastDisplaySize.x = display.getWidth();
		lastDisplaySize.y = display.getHeight();
	}

	menu.bounds.w = 0.33f * display.getWidth();
	menu.bounds.h = fntMenu->getTextHeight() * (1 + menu.getEntries().size());
	menu.bounds.x = 0.05f * display.getWidth();
	menu.bounds.y = 0.95f * display.getHeight() - menu.bounds.h;
}

void MainMenuSimpleListState::onLeave()
{}

void MainMenuSimpleListState::render()
{
	Display& display = game.getDisplay();
	display.clear();
	const float bgScale = display.getHeight()/(float)imgBackground->getHeight();
	imgBackground->drawScaled((display.getWidth() - imgBackground->getWidth()*bgScale)/3, 0, bgScale, bgScale);
	fntTitle->drawText(strTitle, 0.8*(display.getWidth() - fntTitle->getTextWidth(strTitle)), 0.2*(display.getHeight() - fntTitle->getTextHeight()), Color::WHITE);
	menu.draw();
}

void MainMenuSimpleListState::update(float delta)
{}

void MainMenuSimpleListState::onKeyPressed(Keyboard::Key key)
{
	switch(key)
	{
		case Keyboard::KEY_ESCAPE:
			game.running = false;
			break;

		case Keyboard::KEY_ENTER:
			sndCursorIn->play();
			switch(menu.getSelectedIndex())
			{
				case MENU_ITEM_RACE:     game.enterState(Carse::RACE_STATE_ID); break;
				case MENU_ITEM_VEHICLE:  game.enterState(Carse::VEHICLE_SELECTION_SIMPLE_LIST_STATE_ID); break;
				case MENU_ITEM_COURSE:   game.enterState(Carse::COURSE_SELECTION_STATE_ID); break;
				case MENU_ITEM_EDITOR:   game.enterState(Carse::COURSE_EDITOR_STATE_ID); break;
				case MENU_ITEM_SETTINGS: game.enterState(Carse::OPTIONS_MENU_STATE_ID); break;
				case MENU_ITEM_EXIT:     game.running = false; break;
				default: break;
			}
			break;

		case Keyboard::KEY_ARROW_UP:
			menu.moveCursorUp();
			sndCursorMove->play();
			break;

		case Keyboard::KEY_ARROW_DOWN:
			menu.moveCursorDown();
			sndCursorMove->play();
			break;

		case Keyboard::KEY_2:
			game.logic.currentMainMenuStateId = Carse::MAIN_MENU_CLASSIC_LAYOUT_STATE_ID;
			game.enterState(game.logic.currentMainMenuStateId);
			break;

		case Keyboard::KEY_0:  // debug radio state
			game.enterState(Carse::RADIO_SELECTION_STATE_ID);
			break;

		case Keyboard::KEY_9:  // debug race results state
			game.enterState(Carse::RACE_RESULTS_STATE_ID);
			break;

		case Keyboard::KEY_NUMPAD_SUBTRACTION:  // debug side drag race state
			game.enterState(Carse::SIDE_DRAG_RACE_STATE_ID);
			break;

		case Keyboard::KEY_NUMPAD_MULTIPLICATION:  // debug workshop state
			game.enterState(Carse::DEBUG_WORKSHOP_STATE_ID);
			break;

		default:break;
	}
}

void MainMenuSimpleListState::onMouseButtonPressed(Mouse::Button button, int x, int y)
{
	if(button == fgeal::Mouse::BUTTON_LEFT)
	{
		if(menu.bounds.contains(x, y))
		{
			if(menu.getIndexAtLocation(x, y) == menu.getSelectedIndex())
			{
				sndCursorIn->play();
				this->onKeyPressed(Keyboard::KEY_ENTER);
			}
			else
			{
				sndCursorMove->play();
				menu.setSelectedIndexByLocation(x, y);
			}
		}
	}
}

void MainMenuSimpleListState::onMouseWheelMoved(int amount)
{
	if(menu.bounds.contains(Mouse::getPosition())) while(amount != 0)
	{
		if(amount > 0)
		{
			menu.moveCursorUp();
			amount--;
		}
		else
		{
			menu.moveCursorDown();
			amount++;
		}
	}
}

void MainMenuSimpleListState::onJoystickAxisMoved(unsigned joystick, unsigned axis, float value)
{
	GameLogic::passJoystickAxisToKeyboardArrows(this, axis, value);
}

void MainMenuSimpleListState::onJoystickButtonPressed(unsigned joystick, unsigned button)
{
	if(button == 0)
		this->onKeyPressed(Keyboard::KEY_2);
	else
		GameLogic::passJoystickButtonsToEnterReturnKeys(this, button);
}
