/*
 * options_menu_state.cpp
 *
 *  Created on: 5 de set de 2017
 *      Author: carlosfaruolo
 */

#include "options_menu_state.hpp"

#include "carse.hpp"
#include "race_hud.hpp"

#include "util/sizing.hpp"

#include "futil/string_actions.hpp"

using fgeal::Image;
using fgeal::Font;
using fgeal::Sound;
using fgeal::Color;
using fgeal::Menu;
using fgeal::Event;
using fgeal::EventQueue;
using fgeal::Keyboard;
using fgeal::Display;
using fgeal::Vector2D;
using fgeal::Rectangle;
using fgeal::Graphics;
using fgeal::Mouse;

using std::string;
using std::vector;

int OptionsMenuState::getId() { return Carse::OPTIONS_MENU_STATE_ID; }

OptionsMenuState::OptionsMenuState(Carse* game)
: State(*game), game(*game), focusedMenu(&mainMenu), background(null),
  sndCursorMove(null), sndCursorIn(null), sndCursorOut(null),
  titleFont(null), menuFont(null), versionFont(null)
{
	menuList.push_back(&mainMenu);
	menuList.push_back(&gameplayMenu);
	menuList.push_back(&unitsMenu);
	menuList.push_back(&videoMenu);
	menuList.push_back(&resolutionMenu);
	menuList.push_back(&interfaceMenu);
	menuList.push_back(&hudMenu);
}

OptionsMenuState::~OptionsMenuState()
{
	if(background != null) delete background;
	if(titleFont != null) delete titleFont;
	if(menuFont != null) delete menuFont;
	if(versionFont != null) delete versionFont;
}

struct ChangeFocusedMenuEntryActions extends EntryActions {
	const string fixedLabel; ActiveMenu*& focus, &target;
	ChangeFocusedMenuEntryActions(const string& fixedLabel, ActiveMenu*& focus, ActiveMenu& target)
	: fixedLabel(fixedLabel), focus(focus), target(target) {}
	string getUpdatedLabel() { return fixedLabel; }
	void onTrigger(bool backwards) { if(not backwards) focus = &target; }
};

void OptionsMenuState::initialize()
{
	Display& display = game.getDisplay();

	titleFont = new Font(gameFontParams("ui_options_menu_title"));
	menuFont = new Font(gameFontParams("ui_options_menu_items"));
	versionFont = new Font(gameFontParams("default"));

	// loan some shared resources
	sndCursorMove = &game.sharedResources->sndCursorMove;
	sndCursorIn   = &game.sharedResources->sndCursorIn;
	sndCursorOut  = &game.sharedResources->sndCursorOut;

	labelTitle.text.setFont(titleFont);
	labelTitle.text.setColor(Color::WHITE);
	labelTitle.text.setContent("Options");

	versionLabel.text.setFont(versionFont);
	versionLabel.text.setContent("Version "+CARSE_VERSION_STRING+" (using fgeal "+fgeal::VERSION+" on "+fgeal::ADAPTED_LIBRARY_NAME+" "+fgeal::ADAPTED_LIBRARY_VERSION+")");
	versionLabel.text.setColor(~Color::CREAM);
	versionLabel.pack();

	for(unsigned i = 0; i < menuList.size(); i++)
	{
		Menu& menu = *menuList[i];
		menu.setFont(menuFont);
		menu.title.setColor(~Color::CREAM);
		menu.backgroundColor = Color::create(0, 0, 0, 128);
		menu.text.setColor(Color::WHITE);
		menu.focusedEntryBgColor = Color::create(16, 24, 192);
		menu.focusedEntryFontColor = Color::WHITE;
		menu.borderDisabled = true;
		menu.entryOverflowBehavior = fgeal::TextField::OVERFLOW_ABBREVIATE_TRAILING;
	}

	mainMenu.addEntry(new ChangeFocusedMenuEntryActions("Gameplay >", focusedMenu, gameplayMenu));

	gameplayMenu.userData = &mainMenu;
	gameplayMenu.title.setContent("Gameplay options");

	struct SimulationModeEntryActions extends EntryActions {
		GameLogic& logic;
		SimulationModeEntryActions(GameLogic& logic) : logic(logic) {}
		string getUpdatedLabel()
		{
			string tmp("Simulation mode: ");
			switch(logic.nextMatch.race.simulationType)
			{
				default:
				case Mechanics::SIMULATION_TYPE_SLIPLESS:       return tmp + "slipless";
				case Mechanics::SIMULATION_TYPE_WHEEL_LOAD_CAP: return tmp + "slipless, grip-limited power";
				case Mechanics::SIMULATION_TYPE_PACEJKA_BASED:  return tmp + "longitudinal slip (experimental)";
			}
		}
		void onTrigger(bool backwards)
		{
			const int value = logic.nextMatch.race.simulationType + (backwards? -1 : 1);
			logic.nextMatch.race.simulationType = static_cast<Mechanics::SimulationType>(value < 0? Mechanics::SIMULATION_TYPE_COUNT - 1 : value >= Mechanics::SIMULATION_TYPE_COUNT? 0 : value);
		}
	};
	gameplayMenu.addEntry(new SimulationModeEntryActions(game.logic));

	struct EnableJumpEntryActions extends EntryActions {
		GameLogic& logic;
		EnableJumpEntryActions(GameLogic& logic) : logic(logic) {}
		string getUpdatedLabel() { return "Enable jumps (experimental): " + string(logic.nextMatch.race.enableJumpSimulation? "yes" : "no"); }
		void onTrigger(bool) { logic.nextMatch.race.enableJumpSimulation = !logic.nextMatch.race.enableJumpSimulation; }
	};
	gameplayMenu.addEntry(new EnableJumpEntryActions(game.logic));

	gameplayMenu.addEntry(new ChangeFocusedMenuEntryActions("Set units >", focusedMenu, unitsMenu));

	unitsMenu.userData = &gameplayMenu;
	unitsMenu.title.setContent("Units options");

	struct DistanceUnitEntryActions extends EntryActions {
		bool& isImperial, &isImperialUI;
		DistanceUnitEntryActions(bool& isDistanceUnitImperial, bool& isUiDistanceUnitImperial) : isImperial(isDistanceUnitImperial), isImperialUI(isUiDistanceUnitImperial) {}
		string getUpdatedLabel() { return "Distance/speed unit: " + string(isImperial? "imperial" : "metric"); }
		void onTrigger(bool) { isImperialUI = isImperial = !isImperial; }
	};
	unitsMenu.addEntry(new DistanceUnitEntryActions(game.logic.nextMatch.race.isImperialUnit, game.logic.uiUnits.isImperialDistance));

	struct PowerUnitModeEntryActions extends EntryActions {
		GameLogic::PowerUnit& uiUnitPower;
		PowerUnitModeEntryActions(GameLogic::PowerUnit& uiUnitPower) : uiUnitPower(uiUnitPower) {}
		string getUpdatedLabel()
		{
			string tmp("Power unit: ");
			switch(uiUnitPower)
			{
				default:
				case GameLogic::UI_UNIT_POWER_HP: return tmp + "horsepower (hp)";
				case GameLogic::UI_UNIT_POWER_PS: return tmp + "DIN metric horsepower (ps)";
				case GameLogic::UI_UNIT_POWER_KW: return tmp + "kilowatt (kW)";
			}
		}
		void onTrigger(bool backwards)
		{
			const int value = uiUnitPower + (backwards? -1 : 1);
			uiUnitPower = static_cast<GameLogic::PowerUnit>(value < 0? GameLogic::UI_UNIT_POWER_COUNT - 1 : value >= GameLogic::UI_UNIT_POWER_COUNT? 0 : value);
		}
	};
	unitsMenu.addEntry(new PowerUnitModeEntryActions(game.logic.uiUnits.power));

	struct TorqueUnitModeEntryActions extends EntryActions {
		GameLogic::TorqueUnit& uiUnitTorque;
		TorqueUnitModeEntryActions(GameLogic::TorqueUnit& uiUnitTorque) : uiUnitTorque(uiUnitTorque) {}
		string getUpdatedLabel()
		{
			string tmp("Power unit: ");
			switch(uiUnitTorque)
			{
				default:
				case GameLogic::UI_UNIT_TORQUE_LBFT: return tmp + "pound-foot (lb-ft)";
				case GameLogic::UI_UNIT_TORQUE_KGFM: return tmp + "kilogram-force meter (kgf.m)";
				case GameLogic::UI_UNIT_TORQUE_NM:   return tmp + "newton-meter (Nm)";
			}
		}
		void onTrigger(bool backwards)
		{
			const int value = uiUnitTorque + (backwards? -1 : 1);
			uiUnitTorque = static_cast<GameLogic::TorqueUnit>(value < 0? GameLogic::UI_UNIT_TORQUE_COUNT - 1 : value >= GameLogic::UI_UNIT_TORQUE_COUNT? 0 : value);
		}
	};
	unitsMenu.addEntry(new TorqueUnitModeEntryActions(game.logic.uiUnits.torque));

	struct EngineDisplacementUnitEntryActions extends EntryActions {
		GameLogic::EngineDisplacementUnit& uiUnitEngineDisplacement;
		EngineDisplacementUnitEntryActions(GameLogic::EngineDisplacementUnit& uiUnitEngineDisplacement)
		: uiUnitEngineDisplacement(uiUnitEngineDisplacement) {}
		string getUpdatedLabel()
		{
			string tmp("Engine displ. unit: ");
			switch(uiUnitEngineDisplacement)
			{
				default:
				case GameLogic::UI_UNIT_ENGDISP_LITRE: return tmp + "litre (L)";
				case GameLogic::UI_UNIT_ENGDISP_CC: return tmp + "cubic centimetre (cc)";
				case GameLogic::UI_UNIT_ENGDISP_CUIN: return tmp + "cubic inches (cu in)";
				case GameLogic::UI_UNIT_ENGDISP_CID: return tmp + "cubic inches (cid)";
			}
		}
		void onTrigger(bool backwards)
		{
			const int value = uiUnitEngineDisplacement + (backwards? -1 : 1);
			uiUnitEngineDisplacement = static_cast<GameLogic::EngineDisplacementUnit>(value < 0? GameLogic::UI_UNIT_ENGDISP_COUNT - 1 : value >= GameLogic::UI_UNIT_ENGDISP_COUNT? 0 : value);
		}
	};
	unitsMenu.addEntry(new EngineDisplacementUnitEntryActions(game.logic.uiUnits.engineDisplacement));

	struct WeightUnitEntryActions extends EntryActions {
		bool& isImperial;
		WeightUnitEntryActions(bool& isWeightUnitImperial) : isImperial(isWeightUnitImperial) {}
		string getUpdatedLabel() { return "Weight unit: " + string(isImperial? "imperial (lb)" : "metric (kg)"); }
		void onTrigger(bool) { isImperial = !isImperial; }
	};
	unitsMenu.addEntry(new WeightUnitEntryActions(game.logic.uiUnits.isImperialWeight));

	unitsMenu.addEntry(new ChangeFocusedMenuEntryActions("< Back", focusedMenu, gameplayMenu));

	struct LimitSteeringWheelAngularSpeedEntryActions extends EntryActions {
		GameLogic& logic;
		LimitSteeringWheelAngularSpeedEntryActions(GameLogic& logic) : logic(logic) {}
		string getUpdatedLabel() { return "Steering speed limit: " + string(logic.nextMatch.race.disableSteeringSpeedLimit? "disabled" : "enabled"); }
		void onTrigger(bool) { logic.nextMatch.race.disableSteeringSpeedLimit = !logic.nextMatch.race.disableSteeringSpeedLimit; }
	};
	gameplayMenu.addEntry(new LimitSteeringWheelAngularSpeedEntryActions(game.logic));

	gameplayMenu.addEntry(new ChangeFocusedMenuEntryActions("< Back", focusedMenu, mainMenu));

	mainMenu.addEntry(new ChangeFocusedMenuEntryActions("Video >", focusedMenu, videoMenu));

	videoMenu.userData = &mainMenu;
	videoMenu.title.setContent("Video options");

	struct ResolutionEntryActions extends ChangeFocusedMenuEntryActions {
		Display& display;
		ResolutionEntryActions(Display& display, ActiveMenu*& focus, ActiveMenu& target)
		: ChangeFocusedMenuEntryActions("", focus, target), display(display) {}
		string getUpdatedLabel() { return "Resolution: " + futil::to_string(display.getWidth()) + "x" + futil::to_string(display.getHeight()); }
	};
	videoMenu.addEntry(new ResolutionEntryActions(display, focusedMenu, resolutionMenu));

	resolutionMenu.userData = &videoMenu;
	resolutionMenu.title.setContent("Choose resolution");

	struct ResolutionMenuEntryActions extends EntryActions {
		const string desc;
		const Display::Mode mode;
		OptionsMenuState& state;
		ResolutionMenuEntryActions(const Display::Mode& mode, OptionsMenuState& state)
		: desc(futil::to_string(mode.width)+"x"+futil::to_string(mode.height)
				+ " ("+futil::to_string(mode.aspectRatio.first)+":"+futil::to_string(mode.aspectRatio.second)+")"
				+ (mode.description.empty()? "" : " ("+mode.description+")")), mode(mode), state(state) {}
		string getUpdatedLabel() { return desc; }
		void onTrigger(bool backwards)
		{
			if(not backwards)
			{
				state.game.getDisplay().setSize(mode.width, mode.height);
				state.updateGeometry();
				state.videoMenu.updateLabels();
				state.focusedMenu = &state.videoMenu;
			}
		}
	};
	{
		vector<Display::Mode> modes = Display::Mode::getList();
		for(unsigned i = 0; i < modes.size(); i++)
			resolutionMenu.addEntry(new ResolutionMenuEntryActions(modes[i], *this));
	}

	struct FullscreenEntryActions extends EntryActions {
		Display& display;
		FullscreenEntryActions(Display& display) : display(display) {}
		string getUpdatedLabel() { return "Fullscreen: " + string(display.isFullscreen()? "yes" : "no"); }
		void onTrigger(bool) { display.setFullscreen(!display.isFullscreen()); }
	};
	videoMenu.addEntry(new FullscreenEntryActions(display));

	struct CacheGaugeTextureEntryActions extends EntryActions {
		bool& useCachedDialGauge;
		CacheGaugeTextureEntryActions(bool& useCachedDialGauge) : useCachedDialGauge(useCachedDialGauge) {}
		string getUpdatedLabel() { return "Enable dial gauge texture caching: " + string(useCachedDialGauge? "yes" : "no"); }
		void onTrigger(bool) { useCachedDialGauge = !useCachedDialGauge; }
	};
	videoMenu.addEntry(new CacheGaugeTextureEntryActions(game.logic.nextMatch.race.useCachedDialGauge));

	struct SmoothSpritesEntryActions extends EntryActions {
		string getUpdatedLabel() { return "Enable sprite smoothing (if possible): " + string(Image::useImageTransformSmoothingHint? "yes" : "no"); }
		void onTrigger(bool) { Image::useImageTransformSmoothingHint = !Image::useImageTransformSmoothingHint; }
	};
	videoMenu.addEntry(new SmoothSpritesEntryActions());

	struct FasterPrimitivesEntryActions extends EntryActions {
		string getUpdatedLabel() { return "Enable faster gfx primitives (if possible): " + string(Graphics::useFasterPrimitivesHint? "yes" : "no"); }
		void onTrigger(bool) { Graphics::useFasterPrimitivesHint = !Graphics::useFasterPrimitivesHint; }
	};
	videoMenu.addEntry(new FasterPrimitivesEntryActions());

	struct ShowFpsEntryActions extends EntryActions {
		Carse& game;
		ShowFpsEntryActions(Carse& game) : game(game) {}
		string getUpdatedLabel() { return "Show FPS: " + string(game.showFPS? "yes" : "no"); }
		void onTrigger(bool) { game.showFPS = !game.showFPS; }
	};
	videoMenu.addEntry(new ShowFpsEntryActions(game));

	videoMenu.addEntry(new ChangeFocusedMenuEntryActions("< Back", focusedMenu, mainMenu));

	struct VolumeEntryActions extends EntryActions {
		Carse& game;
		VolumeEntryActions(Carse& game) : game(game) {}
		string getUpdatedLabel() { return "Volume: " + futil::to_string(std::floor(100*game.logic.masterVolume)) + "%"; }
		void onTrigger(bool backwards)
		{
			if((not backwards and game.logic.masterVolume < 1.0) or (backwards and game.logic.masterVolume > 0))
			{
				game.logic.masterVolume = (std::floor(100*game.logic.masterVolume) + (backwards? -5 : 5))/100.f;
				game.sharedResources->sndCursorIn.setVolume(game.logic.masterVolume);
				game.sharedResources->sndCursorOut.setVolume(game.logic.masterVolume);
				game.sharedResources->sndCursorMove.setVolume(game.logic.masterVolume);
			}
		}
	};
	mainMenu.addEntry(new VolumeEntryActions(game));

	mainMenu.addEntry(new ChangeFocusedMenuEntryActions("Interface >", focusedMenu, interfaceMenu));

	interfaceMenu.userData = &mainMenu;
	interfaceMenu.title.setContent("Interface options");

	struct CameraBehaviorEntryActions extends EntryActions {
		Pseudo3DCourseAnimation::CameraBehavior& cameraBehavior;
		CameraBehaviorEntryActions(Pseudo3DCourseAnimation::CameraBehavior& cameraBehavior) : cameraBehavior(cameraBehavior) {}
		string getUpdatedLabel()
		{
			string tmp("Camera behavior: ");
			switch(cameraBehavior)
			{
				default:
				case Pseudo3DCourseAnimation::CAMERA_BEHAVIOR_TYPE_A: return tmp + "Type A";
				case Pseudo3DCourseAnimation::CAMERA_BEHAVIOR_TYPE_B: return tmp + "Type B";
				case Pseudo3DCourseAnimation::CAMERA_BEHAVIOR_TYPE_C: return tmp + "Type C";
			}
		}
		void onTrigger(bool backwards)
		{
			const int value = cameraBehavior + (backwards? -1 : 1);
			cameraBehavior = static_cast<Pseudo3DCourseAnimation::CameraBehavior>(value < 0? Pseudo3DCourseAnimation::CAMERA_BEHAVIOR_TYPE_COUNT - 1 : value >= Pseudo3DCourseAnimation::CAMERA_BEHAVIOR_TYPE_COUNT? 0 : value);
		}
	};
	interfaceMenu.addEntry(new CameraBehaviorEntryActions(game.logic.nextMatch.race.cameraBehavior));

	struct HudLayoutEntryActions extends ChangeFocusedMenuEntryActions {
		string& selectedHudLayoutFilename;
		HudLayoutEntryActions(string& selectedHudLayoutFilename, ActiveMenu*& focus, ActiveMenu& target)
		: ChangeFocusedMenuEntryActions("", focus, target), selectedHudLayoutFilename(selectedHudLayoutFilename) {}
		string getUpdatedLabel()
		{
			if(selectedHudLayoutFilename.empty())
				return "HUD layout: ???";
			else
			{
				string value = selectedHudLayoutFilename;
				value = value.substr(value.find_last_of("/\\") + 1);
				return "HUD layout: " + value;
			}
		}
	};
	interfaceMenu.addEntry(new HudLayoutEntryActions(game.logic.nextMatch.dashboardLayoutFilename, focusedMenu, hudMenu));

	hudMenu.userData = &interfaceMenu;
	hudMenu.title.setContent("Choose HUD type");

	struct HudLayoutItemEntryActions extends EntryActions {
		const string filename, description;
		string& selectedHudLayoutFilename;
		OptionsMenuState& state;
		inline static string readable(const string& filename)
		{
			const string desc = filename.substr(filename.find_last_of("/\\") + 1);
			return desc.substr(0, desc.size()-string(".properties").size());
		}
		HudLayoutItemEntryActions(const string& filename, string& selected, OptionsMenuState& state)
		: filename(filename), description(readable(filename)), selectedHudLayoutFilename(selected), state(state) {}
		string getUpdatedLabel() { return description; }
		void onTrigger(bool backwards)
		{
			if(not backwards)
			{
				selectedHudLayoutFilename = filename;
				state.interfaceMenu.updateLabels();
				state.focusedMenu = &state.interfaceMenu;
			}
		}
	};
	{
		const vector<string>& layouts = game.logic.getDashboardLayouts();
		const_foreach(const string&, layout, vector<string>, layouts)
			hudMenu.addEntry(new HudLayoutItemEntryActions(layout, game.logic.nextMatch.dashboardLayoutFilename, *this));
	}

	struct InteriorViewEntryActions extends EntryActions {
		bool& isInteriorView;
		InteriorViewEntryActions(bool& isInteriorView) : isInteriorView(isInteriorView) {}
		string getUpdatedLabel() { return "Cockpit view mode: " + string(isInteriorView? "yes" : "no"); }
		void onTrigger(bool) { isInteriorView = !isInteriorView; }
	};
	interfaceMenu.addEntry(new InteriorViewEntryActions(game.logic.nextMatch.race.isCockpitView));

	interfaceMenu.addEntry(new ChangeFocusedMenuEntryActions("< Back", focusedMenu, mainMenu));

	struct BackToMainEntryActions extends EntryActions {
		Carse& game;
		BackToMainEntryActions(Carse& game) : game(game) {}
		string getUpdatedLabel() { return "Back to main menu"; }
		void onTrigger(bool backwards) { if(not backwards) game.enterState(game.logic.currentMainMenuStateId); }
	};
	mainMenu.addEntry(new BackToMainEntryActions(game));

	// update labels once setup menu items
	for(unsigned i = 0; i < menuList.size(); i++)
		menuList[i]->updateLabels();
}

void OptionsMenuState::onEnter()
{
	static bool once = true;
	if(once) updateGeometry(), once = false;
	focusedMenu = &mainMenu;

	for(unsigned i = 0; i < menuList.size(); i++)
		menuList[i]->setSelectedIndex(0);
}

void OptionsMenuState::updateGeometry()
{
	Display& display = game.getDisplay();
	const FontSizer fs(display.getHeight());
	titleFont->setSize(fs(game.logic.getFontSize("ui_options_menu_title")));
	menuFont->setSize(fs(game.logic.getFontSize("ui_options_menu_items")));

	// update menus bounds
	const Rectangle menuBounds = {0.05f * display.getWidth(), 0.3f * display.getHeight(),
	                              0.90f * display.getWidth(), 0.6f * display.getHeight()};

	for(unsigned i = 0; i < menuList.size(); i++)
	{
		menuList[i]->bounds = menuBounds;
		menuList[i]->updateDrawableText();
	}

	labelTitle.pack();
	labelTitle.bounds.x = display.getWidth()/2 - labelTitle.bounds.w/2;
	labelTitle.bounds.y = display.getHeight()*0.2f - labelTitle.bounds.h;

	versionLabel.bounds.x = display.getWidth() - versionLabel.bounds.w - 4;
	versionLabel.bounds.y = display.getHeight() - versionLabel.bounds.h - 4;

	Image tmpBgImage("assets/ui/options-bg.jpg");
	if(background != null) delete background;
	background = new Image(display.getWidth(), display.getHeight());
	tmpBgImage.blit(*background, 0, 0, display.getWidth() / (float) tmpBgImage.getWidth(), display.getHeight() / (float) tmpBgImage.getHeight());
}

void OptionsMenuState::onLeave()
{}

void OptionsMenuState::update(float delta)
{}

void OptionsMenuState::render()
{
	game.getDisplay().clear();
	background->draw();
	focusedMenu->draw();
	labelTitle.draw();
	versionLabel.draw();
}

void OptionsMenuState::onKeyPressed(Keyboard::Key key)
{
	switch(key)
	{
		case Keyboard::KEY_ESCAPE:
			sndCursorOut->play();
			if(focusedMenu->userData == null)
				game.enterState(game.logic.currentMainMenuStateId);
			else
				focusedMenu = static_cast<ActiveMenu*>(focusedMenu->userData);  // go to parent menu
			break;
		case Keyboard::KEY_ARROW_UP:
			sndCursorMove->play();
			focusedMenu->moveCursorUp();
			break;
		case Keyboard::KEY_ARROW_DOWN:
			sndCursorMove->play();
			focusedMenu->moveCursorDown();
			break;
		case Keyboard::KEY_ARROW_LEFT:
			sndCursorMove->play();
			focusedMenu->triggerSelectedEntry(true);
			focusedMenu->updateLabel(focusedMenu->getSelectedIndex());
			break;
		case Keyboard::KEY_ARROW_RIGHT:
			sndCursorMove->play();
			focusedMenu->triggerSelectedEntry();
			focusedMenu->updateLabel(focusedMenu->getSelectedIndex());
			break;
		case Keyboard::KEY_ENTER:
			sndCursorIn->play();
			focusedMenu->triggerSelectedEntry();
			focusedMenu->updateLabel(focusedMenu->getSelectedIndex());
			break;
		case Keyboard::KEY_M:
			Graphics::drawFilledRectangle(game.getDisplay().getWidth()/3, 3*game.getDisplay().getHeight()/7, game.getDisplay().getWidth()/2, game.getDisplay().getHeight()/7, Color::BLUE);
			menuFont->drawText("  Reloading maps, please wait...", game.getDisplay().getWidth()/3,  game.getDisplay().getHeight()/2, Color::YELLOW);
			game.getDisplay().refresh();
			game.logic.updateCourseList();
			break;
		case Keyboard::KEY_K:
			Graphics::drawFilledRectangle(game.getDisplay().getWidth()/3, 3*game.getDisplay().getHeight()/7, game.getDisplay().getWidth()/2, game.getDisplay().getHeight()/7, Color::BLUE);
			menuFont->drawText("  Reloading cars, please wait...", game.getDisplay().getWidth()/3,  game.getDisplay().getHeight()/2, Color::YELLOW);
			game.getDisplay().refresh();
			game.logic.updateVehicleList();
			break;
		default:
			break;
	}
}

void OptionsMenuState::onMouseButtonPressed(Mouse::Button button, int x, int y)
{
	sndCursorIn->play();
	if(focusedMenu->bounds.contains(x, y))
	{
		focusedMenu->setSelectedIndexByLocation(x, y);
		focusedMenu->triggerSelectedEntry(button == Mouse::BUTTON_RIGHT);
		focusedMenu->updateLabel(focusedMenu->getSelectedIndex());
	}
	else focusedMenu = &mainMenu;
}

void OptionsMenuState::onMouseMoved(int x, int y)
{
	focusedMenu->setSelectedIndexByLocation(x, y);
}

void OptionsMenuState::onMouseWheelMoved(int amount)
{
	while(amount != 0)
	{
		if(amount > 0)
		{
			focusedMenu->moveCursorUp();
			amount--;
		}
		else
		{
			focusedMenu->moveCursorDown();
			amount++;
		}
	}
}

void OptionsMenuState::onJoystickAxisMoved(unsigned joystick, unsigned axis, float value)
{
	GameLogic::passJoystickAxisToKeyboardArrows(this, axis, value);
}

void OptionsMenuState::onJoystickButtonPressed(unsigned joystick, unsigned button)
{
	GameLogic::passJoystickButtonsToEnterReturnKeys(this, button);
}
