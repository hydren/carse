/*
 * options_menu_state.hpp
 *
 *  Created on: 5 de set de 2017
 *      Author: carlosfaruolo
 */

#ifndef OPTIONS_MENU_STATE_HPP_
#define OPTIONS_MENU_STATE_HPP_
#include <ciso646>

#include <vector>

#include "fgeal/fgeal.hpp"
#include "fgeal/extra/game.hpp"
#include "fgeal/extra/gui.hpp"

#include "futil/language.hpp"

#include "util/active_menu.hpp"

// fwd. declared
class Carse;

class OptionsMenuState extends public fgeal::Game::State
{
	Carse& game;

	ActiveMenu* focusedMenu, mainMenu, gameplayMenu, unitsMenu, videoMenu, resolutionMenu, interfaceMenu, hudMenu;
	fgeal::Image* background;
	fgeal::Sound* sndCursorMove, *sndCursorIn, *sndCursorOut;
	fgeal::Font* titleFont, *menuFont, *versionFont;

	fgeal::Label labelTitle, versionLabel;

	std::vector<ActiveMenu*> menuList;

	public:
	virtual int getId();

	OptionsMenuState(Carse* game);
	~OptionsMenuState();

	virtual void initialize();
	virtual void onEnter();
	virtual void onLeave();

	virtual void render();
	virtual void update(float delta);

	virtual void onKeyPressed(fgeal::Keyboard::Key);
	virtual void onMouseButtonPressed(fgeal::Mouse::Button button, int x, int y);
	virtual void onMouseMoved(int x, int y);
	virtual void onMouseWheelMoved(int change);
	virtual void onJoystickAxisMoved(unsigned, unsigned, float);
	virtual void onJoystickButtonPressed(unsigned, unsigned);

	void updateGeometry();
};

#endif /* OPTIONS_MENU_STATE_HPP_ */
