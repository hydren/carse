/*
 * race_state.cpp
 *
 *  Created on: 29 de mar de 2017
 *      Author: carlosfaruolo
 */

#include "pseudo3d_race_state.hpp"

#include "carse.hpp"

#include "util/sizing.hpp"

#include "futil/random.h"
#include "futil/snprintf.h"
#include "futil/string_parse.hpp"
#include "futil/string_actions.hpp"

#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <cstdlib>

//XXX debug
//#include <iostream>
//using std::cout;
//using std::endl;

using std::string;
using std::map;
using std::vector;

using fgeal::Display;
using fgeal::Graphics;
using fgeal::Image;
using fgeal::Font;
using fgeal::Color;
using fgeal::Keyboard;
using fgeal::Event;
using fgeal::EventQueue;
using fgeal::Sound;
using fgeal::Music;
using fgeal::Sprite;
using fgeal::Point;
using fgeal::Vector2D;
using fgeal::Joystick;
using fgeal::Rectangle;

const float Pseudo3DRaceState::MAXIMUM_STRAFE_SPEED_FACTOR = 30;  // undefined unit

static const float MINIMUM_SPEED_TO_SIDESLIP = 5.5556,  // == 20kph
				   STEERING_SPEED = 5,
				   GLOBAL_VEHICLE_SCALE_FACTOR = 0.0048828125,
				   DEFAULT_CAMERA_PROJECTION_OFFSET = 4;

static const Pseudo3DCourse::Spec::RandomGenerationParameters RANDOM_GEN_PARAMS = { 1.5f, 6400, true, true, false };

static inline float computeCorneringStiffness(const float weight)
{
	return 0.575 + 0.575/(1+exp(-0.4*(10.0 - weight/1000.0)));
}

//----------------------------------------------------------------------------------------------------------------------

static void assignDefaultControls1(Pseudo3DRaceState::PlayerControls& playerControls)
{
	playerControls.accelerate.key = Keyboard::KEY_ARROW_UP;
	playerControls.braking.key = Keyboard::KEY_ARROW_DOWN;
	playerControls.upshift.key = Keyboard::KEY_LEFT_SHIFT;
	playerControls.downshift.key = Keyboard::KEY_LEFT_CONTROL;
	playerControls.turnLeft.key = Keyboard::KEY_ARROW_LEFT;
	playerControls.turnRight.key = Keyboard::KEY_ARROW_RIGHT;
	playerControls.powerBoost.key = Keyboard::KEY_N;
}

static void assignDefaultControls2(Pseudo3DRaceState::PlayerControls& playerControls)
{
	playerControls.accelerate.key = Keyboard::KEY_NUMPAD_8;
	playerControls.braking.key = Keyboard::KEY_NUMPAD_5;
	playerControls.upshift.key = Keyboard::KEY_NUMPAD_ADDITION;
	playerControls.downshift.key = Keyboard::KEY_NUMPAD_SUBTRACTION;
	playerControls.turnLeft.key = Keyboard::KEY_NUMPAD_4;
	playerControls.turnRight.key = Keyboard::KEY_NUMPAD_6;
	playerControls.powerBoost.key = Keyboard::KEY_NUMPAD_DECIMAL;
}

static void assignDefaultControls3(Pseudo3DRaceState::PlayerControls& playerControls)
{
	playerControls.accelerate.key = Keyboard::KEY_W;
	playerControls.braking.key = Keyboard::KEY_S;
	playerControls.upshift.key = Keyboard::KEY_X;
	playerControls.downshift.key = Keyboard::KEY_Z;
	playerControls.turnLeft.key = Keyboard::KEY_A;
	playerControls.turnRight.key = Keyboard::KEY_D;
	playerControls.powerBoost.key = Keyboard::KEY_C;
}

static void assignDefaultJoycontrol(Pseudo3DRaceState::PlayerControls& playerControls)
{
	playerControls.accelerate.joystickButtonIndex = 2;
	playerControls.braking.joystickButtonIndex = 3;
	playerControls.upshift.joystickButtonIndex = 5;
	playerControls.downshift.joystickButtonIndex = 4;

	playerControls.turnLeft.joystickButtonIsAxis = true;
	playerControls.turnLeft.joystickButtonIndex = 0;
	playerControls.turnLeft.joystickAxisSignal = -1;

	playerControls.turnRight.joystickButtonIsAxis = true;
	playerControls.turnRight.joystickButtonIndex = 0;
	playerControls.turnRight.joystickAxisSignal = +1;

	playerControls.powerBoost.joystickButtonIndex = 6;

	playerControls.deadzone = 0.2;
}

//----------------------------------------------------------------------------------------------------------------------

int Pseudo3DRaceState::getId(){ return Carse::RACE_STATE_ID; }

Pseudo3DRaceState::Pseudo3DRaceState(Carse* game)
: State(*game), game(*game), lastDisplaySize(),
  fontDebug(null), fontSpeedometer(null), fontSpeedUnit(null), fontGraduation(null), fontGraduationSmall(null),
  fontCountdown(null), fontTimers(null), fontLaps(null),
  imgStopwatch(null),
  music(null),
  sndCountdownBuzzer(null), sndCountdownBuzzerFinal(null),

  spriteSmoke(null), spriteBackfire(null),

  params(),
  coursePositionFactor(Pseudo3DCourse::COURSE_POSITION_FACTOR),
  courseStartPositionOffset(0), racersStartPositionOffset(1000),

  onSceneIntro(), timerSceneIntro(), countdownBuzzerCounter(),
  screenGridLines(), screenGridColumns(), acc0to60clock(0), acc0to60time(0),

  course(), racingVehicles(1),

  raceScreenData(1),

  posHudCountdown(), posHudFinishedCaption(),

  ai(course.spec, coursePositionFactor, onSceneIntro),

  firstCyclesControl(),

  debugMode(true)
{
	raceScreenData[0].dashboard.imgDashboard = null;
	raceScreenData[0].dashboard.imgSteeringWheel = null;
}

Pseudo3DRaceState::~Pseudo3DRaceState()
{
	const Font* const fontsToDelete[] = { fontSpeedometer, fontSpeedUnit, fontCountdown, fontTimers, fontLaps, fontGraduation, fontGraduationSmall, fontDebug };
	for(unsigned i = 0; i < sizeof(fontsToDelete)/sizeof(Font*); i++)
		if(fontsToDelete[i] != null)
			delete fontsToDelete[i];

	if(imgStopwatch != null) delete imgStopwatch;
	if(music != null) delete music;

	if(sndCountdownBuzzer != null) delete sndCountdownBuzzer;
	if(sndCountdownBuzzerFinal != null) delete sndCountdownBuzzerFinal;

	if(spriteSmoke != null) delete spriteSmoke;
	if(spriteBackfire != null) delete spriteBackfire;
}

void Pseudo3DRaceState::initialize()
{
	fontSpeedometer = new Font(gameFontParams("gui_speedometer"));
	fontSpeedUnit = new Font(gameFontParams("gui_speed_unit"));
	fontGraduation = new Font(gameFontParams("gui_dial_graduation"));
	fontGraduationSmall = new Font(gameFontParams("gui_dial_graduation"));
	fontCountdown = new Font(gameFontParams("gui_countdown"));
	fontTimers = new Font(gameFontParams("gui_timers"));
	fontLaps = new Font(gameFontParams("gui_lap_counter"));
	fontDebug = new Font(gameFontParams("default"));

	imgStopwatch = new Image("assets/hud/stopwatch.png");

	sndCountdownBuzzer = new Sound("assets/sound/countdown-buzzer.ogg");
	sndCountdownBuzzerFinal = new Sound("assets/sound/countdown-buzzer-final.ogg");

	spriteSmoke = new Sprite("assets/effects/smoke-sprite.png", 32, 32, 0.036);
	spriteBackfire = new Sprite("assets/effects/backfire-sprite.png", 32, 32, 0.005, 48);

	hudThrottleBar.min = 0;
	hudThrottleBar.max = 1;
	hudThrottleBar.style = Hud::GenericBarGauge::STYLE_FLAT;
	hudThrottleBar.borderDisabled = false;
	hudThrottleBar.borderColor = Color::BLACK.getTransparent();
	hudThrottleBar.fillColor = Color::GREEN.getTransparent();
	hudThrottleBar.backgroundDisabled = true;

	hudBrakeBar = hudThrottleBar;
	hudBrakeBar.fillColor = Color::RED.getTransparent();

	hudSteeringBar = hudThrottleBar;
	hudSteeringBar.min = -1;
	hudSteeringBar.backgroundDisabled = false;
	hudSteeringBar.backgroundColor = Color::BLUE.getTransparent(180);
	hudSteeringBar.fillColor = Color::AZURE.getTransparent(180);

	hudFuelAmount = hudThrottleBar;
	hudFuelAmount.fillColor = Color::PURPLE;

	minimap.geometryOtimizationEnabled = true;
	minimap.usethickLinePrimitive = true;
	minimap.color = Color::LIGHT_GREY;
	minimap.plottedPositions.resize(2);
	minimap.plottedPositions[0].shape = Pseudo3DCourseMap::PlottedPosition::SHAPE_BAR;
	minimap.plottedPositions[0].fillColor = Color::YELLOW;
	minimap.plottedPositions[0].borderEnabled = true;
	minimap.plottedPositions[0].borderColor = Color::BLACK;
	minimap.plottedPositions[1].shape = Pseudo3DCourseMap::PlottedPosition::SHAPE_CIRCLE;
	minimap.plottedPositions[1].borderEnabled = true;
	minimap.plottedPositions[1].borderColor = Color::BLACK;

	hudCurrentLap.text.setFont(fontLaps);
	hudCurrentLap.backgroundDisabled = true;
	hudCurrentLap.text.setColor(Color::WHITE);
	hudCurrentLap.borderDisabled = true;
	hudCurrentLap.padding = Vector2D::NULL_VECTOR;
	hudCurrentLap.borderThickness = 0;

	hudLapLabel.text.setFont(hudCurrentLap.text.getFont());
	hudLapLabel.backgroundDisabled = hudCurrentLap.backgroundDisabled;
	hudLapLabel.text.setColor(hudCurrentLap.text.getColor());
	hudLapLabel.borderDisabled = hudCurrentLap.borderDisabled;
	hudLapLabel.borderThickness = hudCurrentLap.borderThickness;

	hudLapCountGoal = hudLapLabel;

	hudLapLabel.text.setContent("Lap ");

	hudProgress = hudCurrentLap;

	hudProgressLabel = hudLapLabel;
	hudProgressLabel.text.setContent("Progress ");

	hudPlayerRanking = hudCurrentLap;

	hudPlayerRankingLabel = hudLapLabel;
	hudPlayerRankingLabel.text.setContent("Pos ");

	hudPlayerRankingGoal = hudPlayerRankingLabel;

	hudTimerCurrentLap.text.setFont(hudPlayerRanking.text.getFont());
	hudTimerCurrentLap.backgroundDisabled = hudPlayerRanking.backgroundDisabled;
	hudTimerCurrentLap.text.setColor(hudPlayerRanking.text.getColor());
	hudTimerCurrentLap.borderDisabled = hudPlayerRanking.borderDisabled;
	hudTimerCurrentLap.padding = hudPlayerRanking.padding;
	hudTimerCurrentLap.borderThickness = hudPlayerRanking.borderThickness;
	hudTimerCurrentLap.showMillisec = true;

	hudTimerCurrentLapLabel = hudLapLabel;
	hudTimerCurrentLapLabel.text.setContent("Time:");

	hudTimerLastLap = hudTimerCurrentLap;

	hudTimerLastLapLabel = hudLapLabel;
	hudTimerLastLapLabel.text.setContent("Last:");

	hudTimerBestLap = hudTimerCurrentLap;

	hudTimerBestLapLabel = hudLapLabel;
	hudTimerBestLapLabel.text.setContent("Best:");

	hudBoostCounter = hudCurrentLap;
	hudBoostCounter.text.setFont(fontSpeedUnit);

	hudBoostCounterLabel = hudLapLabel;
	hudBoostCounterLabel.text.setFont(fontSpeedUnit);
	hudBoostCounterLabel.text.setContent("Boosts:");

	hudMiniMapBgColor = Color::BLACK;
	hudMiniMapBgColor.a = 128;
}

void Pseudo3DRaceState::onEnter()
{
	Display& display = game.getDisplay();
	const float displayWidth = display.getWidth(),
				displayHeight = display.getHeight();

	params = game.logic.nextMatch.race;

	raceScreenData.resize(std::max(params.playerCount, 1u));  // there should be always at least one screen, even if there are no players

	screenGridLines = screenGridColumns = 1;
	for(;; screenGridColumns++)
		for(screenGridLines = screenGridColumns; screenGridLines <= screenGridColumns + 1; screenGridLines++)
			if(screenGridColumns * screenGridLines >= raceScreenData.size())
				goto afterGridCalculation;
	afterGridCalculation:
	const float screenWidth = displayWidth/screenGridColumns, screenHeight = displayHeight/screenGridLines;
	const FontSizer fs(screenHeight);

	// reload fonts if display size changed
	if(lastDisplaySize.x != screenWidth or lastDisplaySize.y != screenHeight)
	{
		fontSpeedUnit->setSize(fs(game.logic.getFontSize("gui_speed_unit")));
		fontSpeedometer->setSize(fs(game.logic.getFontSize("gui_speedometer")));
		fontGraduation->setSize(fs(game.logic.getFontSize("gui_dial_graduation")));
		fontGraduationSmall->setSize(fs(game.logic.getFontSize("gui_dial_graduation", "small")));
		fontCountdown->setSize(fs(game.logic.getFontSize("gui_countdown")));
		fontTimers->setSize(fs(game.logic.getFontSize("gui_timers")));
		fontLaps->setSize(fs(game.logic.getFontSize("gui_lap_counter")));
		lastDisplaySize.x = screenWidth;
		lastDisplaySize.y = screenHeight;
	}

	playersControls.resize(params.playerCount);

	// TODO load player controls from carse logic instead of hard-codding
	switch(playersControls.size())
	{
		default:
		case 3: assignDefaultControls3(playersControls[2]);  //@suppress("No break at end of case")
		case 2: assignDefaultControls2(playersControls[1]);  //@suppress("No break at end of case")
		case 1: assignDefaultControls1(playersControls[0]);  //@suppress("No break at end of case")
		case 0: break;
	}

	for(unsigned i = 0; i < playersControls.size(); i++)
	{
		assignDefaultJoycontrol(playersControls[i]);
		playersControls[i].joystickIndex = i - 1;
	}

	if(playersControls.size() == 1)
		playersControls[0].joystickIndex = 0;

	if(game.logic.nextMatch.isDebugCourseSpec)
	{
		course.loadSpec(Pseudo3DCourse::Spec::createDebug());
		params.raceType = RACE_TYPE_DEBUG;
	}
	else if(game.logic.nextMatch.isRandomCourseSpec)
	{
		Pseudo3DCourse::Spec spec = Pseudo3DCourse::Spec::createRandom(200, 3000, RANDOM_GEN_PARAMS);
		spec.copyRoadStyle(game.logic.getRandomPresetRoadStyle());
		spec.copyLandscapeStyle(game.logic.getRandomPresetLandscapeStyle());
		spec.musicFilename = game.logic.getMusicFilenames()[futil::random_between(0, game.logic.getMusicFilenames().size())];
		course.loadSpec(spec);
	}
	else
		course.loadSpec(game.logic.nextMatch.courseSpec);

	course.animation.drawAreaWidth = screenWidth;
	course.animation.drawAreaHeight = screenHeight;
	course.animation.drawDistance = 500;
	course.animation.cameraDepth = 0.84;
	course.animation.fogging = course.animation.pseudoFogging = course.animation.lightFiltering = 0;
	course.animation.isHeadlightsEffectEnabled = false;
	course.animation.isHeadlightsEffectFirstPerson = params.isCockpitView;
	course.animation.lengthScale = coursePositionFactor;
	course.animation.cameraHeight = params.isCockpitView? Pseudo3DCourseAnimation::INTERIOR_CAMERA_HEIGHT : Pseudo3DCourseAnimation::DEFAULT_CAMERA_HEIGHT;
	course.animation.cameraBehavior = params.cameraBehavior;
	if(params.isWeatherEffectEnforced)
		course.animation.spec.weatherEffect = params.forcedWeatherEffect;
	course.animation.vehicles.clear();

	trafficVehicles.resize(static_cast<unsigned>(params.trafficDensity * (course.spec.lines.size() * course.spec.roadSegmentLength)/1000.f));
	if(not trafficVehicles.empty())
	{
		const vector<Pseudo3DVehicle::Spec>& trafficVehicleSpecs = game.logic.getTrafficVehicleList();

		// used to point to the vehicle instances that will "own" its respective assets and share with other vehicles with same spec/skin
		vector< vector<Pseudo3DVehicle*> > allSharedVehicles(trafficVehicleSpecs.size());
		for(unsigned i = 0; i < allSharedVehicles.size(); i++)
			allSharedVehicles[i].resize(trafficVehicleSpecs[i].alternateSprites.size()+1, null);

		foreach(Pseudo3DVehicle&, trafficVehicle, vector<Pseudo3DVehicle>, trafficVehicles)
		{
			const unsigned trafficVehicleSpecIndex = futil::random_between(0, trafficVehicleSpecs.size());
			const Pseudo3DVehicle::Spec& spec = trafficVehicleSpecs[trafficVehicleSpecIndex];  // grab randomly chosen spec
			vector<Pseudo3DVehicle*>& sharedVehicles = allSharedVehicles[trafficVehicleSpecIndex];  // grab list of "base" vehicles to use their assets
			const int skinIndex = spec.alternateSprites.empty()? -1 : futil::random_between(-1, spec.alternateSprites.size());
			trafficVehicle.setSpec(spec, skinIndex);

			// if first instance of this spec/skin, load assets and record a pointer
			if(sharedVehicles[skinIndex+1] == null)
			{
				trafficVehicle.animation.loadAssetsData();
				sharedVehicles[skinIndex+1] = &trafficVehicle;
			}
			else  // if repeated spec/skin, use assets from other ("base") vehicle
				trafficVehicle.animation.useAssetsDataFrom(sharedVehicles[skinIndex+1]->animation);

			trafficVehicle.body.simulationType = Mechanics::SIMULATION_TYPE_SLIPLESS;
			trafficVehicle.body.automaticShiftingEnabled = true;
			trafficVehicle.body.gravityAccelerationFactor = GameLogic::PHYSICS_GRAVITY_ACCELERATION_DEFAULT;
			trafficVehicle.corneringStiffness = computeCorneringStiffness(trafficVehicle.body.spec.mass * trafficVehicle.body.gravityAccelerationFactor);
		}

		// apply screen scale to traffic sprites
		foreach(vector<Pseudo3DVehicle*>&, sharedVehiclesOfSpec, vector< vector<Pseudo3DVehicle*> >, allSharedVehicles)
			foreach(Pseudo3DVehicle*, sharedVehicle, vector<Pseudo3DVehicle*>, sharedVehiclesOfSpec)
				if(sharedVehicle != null)
					foreach(Sprite*, sprite, vector<Sprite*>, sharedVehicle->animation.sprites)
						sprite->scale *= GLOBAL_VEHICLE_SCALE_FACTOR;

		foreach(Pseudo3DVehicle&, trafficVehicle, vector<Pseudo3DVehicle>, trafficVehicles)
			course.animation.vehicles.push_back(&trafficVehicle);
	}

	standings.clear();
	if(params.raceType == RACE_TYPE_STANDARD_RACE or params.raceType == RACE_TYPE_DEBUG)
	{
		racingVehicles.resize(params.playerCount + params.cpuOpponentCount);
		const vector<const Pseudo3DVehicle::Spec*> cpuOpponentVehicleSpecs = game.logic.getEligibleCpuOpponentVehicleSpecs();
		for(unsigned i = params.playerCount; i < racingVehicles.size(); i++)
		{
			RacingVehicle& cpuOpponentVehicle = racingVehicles[i];
			const unsigned cpuOpponentVehicleSpecIndex = futil::random_between(0, cpuOpponentVehicleSpecs.size());
			const Pseudo3DVehicle::Spec& spec = *cpuOpponentVehicleSpecs[cpuOpponentVehicleSpecIndex];  // grab randomly chosen spec
			const int skinIndex = spec.alternateSprites.empty()? -1 : futil::random_between(-1, spec.alternateSprites.size());
			cpuOpponentVehicle.animation.smokeSprite = null;
			cpuOpponentVehicle.setSpec(spec, skinIndex);
			cpuOpponentVehicle.loadAssetsData();

			// apply screen scale to opponent vehicle sprites
			foreach(Sprite*, sprite, vector<Sprite*>, cpuOpponentVehicle.animation.sprites)
				sprite->scale *= GLOBAL_VEHICLE_SCALE_FACTOR;

			if(cpuOpponentVehicle.animation.brakelightSprite != null)
				cpuOpponentVehicle.animation.brakelightSprite->scale *= GLOBAL_VEHICLE_SCALE_FACTOR;

			if(cpuOpponentVehicle.animation.shadowSprite != null)
				cpuOpponentVehicle.animation.shadowSprite->scale *= GLOBAL_VEHICLE_SCALE_FACTOR;

			cpuOpponentVehicle.body.simulationType = Mechanics::SIMULATION_TYPE_SLIPLESS;
			cpuOpponentVehicle.body.automaticShiftingEnabled = true;
			cpuOpponentVehicle.body.gravityAccelerationFactor = GameLogic::PHYSICS_GRAVITY_ACCELERATION_DEFAULT;
			cpuOpponentVehicle.corneringStiffness = computeCorneringStiffness(cpuOpponentVehicle.body.spec.mass * cpuOpponentVehicle.body.gravityAccelerationFactor);
			course.animation.vehicles.push_back(&cpuOpponentVehicle);
			standings.push_back(&cpuOpponentVehicle);
		}
	}
	else
		racingVehicles.resize(params.playerCount);

	for(unsigned i = 0; i < params.playerCount; i++)
	{
		RacingVehicle& playerVehicle = racingVehicles[i];
		playerVehicle.animation.smokeSprite = null;
		playerVehicle.setSpec(game.logic.nextMatch.players[i].vehicleSpec, game.logic.nextMatch.players[i].alternateSpriteIndex);
		playerVehicle.loadAssetsData();

		for(unsigned s = 0; s < playerVehicle.animation.sprites.size(); s++)
			playerVehicle.animation.sprites[s]->scale *= GLOBAL_VEHICLE_SCALE_FACTOR;

		if(playerVehicle.animation.brakelightSprite != null)
			playerVehicle.animation.brakelightSprite->scale *= GLOBAL_VEHICLE_SCALE_FACTOR;

		if(playerVehicle.animation.shadowSprite != null)
			playerVehicle.animation.shadowSprite->scale *= GLOBAL_VEHICLE_SCALE_FACTOR;

		playerVehicle.body.simulationType = params.simulationType;
		playerVehicle.body.automaticShiftingEnabled = game.logic.nextMatch.players[i].enableAutomaticShifting;
		playerVehicle.body.gravityAccelerationFactor = GameLogic::PHYSICS_GRAVITY_ACCELERATION_DEFAULT;
		playerVehicle.corneringStiffness = computeCorneringStiffness(playerVehicle.body.spec.mass * playerVehicle.body.gravityAccelerationFactor);
		playerVehicle.powerBoostIncreaseFactor = 2.0;
		playerVehicle.powerBoostDuration = 5;
		course.animation.vehicles.push_back(&playerVehicle);
		standings.push_back(&playerVehicle);
	}

	for(unsigned i = 0; i < raceScreenData.size(); i++)
	{
		RacingVehicle& focusedVehicle = racingVehicles[i];
		focusedVehicle.animation.smokeSprite = spriteSmoke;
		focusedVehicle.animation.backfireSprite = focusedVehicle.animation.spec.backfirePositions.empty()? null : spriteBackfire;
		focusedVehicle.setSoundVolume(game.logic.masterVolume);
		focusedVehicle.screenPosition.x = 0.5f * screenWidth + (i%screenGridColumns)*screenWidth;
		focusedVehicle.screenPosition.y = 0.83f * screenHeight + (i/screenGridColumns)*screenHeight;
		focusedVehicle.overscaleFactor = (4/3.f) * screenHeight;
	}

	spriteSmoke->scale.x = spriteSmoke->scale.y = GLOBAL_VEHICLE_SCALE_FACTOR * 0.75f;
	spriteBackfire->scale.x = spriteBackfire->scale.y = GLOBAL_VEHICLE_SCALE_FACTOR;

	// ------------------- set-up gear throttle/brake/steering bars -------------------

	hudThrottleBar.bounds.x = 0.94f * screenWidth;
	hudThrottleBar.bounds.y = 0.50f * screenHeight;
	hudThrottleBar.bounds.w = 0.07f * screenHeight;
	hudThrottleBar.bounds.h = 0.02f * screenHeight;
	hudThrottleBar.borderThickness = 0.001f * screenHeight;

	hudBrakeBar.bounds = hudThrottleBar.bounds;
	hudBrakeBar.bounds.y = 0.52f * screenHeight;
	hudBrakeBar.borderThickness = hudThrottleBar.borderThickness;

	hudSteeringBar.bounds = hudThrottleBar.bounds;
	hudSteeringBar.bounds.y = 0.54f * screenHeight;
	hudSteeringBar.borderThickness = hudThrottleBar.borderThickness;

	hudFuelAmount.bounds = hudThrottleBar.bounds;
	hudFuelAmount.bounds.y = 0.56f * screenHeight;
	hudFuelAmount.borderThickness = hudThrottleBar.borderThickness;

	Image* imgDefaultCarDashboard = null, *imgDefaultBikeDashboard = null, *imgDefaultSteeringWheel = null;

	for(unsigned i = 0; i < raceScreenData.size(); i++)
	{
		const RacingVehicle& portrayedVehicle = racingVehicles[i];  // racing vehicle which data is displayed in this screen
		Hud::VehicleDashboard& dashboard = raceScreenData[i].dashboard;

		raceScreenData[i].cameraPositionOffset = params.isCockpitView? 0 : DEFAULT_CAMERA_PROJECTION_OFFSET;

		dashboard.loadFromFile(game.logic.nextMatch.dashboardLayoutFilename);

		dashboard.bounds.x = (i % screenGridColumns) * displayWidth  / screenGridColumns;
		dashboard.bounds.y = (i / screenGridColumns) * displayHeight / screenGridLines;
		dashboard.bounds.w = screenWidth;
		dashboard.bounds.h = screenHeight;

		if(portrayedVehicle.body.spec.vehicleType == Mechanics::TYPE_BIKE)
		{
			if(imgDefaultBikeDashboard == null)
				imgDefaultBikeDashboard = new Image("assets/hud/bike-dashboard-background.png");
			dashboard.imgDashboard = imgDefaultBikeDashboard;
			dashboard.imgSteeringWheel = null;
		}
		else
		{
			if(imgDefaultCarDashboard == null)
				imgDefaultCarDashboard = new Image("assets/hud/car-dashboard-background.png");
			dashboard.imgDashboard = imgDefaultCarDashboard;
			if(imgDefaultSteeringWheel == null)
				imgDefaultSteeringWheel = new Image("assets/hud/steering-wheel.png");
			dashboard.imgSteeringWheel = imgDefaultSteeringWheel;
		}

		if(dashboard.isHudTypeUsingDialTacho())
		{
			if(dashboard.dialTachometer.graduationLevel > 0)
			{
				dashboard.dialTachometer.min = 1000;  // TODO set it to zero
				dashboard.dialTachometer.max = 1000.f * static_cast<int>((portrayedVehicle.body.engine.spec.maxRpm+950.f)/1000.f);
			}
			dashboard.dialTachometer.text.setFont(fontGraduation);
		}
		else if(dashboard.isHudTypeUsingBarTacho())
		{
			dashboard.barTachometer.min = 1000;  // TODO set it to zero
			dashboard.barTachometer.max = portrayedVehicle.body.engine.spec.maxRpm;
		}

		if(dashboard.hudType == Hud::VehicleDashboard::HUD_TYPE_FULL_DIGITAL)
			dashboard.speedometerUnitLabel.text.setFont(null);
		else
			dashboard.speedometerUnitLabel.text.setFont(fontSpeedUnit);
		dashboard.speedometerUnitLabel.text.setContent(params.isImperialUnit? "mph" : "kph");

		dashboard.automaticShiftingLabel.text.setFont(dashboard.speedometerUnitLabel.text.getFont());

		const float speedUnitFactor = params.isImperialUnit? GameLogic::PHYSICS_MPS_TO_MPH : GameLogic::PHYSICS_MPS_TO_KPH;
		dashboard.dialSpeedometer.graduationValueScale = speedUnitFactor;

		if(dashboard.isHudTypeUsingDialSpeedo())
			dashboard.dialSpeedometer.text.setFont(fontGraduationSmall);

		if(not dashboard.isHudTypeUsingDialSpeedo() or dashboard.dialSpeedometer.graduationLevel > 0)
		{
			dashboard.dialSpeedometer.min = 0;
			dashboard.dialSpeedometer.max = (((portrayedVehicle.body.getEstimatedTopSpeed() * speedUnitFactor) / 10 + 1) * 10) / speedUnitFactor;
		}
		else
		{
			dashboard.dialSpeedometer.min /= speedUnitFactor;
			dashboard.dialSpeedometer.max /= speedUnitFactor;
		}

		if(dashboard.isHudTypeUsingDialSpeedo() or dashboard.hudType == Hud::VehicleDashboard::HUD_TYPE_FULL_DIGITAL)
			dashboard.numericSpeedometer.text.setFont(null);
		else
			dashboard.numericSpeedometer.text.setFont(fontSpeedometer);

		dashboard.isCockpitView = params.isCockpitView;

		dashboard.setup(params.useCachedDialGauge);
	}

	const Point widgetSpacing = { 0.013f * screenHeight, 0.01f * screenHeight };

	stopwatchIconBounds.w = 0.0427f * screenHeight;
	stopwatchIconBounds.h = imgStopwatch->getHeight()*(stopwatchIconBounds.w/imgStopwatch->getWidth());
	stopwatchIconBounds.x = widgetSpacing.x;
	stopwatchIconBounds.y = widgetSpacing.y;

	hudTimerCurrentLapLabel.pack();
	hudTimerCurrentLapLabel.bounds.x = stopwatchIconBounds.x + stopwatchIconBounds.w + widgetSpacing.x;
	hudTimerCurrentLapLabel.bounds.y = stopwatchIconBounds.y;

	hudTimerCurrentLap.pack();
	hudTimerCurrentLap.bounds.x = hudTimerCurrentLapLabel.bounds.x + hudTimerCurrentLapLabel.bounds.w + widgetSpacing.x;
	hudTimerCurrentLap.bounds.y = hudTimerCurrentLapLabel.bounds.y;

	hudTimerLastLap.pack();
	hudTimerLastLap.bounds.x = hudTimerCurrentLap.bounds.x;
	hudTimerLastLap.bounds.y = hudTimerCurrentLap.bounds.y + hudTimerCurrentLap.bounds.h + widgetSpacing.y;

	hudTimerLastLapLabel.pack();
	hudTimerLastLapLabel.bounds.x = hudTimerLastLap.bounds.x - hudTimerLastLapLabel.bounds.w - widgetSpacing.x;
	hudTimerLastLapLabel.bounds.y = hudTimerLastLap.bounds.y;

	hudTimerBestLap.pack();
	hudTimerBestLap.bounds.x = hudTimerLastLap.bounds.x;
	hudTimerBestLap.bounds.y = hudTimerLastLap.bounds.y + hudTimerLastLap.bounds.h + widgetSpacing.y;

	hudTimerBestLapLabel.pack();
	hudTimerBestLapLabel.bounds.x = hudTimerBestLap.bounds.x - hudTimerBestLapLabel.bounds.w - widgetSpacing.x;
	hudTimerBestLapLabel.bounds.y = hudTimerBestLap.bounds.y;

	hudLapLabel.pack();
	hudLapLabel.bounds.x = hudTimerCurrentLapLabel.bounds.x;
	hudLapLabel.bounds.y = hudTimerBestLap.bounds.y + hudTimerBestLap.bounds.h + widgetSpacing.y;

	hudCurrentLap.packToValue(params.lapCountGoal);
	hudCurrentLap.bounds.x = hudTimerBestLap.bounds.x;
	hudCurrentLap.bounds.y = hudLapLabel.bounds.y;

	hudLapCountGoal.text.setContent("\\" + futil::to_string(params.lapCountGoal));
	hudLapCountGoal.pack();
	hudLapCountGoal.bounds.x = hudCurrentLap.bounds.x + (1 + 2*hudCurrentLap.countDigits(params.lapCountGoal))*widgetSpacing.x;
	hudLapCountGoal.bounds.y = hudCurrentLap.bounds.y;

	hudProgressLabel.pack();
	hudProgressLabel.bounds.x = hudTimerLastLapLabel.bounds.x;
	hudProgressLabel.bounds.y = hudTimerLastLapLabel.bounds.y;

	hudProgress.pack(3);
	hudProgress.bounds.x = hudProgressLabel.bounds.x + hudProgressLabel.bounds.w + widgetSpacing.x;
	hudProgress.bounds.y = hudProgressLabel.bounds.y;

	hudBoostCounter.packToValue(params.powerBoostCount);
	hudBoostCounter.bounds.x = 0.975f * screenWidth;
	hudBoostCounter.bounds.y = 0.575f * screenHeight;

	hudBoostCounterLabel.pack();
	hudBoostCounterLabel.bounds.x = hudBoostCounter.bounds.x - hudBoostCounterLabel.bounds.w - widgetSpacing.x;
	hudBoostCounterLabel.bounds.y = hudBoostCounter.bounds.y;

	posHudCountdown.x = 0.5f*(screenWidth - fontCountdown->getTextWidth("0"));
	posHudCountdown.y = 0.4f*(screenHeight - fontCountdown->getTextHeight());
	posHudFinishedCaption.x = 0.5f*(screenWidth - fontCountdown->getTextWidth("FINISHED"));
	posHudFinishedCaption.y = 0.4f*(screenHeight - fontCountdown->getTextHeight());

	minimap.spec = course.spec;
	minimap.scale = minimap.offset = Vector2D::NULL_VECTOR;
	minimap.bounds.x = 0.02 * screenWidth;
	minimap.bounds.y = 0.3875 * screenHeight;
	minimap.bounds.w = minimap.bounds.h = 0.225 * screenHeight;
	minimap.plottedPositions[0].size = screenHeight/100.f;
	minimap.plottedPositions[0].minimumScaledSize = screenHeight/75.f;
	minimap.plottedPositions[1].size = screenHeight/240.f;
	minimap.plottedPositions[1].minimumScaledSize = screenHeight/180.f;
	minimap.plottedPositions[1].fillColor = Color::WHITE;
	minimap.plottedPositions.resize(2);  // almost clear
	minimap.plottedPositions.resize(1+racingVehicles.size(), minimap.plottedPositions[1]);
	minimap.plottedPositions.back().size = screenHeight/200.f;
	minimap.plottedPositions.back().minimumScaledSize = screenHeight/150.f;
	minimap.plottedPositions.back().fillColor = Color::YELLOW;
	minimap.compile();

	hudPlayerRankingLabel.pack();
	hudPlayerRankingLabel.bounds.x = minimap.bounds.x;
	hudPlayerRankingLabel.bounds.y = minimap.bounds.y + minimap.bounds.h + widgetSpacing.y;

	hudPlayerRanking.packToValue(racingVehicles.size());
	hudPlayerRanking.bounds.x = hudPlayerRankingLabel.bounds.x + 8*widgetSpacing.x;
	hudPlayerRanking.bounds.y = hudPlayerRankingLabel.bounds.y;

	hudPlayerRankingGoal.text.setContent("\\" + futil::to_string(racingVehicles.size()));
	hudPlayerRankingGoal.pack();
	hudPlayerRankingGoal.bounds.x = hudPlayerRanking.bounds.x + (1 + 2*hudPlayerRanking.countDigits(racingVehicles.size()))*widgetSpacing.x;
	hudPlayerRankingGoal.bounds.y = hudPlayerRankingLabel.bounds.y;

	debugTextColor = (course.spec.colorLandscape.r*0.299f + course.spec.colorLandscape.g*0.587f + course.spec.colorLandscape.b*0.114f) > 186? Color::BLACK : Color::WHITE;
	debugMode = (params.raceType == RACE_TYPE_DEBUG);

	if(params.musicOption == -1 and not course.spec.musicFilename.empty())
		music = new Music(course.spec.musicFilename);
	else if(params.musicOption == -2)
		music = new Music(game.logic.getMusicFilenames()[futil::random_between(0, game.logic.getMusicFilenames().size())]);
	else if(params.musicOption >= 0 and params.musicOption < (int) game.logic.getMusicFilenames().size())
		music = new Music(game.logic.getMusicFilenames()[params.musicOption]);

	sndCountdownBuzzer->setVolume(game.logic.masterVolume * 0.8f);
	sndCountdownBuzzerFinal->setVolume(game.logic.masterVolume * 0.8f);

	setupRace();

	if(music != null)
	{
		music->setVolume(game.logic.masterVolume);
		music->loop();
	}

	for(unsigned i = 0; i < raceScreenData.size(); i++)
		racingVehicles[i].engineSound.play();

	firstCyclesControl = 2;
}

void Pseudo3DRaceState::setupRace()
{
	for(unsigned i = 0; i < trafficVehicles.size() + racingVehicles.size(); i++)
	{
		Pseudo3DVehicle& vehicle = i < trafficVehicles.size()? trafficVehicles[i] : racingVehicles[i - trafficVehicles.size()];

		// all vehicles common setup
		vehicle.strafeSpeed = 0;
		vehicle.verticalPosition = 0;
		vehicle.verticalSpeed = 0;
		vehicle.virtualOrientation = 0;
		vehicle.body.reset();
		vehicle.pseudoAngle = 0;
		vehicle.steeringWheelPosition = 0;
		vehicle.curvePull = 0;
		vehicle.finishedLapsCount = 0;
		vehicle.isTireBurnoutOccurring = vehicle.onAir = vehicle.onLongAir = vehicle.justHardLanded = false;
		vehicle.isDisplayedAsPlayer = false;
		vehicle.powerBoostCount = params.powerBoostCount;

		if(i < trafficVehicles.size())
		{
			// traffic vehicle specific setup
			vehicle.position = futil::random_between_decimal(0.1, 0.9) * course.spec.lines.size() * course.spec.roadSegmentLength / coursePositionFactor;
			vehicle.horizontalPosition = course.spec.roadWidth / coursePositionFactor;
			if(course.spec.laneCount < 2)
				vehicle.horizontalPosition *= futil::random_between_decimal(-0.825, 0.825);
			else
				vehicle.horizontalPosition *= (((futil::random_between(0, course.spec.laneCount) * 2 + 1) / (float) course.spec.laneCount) - 1);
		}
		else
		{
			// all racing vehicles common setup
			RacingVehicle& racingVehicle = static_cast<RacingVehicle&>(vehicle);
			racingVehicle.lapTimeBest = racingVehicle.lapTimeLast = racingVehicle.lapTimeCurrent = 0;

			if(i >= trafficVehicles.size() + params.playerCount)
			{
				// opponent vehicles specific setup
				const unsigned oi = i - trafficVehicles.size() - params.playerCount;
				if(racingVehicles.size() > 3)
				{
					racingVehicle.position = courseStartPositionOffset + ((oi + params.playerCount) * racersStartPositionOffset) / coursePositionFactor;
					racingVehicle.horizontalPosition = ((oi + params.playerCount) % 2 == 0? -0.5 : 0.5) * 0.825 * course.spec.roadWidth / coursePositionFactor;
				}
				else
				{
					racingVehicle.position = courseStartPositionOffset / coursePositionFactor;
					racingVehicle.horizontalPosition = (((oi + params.playerCount) * 2 + 1) / ((float) racingVehicles.size()) - 1) * 0.825 * course.spec.roadWidth / coursePositionFactor;
				}
			}
			else
			{
				// player vehicles specific setup
				const unsigned pi = i - trafficVehicles.size();
				if(racingVehicles.size() > 3)
				{
					racingVehicle.position = courseStartPositionOffset + (pi*racersStartPositionOffset) / coursePositionFactor;
					racingVehicle.horizontalPosition = (pi % 2 == 0? -0.5 : 0.5) * 0.825 * course.spec.roadWidth / coursePositionFactor;
				}
				else
				{
					racingVehicle.position = courseStartPositionOffset / coursePositionFactor;
					racingVehicle.horizontalPosition = ((pi * 2 + 1) / ((float) racingVehicles.size()) - 1) * 0.825 * course.spec.roadWidth / coursePositionFactor;
				}
			}
		}
	}

	//reset some screen data
	for(unsigned i = 0; i < raceScreenData.size(); i++)
	{
		raceScreenData[i].isOnSceneFinish = false;
		raceScreenData[i].timerSceneFinish = 0;
		raceScreenData[i].cameraSlopeAngle = 0;
		raceScreenData[i].cameraPositionOffset = params.isCockpitView? 0 : DEFAULT_CAMERA_PROJECTION_OFFSET;

		if(params.cameraBehavior != Pseudo3DCourseAnimation::CAMERA_BEHAVIOR_TYPE_B and params.cameraBehavior != Pseudo3DCourseAnimation::CAMERA_BEHAVIOR_TYPE_C)
			racingVehicles[i].verticalPositionFactor = 0.3/raceScreenData[i].cameraPositionOffset;
		else
			racingVehicles[i].verticalPositionFactor = 0;
	}

	if(params.raceType == RACE_TYPE_DEBUG)
		onSceneIntro = false;
	else
	{
		onSceneIntro = true;
		timerSceneIntro = 4.5;
		countdownBuzzerCounter = 5;
	}

	acc0to60time = acc0to60clock = 0;
	hudBoostCounter.text.setColor(Color::WHITE);
	hudBoostCounterLabel.text.setColor(Color::WHITE);
}

void Pseudo3DRaceState::onLeave()
{
	// stop sounds
	for(unsigned i = 0; i < racingVehicles.size(); i++)
		racingVehicles[i].haltSounds();

	sndCountdownBuzzer->stop();
	sndCountdownBuzzerFinal->stop();

	// cleanup
	if(music != null)
	{
		music->stop();
		delete music;
		music = null;
	}
	vector<Image*> deletedImgDashboards, deletedImgSteeringWheels;
	foreach(RaceScreenData&, screen, vector<RaceScreenData>, raceScreenData)
	{
		foreach(Image*, imgDashboard, vector<Image*>, deletedImgDashboards)
			if(screen.dashboard.imgDashboard == imgDashboard)
				screen.dashboard.imgDashboard = null;  // bypass deletion since it was already deleted
		foreach(Image*, imgSteeringWheel, vector<Image*>, deletedImgSteeringWheels)
			if(screen.dashboard.imgSteeringWheel == imgSteeringWheel)
				screen.dashboard.imgSteeringWheel = null;  // bypass deletion since it was already deleted

		screen.dashboard.freeResources();

		if(screen.dashboard.imgDashboard != null)
			deletedImgDashboards.push_back(screen.dashboard.imgDashboard);
		if(screen.dashboard.imgSteeringWheel != null)
			deletedImgSteeringWheels.push_back(screen.dashboard.imgSteeringWheel);
	}
	foreach(RacingVehicle&, vehicle, vector<RacingVehicle>, racingVehicles)
	{
		vehicle.animation.freeAssetsData();
		vehicle.engineSound.freeAssetsData();
	}
	racingVehicles.clear();  // needed to avoid double deletion caused by vector resize
	foreach(Pseudo3DVehicle&, vehicle, vector<Pseudo3DVehicle>, trafficVehicles)
	{
		vehicle.animation.freeAssetsData();
		vehicle.engineSound.freeAssetsData();
	}
	trafficVehicles.clear();  // needed to avoid double deletion caused by vector resize
	course.animation.freeAssetsData();
}

void Pseudo3DRaceState::translateHudElements(Vector2D offset)
{
	#define rectAddXY(rt) rt.x += offset.x; rt.y += offset.y;
	rectAddXY(minimap.bounds);
	rectAddXY(hudTimerCurrentLapLabel.bounds);
	rectAddXY(hudTimerCurrentLap.bounds);
	rectAddXY(hudLapLabel.bounds);
	rectAddXY(hudCurrentLap.bounds);
	rectAddXY(hudLapCountGoal.bounds);
	rectAddXY(hudTimerLastLapLabel.bounds);
	rectAddXY(hudTimerLastLap.bounds);
	rectAddXY(hudTimerBestLapLabel.bounds);
	rectAddXY(hudTimerBestLap.bounds);
	rectAddXY(hudProgressLabel.bounds);
	rectAddXY(hudProgress.bounds);
	rectAddXY(hudPlayerRankingLabel.bounds);
	rectAddXY(hudPlayerRankingGoal.bounds);
	rectAddXY(hudPlayerRanking.bounds);
	rectAddXY(hudThrottleBar.bounds);
	rectAddXY(hudBrakeBar.bounds);
	rectAddXY(hudSteeringBar.bounds);
	rectAddXY(hudFuelAmount.bounds);
	rectAddXY(hudBoostCounterLabel.bounds);
	rectAddXY(hudBoostCounter.bounds);
	rectAddXY(stopwatchIconBounds);
	#undef rectAddXY

	posHudCountdown += offset;
	posHudFinishedCaption += offset;
}

void Pseudo3DRaceState::render()
{
	game.getDisplay().clear();
	const float displayWidth = game.getDisplay().getWidth(), displayHeight = game.getDisplay().getHeight();

	for(unsigned i = 0; i < racingVehicles.size(); i++)
		minimap.plottedPositions[1 + i].value = racingVehicles[i].position * coursePositionFactor;

	for(unsigned i = 0; i < raceScreenData.size(); i++)
	{
		course.animation.drawAreaOffset.x = (i % screenGridColumns) * displayWidth / screenGridColumns;
		course.animation.drawAreaOffset.y = (i / screenGridColumns) * displayHeight / screenGridLines;
		translateHudElements(course.animation.drawAreaOffset);

		RacingVehicle& focusedVehicle = racingVehicles[i], &previouslyFocusedVehicle = racingVehicles[(i? i : raceScreenData.size())-1];
		previouslyFocusedVehicle.isDisplayedAsPlayer = false;  // disable focusing of previously focused vehicle (because it was the corresponding vehicle of previous screen)
		focusedVehicle.isDisplayedAsPlayer = true;  // enable focusing of currently portrayed vehicle

		if(params.isCockpitView)
		{
			previouslyFocusedVehicle.hidden = false;  // get back to display previously hidden vehicle (because only its dashboard was visible in its corresponding screen)
			focusedVehicle.hidden = true;  // hide currently portrayed vehicle, to show only its dashboard
		}

		const float tmpValue = minimap.plottedPositions.back().value;
		minimap.plottedPositions.back().value = minimap.plottedPositions[1 + i].value;
		minimap.plottedPositions[1 + i].value = tmpValue;

		drawScreen(raceScreenData[i], focusedVehicle);
		translateHudElements(-course.animation.drawAreaOffset);
	}

	if(raceScreenData.size() > 1)
		for(unsigned i = 0; i < screenGridLines; i++)
			Graphics::drawThickLine(0, displayHeight*i/screenGridLines, displayWidth, displayHeight*i/screenGridLines, 0.005*displayHeight, Color::BLACK);

	if(raceScreenData.size() > 2)
		for(unsigned i = 0; i < screenGridColumns; i++)
			Graphics::drawThickLine(displayWidth*i/screenGridColumns, 0, displayWidth*i/screenGridColumns, displayHeight, 0.005*displayHeight, Color::BLACK);

	for(unsigned i = raceScreenData.size(); i < screenGridColumns*screenGridLines; i++)
	{
		const Point offset = { (i%screenGridColumns)*displayWidth/screenGridColumns, (i/screenGridColumns)*displayHeight/screenGridLines };
		Graphics::drawFilledRectangle(offset.x, offset.y, displayWidth/screenGridColumns, displayHeight/screenGridLines, Color::BLACK);
	}

	// DEBUG
	if(debugMode)
		drawDebugInfo();
}

void Pseudo3DRaceState::drawScreen(RaceScreenData& hud, RacingVehicle& focusedVehicle)
{
	const float courseLength = course.spec.lines.size() * course.spec.roadSegmentLength / coursePositionFactor;
//	float cameraPosition = focusedVehicle.position;  // gives better visual results regarding cornering, but causes glitch in collision, making it occur on visually wrong positions
	float cameraPosition = focusedVehicle.position - hud.cameraPositionOffset;
	while(cameraPosition < 0)  // course drawing method cannot receive negative position, take position modulus
		cameraPosition += courseLength;

	const float cameraHeightDiff = course.animation.cameraBehavior == Pseudo3DCourseAnimation::CAMERA_BEHAVIOR_TYPE_B? focusedVehicle.verticalPosition - course.spec.getHeightAt(coursePositionFactor * cameraPosition)
								 : course.animation.cameraBehavior == Pseudo3DCourseAnimation::CAMERA_BEHAVIOR_TYPE_C? focusedVehicle.verticalPosition - course.spec.getHeightAt(coursePositionFactor * focusedVehicle.position)
								 : 0;

	course.animation.isVehicleSpriteLeftOcclusionEnabled = (course.animation.drawAreaOffset.x > 0);
	if(focusedVehicle.animation.spec.stateCount == 1 or params.isCockpitView) course.animation.headlightsYawAngle = 0;
	else course.animation.headlightsYawAngle = sgn(focusedVehicle.pseudoAngle)*focusedVehicle.animation.spec.depictedTurnAngle[focusedVehicle.animation.getCurrentAnimationIndex(-std::abs(focusedVehicle.pseudoAngle))];
	course.animation.cameraHeight += cameraHeightDiff;
	course.animation.draw(cameraPosition * coursePositionFactor, focusedVehicle.horizontalPosition * coursePositionFactor, hud.cameraSlopeAngle + (params.isCockpitView? -M_PI/12 : 0));
	course.animation.cameraHeight -= cameraHeightDiff;

	hud.dashboard.draw(focusedVehicle.body.speed, focusedVehicle.body.engine.rpm,
			focusedVehicle.body.engine.gear, focusedVehicle.body.automaticShiftingEnabled,
			-1.5f*(params.disableSteeringSpeedLimit? focusedVehicle.pseudoAngle : focusedVehicle.steeringWheelPosition));

	Graphics::drawFilledRoundedRectangle(minimap.bounds, 0.02*course.animation.drawAreaHeight, hudMiniMapBgColor);
	minimap.draw();
	imgStopwatch->drawScaled(stopwatchIconBounds.x, stopwatchIconBounds.y, scaledToRect(imgStopwatch, stopwatchIconBounds));
	hudTimerCurrentLapLabel.draw();
	hudTimerCurrentLap.draw(focusedVehicle.lapTimeCurrent);

	if(params.isLapped)
	{
		hudLapLabel.draw();
		hudCurrentLap.draw(focusedVehicle.finishedLapsCount + (hud.isOnSceneFinish? 0 : 1));
		if(params.raceType != RACE_TYPE_PRACTICE)
			hudLapCountGoal.draw();

		hudTimerLastLapLabel.draw();
		if(focusedVehicle.lapTimeLast == 0)
			fontTimers->drawText("--", hudTimerLastLap.bounds.x, hudTimerLastLap.bounds.y, Color::WHITE);
		else
			hudTimerLastLap.draw(focusedVehicle.lapTimeLast);

		hudTimerBestLapLabel.draw();
		if(focusedVehicle.lapTimeBest == 0)
			fontTimers->drawText("--", hudTimerBestLap.bounds.x, hudTimerBestLapLabel.bounds.y, Color::WHITE);
		else
			hudTimerBestLap.draw(focusedVehicle.lapTimeBest);
	}
	else
	{
		hudProgressLabel.draw();
		hudProgress.draw(hud.isOnSceneFinish? 100 : (unsigned) 100*focusedVehicle.position/courseLength);
		fontTimers->drawText("%", hudProgress.bounds.x + hudProgress.bounds.w, hudProgress.bounds.y, Color::WHITE);
	}

	if(params.raceType == RACE_TYPE_STANDARD_RACE)
	{
		hudPlayerRankingLabel.draw();
		hudPlayerRanking.draw(focusedVehicle.ranking);
		hudPlayerRankingGoal.draw();
	}

	hudThrottleBar.draw(focusedVehicle.body.engine.throttlePosition);
	hudBrakeBar.draw(focusedVehicle.body.brakePedalPosition);
	hudSteeringBar.draw(focusedVehicle.steeringWheelPosition);
	hudFuelAmount.draw(focusedVehicle.body.fuelAmount/focusedVehicle.body.spec.fuelCapacity);

	if(params.powerBoostCount > 0)
	{
		hudBoostCounterLabel.draw();
		hudBoostCounter.draw(focusedVehicle.powerBoostCount);
	}

	if(onSceneIntro)
	{
		if(timerSceneIntro > 1 and timerSceneIntro < 4)
			fontCountdown->drawText(futil::to_string((int) timerSceneIntro), posHudCountdown.x, posHudCountdown.y, Color::WHITE);
	}
	else if(timerSceneIntro > 0)
		fontCountdown->drawText("GO", posHudCountdown.x, posHudCountdown.y, Color::WHITE);

	else if(hud.isOnSceneFinish)
		fontCountdown->drawText("FINISHED", posHudFinishedCaption.x, posHudFinishedCaption.y, Color::WHITE);
}

void Pseudo3DRaceState::fastValueDraw(const char* const format, float posx, float posy, float value, const float* const extraValues) const
{
	#define bufferSize 512
	static char buffer[bufferSize];
	static string aux;
	if(extraValues == null)
		futil::snprintf(buffer, bufferSize, format, value);
	else
		futil::snprintf(buffer, bufferSize, format, value, extraValues[0], extraValues[1]);
	fontDebug->drawText(aux=buffer, posx, posy, debugTextColor);
	#undef bufferSize
}

void Pseudo3DRaceState::drawDebugInfo()
{
	const float spacing = fontDebug->getTextHeight(), spacingBig = 1.4f * spacing;
	Point offset = {game.getDisplay().getWidth()/2.f, spacing/2};

	Graphics::drawFilledRectangle(game.getDisplay().getWidth()/2.f, 0, game.getDisplay().getWidth()/2.f, 450, Color::create(255-debugTextColor.r, 255-debugTextColor.g, 255-debugTextColor.b, 64));

	const Pseudo3DVehicle& vehicle = racingVehicles[0];

	fastValueDraw("Position: %2.2fm", offset.x, offset.y, vehicle.position);
	fastValueDraw("Horiz. position: %2.2fm", offset.x + 150, offset.y, vehicle.horizontalPosition);

	offset.y += spacing;
	fastValueDraw("Speed: %2.2fkm/h", offset.x, offset.y, vehicle.body.speed*3.6);
	fastValueDraw("0-60mph: %2.2fs", offset.x + 150, offset.y, acc0to60time);

	offset.y += spacing;
	fastValueDraw("Acc.: %2.2fm/s^2", offset.x, offset.y, vehicle.body.acceleration);

	offset.y += spacingBig;
	fastValueDraw("Height: %2.2fm", offset.x, offset.y, vehicle.verticalPosition);

	offset.y += spacing;
	fastValueDraw("Vertical speed: %2.2fm/s", offset.x, offset.y, vehicle.verticalSpeed);
	fontDebug->drawText(vehicle.onAir? "(On air)" : "(On ground)", offset.x+195, offset.y, debugTextColor);

	offset.y += spacing;
	fastValueDraw("Throtle position: %2.2f%%", offset.x, offset.y, 100*vehicle.body.engine.throttlePosition);

	offset.y += spacing;
	fastValueDraw("Brake pedal pos.: %2.2f%%", offset.x, offset.y, 100*vehicle.body.brakePedalPosition);

	offset.y += spacingBig;
	fastValueDraw("Wheel turn pseudo angle: %2.2f", offset.x, offset.y, vehicle.pseudoAngle);

	offset.y += spacing;
	fastValueDraw("Slope angle: %2.2f", offset.x, offset.y, vehicle.body.slopeAngle);

	offset.y += spacing;
	fastValueDraw("Strafe speed: %2.2fm/s", offset.x, offset.y, vehicle.strafeSpeed);

	offset.y += spacingBig;
	fastValueDraw("Curve pull: %2.2fm/s", offset.x, offset.y, vehicle.curvePull);

	offset.y += spacing;
	fastValueDraw("Slope pull: %2.2fm/s^2", offset.x, offset.y, vehicle.body.slopePullForce);

	offset.y += spacing;
	fastValueDraw("Braking friction: %2.2fN", offset.x, offset.y, vehicle.body.brakingForce);

	offset.y += spacing;
	fastValueDraw("Rolling friction: %2.2fN", offset.x, offset.y, vehicle.body.rollingResistanceForce);

	offset.y += spacing;
	fastValueDraw("Air friction: %2.2fN", offset.x, offset.y, vehicle.body.airDragForce);

	offset.y += spacing;
	fastValueDraw("Combined friction: %2.2fN", offset.x, offset.y, (vehicle.curvePull + vehicle.body.slopePullForce + vehicle.body.brakingForce + vehicle.body.rollingResistanceForce + vehicle.body.airDragForce));

	offset.y += spacingBig;
	fastValueDraw("Drive force: %2.2fN", offset.x, offset.y, vehicle.body.getDriveForce());

	offset.y += spacing;
	fastValueDraw("Torque :%2.2fNm", offset.x, offset.y, vehicle.body.engine.getCurrentTorque());

	offset.y += spacing;
	fastValueDraw("Torque proportion: %2.2f%%", offset.x, offset.y, 100.f*vehicle.body.engine.getCurrentTorque()/vehicle.body.engine.spec.maximumTorque);

	offset.y += spacing;
	fastValueDraw("Power: %2.2fhp", offset.x, offset.y, (vehicle.body.engine.getCurrentTorque()*vehicle.body.engine.rpm)/(5252.0 * 1.355818));

	offset.y += spacing;
	if(vehicle.isPowerBoostActive)
		fontDebug->drawText("(power boost active)", offset.x, offset.y, debugTextColor);

	offset.y += spacingBig;
	fastValueDraw("Driven tires load: %2.2fN", offset.x, offset.y, vehicle.body.getDrivenWheelsWeightLoad());

	offset.y += spacing;
	fastValueDraw("Downforce: %2.2fNm", offset.x, offset.y, vehicle.body.downforce);

	offset.y += spacingBig;
	fastValueDraw("Slip ratio: %2.2f%%", offset.x, offset.y, 100*vehicle.body.slipRatio);

	offset.y += spacingBig;
	fastValueDraw("Wheel Ang. Speed: %2.2frad/s", offset.x, offset.y, vehicle.body.wheelAngularSpeed);

	offset.y += spacing;
	fastValueDraw("Engine RPM: %2.f", offset.x, offset.y, vehicle.body.engine.rpm);

	offset.y += spacing;
	fastValueDraw(vehicle.body.automaticShiftingEnabled? "Gear(auto): " : "Gear: %2.f", offset.x, offset.y, vehicle.body.engine.gear);

	offset.y += spacingBig;
	fontDebug->drawText("Sound profile data: ", offset, debugTextColor);

	const vector<string> engineSoundDegugInfo = racingVehicles[0].engineSound.getDebugInfo(racingVehicles[0].body.engine.rpm);
	for(unsigned i = 0; i < engineSoundDegugInfo.size(); i++)
	{
		offset.y += spacing;
		fontDebug->drawText(engineSoundDegugInfo[i], offset.x, offset.y, debugTextColor);
	}
}

bool Pseudo3DRaceState::RacingVehicle::compare(const RacingVehicle* const rva, const RacingVehicle* const rvb)
{
	return (rva->finishedLapsCount > rvb->finishedLapsCount or (rva->finishedLapsCount == rvb->finishedLapsCount and rva->position > rvb->position));
}

void Pseudo3DRaceState::update(float delta)
{
	float physicsDelta = delta;
	while(physicsDelta > GameLogic::PHYSICS_MAXIMUM_DELTA_TIME)  // process delta too large in smaller delta steps
	{
		handlePhysics(GameLogic::PHYSICS_MAXIMUM_DELTA_TIME);
		physicsDelta -= GameLogic::PHYSICS_MAXIMUM_DELTA_TIME;
	}
	handlePhysics(physicsDelta);

	// scene control
	if(onSceneIntro)
	{
		if(!firstCyclesControl)  // avoids a possibly long delta due to long loading time
			timerSceneIntro -= delta;

		if(countdownBuzzerCounter - timerSceneIntro > 1)
		{
			sndCountdownBuzzer->play();
			countdownBuzzerCounter--;

			if(countdownBuzzerCounter == 2)  // do not play at last call
				countdownBuzzerCounter = 0;
		}

		if(timerSceneIntro < 1)
		{
			onSceneIntro = false;
			for(unsigned i = 0; i < params.playerCount; i++)  // setup all players when race starts
			{
				racingVehicles[i].body.shiftGear(1);
				racingVehicles[i].body.automaticShiftingLastTime = 0;
			}
			for(unsigned i = params.playerCount; i < racingVehicles.size(); i++)  // setup all CPU controlled vehicles when race starts
				racingVehicles[i].body.engine.throttlePosition = 1.0;
			sndCountdownBuzzerFinal->play();
		}
	}
	else if(timerSceneIntro > 0)  // controls displaying of the "GO" text (lasts 1 sec)
		timerSceneIntro -= delta;

	vector<bool> courseEndReachedBy(racingVehicles.size());

	// course looping control
	const float courseLength = course.spec.lines.size() * course.spec.roadSegmentLength / coursePositionFactor;
	for(unsigned i = 0; i < racingVehicles.size(); i++)
	{
		RacingVehicle& focusedVehicle = racingVehicles[i];

		if(courseEndReachedBy[i] = (focusedVehicle.position >= courseLength))  // @suppress("Assignment in condition")
			while(focusedVehicle.position >= courseLength)  // position larger than course length not allowed, take position modulus
				focusedVehicle.position -= courseLength;

		while(focusedVehicle.position < 0)  // negative position is not allowed, take backwards position modulus
			focusedVehicle.position += courseLength;
	}

	// race screen logic control
	for(unsigned i = 0, stillRunningFocusedRacers = raceScreenData.size(); i < raceScreenData.size(); i++)
	{
		RacingVehicle& focusedVehicle = racingVehicles[i];
		RaceScreenData& hud = raceScreenData[i];

		if(not onSceneIntro and not hud.isOnSceneFinish)
			focusedVehicle.lapTimeCurrent += delta;

		if(hud.isOnSceneFinish)
		{
			if(hud.timerSceneFinish >= 0)
				hud.timerSceneFinish -= delta;
			else
			{
				stillRunningFocusedRacers--;
				if(stillRunningFocusedRacers == 0)
				{
					if(game.logic.raceOnlyMode)
						game.running = false;
					else
						game.enterState(game.logic.currentMainMenuStateId);
				}
			}
		}

		if(courseEndReachedBy[i] and not hud.isOnSceneFinish)
		{
			focusedVehicle.finishedLapsCount++;
			if(params.isLapped)
			{
				if(params.raceType != RACE_TYPE_PRACTICE)
				{
					if(focusedVehicle.finishedLapsCount >= params.lapCountGoal)
					{
						hud.isOnSceneFinish = true;
						hud.timerSceneFinish = 8.0;
					}
				}

				focusedVehicle.lapTimeLast = focusedVehicle.lapTimeCurrent;

				if(focusedVehicle.lapTimeCurrent < focusedVehicle.lapTimeBest or focusedVehicle.lapTimeBest == 0)
					focusedVehicle.lapTimeBest = focusedVehicle.lapTimeCurrent;

				if(not hud.isOnSceneFinish)
					focusedVehicle.lapTimeCurrent = 0;
			}
			else
			{
				hud.isOnSceneFinish = true;
				hud.timerSceneFinish = 8.0;
			}
		}

		if(hud.isOnSceneFinish)
			ai.updateVehicleControlAIByPace(focusedVehicle, delta, 0.25f);

		if(focusedVehicle.isPowerBoostActive)
		{
			const Color blinkingColor = ((int) (20*fgeal::uptime())) % 2? Color::RED : Color::WHITE;
			if(hudBoostCounter.text.getColor() != blinkingColor)
			{
				hudBoostCounter.text.setColor(blinkingColor);
				hudBoostCounterLabel.text.setColor(blinkingColor);
			}

			if(not focusedVehicle.sndBoostStart->isPlaying() and not focusedVehicle.sndBoostLoop->isPlaying())
				focusedVehicle.sndBoostLoop->loop();

			else if((fgeal::uptime() - focusedVehicle.powerBoostLastActivationTime) > focusedVehicle.powerBoostDuration)
			{
				focusedVehicle.isPowerBoostActive = false;
				hudBoostCounter.text.setColor(focusedVehicle.powerBoostCount > 0? Color::WHITE : Color::LIGHT_GREY);
				hudBoostCounterLabel.text.setColor(focusedVehicle.powerBoostCount > 0? Color::WHITE : Color::LIGHT_GREY);
				focusedVehicle.sndBoostLoop->stop();
				focusedVehicle.sndBoostEnd->play();
			}
		}

		if(focusedVehicle.justHardLanded)
		{
			focusedVehicle.sndJumpImpact->play();
			focusedVehicle.justHardLanded = false;
		}

		// engine sound control
		focusedVehicle.engineSound.update(focusedVehicle.body.engine.rpm);

		float cameraYawDelta = delta;
		while(cameraYawDelta > GameLogic::PHYSICS_MAXIMUM_DELTA_TIME)  // process delta too large in smaller delta steps
		{
			hud.cameraSlopeAngle += (focusedVehicle.body.slopeAngle - hud.cameraSlopeAngle) * std::abs(focusedVehicle.body.speed) * 0.5f * GameLogic::PHYSICS_MAXIMUM_DELTA_TIME;
			cameraYawDelta -= GameLogic::PHYSICS_MAXIMUM_DELTA_TIME;
		}
		hud.cameraSlopeAngle += (focusedVehicle.body.slopeAngle - hud.cameraSlopeAngle) * std::abs(focusedVehicle.body.speed) * 0.5f * cameraYawDelta;

		// wheelspin logic control
		const bool isPlayerWheelspinOccurring = (
			(focusedVehicle.body.simulationType == Mechanics::SIMULATION_TYPE_SLIPLESS
				and
				(
					// fake burnout mode
					focusedVehicle.body.engine.gear == 1
					and focusedVehicle.body.engine.rpm < 0.5*focusedVehicle.body.engine.spec.maxRpm
					and focusedVehicle.body.engine.throttlePosition > 0
				)
			)
			or
			(focusedVehicle.body.simulationType == Mechanics::SIMULATION_TYPE_WHEEL_LOAD_CAP
				and
				(
					// burnout based on capped drive force
					(focusedVehicle.body.getDriveForce() < 0.75 * focusedVehicle.body.engine.getDriveTorque() / focusedVehicle.body.spec.driveWheelRadius)
					and focusedVehicle.body.engine.gear == 1  // but limited to first gear.
				)
			)
			or
			(focusedVehicle.body.simulationType == Mechanics::SIMULATION_TYPE_PACEJKA_BASED
				and
				(
					// burnout based on real slip ratio
					std::abs(focusedVehicle.body.slipRatio) > GameLogic::PHYSICS_BURNOUT_SLIP_RATIO
					and std::abs(focusedVehicle.body.speed) > 1.0
				)
			)
		);

		const bool isPlayerSideslipOccurring = (
				std::abs(focusedVehicle.body.speed) > MINIMUM_SPEED_TO_SIDESLIP
			and MAXIMUM_STRAFE_SPEED_FACTOR * focusedVehicle.corneringStiffness - std::abs(focusedVehicle.strafeSpeed) < 1
		);

		if(isPlayerWheelspinOccurring and not isOffRoad(focusedVehicle))
		{
			if(focusedVehicle.sndSideslipBurnoutIntro->isPlaying()) focusedVehicle.sndSideslipBurnoutIntro->stop();  // needs individual control per player
			if(focusedVehicle.sndSideslipBurnoutLoop->isPlaying()) focusedVehicle.sndSideslipBurnoutLoop->stop();

			if(not focusedVehicle.isTireBurnoutOccurring)
				focusedVehicle.sndWheelspinBurnoutIntro->play();
			else if(not focusedVehicle.sndWheelspinBurnoutIntro->isPlaying() and not focusedVehicle.sndWheelspinBurnoutLoop->isPlaying())
				focusedVehicle.sndWheelspinBurnoutLoop->loop();

			focusedVehicle.isTireBurnoutOccurring = true;
		}
		else if(isPlayerSideslipOccurring and not isOffRoad(focusedVehicle))
		{
			if(focusedVehicle.sndWheelspinBurnoutIntro->isPlaying()) focusedVehicle.sndWheelspinBurnoutIntro->stop();
			if(focusedVehicle.sndWheelspinBurnoutLoop->isPlaying()) focusedVehicle.sndWheelspinBurnoutLoop->stop();

			if(not focusedVehicle.isTireBurnoutOccurring)
				focusedVehicle.sndSideslipBurnoutIntro->play();
			else if(not focusedVehicle.sndSideslipBurnoutIntro->isPlaying() and not focusedVehicle.sndSideslipBurnoutLoop->isPlaying())
				focusedVehicle.sndSideslipBurnoutLoop->loop();

			focusedVehicle.isTireBurnoutOccurring = true;
		}
		else
		{
			if(focusedVehicle.sndWheelspinBurnoutIntro->isPlaying()) focusedVehicle.sndWheelspinBurnoutIntro->stop();
			if(focusedVehicle.sndWheelspinBurnoutLoop->isPlaying()) focusedVehicle.sndWheelspinBurnoutLoop->stop();
			if(focusedVehicle.sndSideslipBurnoutIntro->isPlaying()) focusedVehicle.sndSideslipBurnoutIntro->stop();
			if(focusedVehicle.sndSideslipBurnoutLoop->isPlaying()) focusedVehicle.sndSideslipBurnoutLoop->stop();
			focusedVehicle.isTireBurnoutOccurring = false;
		}

		focusedVehicle.animation.smokeAnimationEnabled = focusedVehicle.isTireBurnoutOccurring;
		focusedVehicle.animation.backfireAnimationEnabled = focusedVehicle.isPowerBoostActive;
		focusedVehicle.animation.brakelightsAnimationEnabled = focusedVehicle.body.brakePedalPosition > 0;

		if(isOffRoad(focusedVehicle) and std::abs(focusedVehicle.body.speed) > 1)
		{
			if(not focusedVehicle.sndOffroadRunningLoop->isPlaying())
				focusedVehicle.sndOffroadRunningLoop->loop();
		}
		else if(focusedVehicle.sndOffroadRunningLoop->isPlaying())
			focusedVehicle.sndOffroadRunningLoop->stop();

		if(focusedVehicle.isCrashing)
		{
			if(not focusedVehicle.sndCrashImpact->isPlaying())
				focusedVehicle.sndCrashImpact->play();

			focusedVehicle.isCrashing = false;
		}
	}

	// update player vehicle input
	for(unsigned i = 0; i < params.playerCount; i++)
	{
		if(not raceScreenData[i].isOnSceneFinish)
		{
			RacingVehicle& playerVehicle = racingVehicles[i];
			const float desiredThrottlePosition = std::max(playersControls[i].getCommandValue(playersControls[i].accelerate), 0.f),
						desiredThrottlePositionDeviation = desiredThrottlePosition - playerVehicle.body.engine.throttlePosition;
			if(desiredThrottlePositionDeviation != 0)
			{
				if(std::abs(desiredThrottlePositionDeviation) > GameLogic::PHYSICS_PEDAL_POSITION_SPEED*delta)
					playerVehicle.body.engine.throttlePosition += GameLogic::PHYSICS_PEDAL_POSITION_SPEED*delta * sgn(desiredThrottlePositionDeviation);
				else
					playerVehicle.body.engine.throttlePosition = desiredThrottlePosition;
			}

			const float desiredBrakePedalPosition = std::max(playersControls[i].getCommandValue(playersControls[i].braking), 0.f),
						desiredBrakePedalPositionDeviation = desiredBrakePedalPosition - playerVehicle.body.brakePedalPosition;
			if(desiredBrakePedalPositionDeviation != 0)
			{
				if(std::abs(desiredBrakePedalPositionDeviation) > GameLogic::PHYSICS_PEDAL_POSITION_SPEED*delta)
					playerVehicle.body.brakePedalPosition += GameLogic::PHYSICS_PEDAL_POSITION_SPEED*delta * sgn(desiredBrakePedalPositionDeviation);
				else
					playerVehicle.body.brakePedalPosition = desiredBrakePedalPosition;
			}

			if(params.disableSteeringSpeedLimit)
				playerVehicle.steeringWheelPosition = getPlayerSteeringInput(i);
			else
			{
				// change steering wheel position to match steering input at a limited rate per delta
				const float desiredSteeringWheelPosition = getPlayerSteeringInput(i),
							desiredSteeringWheelPositionDeviation = desiredSteeringWheelPosition - playerVehicle.steeringWheelPosition;
				if(desiredSteeringWheelPositionDeviation != 0)
				{
					float steeringSpeed = STEERING_SPEED;
					if(std::abs(desiredSteeringWheelPosition) != 0 and (sgn(desiredSteeringWheelPosition) != sgn(playerVehicle.steeringWheelPosition) or std::abs(desiredSteeringWheelPosition) < std::abs(playerVehicle.steeringWheelPosition)))
						steeringSpeed = STEERING_SPEED * 1.5f;

					if(std::abs(desiredSteeringWheelPositionDeviation) > steeringSpeed * delta)
						playerVehicle.steeringWheelPosition += steeringSpeed * delta * sgn(desiredSteeringWheelPositionDeviation);
					else
						playerVehicle.steeringWheelPosition = desiredSteeringWheelPosition;
				}
			}
		}
	}

	// update traffic AI
	for(unsigned i = 0; i < trafficVehicles.size(); i++)
		ai.updateVehicleControlAIBySpeed(trafficVehicles[i], delta, 15 + 5*(i%3), false);

	for(unsigned i = params.playerCount; i < racingVehicles.size(); i++)
	{
		Pseudo3DVehicle& cpuRacingVehicles = racingVehicles[i];

		// update opponent AI
		ai.updateVehicleControlAIByPace(cpuRacingVehicles, delta, 0.95f);

		// update cpu-controlled racers' laps (except focused ones)
		if(courseEndReachedBy[i] and i > 0)
			cpuRacingVehicles.finishedLapsCount++;
	}

	// update racers ranking
	std::sort(standings.begin(), standings.end(), RacingVehicle::compare);
	for(unsigned i = 0; i < standings.size(); i++)
		standings[i]->ranking = i+1;

	if(firstCyclesControl > 0)
		firstCyclesControl--;

	// == DEBUG ==================================================================================
	// 0-60 time control
	if(acc0to60time == 0)
	{
		if(racingVehicles[0].body.engine.throttlePosition > 0 and racingVehicles[0].body.speed > 0 and acc0to60clock == 0)
			acc0to60clock = fgeal::uptime();
		else if(racingVehicles[0].body.engine.throttlePosition < 0 and acc0to60clock != 0)
			acc0to60clock = 0;
		else if(racingVehicles[0].body.engine.throttlePosition > 0 and racingVehicles[0].body.speed * 3.6 > 96)
			acc0to60time = fgeal::uptime() - acc0to60clock;
	}

	if(Keyboard::isKeyPressed(Keyboard::KEY_DELETE))
	{
		if(Keyboard::isKeyPressed(Keyboard::KEY_RIGHT_SHIFT))
			course.animation.fogging = std::min(1.f, course.animation.fogging + 0.1f*delta);
		if(Keyboard::isKeyPressed(Keyboard::KEY_RIGHT_CONTROL))
			course.animation.pseudoFogging = std::min(1.f, course.animation.pseudoFogging + 0.3f*delta);
		else
			course.animation.lightFiltering = std::min(1.f, course.animation.lightFiltering + 0.2f*delta);
	}
	if(Keyboard::isKeyPressed(Keyboard::KEY_INSERT))
	{
		if(Keyboard::isKeyPressed(Keyboard::KEY_RIGHT_SHIFT))
			course.animation.fogging = std::max(-1.f, course.animation.fogging - 0.1f*delta);
		if(Keyboard::isKeyPressed(Keyboard::KEY_RIGHT_CONTROL))
			course.animation.pseudoFogging = std::max(-1.f, course.animation.pseudoFogging - 0.3f*delta);
		else
			course.animation.lightFiltering = std::max(-1.f, course.animation.lightFiltering - 0.2f*delta);
	}

	if(Keyboard::isKeyPressed(Keyboard::KEY_PAGE_UP))
		course.animation.cameraHeight += 10;
	if(Keyboard::isKeyPressed(Keyboard::KEY_PAGE_DOWN))
		course.animation.cameraHeight -= 10;
	if(Keyboard::isKeyPressed(Keyboard::KEY_EQUALS))
		course.animation.cameraDepth += 0.005;
	if(Keyboard::isKeyPressed(Keyboard::KEY_MINUS) and course.animation.cameraDepth > 0.005)
		course.animation.cameraDepth -= 0.005;
	if(Keyboard::isKeyPressed(Keyboard::KEY_NUMPAD_ADDITION))
		course.animation.drawDistance += 10;
	if(Keyboard::isKeyPressed(Keyboard::KEY_NUMPAD_SUBTRACTION) and course.animation.drawDistance > 10)
		course.animation.drawDistance -= 10;
	if(Keyboard::isKeyPressed(Keyboard::KEY_NUMPAD_MULTIPLICATION))
		for(unsigned i = 0; i < raceScreenData.size(); i++)
			raceScreenData[i].cameraPositionOffset += 0.1f;
	if(Keyboard::isKeyPressed(Keyboard::KEY_NUMPAD_DIVISION))
		for(unsigned i = 0; i < raceScreenData.size(); i++)
			raceScreenData[i].cameraPositionOffset -= 0.1f;
}

void Pseudo3DRaceState::onKeyPressed(Keyboard::Key key)
{
	for(unsigned i = 0; i < playersControls.size(); i++)
	{
		PlayerControls& playerControls = playersControls[i];
		RacingVehicle& playerVehicle = racingVehicles[i];

		if(playerControls.upshift.isCommandTriggeredByKeyPress(key))
			playerVehicle.body.shiftGear(playerVehicle.body.engine.gear+1);  // TODO play gear shift sound

		if(playerControls.downshift.isCommandTriggeredByKeyPress(key))
			playerVehicle.body.shiftGear(playerVehicle.body.engine.gear-1);  // TODO play gear shift sound

		if(playerControls.powerBoost.isCommandTriggeredByKeyPress(key) and not playerVehicle.isPowerBoostActive)
		{
			if(playerVehicle.powerBoostCount > 0)
			{
				playerVehicle.isPowerBoostActive = true;
				playerVehicle.powerBoostLastActivationTime = fgeal::uptime();
				playerVehicle.powerBoostCount--;
				playerVehicle.sndBoostStart->play();
			}
			else
			{
				// TODO play "empty" nitro sound and effects
			}
		}
	}

	switch(key)
	{
		case Keyboard::KEY_ESCAPE:
			if(game.logic.raceOnlyMode)
				game.running = false;
			else
				game.enterState(game.logic.currentMainMenuStateId);
			break;

		// == DEBUG ==================================================================================
		case Keyboard::KEY_BACKSPACE:
			setupRace();
			break;
		case Keyboard::KEY_T:
			for(unsigned i = 0; i < params.playerCount; i++)
				racingVehicles[i].body.automaticShiftingEnabled = !racingVehicles[i].body.automaticShiftingEnabled;

			break;
		case Keyboard::KEY_M:
			if(music != null)
			{
				if(music->isPlaying())
					music->pause();
				else
					music->resume();
			}
			break;
		case Keyboard::KEY_D:
			debugMode = !debugMode;
			break;
		case Keyboard::KEY_H:
			if(Keyboard::isKeyPressed(Keyboard::KEY_RIGHT_CONTROL))
				course.animation.pseudoFogggingAvoidBackground = !course.animation.pseudoFogggingAvoidBackground;
			else
				course.animation.isHeadlightsEffectEnabled = !course.animation.isHeadlightsEffectEnabled;
			break;
		case Keyboard::KEY_END:
			if(Keyboard::isKeyPressed(Keyboard::KEY_RIGHT_SHIFT))
				course.animation.fogging = 0;
			if(Keyboard::isKeyPressed(Keyboard::KEY_RIGHT_CONTROL))
				course.animation.pseudoFogging = 0;
			else
				course.animation.lightFiltering = 0;
			break;

		case Keyboard::KEY_L:
			course.animation.spec.laneCount++;
			break;
		case Keyboard::KEY_O:
			if(course.animation.spec.laneCount > 0)
				course.animation.spec.laneCount--;
			break;
		// ===========================================================================================
		default:
			break;
	}
}

void Pseudo3DRaceState::onJoystickButtonPressed(unsigned joystick, unsigned button)
{
	for(unsigned i = 0; i < playersControls.size(); i++)
	{
		PlayerControls& playerControls = playersControls[i];
		RacingVehicle& playerVehicle = racingVehicles[i];
		if(playerControls.isCommandTriggeredByJoypadButtonPress(playerControls.upshift, joystick, button))
			playerVehicle.body.shiftGear(playerVehicle.body.engine.gear+1);  // TODO play gear shift sound

		if(playerControls.isCommandTriggeredByJoypadButtonPress(playerControls.downshift, joystick, button))
			playerVehicle.body.shiftGear(playerVehicle.body.engine.gear-1);  // TODO play gear shift sound

		if(playerControls.isCommandTriggeredByJoypadButtonPress(playerControls.powerBoost, joystick, button) and not playerVehicle.isPowerBoostActive)
		{
			if(playerVehicle.powerBoostCount > 0)
			{
				playerVehicle.isPowerBoostActive = true;
				playerVehicle.powerBoostLastActivationTime = fgeal::uptime();
				playerVehicle.powerBoostCount--;
				playerVehicle.sndBoostStart->play();
			}
			else
			{
				// TODO play "empty" nitro sound and effects
			}
		}
	}
}

float Pseudo3DRaceState::getPlayerSteeringInput(unsigned playerIndex)
{
	return (std::max(playersControls[playerIndex].getCommandValue(playersControls[playerIndex].turnRight), 0.f) - std::max(playersControls[playerIndex].getCommandValue(playersControls[playerIndex].turnLeft), 0.f));
}

bool Pseudo3DRaceState::PlayerControls::Command::isCommandTriggeredByKeyPress(Keyboard::Key key) const
{
	return this->key != 0 and key == this->key;
}

bool Pseudo3DRaceState::PlayerControls::isCommandTriggeredByJoypadButtonPress(const Command& command, unsigned joypadIndex, unsigned joypadButtonIndex) const
{
	return joypadIndex == this->joystickIndex and command.joystickButtonIndex != -1
	and not command.joystickButtonIsAxis and (int) joypadButtonIndex == command.joystickButtonIndex;
}

bool Pseudo3DRaceState::PlayerControls::isCommandTriggeredByJoypadAxisMotion(const Command& command, unsigned joypadIndex, unsigned joypadAxisIndex, float joypadAxisPosition) const
{
	return joypadIndex == this->joystickIndex and command.joystickButtonIndex != -1
	and command.joystickButtonIsAxis and (int) joypadAxisIndex == command.joystickButtonIndex
	and sgn(joypadAxisPosition) == command.joystickAxisSignal and command.joystickAxisSignal * joypadAxisPosition > deadzone;
}

float Pseudo3DRaceState::PlayerControls::getCommandValue(const Command& command) const
{
	if(command.key != 0 and Keyboard::isKeyPressed(command.key))
		return 1;

	else if(command.joystickButtonIndex != -1 and joystickIndex < Joystick::getCount())
	{
		if(command.joystickButtonIsAxis)
		{
			const float value = Joystick::getAxisPosition(joystickIndex, command.joystickButtonIndex);
			if(sgn(value) == command.joystickAxisSignal and command.joystickAxisSignal * value > deadzone)
				return command.joystickAxisSignal * value - deadzone;
		}
		else if(Joystick::isButtonPressed(joystickIndex, command.joystickButtonIndex))
			return 1;
	}

	return 0;
}
