/*
 * race_state.hpp
 *
 *  Created on: 29 de mar de 2017
 *      Author: carlosfaruolo
 */

#ifndef PSEUDO3D_RACE_STATE_HPP_
#define PSEUDO3D_RACE_STATE_HPP_
#include <ciso646>

#include "course.hpp"
#include "vehicle.hpp"
#include "vehicle_ai.hpp"

#include "engine_sound.hpp"

#include "race_hud.hpp"
#include "course_map_gfx.hpp"

#include "futil/language.hpp"

#include "fgeal/fgeal.hpp"
#include "fgeal/extra/game.hpp"
#include "fgeal/extra/sprite.hpp"

#include <vector>
#include <utility>

// fwd. declared
class Carse;

class Pseudo3DRaceState extends public fgeal::Game::State
{
	public:
	enum RaceType
	{
		RACE_TYPE_DEBUG,
		RACE_TYPE_PRACTICE,
		RACE_TYPE_TIME_TRIAL,
		//RACE_TYPE_TIME_ATTACK,
		RACE_TYPE_STANDARD_RACE,
		//RACE_TYPE_LAP_KNOCK_OUT,
		//RACE_TYPE_TIME_KNOCK_OUT,
		//RACE_TYPE_DISTANCE_KNOCK_OUT,
		//RACE_TYPE_DRAG_RACE,
		//RACE_TYPE_DRAG_PRACTICE,
		//RACE_TYPE_SIDE_DRAG_RACE,
		//RACE_TYPE_SIDE_DRAG_PRACTICE,
		//RACE_TYPE_DRIFT_PRACTICE,
		//RACE_TYPE_DRIFT_COMPETITION,
		//RACE_TYPE_OVERALL_SPEED_COMPETITION,
		//RACE_TYPE_AVERAGE_SPEED_COMPETITION,
		//RACE_TYPE_OVERTAKING_DUEL,
		//RACE_TYPE_PURSUIT
		RACE_TYPE_COUNT
	};

	struct Parameters
	{
		RaceType raceType;

		bool isLapped;
		unsigned lapCountGoal;

		unsigned playerCount;
		unsigned cpuOpponentCount;
		std::string opponentVehicleCategory;
		float trafficDensity;

		Mechanics::SimulationType simulationType;

		bool enableJumpSimulation;  // flag to enable vehicle jump simulation (experimental)
		bool disableSteeringSpeedLimit;  // flag to disable limiting steering speed to a reasonable, human-like value (this is NOT cornering/turning/"strafing" speed)

		unsigned powerBoostCount;  // TODO refine this parameter

		bool isWeatherEffectEnforced;
		Pseudo3DCourseAnimation::Spec::WeatherEffect forcedWeatherEffect;

		Pseudo3DCourseAnimation::CameraBehavior cameraBehavior;
		bool isImperialUnit, useCachedDialGauge, isCockpitView;

		int musicOption;  // -1 means default; -2 means random; -3 means no music; positive values are music list indexes
	};

	struct PlayerControls
	{
		unsigned joystickIndex;
		float deadzone;
		struct Command
		{
			int joystickButtonIndex;  // either a button index or an axis index (or -1 if unassigned)
			fgeal::Keyboard::Key key;  // 0 if unassigned
			signed char joystickAxisSignal;  // if joypad axis, either 1 (axis+) or -1 (axis-), otherwise unused
			bool joystickButtonIsAxis;  // flags if this control's joystick button is actually an axis (analogue input)

			Command() : joystickButtonIndex(-1), key(static_cast<fgeal::Keyboard::Key> (0)), joystickAxisSignal(), joystickButtonIsAxis() {}

			bool isCommandTriggeredByKeyPress(fgeal::Keyboard::Key) const;
		};

		bool isCommandTriggeredByJoypadButtonPress(const Command& command, unsigned joypadIndex, unsigned joypadButtonIndex) const;
		bool isCommandTriggeredByJoypadAxisMotion(const Command& command, unsigned joypadIndex, unsigned joypadAxisIndex, float joypadAxisPosition) const;
		float getCommandValue(const Command& command) const;

		Command accelerate, braking, turnLeft, turnRight, upshift, downshift, powerBoost;
	};

	static const float MAXIMUM_STRAFE_SPEED_FACTOR;

	//==================================================================================================================
	private:
	struct RacingVehicle extends Pseudo3DVehicle
	{
		unsigned ranking;
		float lapTimeCurrent, lapTimeLast, lapTimeBest;

		static bool compare(const RacingVehicle* const rva, const RacingVehicle* const rvb);
	};

	Carse& game;

	fgeal::Vector2D lastDisplaySize;

	fgeal::Font* fontDebug, *fontSpeedometer, *fontSpeedUnit, *fontGraduation, *fontGraduationSmall, *fontCountdown, *fontTimers, *fontLaps;

	fgeal::Image* imgStopwatch;
	fgeal::Music* music;

	fgeal::Sound* sndCountdownBuzzer, *sndCountdownBuzzerFinal;

	fgeal::Sprite* spriteSmoke, *spriteBackfire;

	Parameters params;

	// value used to convert physics position to course segment position
	float coursePositionFactor;

	// offset of the starting position of the player
	float courseStartPositionOffset;

	// the space between racers in starting grid
	float racersStartPositionOffset;

	bool onSceneIntro;
	float timerSceneIntro;

	unsigned countdownBuzzerCounter;

	unsigned screenGridLines, screenGridColumns;

	//debug
	float acc0to60clock, acc0to60time;

	Pseudo3DCourse course;

	// a vector containing all racing vehicles; the player-controlled ones are stored first, then the CPU-controlled right after
	std::vector<RacingVehicle> racingVehicles;
	std::vector<Pseudo3DVehicle> trafficVehicles;

	// aux. vector meant for accounting for racing vehicles standings during the race
	std::vector<RacingVehicle*> standings;

	// data used per race screen
	struct RaceScreenData
	{
		float cameraPositionOffset, cameraSlopeAngle;
		Hud::VehicleDashboard dashboard;
		float timerSceneFinish;
		bool isOnSceneFinish;
	};
	std::vector<RaceScreenData> raceScreenData;  // the camera target will be the vehicle in racingVehicles at same index

	// common HUD stuff --------------------------------------------------------------
	Hud::BarGauge<float> hudThrottleBar, hudBrakeBar, hudSteeringBar, hudFuelAmount;

	fgeal::Label hudTimerCurrentLapLabel;
	Hud::TimerDisplayWidget hudTimerCurrentLap;

	fgeal::Label hudTimerLastLapLabel;
	Hud::TimerDisplayWidget hudTimerLastLap;

	fgeal::Label hudTimerBestLapLabel;
	Hud::TimerDisplayWidget hudTimerBestLap;

	fgeal::Rectangle stopwatchIconBounds;

	fgeal::Label hudLapLabel, hudLapCountGoal;
	Hud::DigitDisplayWidget hudCurrentLap;

	fgeal::Label hudProgressLabel;
	Hud::DigitDisplayWidget hudProgress;

	fgeal::Label hudPlayerRankingLabel, hudPlayerRankingGoal;
	Hud::DigitDisplayWidget hudPlayerRanking;

	fgeal::Label hudBoostCounterLabel;
	Hud::DigitDisplayWidget hudBoostCounter;

	Pseudo3DCourseMap minimap;
	fgeal::Color hudMiniMapBgColor;

	fgeal::Point posHudCountdown, posHudFinishedCaption;

	std::vector<PlayerControls> playersControls;

	Pseudo3DVehicleAI ai;

	// var to control big deltas in first cycles due to long loading times
	int firstCyclesControl;

	fgeal::Color debugTextColor;
	bool debugMode;

	float getPlayerSteeringInput(unsigned playerIndex);

	public:
	virtual int getId();

	Pseudo3DRaceState(Carse* game);
	~Pseudo3DRaceState();

	virtual void initialize();

	virtual void onEnter();
	virtual void onLeave();

	virtual void update(float delta);
	virtual void render();

	virtual void onKeyPressed(fgeal::Keyboard::Key);
	virtual void onJoystickButtonPressed(unsigned joystick, unsigned button);

	private:
	void setupRace();

	void handlePhysics(float delta);
	void updateVehiclePhysics(Pseudo3DVehicle&, float delta, bool curvePullEnabled=true);

	void translateHudElements(fgeal::Vector2D offset);
	void drawScreen(RaceScreenData&, RacingVehicle&);

	// debug
	void drawDebugInfo();
	void fastValueDraw(const char* const format, float posx, float posy, float value, const float* const extraValues=null) const;

	bool isOffRoad(const Pseudo3DVehicle& vehicle);
};

#endif /* PSEUDO3D_RACE_STATE_HPP_ */
