/*
 * race_state_physics.cpp
 *
 *  Created on: 24 de jul de 2018
 *      Author: carlosfaruolo
 */

#include "pseudo3d_race_state.hpp"

#include "carse.hpp"

#include "util/sizing.hpp"

#include <cfloat>
#include <cmath>

using std::vector;

template <typename T> static inline
T pow2(T val) { return val*val; }

static inline
float fractional_part(float value)
{
	return value - (int) value;
}

static const float COLLISION_RESTITUTION_COEFFICIENT = 0.5,
				   PROP_COLLISION_MAX_STUMBLE_SPEED = 4.2,  // ~= 15kph
				   PSEUDO_ANGLE_YAW_SPEED_MAGNITUDE = 2.0,
				   MINIMUM_SPEED_ALLOW_TURN = 1.0/36.0,  // == 1kph
				   MINIMUM_SPEED_CORNERING_LEECH = 10;  // == 36kph

void Pseudo3DRaceState::updateVehiclePhysics(Pseudo3DVehicle& vehicle, float delta, bool curvePullEnabled)
{
	const unsigned courseSegmentCount = course.spec.lines.size(), courseSegmentIndex = static_cast<int>(vehicle.position * coursePositionFactor / course.spec.roadSegmentLength) % courseSegmentCount;
	const CourseSpec::Segment& courseSegment = course.spec.lines[courseSegmentIndex], &nextCourseSegment = course.spec.lines[(courseSegmentIndex+1) % courseSegmentCount];
	const float courseSegmentProportion = 1 - fractional_part(vehicle.position * coursePositionFactor / course.spec.roadSegmentLength),
				courseSegmentInterpolatedHeight = courseSegmentProportion*courseSegment.y + (1-courseSegmentProportion)*nextCourseSegment.y,
				courseSegmentHeightDiff = nextCourseSegment.y - courseSegment.y,
				corneringForceLeechFactor = vehicle.body.speed > MINIMUM_SPEED_CORNERING_LEECH? (vehicle.body.spec.vehicleType == Mechanics::TYPE_BIKE? 0.4 : 0.5) : 0,
				offRoadingFactor = 1 + 0.5f * std::max(0.f, std::abs(vehicle.horizontalPosition * coursePositionFactor)/course.spec.roadWidth - 1) * std::max(vehicle.body.speed - MINIMUM_SPEED_CORNERING_LEECH, 0.f),
				maxHorizontalPosition = 4 * course.spec.roadWidth;

	// update steering
	const float pseudoAngleYawVelocity = std::abs(vehicle.body.speed) < MINIMUM_SPEED_ALLOW_TURN? 0 : PSEUDO_ANGLE_YAW_SPEED_MAGNITUDE * vehicle.steeringWheelPosition;

	// lame self-aligning behavior
//	if(pseudoAngleYawVelocity == 0 or sgn(pseudoAngleYawVelocity) != sgn(vehicle.pseudoAngle))
//		vehicle.pseudoAngle *= 1/(1+5*delta);

	// lame self-aligning behavior, improved to act gradually on the absence of active steering (or when counter-steering)
	vehicle.pseudoAngle += std::max(1-std::abs(vehicle.steeringWheelPosition), (float)(sgn(pseudoAngleYawVelocity) != sgn(vehicle.pseudoAngle))) * (1/(1+5*delta)-1) * vehicle.pseudoAngle;

	// active steering
	vehicle.pseudoAngle += pseudoAngleYawVelocity * delta;

	// another approach which combines both steering and self-alignment in a single formula
//	vehicle.pseudoAngle += (pseudoAngleYawVelocity - sgn(vehicle.pseudoAngle)) * delta * 2.f;

	if(std::abs(vehicle.pseudoAngle) > 1.0f)
		vehicle.pseudoAngle = sgn(vehicle.pseudoAngle);

	const float wheelAngleFactor = 1 - corneringForceLeechFactor * std::abs(vehicle.pseudoAngle);

	vehicle.body.airDensityFactor = GameLogic::PHYSICS_AIR_DENSITY_DEFAULT;  // TODO dynamically lower this when slipstreaming
	vehicle.body.tireFrictionFactor = vehicle.onAir? 0 : isOffRoad(vehicle)? course.spec.offRoadFrictionCoefficient : course.spec.roadFrictionCoefficient;
	vehicle.body.rollingResistanceFactor = vehicle.onAir? 0 : (isOffRoad(vehicle)? course.spec.offRoadRollingResistance : course.spec.roadRollingResistance) * offRoadingFactor;
	vehicle.body.arbitraryForceFactor = wheelAngleFactor * (vehicle.isPowerBoostActive? vehicle.powerBoostIncreaseFactor : 1.f);

	if(params.enableJumpSimulation)
	{
		if(vehicle.onAir)
			vehicle.verticalSpeed -= 10 * vehicle.body.gravityAccelerationFactor * delta;
		else
			vehicle.verticalSpeed = std::abs(vehicle.body.speed) * sin(vehicle.body.slopeAngle);

		vehicle.verticalPosition += coursePositionFactor * vehicle.verticalSpeed * delta;

		if(courseSegmentInterpolatedHeight >= vehicle.verticalPosition)
		{
			vehicle.verticalPosition = courseSegmentInterpolatedHeight;
			if(vehicle.onLongAir)
				vehicle.justHardLanded = true;
			vehicle.onAir = vehicle.onLongAir = false;
		}
		else
		{
			vehicle.onAir = true;
			if(vehicle.verticalPosition - courseSegmentInterpolatedHeight > 500)
				vehicle.onLongAir = true;
		}
	}
	else
	{
		// update vertical position (set on ground)
		vehicle.verticalPosition = courseSegmentInterpolatedHeight;
	}

	if(vehicle.onAir)
		// makes the vehicle tilt forward, slower when speed is higher; also, gravity acts on center of mass, so there is no torque due to uneven weight dist.
		vehicle.body.slopeAngle += delta*(-M_PI_2 - vehicle.body.slopeAngle)/(1 + vehicle.body.speed * 0.05f);
	else
		// TODO replace this approximation with a proper interpolation between slope angles of current segment and next one (should involve 3 consecutive segments in the formula)
		vehicle.body.slopeAngle = atan2(courseSegmentHeightDiff, course.spec.roadSegmentLength);

	vehicle.body.updatePowertrain(delta);

	// update position
	vehicle.position += vehicle.body.speed*delta;

	// update strafing
	vehicle.strafeSpeed = vehicle.onLongAir? 0 : vehicle.pseudoAngle * vehicle.body.speed;

	// limit strafing speed by magic constant
	const float maxStrafeSpeed = MAXIMUM_STRAFE_SPEED_FACTOR * vehicle.corneringStiffness;
	if(std::abs(vehicle.strafeSpeed) > maxStrafeSpeed)
		vehicle.strafeSpeed = maxStrafeSpeed * sgn(vehicle.strafeSpeed);

	// update curve pull
//	const float curvatureFactor = 2 * sin(0.5 * courseSegment.curve);  // correct formula according to theory, assuming 'courseSegment.curve' is the degree of curvature, in radians. however it does not behave nicely...
//	const float curvatureFactor = 2 * sin(atan(0.5 * courseSegment.curve));
//	const float curvatureFactor = 2 * sin(atan(0.4 * pow2(courseSegment.curve))) * sgn(courseSegment.curve);
//	const float curvatureFactor = sqrt(2 * courseSegment.curve) * atan(0.5 * courseSegment.curve);
//	const float curvatureFactor = sqrt(2 * std::abs(courseSegment.curve)) * atan(0.25 * pow2(courseSegment.curve)) * sgn(courseSegment.curve);
//	const float curvatureFactor = 1.25 * courseSegment.curve - atan(courseSegment.curve);
	const float curvatureFactor = 1.25 * courseSegment.curve;

	if(curvePullEnabled)
		vehicle.curvePull += ((pow2(vehicle.body.speed) * curvatureFactor / course.spec.roadSegmentLength) - vehicle.curvePull) * 10 * delta;
	else
		vehicle.curvePull = 0;

//	if(curvePullEnabled)
//		vehicle.curvePull += (courseSegment.curve * vehicle.body.speed - vehicle.curvePull) * vehicle.body.speed * delta;

	// update strafe position
	vehicle.horizontalPosition += (vehicle.strafeSpeed - vehicle.curvePull)*delta;

	if(std::abs(vehicle.horizontalPosition * coursePositionFactor) > maxHorizontalPosition)
		vehicle.horizontalPosition = sgn(vehicle.horizontalPosition) * maxHorizontalPosition / coursePositionFactor;

	// update "virtual" orientation
	vehicle.virtualOrientation += courseSegment.curve * vehicle.body.speed * delta;  // FIXME get a proper formula for this, instead of this rough approximation
}

#define VEHICLE_VIRTUAL_LENGTH 2.0f
enum Side { SIDE_NONE=0, SIDE_TOP, SIDE_BOTTOM, SIDE_LEFT, SIDE_RIGHT };

static inline float widthOf(const Pseudo3DVehicle& vehicle)
{
	return vehicle.animation.spec.depictedVehicleWidth * vehicle.animation.sprites.back()->scale.x * 7;
}

// returns in which side of former vehicle the collision was or SIDE_NONE if no collisions occurred
static inline Side collides(const Pseudo3DVehicle& former, const Pseudo3DVehicle& latter)
{
	const fgeal::Rectangle body1 = { former.horizontalPosition - 0.5f*widthOf(former), former.position, widthOf(former), VEHICLE_VIRTUAL_LENGTH },
						   body2 = { latter.horizontalPosition - 0.5f*widthOf(latter), latter.position, widthOf(latter), VEHICLE_VIRTUAL_LENGTH };
	if(body1.intersects(body2))
	{
		const float wy = (body2.w + body1.w) * (body2.y - body1.y + 0.5 * (body2.h - body1.h)),
					hx = (body2.h + body1.h) * (body2.x - body1.x + 0.5 * (body2.w - body1.w));
		if (wy > hx)
			if (wy > -hx)
				return SIDE_TOP;
			else
				return SIDE_LEFT;
		else
			if (wy > -hx)
				return SIDE_RIGHT;
			else
				return SIDE_BOTTOM;
	}
	else return SIDE_NONE;
}

static inline void resolveRigidLatterCollision(Pseudo3DVehicle& former, const Pseudo3DVehicle& latter, const Side sideOfFormer)
{
	switch(sideOfFormer)
	{
		case SIDE_TOP:
		{
			if(former.body.speed > latter.body.speed)
				former.body.speed = latter.body.speed;
			former.position = latter.position - VEHICLE_VIRTUAL_LENGTH - FLT_EPSILON;
			break;
		}
		case SIDE_BOTTOM:
		{
			if(former.body.speed < latter.body.speed)
				former.body.speed = latter.body.speed;
			former.position = latter.position + VEHICLE_VIRTUAL_LENGTH + FLT_EPSILON;
			break;
		}
		case SIDE_LEFT:
		{
			former.horizontalPosition = latter.horizontalPosition + widthOf(latter) + FLT_EPSILON;
			break;
		}
		case SIDE_RIGHT:
		{
			former.horizontalPosition = latter.horizontalPosition - widthOf(former) - FLT_EPSILON;
			break;
		}
		default:break;
	}
}
static inline void resolveCollision(Pseudo3DVehicle& former, Pseudo3DVehicle& latter, const Side sideOfFormer)
{
	switch(sideOfFormer)
	{
		case SIDE_TOP:
		case SIDE_BOTTOM:
		{
			const float v1PreviousSpeed = former.body.speed;

			// exchange former vehicle's speed with latter's using collision formula
			former.body.speed = (COLLISION_RESTITUTION_COEFFICIENT * latter.body.spec.mass * (latter.body.speed - former.body.speed)
										+ former.body.spec.mass * former.body.speed + latter.body.spec.mass * latter.body.speed)
												/(former.body.spec.mass + latter.body.spec.mass);


			latter.body.speed = (COLLISION_RESTITUTION_COEFFICIENT * former.body.spec.mass * (v1PreviousSpeed - latter.body.speed)
										+ latter.body.spec.mass * latter.body.speed + former.body.spec.mass * v1PreviousSpeed)
												/(latter.body.spec.mass + former.body.spec.mass);
			if(sideOfFormer == SIDE_TOP)
			{
				const float posDiff = former.position + VEHICLE_VIRTUAL_LENGTH - latter.position;
				former.position -= FLT_EPSILON + posDiff * former.body.speed / (former.body.speed + latter.body.speed);
				latter.position += FLT_EPSILON + posDiff * latter.body.speed / (former.body.speed + latter.body.speed);
			}
			else
			{
				const float posDiff = latter.position + VEHICLE_VIRTUAL_LENGTH - former.position;
				former.position += FLT_EPSILON + posDiff * former.body.speed / (former.body.speed + latter.body.speed);
				latter.position -= FLT_EPSILON + posDiff * latter.body.speed / (former.body.speed + latter.body.speed);
			}
			break;
		}
		case SIDE_LEFT:
		case SIDE_RIGHT:
		{
			/*
			const float v1PreviousSpeed = former.strafeSpeed;

			// exchange former vehicle's speed with latter's using collision formula
			former.strafeSpeed = (COLLISION_RESTITUTION_COEFFICIENT * latter.body.mass * (latter.strafeSpeed - former.strafeSpeed)
										+ former.body.mass * former.strafeSpeed + latter.body.mass * latter.strafeSpeed)
												/(former.body.mass + latter.body.mass);


			latter.strafeSpeed = (COLLISION_RESTITUTION_COEFFICIENT * former.body.mass * (v1PreviousSpeed - latter.strafeSpeed)
										+ latter.body.mass * latter.strafeSpeed + former.body.mass * v1PreviousSpeed)
												/(latter.body.mass + former.body.mass);
			*/
			former.pseudoAngle = latter.pseudoAngle = (former.pseudoAngle + latter.pseudoAngle)/2;

			if(sideOfFormer == SIDE_LEFT)
			{
				const float posDiff = latter.horizontalPosition + widthOf(latter) - former.horizontalPosition;
				former.horizontalPosition += FLT_EPSILON + posDiff * former.strafeSpeed / (former.strafeSpeed + latter.strafeSpeed);
				latter.horizontalPosition -= FLT_EPSILON + posDiff * latter.strafeSpeed / (former.strafeSpeed + latter.strafeSpeed);
			}
			else
			{
				const float posDiff = former.horizontalPosition + widthOf(former) - latter.horizontalPosition;
				former.horizontalPosition -= FLT_EPSILON + posDiff * former.strafeSpeed / (former.strafeSpeed + latter.strafeSpeed);
				latter.horizontalPosition += FLT_EPSILON + posDiff * latter.strafeSpeed / (former.strafeSpeed + latter.strafeSpeed);
			}
			break;
		}
		default:break;
	}
}

void Pseudo3DRaceState::handlePhysics(float delta)
{
	const unsigned courseSegmentCount = course.spec.lines.size();

	// update traffic vehicles
	for(unsigned i = 0; i <  trafficVehicles.size(); i++)
		updateVehiclePhysics(trafficVehicles[i], delta, false);

	// update opponent vehicles
	for(unsigned i = params.playerCount; i < racingVehicles.size(); i++)
		updateVehiclePhysics(racingVehicles[i], delta);

	for(unsigned i = 0; i < params.playerCount; i++)
	{
		if(onSceneIntro)
			racingVehicles[i].body.engine.gear = 0;

		updateVehiclePhysics(racingVehicles[i], delta);
	}

	// collision checking
	for(unsigned i = 0; i < params.playerCount; i++)
	{
		RacingVehicle& playerVehicle = racingVehicles[i];
		const unsigned courseSegmentIndex = static_cast<int>(playerVehicle.position * coursePositionFactor / course.spec.roadSegmentLength) % courseSegmentCount;
		const CourseSpec::Segment& courseSegment = course.spec.lines[courseSegmentIndex];

		// verify for ornament collision
		bool collidesWithOrnament = false;
		if(courseSegment.decoration) for(unsigned j = 0; j < course.spec.decorations[courseSegment.decoration].size(); j++)
		{
			const CourseSpec::Ornament& ornament = course.spec.decorations[courseSegment.decoration][j];
			if(course.spec.ornaments[ornament.spec].blocking)
			{
				const fgeal::Rectangle body1 = { playerVehicle.horizontalPosition - 0.0005f * playerVehicle.body.spec.width, 0,
				                                 playerVehicle.body.spec.width * 0.001f, 1 },
									   body2 = { ornament.x - 0.5f * course.spec.ornaments[ornament.spec].width, 0,
				                                 course.spec.ornaments[ornament.spec].width, 1 };
				collidesWithOrnament = body1.intersects(body2);
			}
		}
		if(collidesWithOrnament)
		{
			playerVehicle.position -= sgn(playerVehicle.body.speed) * course.spec.roadSegmentLength/coursePositionFactor;
			playerVehicle.body.speed = -PROP_COLLISION_MAX_STUMBLE_SPEED * playerVehicle.body.speed/(1 + std::abs(playerVehicle.body.speed));
			playerVehicle.body.engine.gear = 1;
			playerVehicle.isCrashing = true;
		}
		// verify for vehicle collisions
		else
		{
			// verify for player-traffic collisions
			for(unsigned k = 0; k <  trafficVehicles.size(); k++)
				if(Side where = collides(playerVehicle, trafficVehicles[k]))
					resolveRigidLatterCollision(playerVehicle, trafficVehicles[k], where), playerVehicle.isCrashing = true;

			// verify for player-player and player-cpu-opponent collisions
			for(unsigned k = 0; k < racingVehicles.size(); k++)
				if(k != i)  // no self collision
					if(Side where = collides(playerVehicle, racingVehicles[k]))
						resolveCollision(playerVehicle, racingVehicles[k], where), playerVehicle.isCrashing = true;
		}
	}
}

bool Pseudo3DRaceState::isOffRoad(const Pseudo3DVehicle& vehicle)
{
	return std::abs(vehicle.horizontalPosition * coursePositionFactor) > 1.2*course.spec.roadWidth;
}
