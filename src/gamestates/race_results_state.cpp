/*
 * race_results_state.cpp
 *
 *  Created on: 6 de mai de 2020
 *      Author: hydren
 */

#include "race_results_state.hpp"

#include "carse.hpp"

#include "util/sizing.hpp"

using fgeal::Display;
using fgeal::Image;
using fgeal::Keyboard;
using fgeal::Graphics;
using fgeal::Color;
using fgeal::Font;

int RaceResultsState::getId() { return Carse::RACE_RESULTS_STATE_ID; }

RaceResultsState::RaceResultsState(Carse* game)
: State(*game), game(*game),
  background(null),
  sndCursorMove(null), sndCursorIn(null), sndCursorOut(null),
  font(null), fontTitle(null)
{}

RaceResultsState::~RaceResultsState()
{
	if(background != null) delete background;
	if(font != null) delete font;
	if(fontTitle != null) delete fontTitle;
}

void RaceResultsState::initialize()
{
	background = new Image("assets/ui/racetrack-bg.jpg");
	font = new Font(gameFontParams("ui_race_results_items"));
	fontTitle = new Font(gameFontParams("ui_race_results_title"));

	// loan some shared resources
	sndCursorMove = &game.sharedResources->sndCursorMove;
	sndCursorIn   = &game.sharedResources->sndCursorIn;
	sndCursorOut  = &game.sharedResources->sndCursorOut;
}

void RaceResultsState::onEnter()
{
	// reload fonts if display size changed
	if(lastDisplaySize.x != game.getDisplay().getWidth() or lastDisplaySize.y != game.getDisplay().getHeight())
	{
		const FontSizer fs(game.getDisplay().getHeight());
		font->setSize(fs(game.logic.getFontSize("ui_race_results_items")));
		fontTitle->setSize(fs(game.logic.getFontSize("ui_race_results_title")));
	}
}

void RaceResultsState::render()
{
	Display& display = game.getDisplay();
	display.clear();

	background->drawScaled(0, 0, scaledToSize(background, display), Image::FLIP_HORIZONTAL);
	fontTitle->drawText("Race results", display.getWidth()/10, display.getHeight()/25, Color::BLACK);
	fontTitle->drawText("Race results", display.getWidth()/10, display.getHeight()/30, ~Color::CREAM);
	Graphics::drawFilledRectangle(display.getWidth()/15, display.getHeight()/6, 0.875f*display.getWidth(), 0.75f*display.getHeight(), Color::create(64, 64, 64, 64));
}

void RaceResultsState::update(float delta)
{}

void RaceResultsState::onKeyPressed(Keyboard::Key key)
{
	switch(key)
	{
		case Keyboard::KEY_ESCAPE:
		case Keyboard::KEY_ENTER:
			game.enterState(game.logic.currentMainMenuStateId);
			break;
		case Keyboard::KEY_ARROW_UP:
			sndCursorMove->play();
			break;
		case Keyboard::KEY_ARROW_DOWN:
			sndCursorMove->play();
			break;
		case Keyboard::KEY_ARROW_LEFT:
			sndCursorMove->play();
			break;
		case Keyboard::KEY_ARROW_RIGHT:
			sndCursorMove->play();
			break;
		default:
			break;
	}
}

void RaceResultsState::onMouseButtonPressed(fgeal::Mouse::Button button, int x, int y)
{}

void RaceResultsState::onLeave()
{}
