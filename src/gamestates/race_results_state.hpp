/*
 * race_results_state.hpp
 *
 *  Created on: 6 de mai de 2020
 *      Author: hydren
 */

#ifndef RACE_RESULTS_STATE_HPP_
#define RACE_RESULTS_STATE_HPP_
#include <ciso646>

#include "fgeal/fgeal.hpp"
#include "fgeal/extra/game.hpp"

// fwd. declared
class Carse;

class RaceResultsState extends public fgeal::Game::State
{
	Carse& game;

	fgeal::Vector2D lastDisplaySize;

	fgeal::Image* background;
	fgeal::Sound* sndCursorMove, *sndCursorIn, *sndCursorOut;
	fgeal::Font* font, *fontTitle;

	public:
	virtual int getId();

	RaceResultsState(Carse* game);
	~RaceResultsState();

	virtual void initialize();
	virtual void onEnter();
	virtual void onLeave();

	virtual void onKeyPressed(fgeal::Keyboard::Key k);
	virtual void onMouseButtonPressed(fgeal::Mouse::Button button, int x, int y);

	virtual void render();
	virtual void update(float delta);
};

#endif /* RACE_RESULTS_STATE_HPP_ */
