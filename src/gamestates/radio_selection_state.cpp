/*
 * radio_selection_state.cpp
 *
 *  Created on: 24 de mar de 2020
 *      Author: hydren
 */

#include "radio_selection_state.hpp"

#include "carse.hpp"

#include "util/sizing.hpp"
#include "futil/string_actions.hpp"

using fgeal::Display;
using fgeal::Image;
using fgeal::Keyboard;
using fgeal::Graphics;
using fgeal::Color;
using fgeal::Font;
using std::string;

string toFormatedTimer(float elapsedTime)
{
	int timeSec = elapsedTime, timeMin = timeSec/60;
	timeSec -= timeMin*60;
	return (timeMin < 10?  "0" :  "") + futil::to_string(timeMin) + (timeSec < 10? " 0" : " ") + futil::to_string(timeSec);
}

//----------------------------------------------------------------------------------------------------------------------

int RadioSelectionState::getId() { return Carse::RADIO_SELECTION_STATE_ID; }

RadioSelectionState::RadioSelectionState(Carse* game)
: State(*game), game(*game),
  background(null), fontTitle(null), fontDisplay(null),
  sndCursorMove(null), sndCursorIn(null), sndCursorOut(null), musicPreview(null),
  radioPanel(), radioNameDisplay(), radioTimeDisplay(),
  skipNextButton(), skipBackButton(), playButton(),
  selectedMusicOptionIndex(), playbacktTime()
{}

RadioSelectionState::~RadioSelectionState()
{
	if(playButton.icon != null) delete playButton.icon;
	if(skipNextButton.icon != null) delete skipNextButton.icon;
	if(skipBackButton.icon != null) delete skipBackButton.icon;
	if(background != null) delete background;
	if(fontTitle != null) delete fontTitle;
	if(fontDisplay != null) delete fontDisplay;
	if(musicPreview != null) delete musicPreview;
}

void RadioSelectionState::initialize()
{
	background = new Image("assets/ui/dashboard-bg.jpg");
	fontTitle = new Font(gameFontParams("ui_radio_selection_title"));
	fontDisplay = new Font(gameFontParams("ui_radio_selection_display"));

	radioPanel.backgroundColor = Color::create(48, 48, 48);
	radioPanel.borderColor = Color::create(4, 4, 4);

	radioNameDisplay.backgroundColor = Color::create(8, 8, 8);
	radioNameDisplay.shape = fgeal::TextField::SHAPE_ROUNDED_RECTANGULAR;
	radioNameDisplay.caretVisible = false;
	radioNameDisplay.text.setColor(Color::AZURE);
	radioNameDisplay.text.setFont(fontDisplay);
	radioNameDisplay.textVerticalAlignment = fgeal::TextField::ALIGN_LEADING;
	radioNameDisplay.overflowBehavior = fgeal::TextField::OVERFLOW_ABBREVIATE_TRAILING;
	radioPanel.addComponent(radioNameDisplay);

	radioTimeDisplay.text = radioNameDisplay.text;
	radioTimeDisplay.text.setFont(null);
	radioTimeDisplay.text.drawCharFunction = fgeal::primitives::drawSevenSegmentCharacter;
	radioTimeDisplay.textHorizontalAlignment = fgeal::Label::ALIGN_LEADING;
	radioTimeDisplay.textVerticalAlignment = fgeal::Label::ALIGN_LEADING;
	radioPanel.addComponent(radioTimeDisplay);

	playButton.shape = fgeal::Button::SHAPE_ROUNDED_RECTANGULAR;
	playButton.backgroundColor = Color::create(64, 64, 64);
	playButton.borderColor = Color::create(16, 16, 16);
	playButton.highlightColor = Color::AZURE;
	playButton.icon = new Image("assets/ui/radio-play.png");
	radioPanel.addHighlightableComponent(playButton);

	skipNextButton = playButton;
	skipNextButton.icon = new Image("assets/ui/radio-next.png");
	radioPanel.addHighlightableComponent(skipNextButton);

	skipBackButton = playButton;
	skipBackButton.icon = new Image("assets/ui/radio-prev.png");
	radioPanel.addHighlightableComponent(skipBackButton);

	musicOptions.resize(game.logic.getMusicFilenames().size()+3);
	musicOptions[0] = "no music";
	musicOptions[1] = "random music";
	musicOptions[2] = "default music";
	for(unsigned i = 0; i < game.logic.getMusicFilenames().size(); i++)
	{
		std::string filename = game.logic.getMusicFilenames()[i];
		filename = filename.substr(filename.find_last_of("/\\") + 1);
		musicOptions[i+3] = filename.substr(0, filename.find_last_of("."));
	}

	// loan some shared resources
	sndCursorMove = &game.sharedResources->sndCursorMove;
	sndCursorIn   = &game.sharedResources->sndCursorIn;
	sndCursorOut  = &game.sharedResources->sndCursorOut;
}

void RadioSelectionState::onEnter()
{
	const float dw = game.getDisplay().getWidth(), dh = game.getDisplay().getHeight();

	// reload fonts if display size changed
	if(lastDisplaySize.x != dw or lastDisplaySize.y != dh)
	{
		const FontSizer fs(dh);
		fontTitle->setSize(fs(game.logic.getFontSize("ui_radio_selection_title")));
		fontDisplay->setSize(fs(game.logic.getFontSize("ui_radio_selection_display")));
	}

	playbacktTime = 0;
	selectedMusicOptionIndex = 2;

	radioPanel.bounds.x = 0;
	radioPanel.bounds.y = 0.2f*dh;
	radioPanel.bounds.w = dw;
	radioPanel.bounds.h = 0.6f*dh;
	radioPanel.borderThickness = 0.005f*dh;

	radioNameDisplay.bounds.x = 0.05f*dw;
	radioNameDisplay.bounds.y = dh*0.3f;
	radioNameDisplay.bounds.w = 0.9f*dw;
	radioNameDisplay.bounds.h = 0.3f*dh;
	radioNameDisplay.padding.x = 0.03f*dw;
	radioNameDisplay.padding.y = 0.05f*dh;
	radioNameDisplay.content = musicOptions[selectedMusicOptionIndex];
	radioNameDisplay.updateDrawableText();

	radioTimeDisplay.bounds.x = radioNameDisplay.bounds.x;
	radioTimeDisplay.bounds.y = radioNameDisplay.bounds.y + radioNameDisplay.bounds.h/2;
	radioTimeDisplay.bounds.w = radioNameDisplay.bounds.w;
	radioTimeDisplay.bounds.h = radioNameDisplay.bounds.h/2;
	radioTimeDisplay.padding.x = radioNameDisplay.padding.x;
	radioTimeDisplay.text.drawCharFunctionSize = 0.6f*radioTimeDisplay.bounds.h;
	radioTimeDisplay.text.setContent(toFormatedTimer(playbacktTime));

	playButton.bounds.h = 0.1*dh;
	playButton.bounds.w = 1.5*playButton.bounds.h;
	playButton.bounds.x = radioNameDisplay.bounds.x + (radioNameDisplay.bounds.w - playButton.bounds.w)/2;
	playButton.bounds.y = radioNameDisplay.bounds.y + radioNameDisplay.bounds.h + 0.05*dh;

	skipBackButton.bounds = playButton.bounds;
	skipBackButton.bounds.x -= skipBackButton.bounds.w + 0.05*dw;

	skipNextButton.bounds = playButton.bounds;
	skipNextButton.bounds.x += playButton.bounds.w + 0.05*dw;
}

void RadioSelectionState::render()
{
	Display& display = game.getDisplay();
	display.clear();
	const float dw = display.getWidth(), dh = display.getHeight();

	background->drawScaled(0, 0, scaledToSize(background, display), Image::FLIP_HORIZONTAL);
	fontTitle->drawText("Select a track", 0.1f*dw, 0.04f*dh, Color::BLACK);
	fontTitle->drawText("Select a track", 0.1f*dw, 0.03f*dh, Color::WHITE);
	radioPanel.draw();
	Graphics::drawFilledCircle(radioTimeDisplay.bounds.x + 2.25f*radioTimeDisplay.text.drawCharFunctionSize,
			                   radioTimeDisplay.bounds.y + 0.35f*radioTimeDisplay.text.drawCharFunctionSize,
							   0.08f*radioTimeDisplay.text.drawCharFunctionSize, Color::AZURE);
	Graphics::drawFilledCircle(radioTimeDisplay.bounds.x + 2.25f*radioTimeDisplay.text.drawCharFunctionSize,
			                   radioTimeDisplay.bounds.y + 0.65f*radioTimeDisplay.text.drawCharFunctionSize,
							   0.08f*radioTimeDisplay.text.drawCharFunctionSize, Color::AZURE);
}

void RadioSelectionState::update(float delta)
{
	if(musicPreview != null and musicPreview->isPlaying())
	{
		if(playbacktTime + delta - std::floor(playbacktTime) > 0)
		{
			playbacktTime += delta;
			radioTimeDisplay.text.setContent(toFormatedTimer(playbacktTime));
		}
		else playbacktTime += delta;
	}
}

void RadioSelectionState::onKeyPressed(Keyboard::Key key)
{
	bool changeMusic = false;
	switch(key)
	{
		case Keyboard::KEY_ESCAPE:
			sndCursorOut->play();
			game.enterState(game.logic.currentMainMenuStateId);
			break;
		case Keyboard::KEY_ENTER:
			sndCursorIn->play();
			if(selectedMusicOptionIndex >= 3)
				game.logic.nextMatch.race.musicOption = selectedMusicOptionIndex-3;
			else if(selectedMusicOptionIndex == 0)
				game.logic.nextMatch.race.musicOption = -3;
			else if(selectedMusicOptionIndex == 1)
				game.logic.nextMatch.race.musicOption = -2;
			else if(selectedMusicOptionIndex == 2)
				game.logic.nextMatch.race.musicOption = -1;
			game.enterState(Carse::RACE_STATE_ID); break;
			break;
		case Keyboard::KEY_ARROW_LEFT:
			sndCursorMove->play();
			if(selectedMusicOptionIndex > 0)
				selectedMusicOptionIndex--;
			else
				selectedMusicOptionIndex = musicOptions.size()-1;
			changeMusic = true;
			break;
		case Keyboard::KEY_ARROW_RIGHT:
			sndCursorMove->play();
			if(selectedMusicOptionIndex < musicOptions.size()-1)
				selectedMusicOptionIndex++;
			else
				selectedMusicOptionIndex = 0;
			changeMusic = true;
			break;
		case Keyboard::KEY_SPACE:
			if(musicPreview != null)
			{
				if(musicPreview->isPlaying())
					musicPreview->pause();
				else if(musicPreview->isPaused())
					musicPreview->resume();
				else
					musicPreview->loop();
			}
			break;
		default:
			break;
	}

	if(changeMusic)
	{
		if(musicPreview != null)
		{
			musicPreview->stop();
			delete musicPreview;
			musicPreview = null;
		}
		playbacktTime = 0;

		if(selectedMusicOptionIndex >= 3)
			musicPreview = new fgeal::Music(game.logic.getMusicFilenames()[selectedMusicOptionIndex-3]);

		radioNameDisplay.content = musicOptions[selectedMusicOptionIndex];
		radioNameDisplay.updateDrawableText();

		radioTimeDisplay.text.setContent(toFormatedTimer(playbacktTime));
	}
}

void RadioSelectionState::onMouseButtonPressed(fgeal::Mouse::Button button, int x, int y)
{
	if(button == fgeal::Mouse::BUTTON_LEFT)
	{
		if(playButton.bounds.contains(x, y))
			this->onKeyPressed(Keyboard::KEY_SPACE);
		else if(skipBackButton.bounds.contains(x, y))
			this->onKeyPressed(Keyboard::KEY_ARROW_LEFT);
		else if(skipNextButton.bounds.contains(x, y))
			this->onKeyPressed(Keyboard::KEY_ARROW_RIGHT);
		else
			this->onKeyPressed(Keyboard::KEY_ENTER);
	}
	else if(button == fgeal::Mouse::BUTTON_RIGHT)
		this->onKeyPressed(Keyboard::KEY_ESCAPE);
}

void RadioSelectionState::onJoystickAxisMoved(unsigned joystick, unsigned axis, float value)
{
	GameLogic::passJoystickAxisToKeyboardArrows(this, axis, value);
}

void RadioSelectionState::onJoystickButtonPressed(unsigned joystick, unsigned button)
{
	if(button == 2)
		this->onKeyPressed(Keyboard::KEY_SPACE);
	else
		GameLogic::passJoystickButtonsToEnterReturnKeys(this, button);
}

void RadioSelectionState::onLeave()
{
	if(musicPreview != null)
	{
		musicPreview->stop();
		delete musicPreview;
		musicPreview = null;
	}
}
