/*
 * radio_selection_state.hpp
 *
 *  Created on: 24 de mar de 2020
 *      Author: hydren
 */

#ifndef RADIO_SELECTION_STATE_HPP_
#define RADIO_SELECTION_STATE_HPP_
#include <ciso646>

#include "fgeal/fgeal.hpp"
#include "fgeal/extra/game.hpp"
#include "fgeal/extra/gui.hpp"

// fwd. declared
class Carse;

class RadioSelectionState extends public fgeal::Game::State
{
	Carse& game;

	fgeal::Vector2D lastDisplaySize;

	fgeal::Image* background;
	fgeal::Font* fontTitle, *fontDisplay;
	fgeal::Sound* sndCursorMove, *sndCursorIn, *sndCursorOut;
	fgeal::Music* musicPreview;

	fgeal::Panel radioPanel;
	fgeal::TextField radioNameDisplay;
	fgeal::Label radioTimeDisplay;
	fgeal::IconButton skipNextButton, skipBackButton, playButton;

	std::vector<std::string> musicOptions;
	unsigned selectedMusicOptionIndex;
	float playbacktTime;

	public:
	virtual int getId();

	RadioSelectionState(Carse* game);
	~RadioSelectionState();

	virtual void initialize();
	virtual void onEnter();
	virtual void onLeave();

	virtual void onKeyPressed(fgeal::Keyboard::Key k);
	virtual void onMouseButtonPressed(fgeal::Mouse::Button button, int x, int y);
	virtual void onJoystickAxisMoved(unsigned, unsigned, float);
	virtual void onJoystickButtonPressed(unsigned, unsigned);

	virtual void render();
	virtual void update(float delta);
};

#endif /* RADIO_SELECTION_STATE_HPP_ */
