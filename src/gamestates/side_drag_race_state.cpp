/*
 * side_drag_race_state.cpp
 *
 *  Created on: 4 de ago de 2022
 *      Author: hydren
 */

#include "side_drag_race_state.hpp"

#include "carse.hpp"
#include "fgeal/extra/sprite.hpp"
#include "fgeal/extra/primitives.hpp"
#include "util/sizing.hpp"

using fgeal::Image;
using fgeal::Sprite;
using fgeal::Keyboard;
using fgeal::Graphics;
using fgeal::Color;
using fgeal::Vector2D;
using fgeal::Sound;
namespace primitives = fgeal::primitives;

int SideDragRaceState::getId(){ return Carse::SIDE_DRAG_RACE_STATE_ID; }

struct SideDragRaceState::Assets
{
	Image imgVehicle, imgWheel, imgBg;
	Sprite sprSmoke;
	Sound sndWheelspinBurnoutLoop;
	Assets()
	: imgVehicle("assets/sprites/car-side-default.png"),
	  imgWheel("assets/sprites/wheel.png"),
	  imgBg("assets/landscapes/city-sunset.jpg"),
	  sprSmoke("assets/effects/smoke-sprite.png", 32, 32, 0.036),
	  sndWheelspinBurnoutLoop("assets/sound/tire_burnout_stand1_loop.ogg")
	{}
};

SideDragRaceState::SideDragRaceState(Carse* game)
: State(*game), game(*game), courseLength(2000), body(Engine(), Mechanics::Spec::EXAMPLE_CAR),
  position(), frontWheelAngle(), rearWheelAngle(), isWheelSlipping(), engineSound(), dashboard(),
  assets(null)
{}

void SideDragRaceState::initialize()
{
	assets = new Assets(); // @suppress("Symbol is not resolved")
	dashboard.hudType = Hud::VehicleDashboard::HUD_TYPE_FULL_DIGITAL;
	dashboard.bounds.x = dashboard.bounds.y = 0;
}

SideDragRaceState::~SideDragRaceState()
{
	if(assets != null) delete assets;
}

void SideDragRaceState::onEnter()
{
	const float dw = game.getDisplay().getWidth(), dh = game.getDisplay().getHeight();

	const Pseudo3DVehicle::Spec& testSpec = game.logic.nextMatch.players[0].vehicleSpec;
	body = Mechanics(Engine(testSpec.engine), testSpec.body);
	body.simulationType = game.logic.nextMatch.race.simulationType;
	body.automaticShiftingEnabled = true;
	body.tireFrictionFactor = game.logic.getSurfaceTypeFrictionCoefficient("default");
	body.rollingResistanceFactor = game.logic.getSurfaceTypeRollingResistance("default");
	body.gravityAccelerationFactor = GameLogic::PHYSICS_GRAVITY_ACCELERATION_DEFAULT;
	body.airDensityFactor = GameLogic::PHYSICS_AIR_DENSITY_DEFAULT;
	engineSound.setProfile(testSpec.soundProfile, body.engine.spec.maxRpm);
	engineSound.loadAssetsData();
	engineSound.setVolume(game.logic.masterVolume);
	assets->sndWheelspinBurnoutLoop.setVolume(game.logic.masterVolume);
	dashboard.barTachometer.max = body.engine.spec.maxRpm;
	dashboard.barTachometer.min = body.engine.spec.minRpm;
	position = frontWheelAngle = rearWheelAngle = 0;
	isWheelSlipping = false;
	body.reset();
	engineSound.play();

	dashboard.bounds.w = dw;
	dashboard.bounds.h = dh;
	dashboard.borderThickness = dh * 0.01f;
	dashboard.speedometerUnitLabel.text.setContent(game.logic.nextMatch.race.isImperialUnit? "mph" : "kph");
	dashboard.dialSpeedometer.graduationValueScale = game.logic.nextMatch.race.isImperialUnit? 2.237 : 3.6;
	dashboard.setup();
}

void SideDragRaceState::onLeave()
{
	engineSound.halt();
	assets->sndWheelspinBurnoutLoop.stop();
}

void SideDragRaceState::render()
{
	const float dw = game.getDisplay().getWidth(), dh = game.getDisplay().getHeight(),
				vs = 0.2f*dh/assets->imgVehicle.getHeight(),
				ws = 62*vs/assets->imgWheel.getWidth(),
				va = 0.01f*M_PI*(body.acceleration/(10+std::abs(body.acceleration)));

	const Vector2D imgVehiclePos = { dw/4, 0.6f*dh }, imgVehicleScale = {vs, vs},
			       imgWheelPos1 = {imgVehiclePos.x+((84 -assets->imgVehicle.getWidth()/2)*vs), imgVehiclePos.y+((115-assets->imgVehicle.getHeight()/2)*vs)},
				   imgWheelPos2 = {imgVehiclePos.x+((386-assets->imgVehicle.getWidth()/2)*vs), imgWheelPos1.y},
				   wheelScale = {ws, ws};

	const float bs = 0.5f*dh/assets->imgBg.getHeight(), sms = 3*vs;
	assets->imgBg.drawScaled(-fmod(position*20,bs*assets->imgBg.getWidth()-dw), 0, bs, bs);
	Graphics::drawFilledRectangle(0, dh/2, dw, dh/2, Color::DARK_GREY);
	Graphics::drawFilledRectangle(0, dh/2, dw, dh/16, Color::GREY);

	assets->imgVehicle.drawScaledRotated(imgVehiclePos, imgVehicleScale, va);
	assets->imgWheel.drawScaledRotated(imgWheelPos1, wheelScale, rearWheelAngle);
	assets->imgWheel.drawScaledRotated(imgWheelPos2, wheelScale, frontWheelAngle);
	if(isWheelSlipping)
	{
		assets->sprSmoke.scale.x = assets->sprSmoke.scale.y = sms;
		assets->sprSmoke.computeCurrentFrame();
		if(body.spec.driveWheelType == Mechanics::DRIVE_WHEEL_REAR or body.spec.driveWheelType == Mechanics::DRIVE_WHEEL_ALL)
			assets->sprSmoke.draw(imgWheelPos1.x+ws*assets->imgWheel.getWidth()/3-sms*assets->sprSmoke.width, imgWheelPos1.y+ws*assets->imgWheel.getHeight()/2-sms*assets->sprSmoke.height);
		if(body.spec.driveWheelType == Mechanics::DRIVE_WHEEL_FRONT or body.spec.driveWheelType == Mechanics::DRIVE_WHEEL_ALL)
			assets->sprSmoke.draw(imgWheelPos2.x+ws*assets->imgWheel.getWidth()/3-sms*assets->sprSmoke.width, imgWheelPos2.y+ws*assets->imgWheel.getHeight()/2-sms*assets->sprSmoke.height);
	}

	dashboard.draw(body.speed, body.engine.rpm, body.engine.gear, body.automaticShiftingEnabled);
	primitives::drawDigits(position, dw/32, dh/32, 10, Color::WHITE);
}

void SideDragRaceState::handlePhysics(float delta)
{
	body.updatePowertrain(delta);

	// update position
	position += body.speed*delta;

	if(body.spec.driveWheelType == Mechanics::DRIVE_WHEEL_FRONT or body.spec.driveWheelType == Mechanics::DRIVE_WHEEL_ALL)
		frontWheelAngle -= body.wheelAngularSpeed*delta;
	else
		frontWheelAngle = -position/body.spec.driveWheelRadius;

	if(body.spec.driveWheelType == Mechanics::DRIVE_WHEEL_REAR or body.spec.driveWheelType == Mechanics::DRIVE_WHEEL_ALL)
		rearWheelAngle -= body.wheelAngularSpeed*delta;
	else
		rearWheelAngle = -position/body.spec.driveWheelRadius;

	while(frontWheelAngle < -2*M_PI) frontWheelAngle += 2*M_PI;
	while(rearWheelAngle < -2*M_PI) rearWheelAngle += 2*M_PI;

	isWheelSlipping = std::abs(body.slipRatio) > GameLogic::PHYSICS_BURNOUT_SLIP_RATIO and std::abs(body.speed) > 1.0;
}

void SideDragRaceState::update(float delta)
{
	const float desiredThrottlePosition = Keyboard::isKeyPressed(Keyboard::KEY_ARROW_UP)? 1 : 0,
				desiredThrottlePositionDeviation = desiredThrottlePosition - body.engine.throttlePosition;
	if(desiredThrottlePositionDeviation != 0)
	{
		if(std::abs(desiredThrottlePositionDeviation) > GameLogic::PHYSICS_PEDAL_POSITION_SPEED*delta)
			body.engine.throttlePosition += GameLogic::PHYSICS_PEDAL_POSITION_SPEED*delta * sgn(desiredThrottlePositionDeviation);
		else
			body.engine.throttlePosition = desiredThrottlePosition;
	}

	const float desiredBrakePedalPosition = Keyboard::isKeyPressed(Keyboard::KEY_ARROW_DOWN)? 1 : 0,
				desiredBrakePedalPositionDeviation = desiredBrakePedalPosition - body.brakePedalPosition;
	if(desiredBrakePedalPositionDeviation != 0)
	{
		if(std::abs(desiredBrakePedalPositionDeviation) > GameLogic::PHYSICS_PEDAL_POSITION_SPEED*delta)
			body.brakePedalPosition += GameLogic::PHYSICS_PEDAL_POSITION_SPEED*delta * sgn(desiredBrakePedalPositionDeviation);
		else
			body.brakePedalPosition = desiredBrakePedalPosition;
	}

	float physicsDelta = delta;
	while(physicsDelta > GameLogic::PHYSICS_MAXIMUM_DELTA_TIME)  // process delta too large in smaller delta steps
	{
		handlePhysics(GameLogic::PHYSICS_MAXIMUM_DELTA_TIME);
		physicsDelta -= GameLogic::PHYSICS_MAXIMUM_DELTA_TIME;
	}
	handlePhysics(physicsDelta);

	engineSound.update(body.engine.rpm);

	// wheelspin logic control
	if(isWheelSlipping)
	{
		if(not assets->sndWheelspinBurnoutLoop.isPlaying())
			assets->sndWheelspinBurnoutLoop.loop();
	}
	else assets->sndWheelspinBurnoutLoop.stop();
}

void SideDragRaceState::onKeyPressed(Keyboard::Key key)
{
	switch(key)
	{
		case Keyboard::KEY_ESCAPE:
			game.enterState(game.logic.currentMainMenuStateId);
			break;

		case Keyboard::KEY_LEFT_SHIFT:
			body.shiftGear(body.engine.gear+1);
			break;

		case Keyboard::KEY_LEFT_CONTROL:
			body.shiftGear(body.engine.gear-1);
			break;

		case Keyboard::KEY_T:
			body.automaticShiftingEnabled = !body.automaticShiftingEnabled;
			break;

		default:
			break;
	}
}
