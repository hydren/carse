/*
 * side_drag_race_state.hpp
 *
 *  Created on: 4 de ago de 2022
 *      Author: hydren
 */

#ifndef GAMESTATES_SIDE_DRAG_RACE_STATE_HPP_
#define GAMESTATES_SIDE_DRAG_RACE_STATE_HPP_
#include <ciso646>

#include "vehicle_mechanics.hpp"
#include "engine_sound.hpp"

#include "fgeal/fgeal.hpp"
#include "fgeal/extra/game.hpp"
#include "race_hud.hpp"

// fwd. declared
class Carse;

class SideDragRaceState extends public fgeal::Game::State
{
	Carse& game;

	unsigned courseLength;

	// physics simulation
	Mechanics body;
	float position, frontWheelAngle, rearWheelAngle;
	bool isWheelSlipping;

	// sound effect
	EngineSoundSimulator engineSound;

	// hud
	Hud::VehicleDashboard dashboard;

	struct Assets;
	Assets *assets;

	public:
	virtual int getId();

	SideDragRaceState(Carse* game);
	~SideDragRaceState();

	virtual void initialize();

	virtual void onEnter();
	virtual void onLeave();

	void handlePhysics(float delta);
	virtual void update(float delta);
	virtual void render();

	virtual void onKeyPressed(fgeal::Keyboard::Key);
};

#endif /* GAMESTATES_SIDE_DRAG_RACE_STATE_HPP_ */
