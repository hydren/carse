/*
 * vehicle_selection_showroom_layout_state.cpp
 *
 *  Created on: 17 de set de 2018
 *      Author: carlosfaruolo
 */

#include "vehicle_selection_showroom_layout_state.hpp"

#include "carse.hpp"

#include "util/sizing.hpp"

#include "futil/string_split.hpp"
#include "futil/string_actions.hpp"

#include <algorithm>

using fgeal::Display;
using fgeal::Keyboard;
using fgeal::Mouse;
using fgeal::Font;
using fgeal::Color;
using fgeal::Image;
using fgeal::Point;
using fgeal::Graphics;
using fgeal::Button;
using std::vector;
using std::string;

int VehicleSelectionShowroomLayoutState::getId() { return Carse::VEHICLE_SELECTION_SHOWROOM_LAYOUT_STATE_ID; }

VehicleSelectionShowroomLayoutState::VehicleSelectionShowroomLayoutState(Carse* game)
: AbstractVehicleSelectionState(game),
  imgBackground(null),
  fontTitle(null), fontSubtitle(null), fontInfo(null), fontGui(null),
  sndCursorMove(null), sndCursorIn(null), sndCursorOut(null),
  selectedVehicleIndex(),
  previewCurrentSprite(null), previewPreviousSprite(null), previewNextSprite(null), previewTransSprite(null),
  previewCurrentSpriteVehicleIndex(), previewPreviousSpriteVehicleIndex(), previewNextSpriteVehicleIndex(), previewTransSpriteVehicleIndex(),
  previewCurrentSpriteVehicleAlternateSpriteIndex(), previewPreviousSpriteVehicleAlternateSpriteIndex(), previewNextSpriteVehicleAlternateSpriteIndex(), previewTransSpriteVehicleAlternateSpriteIndex(),
  isSelectionTransitioning(), isTransitionForwards(), selectionTransitionProgress(),
  focus(), submenuFocus(SUBMENU_FOCUS_ON_APPEARANCE),
  keydownTimestamp()
{}

VehicleSelectionShowroomLayoutState::~VehicleSelectionShowroomLayoutState()
{
	if(fontTitle != null) delete fontTitle;
	if(fontSubtitle != null) delete fontSubtitle;
	if(fontInfo != null) delete fontInfo;
	if(fontGui != null) delete fontGui;
	if(accelerationChart.font != null) delete accelerationChart.font;

	if(previewCurrentSprite != null) delete previewCurrentSprite;
	if(previewPreviousSprite != null) delete previewPreviousSprite;
	if(previewNextSprite != null) delete previewNextSprite;

	if(imgBackground != null) delete imgBackground;
}

void VehicleSelectionShowroomLayoutState::initialize()
{
	fontTitle = new Font(gameFontParams("ui_vehicle_selection_title"));
	fontSubtitle = new Font(gameFontParams("ui_vehicle_selection_subtitle"));
	fontInfo = new Font(gameFontParams("ui_vehicle_selection_info"));
	fontGui = new Font(gameFontParams("ui_button"));

	// loan some shared resources
	sndCursorMove = &game.sharedResources->sndCursorMove;
	sndCursorIn   = &game.sharedResources->sndCursorIn;
	sndCursorOut  = &game.sharedResources->sndCursorOut;

	title.bounds.x = title.bounds.y = 0;
	title.backgroundColor = Color::MAROON;
	title.text.setFont(fontTitle);
	title.text.setContent("Choose your vehicle");
	title.text.setColor(Color::BLACK);
	title.textHorizontalAlignment = fgeal::Label::ALIGN_TRAILING;
	title.textVerticalAlignment = fgeal::Label::ALIGN_LEADING;

	titleFgText = title.text;
	titleFgText.setColor(Color::WHITE);

	playerDescBgText = title.text;
	playerDescFgText = titleFgText;

	vehicleNameLabel.text.setContent("--");
	vehicleNameLabel.text.setColor(Color::WHITE);
	vehicleNameLabel.text.setFont(fontSubtitle);
	vehicleNameLabel.backgroundColor = Color::AZURE;
	vehicleNameLabel.textHorizontalAlignment = fgeal::Label::ALIGN_LEADING;
	vehicleNameLabel.textVerticalAlignment = fgeal::Label::ALIGN_CENTER;
	vehicleNameLabel.bounds.x = 0;

	alternateSpriteIndexSelections.clear();
	alternateSpriteIndexSelections.resize(vehicleIndexList.size(), -1);

	vehicleInfoChart.setFont(fontInfo);
	vehicleInfoChart.setColor(Color::WHITE);
	vehicleInfoChart.title.setColor(~Color::CREAM);
	vehicleInfoChart.showType = false;
	vehicleInfoChart.layoutMode = fgeal::Menu::STRETCH_SPACING;

	accelerationChart.backgroundColor = Color::WHITE;
	accelerationChart.borderColor = Color::DARK_GREY;
	accelerationChart.font = new Font(gameFontParams("ui_vehicle_selection_acc_chart"));
	accelerationChart.gridColor = Color::DARK_GREY;
	accelerationChart.textColor = Color::BLUE;
	accelerationChart.speedDataSeries.color = Color::AZURE;
	accelerationChart.speedGearChangeDataSeries.color = Color::ORANGE;
	accelerationChart.tireFrictionFactor = game.logic.getSurfaceTypeFrictionCoefficient("default");
	accelerationChart.rollingResistanceFactor = game.logic.getSurfaceTypeRollingResistance("default");
//	accelerationChart.showLegend = true;

	powerTorqueGraph.backgroundColor = Color::WHITE;
	powerTorqueGraph.borderColor = Color::DARK_GREY;
	powerTorqueGraph.font = accelerationChart.font;
	powerTorqueGraph.gridColor = Color::DARK_GREY;
	powerTorqueGraph.textColor = Color::BLUE;
	powerTorqueGraph.powerDataSeries.color = Color::create(108, 208, 16);
	powerTorqueGraph.torqueDataSeries.color = Color::ORANGE;
	powerTorqueGraph.maximumDataSeries.color = Color::X11_HOT_PINK;

	orderMenu.title.setContent(" Order vehicles by: ");
	orderMenu.title.setColor(~Color::CELESTE);
	orderMenu.setFont(fontInfo);
	orderMenu.setColor(Color::WHITE);
	orderMenu.backgroundColor = Color::GREY.getTransparent(192);
	orderMenu.focusedEntryFontColor = Color::DARK_GREY;
	orderMenu.borderColor = Color::LIGHT_GREY;

	for(unsigned i = 0; i < VehicleSorter::ORDER_COUNT; i++)
		orderMenu.addEntry(vehicleSorter.getOrderDescription(static_cast<VehicleSorter::Order>(i)));
	orderMenu.addEntry("filename");

	detailsButton.shape = Button::SHAPE_ELLIPSOIDAL;
	detailsButton.backgroundColor = Color::AZURE;
	detailsButton.highlightColor = Color::RED;
	detailsButton.text.setFont(fontGui);
	detailsButton.text.setColor(Color::WHITE);
	detailsButton.icon = new Image("assets/ui/icon_info.png");
	detailsButton.textVerticalAlignment = Button::ALIGN_CENTER;
	submenuButtonsPanel.addComponent(detailsButton);

	orderButton = detailsButton;
	orderButton.icon = new Image("assets/ui/icon_order.png");
	submenuButtonsPanel.addComponent(orderButton);

	appearanceButton = detailsButton;
	appearanceButton.icon = new Image("assets/ui/icon_vehicle_appearance.png");
	submenuButtonsPanel.addComponent(appearanceButton);

	automaticButton = detailsButton;
	automaticButton.text.setFont(fontGui);
	automaticButton.text.setContent("A");
	submenuButtonsPanel.addComponent(automaticButton);

	backButton = detailsButton;
	backButton.icon = new Image("assets/ui/icon_x.png");
	submenuButtonsPanel.addComponent(backButton);

	selectButton = backButton;
	selectButton.icon = new Image("assets/ui/icon_check.png");
	submenuButtonsPanel.addComponent(selectButton);

	prevVehicleButton.shape = Button::SHAPE_CONST_ROUNDED_RECTANGULAR;
	prevVehicleButton.backgroundColor = Color::AZURE;
	prevVehicleButton.highlightColor = Color::RED;
	prevVehicleButton.arrowColor = Color::WHITE;
	prevVehicleButton.arrowOrientation = fgeal::ArrowIconButton::ARROW_LEFT;

	nextVehicleButton = prevVehicleButton;
	nextVehicleButton.arrowOrientation = fgeal::ArrowIconButton::ARROW_RIGHT;

	appearancePrevButton = prevVehicleButton;
	appearancePrevButton.arrowOrientation = fgeal::ArrowIconButton::ARROW_LEFT;
	appearancePrevButton.backgroundColor = Color::AZURE;
	appearancePrevButton.arrowColor = appearancePrevButton.backgroundColor.getBrighter();

	appearanceNextButton = appearancePrevButton;
	appearanceNextButton.arrowOrientation = fgeal::ArrowIconButton::ARROW_RIGHT;

	appearanceLabel.text.setFont(fontInfo);
	appearanceLabel.text.setColor(Color::AZURE);
	appearanceLabel.text.setContent("Standard appearance");

	imgBackground = new Image("assets/ui/showroom-bg.jpg");
}

void VehicleSelectionShowroomLayoutState::onEnter()
{
	Display& display = game.getDisplay();
	const unsigned dw = display.getWidth(), dh = display.getHeight();

	// reload fonts if display size changed
	if(lastDisplaySize.x != dw or lastDisplaySize.y != dh)
	{
		const FontSizer fs(display.getHeight());
		fontTitle->setSize(fs(game.logic.getFontSize("ui_vehicle_selection_title")));
		fontSubtitle->setSize(fs(game.logic.getFontSize("ui_vehicle_selection_subtitle")));
		fontInfo->setSize(fs(game.logic.getFontSize("ui_vehicle_selection_info")));
		fontGui->setSize(fs(game.logic.getFontSize("ui_button")));
		accelerationChart.font->setSize(fs(game.logic.getFontSize("ui_vehicle_selection_acc_chart")));
		lastDisplaySize.x = dw;
		lastDisplaySize.y = dh;
	}

	title.bounds.w = dw;
	title.bounds.h = 0.09*dh;
	title.padding.x = 0.05*dw;
	title.padding.y = 0.016*dh;

	if(game.logic.nextMatch.race.playerCount > 1)
		playerDescBgText.setContent(futil::to_string(playerIndex+1)+"P");
	else if(game.logic.nextMatch.race.playerCount == 0)
		playerDescBgText.setContent("CPU");
	else
		playerDescBgText.setContent(string());
	playerDescFgText.setContent(playerDescBgText.getContent());

	prevVehicleButton.bounds.x = 0.01*dw;
	prevVehicleButton.bounds.y = 0.775*dh;
	prevVehicleButton.bounds.w = 0.05*dw;
	prevVehicleButton.bounds.h = 0.125*dh;
	prevVehicleButton.highlightSpacing = 0.003*display.getHeight();

	nextVehicleButton.bounds = prevVehicleButton.bounds;
	nextVehicleButton.bounds.x = 0.99*dw - nextVehicleButton.bounds.w;
	nextVehicleButton.highlightSpacing = prevVehicleButton.highlightSpacing;

	appearancePrevButton.bounds.w = 0.030*dw;
	appearancePrevButton.bounds.h = 0.050*dw;
	appearancePrevButton.bounds.x = (dw - appearancePrevButton.bounds.w)/2 - dh/4;
	appearancePrevButton.bounds.y = 0.400*dh;

	appearanceNextButton.bounds = appearancePrevButton.bounds;
	appearanceNextButton.bounds.x = (dw - appearanceNextButton.bounds.w)/2 + dh/4;

	appearanceLabel.pack();
	appearanceLabel.bounds.x = (dw - appearanceLabel.bounds.w)/2;
	appearanceLabel.bounds.y = 0.610*dh;
	appearanceLabel.textHorizontalAlignment = fgeal::Label::ALIGN_CENTER;

	vehicleNameLabel.bounds.y = 0.65f*dh;
	vehicleNameLabel.bounds.w = dw;
	vehicleNameLabel.bounds.h = 0.08f*dh;

	vehicleInfoChart.bounds.x = 0.25*dw;
	vehicleInfoChart.bounds.y = 0.7425*dh;
	vehicleInfoChart.bounds.w = dw - vehicleInfoChart.bounds.x;
	vehicleInfoChart.bounds.h = 0.19*dh;
	isVehicleInfoChartExpired = true;

	accelerationChart.bounds.x = 0.25*dw;
	accelerationChart.bounds.y = 0.74*dh;
	accelerationChart.bounds.w = 0.50*dw;
	accelerationChart.bounds.h = 0.19*dh;

	if(game.logic.nextMatch.race.isImperialUnit)
	{
		accelerationChart.verticalAxisLegend = "MPH";
		accelerationChart.speedFactor = GameLogic::PHYSICS_MPS_TO_MPH;
		accelerationChart.maximum.y = 200;
		accelerationChart.interval.y = 50;
	}
	else
	{
		accelerationChart.verticalAxisLegend = "KM/H";
		accelerationChart.speedFactor = GameLogic::PHYSICS_MPS_TO_KPH;
		accelerationChart.maximum.y = 300;
		accelerationChart.interval.y = 50;
	}
	accelerationChart.simulationType = game.logic.nextMatch.race.simulationType;
	isAccelerationChartVisible = false;
	isAccelerationChartExpired = true;

	powerTorqueGraph.bounds = accelerationChart.bounds;
	switch(game.logic.uiUnits.power)
	{
		default: case GameLogic::UI_UNIT_POWER_HP:
		{
			powerTorqueGraph.verticalAxisLegend = "HP";
			powerTorqueGraph.powerFactor = 1;
			break;
		}
		case GameLogic::UI_UNIT_POWER_PS:
		{
			powerTorqueGraph.verticalAxisLegend = "PS";
			powerTorqueGraph.powerFactor = GameLogic::PHYSICS_HP_TO_PS;
			break;
		}
		case GameLogic::UI_UNIT_POWER_KW:
		{
			powerTorqueGraph.verticalAxisLegend = "KW";
			powerTorqueGraph.powerFactor = GameLogic::PHYSICS_HP_TO_KW;
			break;
		}
	}
	switch(game.logic.uiUnits.torque)
	{
		default: case GameLogic::UI_UNIT_TORQUE_NM:
		{
			powerTorqueGraph.verticalAxisLegend += " - NM";
			powerTorqueGraph.torqueFactor = 1;
			break;
		}
		case GameLogic::UI_UNIT_TORQUE_LBFT:
		{
			powerTorqueGraph.verticalAxisLegend += " - LBFT";
			powerTorqueGraph.torqueFactor = GameLogic::PHYSICS_NM_TO_LBFT;
			break;
		}
		case GameLogic::UI_UNIT_TORQUE_KGFM:
		{
			powerTorqueGraph.verticalAxisLegend += " - KGFM";
			powerTorqueGraph.torqueFactor = GameLogic::PHYSICS_NM_TO_KGFM;
			break;
		}
	}
	isPowerTorqueGraphVisible = false;
	isPowerTorqueGraphExpired = true;

	orderMenu.bounds.x = dw/3;
	orderMenu.bounds.y = dh/3;
	orderMenu.bounds.w = dw/3;
	orderMenu.bounds.h = dh/3;

	const float subBtnSize = 0.066*dh, sideBtnSpacing = 0.015*dw;
	submenuButtonsPanel.bounds.h = subBtnSize;
	submenuButtonsPanel.bounds.w = 6 * (sideBtnSpacing + subBtnSize);
	submenuButtonsPanel.bounds.x = (dw - submenuButtonsPanel.bounds.w)/2;
	submenuButtonsPanel.bounds.y = dh - submenuButtonsPanel.bounds.h;

	backButton.bounds = submenuButtonsPanel.bounds;
	backButton.bounds.x += subBtnSize/6;
	backButton.bounds.w = backButton.bounds.h;

	detailsButton.bounds = backButton.bounds;
	detailsButton.bounds.x += backButton.bounds.w + sideBtnSpacing;

	orderButton.bounds = detailsButton.bounds;
	orderButton.bounds.x += detailsButton.bounds.w + sideBtnSpacing;

	appearanceButton.bounds = orderButton.bounds;
	appearanceButton.bounds.x += orderButton.bounds.w + sideBtnSpacing;

	autoShiftingChosen = game.logic.nextMatch.players[playerIndex].enableAutomaticShifting;
	automaticButton.bounds = appearanceButton.bounds;
	automaticButton.bounds.x += appearanceButton.bounds.w + sideBtnSpacing;
	automaticButton.text.setContent(autoShiftingChosen? "A" : "M");

	selectButton.bounds = automaticButton.bounds;
	selectButton.bounds.x += automaticButton.bounds.w + sideBtnSpacing;

	selectedVehicleIndex = 0;
	alternateSpriteIndexSelections[selectedVehicleIndex] = -1;
	if(not game.logic.nextMatch.players[playerIndex].vehicleSpec.filename.empty())
		for(unsigned i = 0; i < vehicleIndexList.size(); i++)
			if(game.logic.getVehicleList()[vehicleIndexList[i]].filename == game.logic.nextMatch.players[playerIndex].vehicleSpec.filename)
	{
		selectedVehicleIndex = i;
		alternateSpriteIndexSelections[i] = game.logic.nextMatch.players[playerIndex].alternateSpriteIndex;
		break;
	}

	focus = FOCUS_ON_VEHICLE;
	submenuFocus = SUBMENU_FOCUS_ON_APPEARANCE;
	updateSubmenuButtonsAppearance(true);
	keydownTimestamp = 0;
}

void VehicleSelectionShowroomLayoutState::onLeave()
{}

void VehicleSelectionShowroomLayoutState::update(float delta)
{
	if(isSelectionTransitioning)
	{
		if(selectionTransitionProgress == 0)  // transition start
		{
			if(previewTransSprite != null)
				delete previewTransSprite;

			const int tmp = selectedVehicleIndex + (isTransitionForwards? 2 : -2), vehicleCount = vehicleIndexList.size();
			const unsigned transVehicleIndex = tmp + (tmp < 0? vehicleCount : tmp >= vehicleCount? -vehicleCount : 0);

			const Pseudo3DVehicle::Spec& vspec = game.logic.getVehicleList()[vehicleIndexList[transVehicleIndex]];
			const Pseudo3DVehicleAnimation::Spec& spriteSpec = (alternateSpriteIndexSelections[transVehicleIndex] == -1? vspec.sprite : vspec.alternateSprites[alternateSpriteIndexSelections[transVehicleIndex]]);
			previewTransSprite = new Image(spriteSpec.sheetFilename);
			previewTransSpriteVehicleIndex = transVehicleIndex;
			previewTransSpriteVehicleAlternateSpriteIndex = alternateSpriteIndexSelections[transVehicleIndex];
		}

		selectionTransitionProgress += delta * (isTransitionForwards? 6 : -6);

		if(std::abs(selectionTransitionProgress) > 0.99)
		{
			isSelectionTransitioning = false;
			if(isTransitionForwards)
			{
				if(selectedVehicleIndex < vehicleIndexList.size()-1)
					selectedVehicleIndex++;
				else
					selectedVehicleIndex = 0;

				Image* const tmpSprite = previewPreviousSprite;
				const unsigned tmpIndex = previewPreviousSpriteVehicleIndex;
				const int tmpAltIndex = previewPreviousSpriteVehicleAlternateSpriteIndex;

				previewPreviousSprite = previewCurrentSprite;
				previewPreviousSpriteVehicleIndex = previewCurrentSpriteVehicleIndex;
				previewPreviousSpriteVehicleAlternateSpriteIndex = previewCurrentSpriteVehicleAlternateSpriteIndex;

				previewCurrentSprite = previewNextSprite;
				previewCurrentSpriteVehicleIndex = previewNextSpriteVehicleIndex;
				previewCurrentSpriteVehicleAlternateSpriteIndex = previewNextSpriteVehicleAlternateSpriteIndex;

				// attempt to reuse previous to next and force cache miss / deletion if it was not possible
				previewNextSprite = tmpSprite;
				previewNextSpriteVehicleIndex = tmpIndex;
				previewNextSpriteVehicleAlternateSpriteIndex = tmpAltIndex;
			}
			else
			{
				if(selectedVehicleIndex > 0)
					selectedVehicleIndex--;
				else
					selectedVehicleIndex = vehicleIndexList.size()-1;

				Image* const tmpSprite = previewNextSprite;
				const unsigned tmpIndex = previewNextSpriteVehicleIndex;
				const int tmpAltIndex = previewNextSpriteVehicleAlternateSpriteIndex;

				previewNextSprite = previewCurrentSprite;
				previewNextSpriteVehicleIndex = previewCurrentSpriteVehicleIndex;
				previewNextSpriteVehicleAlternateSpriteIndex = previewCurrentSpriteVehicleAlternateSpriteIndex;

				previewCurrentSprite = previewPreviousSprite;
				previewCurrentSpriteVehicleIndex = previewPreviousSpriteVehicleIndex;
				previewCurrentSpriteVehicleAlternateSpriteIndex = previewPreviousSpriteVehicleAlternateSpriteIndex;

				// attempt to reuse next to previous and force cache miss / deletion if it was not possible
				previewPreviousSprite = tmpSprite;
				previewPreviousSpriteVehicleIndex = tmpIndex;
				previewPreviousSpriteVehicleAlternateSpriteIndex = tmpAltIndex;
			}

			vehicleNameLabel.text.setContent(game.logic.getVehicleList()[vehicleIndexList[selectedVehicleIndex]].name.empty()? "--" : game.logic.getVehicleList()[vehicleIndexList[selectedVehicleIndex]].name);
			appearanceLabel.text.setContent(previewCurrentSpriteVehicleAlternateSpriteIndex == -1? "Standard appearance" : "Alternate appearance " + (previewCurrentSpriteVehicleAlternateSpriteIndex? futil::to_string(previewCurrentSpriteVehicleAlternateSpriteIndex+1) : ""));
			isAccelerationChartExpired = isPowerTorqueGraphExpired = isVehicleInfoChartExpired = true;
			updateSubmenuButtonsAppearance(true);

			delete previewTransSprite;
			previewTransSprite = null;
		}
	}

	if(not isSelectionTransitioning)
	{
		const unsigned previousVehicleIndex = (selectedVehicleIndex == 0? vehicleIndexList.size() : selectedVehicleIndex) - 1;
		if(previewPreviousSprite == null or previewPreviousSpriteVehicleIndex != previousVehicleIndex or previewPreviousSpriteVehicleAlternateSpriteIndex != alternateSpriteIndexSelections[previousVehicleIndex])
		{
			if(previewPreviousSprite != null)
				delete previewPreviousSprite;

			const Pseudo3DVehicle::Spec& vspec = game.logic.getVehicleList()[vehicleIndexList[previousVehicleIndex]];
			const Pseudo3DVehicleAnimation::Spec& spriteSpec = (alternateSpriteIndexSelections[previousVehicleIndex] == -1? vspec.sprite : vspec.alternateSprites[alternateSpriteIndexSelections[previousVehicleIndex]]);
			previewPreviousSprite = new Image(spriteSpec.sheetFilename);
			previewPreviousSpriteVehicleIndex = previousVehicleIndex;
			previewPreviousSpriteVehicleAlternateSpriteIndex = alternateSpriteIndexSelections[previousVehicleIndex];
		}

		if(previewCurrentSprite == null or previewCurrentSpriteVehicleIndex != selectedVehicleIndex or previewCurrentSpriteVehicleAlternateSpriteIndex != alternateSpriteIndexSelections[selectedVehicleIndex])
		{
			if(previewCurrentSprite != null)
				delete previewCurrentSprite;

			const Pseudo3DVehicle::Spec& vspec = game.logic.getVehicleList()[vehicleIndexList[selectedVehicleIndex]];
			const Pseudo3DVehicleAnimation::Spec& spriteSpec = (alternateSpriteIndexSelections[selectedVehicleIndex] == -1? vspec.sprite : vspec.alternateSprites[alternateSpriteIndexSelections[selectedVehicleIndex]]);
			previewCurrentSprite = new Image(spriteSpec.sheetFilename);
			previewCurrentSpriteVehicleIndex = selectedVehicleIndex;
			previewCurrentSpriteVehicleAlternateSpriteIndex = alternateSpriteIndexSelections[selectedVehicleIndex];
			vehicleNameLabel.text.setContent(vspec.name.empty()? "--" : vspec.name);
			appearanceLabel.text.setContent(previewCurrentSpriteVehicleAlternateSpriteIndex == -1? "Standard appearance" : "Alternate appearance " + (previewCurrentSpriteVehicleAlternateSpriteIndex? futil::to_string(previewCurrentSpriteVehicleAlternateSpriteIndex+1) : ""));
		}

		const unsigned nextVehicleIndex = (selectedVehicleIndex == vehicleIndexList.size()-1? 0 : selectedVehicleIndex + 1);
		if(previewNextSprite == null or previewNextSpriteVehicleIndex != nextVehicleIndex or previewNextSpriteVehicleAlternateSpriteIndex != alternateSpriteIndexSelections[nextVehicleIndex])
		{
			if(previewNextSprite != null)
				delete previewNextSprite;

			const Pseudo3DVehicle::Spec& vspec = game.logic.getVehicleList()[vehicleIndexList[nextVehicleIndex]];
			const Pseudo3DVehicleAnimation::Spec& spriteSpec = (alternateSpriteIndexSelections[nextVehicleIndex] == -1? vspec.sprite : vspec.alternateSprites[alternateSpriteIndexSelections[nextVehicleIndex]]);
			previewNextSprite = new Image(spriteSpec.sheetFilename);
			previewNextSpriteVehicleIndex = nextVehicleIndex;
			previewNextSpriteVehicleAlternateSpriteIndex = alternateSpriteIndexSelections[nextVehicleIndex];
		}

		if(isVehicleInfoChartExpired)
		{
			vehicleInfoChart.generate(game.logic.getVehicleList()[vehicleIndexList[selectedVehicleIndex]], game.logic.uiUnits);
			vehicleInfoChart.updateDrawableText();
			isVehicleInfoChartExpired = false;
		}

		if(isAccelerationChartVisible and isAccelerationChartExpired)
		{
			accelerationChart.generate(game.logic.getVehicleList()[vehicleIndexList[selectedVehicleIndex]]);
			isAccelerationChartExpired = false;
		}

		if(isPowerTorqueGraphVisible and isPowerTorqueGraphExpired)
		{
			powerTorqueGraph.generate(game.logic.getVehicleList()[vehicleIndexList[selectedVehicleIndex]]);
			isPowerTorqueGraphExpired = false;
		}
	}

	static float lastCursorChangeTimestamp = 0;
	if(focus == FOCUS_ON_VEHICLE and fgeal::uptime() - keydownTimestamp > 0.6f and fgeal::uptime() - lastCursorChangeTimestamp > 0.05f)
	{
		if(Keyboard::isKeyPressed(Keyboard::KEY_ARROW_LEFT) or (Mouse::isButtonPressed(Mouse::BUTTON_LEFT) and prevVehicleButton.bounds.contains(Mouse::getPosition())))
		{
			const float tmp = keydownTimestamp;
			onKeyPressed(Keyboard::KEY_ARROW_LEFT);
			keydownTimestamp = tmp;
			lastCursorChangeTimestamp = fgeal::uptime();
		}

		else if(Keyboard::isKeyPressed(Keyboard::KEY_ARROW_RIGHT) or (Mouse::isButtonPressed(Mouse::BUTTON_LEFT) and nextVehicleButton.bounds.contains(Mouse::getPosition())))
		{
			const float tmp = keydownTimestamp;
			onKeyPressed(Keyboard::KEY_ARROW_RIGHT);
			keydownTimestamp = tmp;
			lastCursorChangeTimestamp = fgeal::uptime();
		}
	}

	const Point mousePos = Mouse::getPosition();
	     if(detailsButton.bounds.contains(mousePos))    { submenuFocus = SUBMENU_FOCUS_ON_DETAILS;       updateSubmenuButtonsAppearance(); }
	else if(orderButton.bounds.contains(mousePos))      { submenuFocus = SUBMENU_FOCUS_ON_ORDER;         updateSubmenuButtonsAppearance(); }
	else if(appearanceButton.bounds.contains(mousePos)) { submenuFocus = SUBMENU_FOCUS_ON_APPEARANCE;    updateSubmenuButtonsAppearance(); }
	else if(automaticButton.bounds.contains(mousePos))  { submenuFocus = SUBMENU_FOCUS_ON_AUTO;          updateSubmenuButtonsAppearance(); }
	else if(backButton.bounds.contains(mousePos))       { submenuFocus = SUBMENU_FOCUS_ON_RETURN_BUTTON; updateSubmenuButtonsAppearance(); }
	else if(selectButton.bounds.contains(mousePos))     { submenuFocus = SUBMENU_FOCUS_ON_SELECT_BUTTON; updateSubmenuButtonsAppearance(); }
	else if(detailsButton.bounds.contains(mousePos))    { submenuFocus = SUBMENU_FOCUS_ON_DETAILS;       updateSubmenuButtonsAppearance(); }
}

void VehicleSelectionShowroomLayoutState::drawVehiclePreview(Image* sprite, const Pseudo3DVehicleAnimation::Spec& spriteSpec, float x, float y, float scale, int angleType)
{
	const Image::FlipMode flipMode = (angleType > 0 ? Image::FLIP_HORIZONTAL : Image::FLIP_NONE);
	const float subscale = scale * 0.00651f * game.getDisplay().getHeight(),
				posX = x - subscale * spriteSpec.scale.x * 0.5f * spriteSpec.frameWidth,
				posY = y - subscale * spriteSpec.scale.y * (spriteSpec.frameHeight - spriteSpec.contactOffset),
				offsetY = (angleType == 0? 0 : spriteSpec.frameHeight * (spriteSpec.stateCount/2));

	sprite->drawScaledRegion(posX, posY, subscale * spriteSpec.scale.x, subscale * spriteSpec.scale.y, flipMode, 0, offsetY, spriteSpec.frameWidth, spriteSpec.frameHeight);
}

void VehicleSelectionShowroomLayoutState::render()
{
	Display& display = game.getDisplay();
	display.clear();
	const float dw = display.getWidth(), dh = display.getHeight();
	const Point mousePos = Mouse::getPosition();
	const bool blinkCycle = (cos(20*fgeal::uptime()) > 0);

	// transition effects
	const float trans = isSelectionTransitioning? selectionTransitionProgress : 0,
				doff = 0.35*sin(trans),  // dynamic offset
	            doffp = -0.15*doff,
				doffc = -0.15*fabs(doff),
				doffn = 0.15*doff;

	imgBackground->drawScaled(0, 0, scaledToSize(imgBackground, display));

	// draw previous vehicle
	if(previewPreviousSprite != null)
	{
		const Pseudo3DVehicle::Spec& spec = game.logic.getVehicleList()[vehicleIndexList[previewPreviousSpriteVehicleIndex]];
		drawVehiclePreview(previewPreviousSprite,
						   previewPreviousSpriteVehicleAlternateSpriteIndex == -1? spec.sprite
								   	   	   	   	   	   	   	   	   	   	   	     : spec.alternateSprites[previewPreviousSpriteVehicleAlternateSpriteIndex],
							(0.2-doff)*dw, (0.6-doffp)*dh, 1.05-0.05*fabs(trans), trans < -0.5? 0 : -1);
	}

	// draw next vehicle
	if(previewNextSprite != null)
	{
		const Pseudo3DVehicle::Spec& spec = game.logic.getVehicleList()[vehicleIndexList[previewNextSpriteVehicleIndex]];
		drawVehiclePreview(previewNextSprite,
						   previewNextSpriteVehicleAlternateSpriteIndex == -1? spec.sprite
								   	   	   	   	   	   	   	   	   	   	     : spec.alternateSprites[previewNextSpriteVehicleAlternateSpriteIndex],
						   (0.8-doff)*dw, (0.6-doffn)*dh, 1.05-0.05*fabs(trans), trans > 0.5? 0 : +1);
	}

	if(previewTransSprite != null)
	{
		const Pseudo3DVehicle::Spec& spec = game.logic.getVehicleList()[vehicleIndexList[previewTransSpriteVehicleIndex]];
		drawVehiclePreview(previewTransSprite,
						   previewTransSpriteVehicleAlternateSpriteIndex == -1? spec.sprite
								   	   	   	   	   	   	   	   	   	   	      : spec.alternateSprites[previewTransSpriteVehicleAlternateSpriteIndex],
						   isTransitionForwards? (1.1-doff)*dw : (-0.1-doff)*dw,
						   isTransitionForwards? (0.65-doffn)*dh : (0.65-doffp)*dh,
						   1.1-0.05*fabs(trans),
						   isTransitionForwards? 1 : -1);
	}

	// darkening other vehicles
	Graphics::drawFilledRectangle(0, 0, dw, dh, Color::create(0, 0, 0, 128));

	// draw current vehicle
	const Pseudo3DVehicle::Spec& currentSpec = game.logic.getVehicleList()[vehicleIndexList[previewCurrentSpriteVehicleIndex]];
	if(previewCurrentSprite != null)
	{
		drawVehiclePreview(previewCurrentSprite,
						   previewCurrentSpriteVehicleAlternateSpriteIndex == -1? currentSpec.sprite
								   	   	   	   	   	   	   	   	   	   	   	    : currentSpec.alternateSprites[previewCurrentSpriteVehicleAlternateSpriteIndex],
						  (0.5-doff)*dw, (0.55-doffc)*dh, 1.0+0.05*fabs(trans), trans > 0.5? -1 : trans < -0.5? +1 : 0);
	}

	title.draw();
	titleFgText.draw(title.bounds.x + title.bounds.w - titleFgText.getWidth() - title.padding.x, title.bounds.y + 0.010*dh);
	playerDescBgText.draw(title.bounds.x + 0.01*dw, title.bounds.y + title.padding.y);
	playerDescFgText.draw(title.bounds.x + 0.01*dw, title.bounds.y + 0.010*dh);

	// draw current vehicle info
	const float nameWidth = vehicleNameLabel.text.getWidth();
	vehicleNameLabel.padding.x = 0.5*(dw-nameWidth) + (nameWidth > dw? 0.6f*std::sin(fgeal::uptime())*(nameWidth - dw) : 0);
	vehicleNameLabel.draw();

	Graphics::drawFilledRectangle(0, 0.73f*dh, dw, 0.275f*dh, Color::NAVY);
	if(isAccelerationChartVisible)
		accelerationChart.draw();
	else if(isPowerTorqueGraphVisible)
		powerTorqueGraph.draw();
	else
		vehicleInfoChart.draw();

	if(focus == FOCUS_ON_APPEARANCE_SELECTOR)
	{
		appearancePrevButton.backgroundColor.a = appearancePrevButton.arrowColor.a = appearanceNextButton.backgroundColor.a = appearanceNextButton.arrowColor.a = blinkCycle? 255 : 128;
		appearanceNextButton.draw();
		appearancePrevButton.draw();
		appearanceLabel.draw();
	}

	nextVehicleButton.highlighted = blinkCycle and nextVehicleButton.bounds.contains(mousePos);
	nextVehicleButton.backgroundColor.a = isSelectionTransitioning and isTransitionForwards? 128 : 255;
	nextVehicleButton.draw();

	prevVehicleButton.highlighted = blinkCycle and prevVehicleButton.bounds.contains(mousePos);
	prevVehicleButton.backgroundColor.a = isSelectionTransitioning and not isTransitionForwards? 128 : 255;
	prevVehicleButton.draw();

	submenuButtonsPanel.draw();
	if(focus == FOCUS_ON_SUBMENU and blinkCycle)
		Graphics::drawRoundedRectangle(submenuButtonsPanel.bounds, submenuButtonsPanel.bounds.h/2, Color::RED);

	else if(focus == FOCUS_ON_ORDER_MENU)
		orderMenu.draw();
}


void VehicleSelectionShowroomLayoutState::updateSubmenuButtonsAppearance(bool clear)
{
	Button* focusedSubButton = null;
	if(not clear) switch(submenuFocus)
	{
		case SUBMENU_FOCUS_ON_RETURN_BUTTON: focusedSubButton = &backButton;       break;
		case SUBMENU_FOCUS_ON_DETAILS:       focusedSubButton = &detailsButton;    break;
		case SUBMENU_FOCUS_ON_ORDER:         focusedSubButton = &orderButton;      break;
		case SUBMENU_FOCUS_ON_APPEARANCE:    focusedSubButton = &appearanceButton; break;
		case SUBMENU_FOCUS_ON_AUTO:          focusedSubButton = &automaticButton;  break;
		case SUBMENU_FOCUS_ON_SELECT_BUTTON: focusedSubButton = &selectButton;     break;
		default:break;
	}

	Button* const subBtns[] = { &detailsButton, &orderButton, &automaticButton, &backButton, &selectButton };
	for(unsigned i = 0; i < sizeof(subBtns)/sizeof(Button*); i++)
	{
		subBtns[i]->backgroundColor = subBtns[i] == focusedSubButton? Color::AZURE.getBrighter() : Color::AZURE;
		subBtns[i]->text.setColor(Color::WHITE);
	}

	const bool hasAlternateSprites = not game.logic.getVehicleList()[vehicleIndexList[selectedVehicleIndex]].alternateSprites.empty();
	appearanceButton.backgroundColor = focusedSubButton == &appearanceButton? hasAlternateSprites? Color::AZURE.getBrighter() : Color::LIGHT_GREY : hasAlternateSprites? Color::AZURE : Color::GREY;
}


void VehicleSelectionShowroomLayoutState::onKeyPressed(Keyboard::Key key)
{
	if(key == Keyboard::KEY_1)
	{
		playerIndex = 0;  // for safety
		game.enterState(Carse::VEHICLE_SELECTION_SIMPLE_LIST_STATE_ID);
		sndCursorOut->play();
	}

	else if(key == Keyboard::KEY_SPACE)
	{
		if(isAccelerationChartVisible)
		{
			isAccelerationChartVisible = false;
			isPowerTorqueGraphVisible = true;
		}
		else if(isPowerTorqueGraphVisible)
		{
			isAccelerationChartVisible = false;
			isPowerTorqueGraphVisible = false;
		}
		else
		{
			isAccelerationChartVisible = true;
			isPowerTorqueGraphVisible = false;
		}
		sndCursorMove->play();
	}

	else switch(focus)
	{
		case FOCUS_ON_VEHICLE:
			onKeyPressedFocusOnVehicle(key);
			break;

		case FOCUS_ON_SUBMENU:
			onKeyPressedFocusOnSubmenu(key);
			break;

		case FOCUS_ON_APPEARANCE_SELECTOR:
			onKeyPressedFocusOnAppearanceSelector(key);
			break;

		case FOCUS_ON_ORDER_MENU:
			onKeyPressedFocusOnOrderMenu(key);
			break;

		default:break;
	}
}

void VehicleSelectionShowroomLayoutState::onKeyPressedFocusOnVehicle(Keyboard::Key key)
{
	switch(key)
	{
		case Keyboard::KEY_ESCAPE:
			sndCursorOut->play();
			if(playerIndex == 0)
				game.enterState(game.logic.currentMainMenuStateId);
			else
			{
				playerIndex--;
				game.enterState(getId());
			}
			break;
		case Keyboard::KEY_ENTER:
			sndCursorIn->play();
			game.logic.nextMatch.players[playerIndex].vehicleSpec = game.logic.getVehicleList().at(vehicleIndexList[selectedVehicleIndex]);
			game.logic.nextMatch.players[playerIndex].alternateSpriteIndex = alternateSpriteIndexSelections[selectedVehicleIndex];
			game.logic.nextMatch.players[playerIndex].enableAutomaticShifting = autoShiftingChosen;
			if(game.logic.nextMatch.race.playerCount == 0 or playerIndex == game.logic.nextMatch.race.playerCount - 1)
			{
				playerIndex = 0;
				game.enterState(game.logic.currentMainMenuStateId);
			}
			else
			{
				playerIndex++;
				game.enterState(getId());
			}
			break;

		case Keyboard::KEY_ARROW_UP:
		case Keyboard::KEY_ARROW_DOWN:
		{
			focus = FOCUS_ON_SUBMENU;
			updateSubmenuButtonsAppearance();
			sndCursorMove->play();
			break;
		}

		case Keyboard::KEY_ARROW_LEFT:
			if(not isSelectionTransitioning)
			{
				isSelectionTransitioning = true;
				isTransitionForwards = false;
				selectionTransitionProgress = 0;
				game.sharedResources->sndCursorMove.play();
				keydownTimestamp = fgeal::uptime();
			}
			break;

		case Keyboard::KEY_ARROW_RIGHT:
			if(not isSelectionTransitioning)
			{
				isSelectionTransitioning = true;
				isTransitionForwards = true;
				selectionTransitionProgress = 0;
				game.sharedResources->sndCursorMove.play();
				keydownTimestamp = fgeal::uptime();
			}
			break;

		default:
			break;
	}
}

void VehicleSelectionShowroomLayoutState::onKeyPressedFocusOnSubmenu(Keyboard::Key key)
{
	if(key == Keyboard::KEY_ESCAPE)
		onKeyPressedFocusOnVehicle(Keyboard::KEY_ESCAPE);

	else if(key == Keyboard::KEY_ARROW_LEFT and submenuFocus > 0)
	{
		submenuFocus = static_cast<SubMenuFocus>(submenuFocus-1);
		updateSubmenuButtonsAppearance();
		sndCursorMove->play();
	}

	else if(key == Keyboard::KEY_ARROW_RIGHT and submenuFocus < SUBMENU_FOCUS_COUNT-1)
	{
		submenuFocus = static_cast<SubMenuFocus>(submenuFocus+1);
		updateSubmenuButtonsAppearance();
		sndCursorMove->play();
	}

	else if(key == Keyboard::KEY_ARROW_UP or key == Keyboard::KEY_ARROW_DOWN)
	{
		focus = FOCUS_ON_VEHICLE;
		updateSubmenuButtonsAppearance(true);
		sndCursorMove->play();
	}

	else if(key == Keyboard::KEY_ENTER) switch(submenuFocus)
	{
		case SUBMENU_FOCUS_ON_RETURN_BUTTON:
		{
			onKeyPressedFocusOnVehicle(Keyboard::KEY_ESCAPE);
			break;
		}

		case SUBMENU_FOCUS_ON_DETAILS:
		{
			if(isAccelerationChartVisible)
			{
				isAccelerationChartVisible = false;
				isPowerTorqueGraphVisible = true;
			}
			else if(isPowerTorqueGraphVisible)
			{
				isAccelerationChartVisible = false;
				isPowerTorqueGraphVisible = false;
			}
			else
			{
				isAccelerationChartVisible = true;
				isPowerTorqueGraphVisible = false;
			}
			sndCursorMove->play();
			break;
		}

		case SUBMENU_FOCUS_ON_ORDER:
		{
			focus = FOCUS_ON_ORDER_MENU;
			sndCursorIn->play();
			break;
		}

		case SUBMENU_FOCUS_ON_APPEARANCE:
		{
			const unsigned alternateSpritesCount = game.logic.getVehicleList()[vehicleIndexList[selectedVehicleIndex]].alternateSprites.size();
			if(alternateSpritesCount > 0)
			{
				sndCursorIn->play();
				focus = FOCUS_ON_APPEARANCE_SELECTOR;
			}
			else
				sndCursorOut->play();
			break;
		}

		case SUBMENU_FOCUS_ON_AUTO:
		{
			autoShiftingChosen = !autoShiftingChosen;
			automaticButton.text.setContent(autoShiftingChosen? "A" : "M");
			sndCursorIn->play();
			break;
		}

		case SUBMENU_FOCUS_ON_SELECT_BUTTON:
		{
			onKeyPressedFocusOnVehicle(Keyboard::KEY_ENTER);
			break;
		}

		default:break;
	}
}

void VehicleSelectionShowroomLayoutState::onKeyPressedFocusOnAppearanceSelector(Keyboard::Key key)
{
	if(key == Keyboard::KEY_ENTER or key == Keyboard::KEY_ESCAPE)
	{
		focus = FOCUS_ON_VEHICLE;
		sndCursorOut->play();
	}
	else if(key == Keyboard::KEY_ARROW_DOWN or key == Keyboard::KEY_ARROW_UP)
	{
		focus = FOCUS_ON_SUBMENU;
		sndCursorOut->play();
	}
	else if(key == Keyboard::KEY_ARROW_LEFT or key == Keyboard::KEY_ARROW_RIGHT)
	{
		const unsigned alternateSpritesCount = game.logic.getVehicleList()[vehicleIndexList[selectedVehicleIndex]].alternateSprites.size();
		if(key == Keyboard::KEY_ARROW_RIGHT)  // forwards
		{
			if(alternateSpriteIndexSelections[selectedVehicleIndex] == (int) alternateSpritesCount - 1)
				alternateSpriteIndexSelections[selectedVehicleIndex] = -1;
			else
				alternateSpriteIndexSelections[selectedVehicleIndex]++;
		}
		else  // backwards
		{
			if(alternateSpriteIndexSelections[selectedVehicleIndex] == -1)
				alternateSpriteIndexSelections[selectedVehicleIndex] = alternateSpritesCount - 1;
			else
				alternateSpriteIndexSelections[selectedVehicleIndex]--;
		}
		sndCursorMove->play();
	}
}

void VehicleSelectionShowroomLayoutState::onKeyPressedFocusOnOrderMenu(Keyboard::Key key)
{
	if(key == Keyboard::KEY_ESCAPE)
	{
		focus = FOCUS_ON_VEHICLE;
		sndCursorOut->play();
	}
	else if(key == Keyboard::KEY_ENTER)
	{
		const unsigned previousSelectedVehicleIndex = vehicleIndexList[selectedVehicleIndex];
		const vector<unsigned> vehicleListBackup = vehicleIndexList;

		if(orderMenu.getSelectedIndex() < VehicleSorter::ORDER_COUNT)
		{
			vehicleSorter.order = static_cast<VehicleSorter::Order>(orderMenu.getSelectedIndex());
			std::sort(vehicleIndexList.begin(), vehicleIndexList.end(), vehicleSorter);
		}

		const vector<int> alternateSpriteIndexSelectionsBackup = alternateSpriteIndexSelections;

		for(unsigned i = 0; i < alternateSpriteIndexSelections.size(); i++)
			alternateSpriteIndexSelections[i] = -1;

		for(unsigned i = 0; i < vehicleIndexList.size(); i++)
		{
			if(vehicleIndexList[i] == previousSelectedVehicleIndex)
				selectedVehicleIndex = i;

			for(unsigned j = 0; j < vehicleListBackup.size(); j++)
			{
				if(vehicleIndexList[i] == vehicleListBackup[j])
				{
					alternateSpriteIndexSelections[i] = alternateSpriteIndexSelectionsBackup[j];
					break;
				}
			}
		}

		if(previewPreviousSprite != null) delete previewPreviousSprite;
		if(previewCurrentSprite != null)  delete previewCurrentSprite;
		if(previewNextSprite != null)     delete previewNextSprite;
		if(previewTransSprite != null)    delete previewTransSprite;
		previewPreviousSprite = null;
		previewCurrentSprite = null;
		previewNextSprite = null;
		previewTransSprite = null;

		focus = FOCUS_ON_VEHICLE;
		sndCursorOut->play();
	}
	else if(key == Keyboard::KEY_ARROW_UP)
	{
		orderMenu.moveCursorUp();
		sndCursorMove->play();
	}
	else if(key == Keyboard::KEY_ARROW_DOWN)
	{
		orderMenu.moveCursorDown();
		sndCursorMove->play();
	}
}

void VehicleSelectionShowroomLayoutState::onMouseButtonPressed(Mouse::Button button, int x, int y)
{
	if(button == fgeal::Mouse::BUTTON_LEFT)
	{
		if(focus == FOCUS_ON_APPEARANCE_SELECTOR)
		{
			if(appearancePrevButton.bounds.contains(x, y))
				this->onKeyPressedFocusOnAppearanceSelector(Keyboard::KEY_ARROW_LEFT);

			else if(appearanceNextButton.bounds.contains(x, y))
				this->onKeyPressedFocusOnAppearanceSelector(Keyboard::KEY_ARROW_RIGHT);

			else if(submenuButtonsPanel.bounds.contains(x, y))
				focus = FOCUS_ON_SUBMENU;
			else
				this->onKeyPressedFocusOnSubmenu(Keyboard::KEY_ARROW_UP);
		}

		else if(focus == FOCUS_ON_ORDER_MENU)
		{
			if(orderMenu.bounds.contains(x, y))
			{
				orderMenu.setSelectedIndexByLocation(x, y);
				this->onKeyPressed(Keyboard::KEY_ENTER);
			}
			else if(submenuButtonsPanel.bounds.contains(x, y))
				focus = FOCUS_ON_SUBMENU;
			else
				this->onKeyPressedFocusOnSubmenu(Keyboard::KEY_ARROW_UP);
		}

		else if(focus == FOCUS_ON_SUBMENU and not submenuButtonsPanel.bounds.contains(x, y))
			this->onKeyPressedFocusOnSubmenu(Keyboard::KEY_ARROW_UP);

		else if(prevVehicleButton.bounds.contains(x, y))
			this->onKeyPressedFocusOnVehicle(Keyboard::KEY_ARROW_LEFT);

		else if(nextVehicleButton.bounds.contains(x, y))
			this->onKeyPressedFocusOnVehicle(Keyboard::KEY_ARROW_RIGHT);

		else if(backButton.bounds.contains(x, y))
			this->onKeyPressedFocusOnSubmenu(Keyboard::KEY_ESCAPE);

		else if(detailsButton.bounds.contains(x, y))
			this->onKeyPressedFocusOnSubmenu(Keyboard::KEY_ENTER);

		else if(orderButton.bounds.contains(x, y))
			this->onKeyPressedFocusOnSubmenu(Keyboard::KEY_ENTER);

		else if(appearanceButton.bounds.contains(x, y))
			this->onKeyPressedFocusOnSubmenu(Keyboard::KEY_ENTER);

		else if(automaticButton.bounds.contains(x, y))
			this->onKeyPressedFocusOnSubmenu(Keyboard::KEY_ENTER);

		else if(selectButton.bounds.contains(x, y))
			this->onKeyPressedFocusOnSubmenu(Keyboard::KEY_ENTER);
	}
}

void VehicleSelectionShowroomLayoutState::onJoystickAxisMoved(unsigned joystick, unsigned axis, float value)
{
	GameLogic::passJoystickAxisToKeyboardArrows(this, axis, value);
}

void VehicleSelectionShowroomLayoutState::onJoystickButtonPressed(unsigned joystick, unsigned button)
{
	if(button == 0)
		this->onKeyPressed(Keyboard::KEY_1);
	else
		GameLogic::passJoystickButtonsToEnterReturnKeys(this, button);
}

void VehicleSelectionShowroomLayoutState::ImageIconButton::draw()
{
	Button::draw();
	if(icon != null)
	{
		const Point alignOffset = { iconHorizontalAlignment == ALIGN_LEADING? 0 : ((bounds.w * (1 - iconScale)) * (iconHorizontalAlignment == ALIGN_TRAILING? 1 : 0.5f)),
									  iconVerticalAlignment == ALIGN_LEADING? 0 : ((bounds.h * (1 - iconScale)) *   (iconVerticalAlignment == ALIGN_TRAILING? 1 : 0.5f))};
		icon->drawScaled(bounds.x + alignOffset.x, bounds.y + alignOffset.y, iconScale * bounds.w / icon->getWidth(), iconScale * bounds.h / icon->getHeight());
	}
}

