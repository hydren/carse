/*
 * vehicle_selection_showroom_layout_state.hpp
 *
 *  Created on: 17 de set de 2018
 *      Author: carlosfaruolo
 */

#ifndef VEHICLE_SELECTION_SHOWROOM_LAYOUT_STATE_HPP_
#define VEHICLE_SELECTION_SHOWROOM_LAYOUT_STATE_HPP_
#include <ciso646>

#include "vehicle_selection_state.hpp"

// fwd decl.
class Carse;

class VehicleSelectionShowroomLayoutState extends public AbstractVehicleSelectionState
{
	fgeal::Image* imgBackground;
	fgeal::Font* fontTitle, *fontSubtitle, *fontInfo, *fontGui;
	fgeal::Sound* sndCursorMove, *sndCursorIn, *sndCursorOut;

	fgeal::Label title, vehicleNameLabel;
	fgeal::DrawableText titleFgText, playerDescBgText, playerDescFgText;

	fgeal::ArrowIconButton nextVehicleButton, prevVehicleButton, appearancePrevButton, appearanceNextButton;

	struct ImageIconButton extends fgeal::Button
	{
		fgeal::Image* icon;
		float iconScale;  // scale relative to button size
		Alignment iconHorizontalAlignment, iconVerticalAlignment;

		ImageIconButton(fgeal::Image* img=null) : Button(), icon(img), iconScale(0.7), iconHorizontalAlignment(), iconVerticalAlignment() {}
		ImageIconButton(const std::string& imgFilename) : Button(), icon(new fgeal::Image(imgFilename)), iconScale(0.7), iconHorizontalAlignment(), iconVerticalAlignment() {}
		~ImageIconButton() { if(icon != null) delete icon; }
		void draw();
	}
	selectButton, backButton, detailsButton, orderButton, appearanceButton;

	fgeal::Button automaticButton;

	fgeal::Panel submenuButtonsPanel;

	fgeal::Menu orderMenu;

	unsigned selectedVehicleIndex;

	fgeal::Image* previewCurrentSprite, *previewPreviousSprite, *previewNextSprite, *previewTransSprite;
	unsigned previewCurrentSpriteVehicleIndex, previewPreviousSpriteVehicleIndex, previewNextSpriteVehicleIndex, previewTransSpriteVehicleIndex;
	int previewCurrentSpriteVehicleAlternateSpriteIndex, previewPreviousSpriteVehicleAlternateSpriteIndex, previewNextSpriteVehicleAlternateSpriteIndex, previewTransSpriteVehicleAlternateSpriteIndex;
	fgeal::Label appearanceLabel;

	bool isSelectionTransitioning, isTransitionForwards;
	float selectionTransitionProgress;

	enum Focus { FOCUS_ON_VEHICLE, FOCUS_ON_SUBMENU, FOCUS_ON_APPEARANCE_SELECTOR, FOCUS_ON_ORDER_MENU } focus;
	enum SubMenuFocus {
		SUBMENU_FOCUS_ON_RETURN_BUTTON,
		SUBMENU_FOCUS_ON_DETAILS,
		SUBMENU_FOCUS_ON_ORDER,
		SUBMENU_FOCUS_ON_APPEARANCE,
		SUBMENU_FOCUS_ON_AUTO,
		SUBMENU_FOCUS_ON_SELECT_BUTTON,
		SUBMENU_FOCUS_COUNT
	} submenuFocus;

	float keydownTimestamp;

	public:
	virtual int getId();

	VehicleSelectionShowroomLayoutState(Carse* game);
	~VehicleSelectionShowroomLayoutState();

	virtual void initialize();
	virtual void onEnter();
	virtual void onLeave();

	virtual void render();
	virtual void update(float delta);

	virtual void onKeyPressed(fgeal::Keyboard::Key);
	virtual void onMouseButtonPressed(fgeal::Mouse::Button button, int x, int y);
	virtual void onJoystickAxisMoved(unsigned, unsigned, float);
	virtual void onJoystickButtonPressed(unsigned, unsigned);

	private:
	void onKeyPressedFocusOnVehicle(fgeal::Keyboard::Key);
	void onKeyPressedFocusOnSubmenu(fgeal::Keyboard::Key);
	void onKeyPressedFocusOnAppearanceSelector(fgeal::Keyboard::Key);
	void onKeyPressedFocusOnOrderMenu(fgeal::Keyboard::Key);

	void drawVehiclePreview(fgeal::Image* sprite, const Pseudo3DVehicleAnimation::Spec& spriteSpec, float x, float y, float scale, int angleType);
	void updateSubmenuButtonsAppearance(bool clear=false);
};

#endif /* VEHICLE_SELECTION_SHOWROOM_LAYOUT_STATE_HPP_ */
