/*
 * vehicle_selection_simple_list_state.cpp
 *
 *  Created on: 17 de set de 2018
 *      Author: carlosfaruolo
 */

#include "vehicle_selection_simple_list_state.hpp"

#include "carse.hpp"

#include "util/sizing.hpp"

#include "futil/string_split.hpp"
#include "futil/string_actions.hpp"

#include <algorithm>

using fgeal::Display;
using fgeal::Keyboard;
using fgeal::Font;
using fgeal::Color;
using fgeal::Sprite;
using fgeal::Button;
using fgeal::Mouse;
using fgeal::Graphics;
using fgeal::Point;
using std::vector;
using std::string;

int VehicleSelectionSimpleListState::getId() { return Carse::VEHICLE_SELECTION_SIMPLE_LIST_STATE_ID; }

VehicleSelectionSimpleListState::VehicleSelectionSimpleListState(Carse* game)
: AbstractVehicleSelectionState(game),
  fontTitle(null), fontInfo(null), fontSmall(null), fontMenu(null),
  sndCursorMove(null), sndCursorIn(null), sndCursorOut(null),
  menuUpButton(), menuDownButton(), appearancePrevButton(), appearanceNextButton(),
  selectButton(), backButton(), detailsButton(), orderButton(), appearanceButton(), automaticButton(),
  previewSprite(null), previewSpriteVehicleIndex(), previewSpriteVehicleAlternateSpriteIndex(),
  hasAlternateSprites(), focus(), sideMenuFocus(), keydownTimestamp()
{}

VehicleSelectionSimpleListState::~VehicleSelectionSimpleListState()
{
	if(fontTitle != null) delete fontTitle;
	if(fontInfo != null) delete fontInfo;
	if(fontSmall != null) delete fontSmall;
	if(fontMenu != null) delete fontMenu;
	if(accelerationChart.font != null) delete accelerationChart.font;
	if(detailsButton.text.getFont() != null) delete detailsButton.text.getFont();

	if(previewSprite != null) delete previewSprite;
}

void VehicleSelectionSimpleListState::initialize()
{
	Display& display = game.getDisplay();

	fontTitle = new Font(gameFontParams("ui_vehicle_selection_title"));
	fontInfo = new Font(gameFontParams("ui_vehicle_selection_info"));
	fontSmall = new Font(gameFontParams("ui_vehicle_selection_small"));
	fontMenu = new Font(gameFontParams("ui_vehicle_selection_items"));

	// loan some shared resources
	sndCursorMove = &game.sharedResources->sndCursorMove;
	sndCursorIn   = &game.sharedResources->sndCursorIn;
	sndCursorOut  = &game.sharedResources->sndCursorOut;

	menu.setFont(fontMenu);
	menu.setColor(Color::WHITE);
	menu.cursorWrapAroundEnabled = true;
	menu.entryOverflowBehavior = fgeal::TextField::OVERFLOW_TRUNCATE_TRAILING;
	menu.backgroundColor = Color::AZURE;
	menu.focusedEntryFontColor = Color::NAVY;

	for(unsigned i = 0; i < vehicleIndexList.size(); i++)
		menu.addEntry(game.logic.getVehicleList()[i].name);

	alternateSpriteIndexSelections.clear();
	alternateSpriteIndexSelections.resize(vehicleIndexList.size(), -1);

	menuUpButton.shape = Button::SHAPE_ROUNDED_RECTANGULAR;
	menuUpButton.backgroundColor = Color::AZURE.getBrighter();
	menuUpButton.highlightColor = Color::RED;
	menuUpButton.highlightSpacing = 0.003*display.getHeight();
	menuUpButton.arrowColor = menu.focusedEntryFontColor;
	menuUpButton.arrowOrientation = fgeal::ArrowIconButton::ARROW_UP;

	menuDownButton = menuUpButton;
	menuDownButton.arrowOrientation = fgeal::ArrowIconButton::ARROW_DOWN;

	orderMenu.title.setContent("Order by:");
	orderMenu.title.setColor(Color::ORANGE);
	orderMenu.setFont(fontSmall);
	orderMenu.setColor(Color::WHITE);
	orderMenu.cursorWrapAroundEnabled = true;
	orderMenu.backgroundColor = Color::GREY;
	orderMenu.focusedEntryFontColor = Color::DARK_GREY;

	for(unsigned i = 0; i < VehicleSorter::ORDER_COUNT; i++)
		orderMenu.addEntry(vehicleSorter.getOrderDescription(static_cast<VehicleSorter::Order>(i)));
	orderMenu.addEntry("filename");

	appearancePrevButton = menuUpButton;
	appearancePrevButton.arrowOrientation = fgeal::ArrowIconButton::ARROW_LEFT;

	appearanceNextButton = menuUpButton;
	appearanceNextButton.arrowOrientation = fgeal::ArrowIconButton::ARROW_RIGHT;

	appearanceLabel.text.setFont(fontSmall);
	appearanceLabel.text.setColor(Color::WHITE);
	appearanceLabel.text.setContent("Standard appearance");

	detailsButton = menuUpButton;
	detailsButton.backgroundColor = menu.backgroundColor;
	detailsButton.text.setFont(new Font(gameFontParams("ui_button")));
	detailsButton.text.setColor(Color::WHITE);
	detailsButton.text.setContent("i");
	detailsButton.textVerticalAlignment = Button::ALIGN_CENTER;

	orderButton = detailsButton;
	orderButton.text.setContent("::");

	appearanceButton = detailsButton;
	appearanceButton.text.setContent("!");

	automaticButton = detailsButton;
	automaticButton.text.setContent("A");

	backButton = detailsButton;
	backButton.text.setContent(" Back ");

	selectButton = backButton;
	selectButton.text.setContent(" Select ");

	title.text.setFont(fontTitle);
	title.text.setContent("Choose your vehicle");
	title.text.setColor(Color::WHITE);

	playerLabel.text.setFont(fontTitle);
	playerLabel.text.setColor(Color::WHITE);

	vehicleNameLabel.text.setContent("--");
	vehicleNameLabel.text.setColor(~Color::CREAM);
	vehicleNameLabel.text.setFont(fontInfo);
	vehicleNameLabel.textHorizontalAlignment = fgeal::Label::ALIGN_LEADING;

	vehicleInfoChart.setFont(fontInfo);
	vehicleInfoChart.setColor(Color::WHITE);
	vehicleInfoChart.title.setColor(~Color::CREAM);
	vehicleInfoChart.layoutMode = fgeal::Menu::STRETCH_SPACING;

	accelerationChart.backgroundColor = Color::WHITE;
	accelerationChart.borderColor = Color::DARK_GREY;
	accelerationChart.font = new Font(gameFontParams("ui_vehicle_selection_acc_chart"));
	accelerationChart.gridColor = Color::DARK_GREY;
	accelerationChart.textColor = Color::BLUE;
	accelerationChart.speedDataSeries.color = Color::AZURE;
	accelerationChart.speedGearChangeDataSeries.color = Color::ORANGE;
	accelerationChart.tireFrictionFactor = game.logic.getSurfaceTypeFrictionCoefficient("default");
	accelerationChart.rollingResistanceFactor = game.logic.getSurfaceTypeRollingResistance("default");

	powerTorqueGraph.backgroundColor = Color::WHITE;
	powerTorqueGraph.borderColor = Color::DARK_GREY;
	powerTorqueGraph.font = accelerationChart.font;
	powerTorqueGraph.gridColor = Color::DARK_GREY;
	powerTorqueGraph.textColor = Color::BLUE;
	powerTorqueGraph.powerDataSeries.color = Color::create(108, 208, 16);
	powerTorqueGraph.torqueDataSeries.color = Color::ORANGE;
	powerTorqueGraph.maximumDataSeries.color = Color::X11_HOT_PINK;
}

void VehicleSelectionSimpleListState::onEnter()
{
	Display& display = game.getDisplay();
	const unsigned dw = display.getWidth(), dh = display.getHeight();

	// reload fonts if display size changed
	if(lastDisplaySize.x != dw or lastDisplaySize.y != dh)
	{
		const FontSizer fs(display.getHeight());
		fontTitle->setSize(fs(game.logic.getFontSize("ui_vehicle_selection_title")));
		fontInfo->setSize(fs(game.logic.getFontSize("ui_vehicle_selection_info")));
		fontSmall->setSize(fs(game.logic.getFontSize("ui_vehicle_selection_small")));
		fontMenu->setSize(fs(game.logic.getFontSize("ui_vehicle_selection_items")));
		accelerationChart.font->setSize(fs(game.logic.getFontSize("ui_vehicle_selection_acc_chart")));
		detailsButton.text.getFont()->setSize(fs(game.logic.getFontSize("ui_button")));
		lastDisplaySize.x = dw;
		lastDisplaySize.y = dh;
	}

	if(not game.logic.nextMatch.players[playerIndex].vehicleSpec.filename.empty())
		for(unsigned i = 0; i < vehicleIndexList.size(); i++)
			if(game.logic.getVehicleList()[vehicleIndexList[i]].filename == game.logic.nextMatch.players[playerIndex].vehicleSpec.filename)
	{
		menu.setSelectedIndex(i);
		alternateSpriteIndexSelections[i] = game.logic.nextMatch.players[playerIndex].alternateSpriteIndex;
		break;
	}
	autoShiftingChosen = game.logic.nextMatch.players[playerIndex].enableAutomaticShifting;

	menu.bounds.w = 0.880*dw;
	menu.bounds.h = 0.325*dh;
	menu.bounds.x = 0.050*dw;
	menu.bounds.y = 0.575*dh;
	menu.updateDrawableText();

	menuUpButton.bounds.w = 0.025*dw;
	menuUpButton.bounds.h = 0.025*dh;
	menuUpButton.bounds.x = menu.bounds.x + menu.bounds.w - menuUpButton.bounds.w;
	menuUpButton.bounds.y = menu.bounds.y;

	menuDownButton.bounds = menuUpButton.bounds;
	menuDownButton.bounds.y = menu.bounds.y + menu.bounds.h - menuDownButton.bounds.h;

	appearancePrevButton.bounds.x = 0.62*dw;
	appearancePrevButton.bounds.y = 0.435*dh;
	appearancePrevButton.bounds.w = 0.03*dw;
	appearancePrevButton.bounds.h = 0.03*dh;
	appearancePrevButton.highlightSpacing = 0.007*dh;

	appearanceNextButton.bounds = appearancePrevButton.bounds;
	appearanceNextButton.bounds.x += 0.33*dw;
	appearanceNextButton.highlightSpacing = appearancePrevButton.highlightSpacing;

	appearanceLabel.pack();
	appearanceLabel.bounds.x = 0.64f*dw;
	appearanceLabel.bounds.y = 0.54f*dh;
	appearanceLabel.bounds.w = 0.32f*dw;
	appearanceLabel.textHorizontalAlignment = fgeal::Label::ALIGN_CENTER;

	const float sideBtnSpacing = 0.005*dh;

	detailsButton.bounds.x = 0.935*dw;
	detailsButton.bounds.y = menu.bounds.y + sideBtnSpacing;
	detailsButton.bounds.w = 0.040*dw;
	detailsButton.bounds.h = menu.bounds.h * 0.225;
	detailsButton.highlightSpacing = sideBtnSpacing/2;

	orderButton.bounds = detailsButton.bounds;
	orderButton.bounds.y += detailsButton.bounds.h + sideBtnSpacing;
	orderButton.highlightSpacing = detailsButton.highlightSpacing;

	appearanceButton.bounds = orderButton.bounds;
	appearanceButton.bounds.y += orderButton.bounds.h + sideBtnSpacing;
	appearanceButton.highlightSpacing = orderButton.highlightSpacing;

	automaticButton.bounds = appearanceButton.bounds;
	automaticButton.bounds.y += appearanceButton.bounds.h + sideBtnSpacing;
	automaticButton.highlightSpacing = appearanceButton.highlightSpacing;
	automaticButton.text.setContent(autoShiftingChosen? "A" : "M");

	selectButton.padding.x = selectButton.padding.y = 0.002*dw;
	selectButton.pack();
	selectButton.bounds.x = 3*dw/5 - selectButton.bounds.w/2;
	selectButton.bounds.y = 0.925*dh;

	backButton.bounds = selectButton.bounds;
	backButton.bounds.x = 2*dw/5 - backButton.bounds.w/2;
	backButton.bounds.y = 0.925*dh;

	orderMenu.bounds.x = 0.65*dw;
	orderMenu.bounds.y = 0.60*dh;
	orderMenu.bounds.w = 0.20*dw;
	orderMenu.bounds.h = 0.25*dh;

	title.pack();
	title.bounds.x = 0.05f*dw;
	title.bounds.y = dh/12;

	if(game.logic.nextMatch.race.playerCount > 1)
		playerLabel.text.setContent(futil::to_string(playerIndex+1)+"P");
	else if(game.logic.nextMatch.race.playerCount == 0)
		playerLabel.text.setContent("CPU");
	else
		playerLabel.text.setContent(string());
	playerLabel.pack();
	playerLabel.bounds.x = 0.85f*dw;
	playerLabel.bounds.y = title.bounds.y;

	vehicleNameLabel.pack();
	vehicleNameLabel.bounds.x = 0.05*dw;
	vehicleNameLabel.bounds.y = 0.21*dh;
	vehicleNameLabel.bounds.w = dw - 2*vehicleNameLabel.bounds.x;

	vehicleInfoChart.bounds.x = vehicleNameLabel.bounds.x;
	vehicleInfoChart.bounds.y = 0.25*dh;
	vehicleInfoChart.bounds.w = vehicleNameLabel.bounds.w;
	vehicleInfoChart.bounds.h = 0.30*dh;
	isVehicleInfoChartExpired = true;

	accelerationChart.bounds = vehicleInfoChart.bounds;
	accelerationChart.bounds.w = 0.55*dw;

	if(game.logic.nextMatch.race.isImperialUnit)
	{
		accelerationChart.verticalAxisLegend = "MPH";
		accelerationChart.speedFactor = GameLogic::PHYSICS_MPS_TO_MPH;
		accelerationChart.maximum.y = 200;
		accelerationChart.interval.y = 50;
	}
	else
	{
		accelerationChart.verticalAxisLegend = "KM/H";
		accelerationChart.speedFactor = GameLogic::PHYSICS_MPS_TO_KPH;
		accelerationChart.maximum.y = 300;
		accelerationChart.interval.y = 50;
	}
	accelerationChart.simulationType = game.logic.nextMatch.race.simulationType;
	isAccelerationChartVisible = false;
	isAccelerationChartExpired = true;

	powerTorqueGraph.bounds = accelerationChart.bounds;
	switch(game.logic.uiUnits.power)
	{
		default: case GameLogic::UI_UNIT_POWER_HP:
		{
			powerTorqueGraph.verticalAxisLegend = "HP";
			powerTorqueGraph.powerFactor = 1;
			break;
		}
		case GameLogic::UI_UNIT_POWER_PS:
		{
			powerTorqueGraph.verticalAxisLegend = "PS";
			powerTorqueGraph.powerFactor = GameLogic::PHYSICS_HP_TO_PS;
			break;
		}
		case GameLogic::UI_UNIT_POWER_KW:
		{
			powerTorqueGraph.verticalAxisLegend = "KW";
			powerTorqueGraph.powerFactor = GameLogic::PHYSICS_HP_TO_KW;
			break;
		}
	}
	switch(game.logic.uiUnits.torque)
	{
		default: case GameLogic::UI_UNIT_TORQUE_NM:
		{
			powerTorqueGraph.verticalAxisLegend += " - NM";
			powerTorqueGraph.torqueFactor = 1;
			break;
		}
		case GameLogic::UI_UNIT_TORQUE_LBFT:
		{
			powerTorqueGraph.verticalAxisLegend += " - LBFT";
			powerTorqueGraph.torqueFactor = GameLogic::PHYSICS_NM_TO_LBFT;
			break;
		}
		case GameLogic::UI_UNIT_TORQUE_KGFM:
		{
			powerTorqueGraph.verticalAxisLegend += " - KGFM";
			powerTorqueGraph.torqueFactor = GameLogic::PHYSICS_NM_TO_KGFM;
			break;
		}
	}
	isPowerTorqueGraphVisible = false;
	isPowerTorqueGraphExpired = true;

	focus = FOCUS_ON_LIST;
	sideMenuFocus = SIDE_MENU_APPEARANCE;
	keydownTimestamp = 0;
}

void VehicleSelectionSimpleListState::onLeave()
{}

void VehicleSelectionSimpleListState::update(float delta)
{
	if(previewSpriteVehicleIndex != menu.getSelectedIndex() or previewSpriteVehicleAlternateSpriteIndex != alternateSpriteIndexSelections[menu.getSelectedIndex()])
	{
		if(previewSprite != null)
			delete previewSprite;

		const Pseudo3DVehicle::Spec& vspec = game.logic.getVehicleList()[vehicleIndexList[menu.getSelectedIndex()]];
		const Pseudo3DVehicleAnimation::Spec& spriteSpec = (alternateSpriteIndexSelections[menu.getSelectedIndex()] == -1? vspec.sprite : vspec.alternateSprites[alternateSpriteIndexSelections[menu.getSelectedIndex()]]);
		previewSprite = new Sprite(spriteSpec.sheetFilename, spriteSpec.frameWidth, spriteSpec.frameHeight, -1, 1);
		previewSprite->scale = spriteSpec.scale * 0.00651f * game.getDisplay().getHeight();
		previewSprite->referencePixelX = previewSprite->scale.x * previewSprite->width / 2;
		previewSprite->referencePixelY = previewSprite->scale.y * (previewSprite->height - spriteSpec.contactOffset);
		previewSpriteVehicleIndex = menu.getSelectedIndex();
		previewSpriteVehicleAlternateSpriteIndex = alternateSpriteIndexSelections[menu.getSelectedIndex()];
		hasAlternateSprites = not vspec.alternateSprites.empty();
		vehicleNameLabel.text.setContent(vspec.year != 0? futil::to_string(vspec.year) + ' ' + vspec.name : vspec.name);
		appearanceLabel.text.setContent(previewSpriteVehicleAlternateSpriteIndex == -1? "Standard appearance" : "Alternate appearance " + (previewSpriteVehicleAlternateSpriteIndex? futil::to_string(previewSpriteVehicleAlternateSpriteIndex+1) : ""));
		updateSideMenuButtonsAppearance(focus != FOCUS_ON_SIDE_MENU);
		isAccelerationChartExpired = isPowerTorqueGraphExpired = isVehicleInfoChartExpired = true;
	}

	if(isVehicleInfoChartExpired)
	{
		vehicleInfoChart.generate(game.logic.getVehicleList()[vehicleIndexList[menu.getSelectedIndex()]], game.logic.uiUnits);
		vehicleInfoChart.updateDrawableText();
		isVehicleInfoChartExpired = false;
	}

	if(isAccelerationChartVisible and isAccelerationChartExpired)
	{
		accelerationChart.generate(game.logic.getVehicleList()[vehicleIndexList[menu.getSelectedIndex()]]);
		isAccelerationChartExpired = false;
	}

	if(isPowerTorqueGraphVisible and isPowerTorqueGraphExpired)
	{
		powerTorqueGraph.generate(game.logic.getVehicleList()[vehicleIndexList[menu.getSelectedIndex()]]);
		isPowerTorqueGraphExpired = false;
	}

	static float lastCursorChangeTimestamp = 0;
	if(fgeal::uptime() - keydownTimestamp > 0.6f and fgeal::uptime() - lastCursorChangeTimestamp > 0.1f and focus == FOCUS_ON_LIST)
	{
		if(Keyboard::isKeyPressed(Keyboard::KEY_ARROW_UP) or (Mouse::isButtonPressed(Mouse::BUTTON_LEFT) and menuUpButton.bounds.contains(Mouse::getPosition())))
		{
			menu.moveCursorUp();
			sndCursorMove->play();
			lastCursorChangeTimestamp = fgeal::uptime();
		}

		else if(Keyboard::isKeyPressed(Keyboard::KEY_ARROW_DOWN) or (Mouse::isButtonPressed(Mouse::BUTTON_LEFT) and menuDownButton.bounds.contains(Mouse::getPosition())))
		{
			menu.moveCursorDown();
			sndCursorMove->play();
			lastCursorChangeTimestamp = fgeal::uptime();
		}
	}
}

void VehicleSelectionSimpleListState::render()
{
	Display& display = game.getDisplay();
	display.clear();
	const float dw = display.getWidth(), dh = display.getHeight();
	const Point mousePos = Mouse::getPosition();
	const bool blinkCycle = (cos(20*fgeal::uptime()) > 0);

	Graphics::drawFilledQuadrangle(0.63f*dw, 0.55f*dh, 0.69f*dw, 0.475f*dh, 0.91f*dw, 0.475f*dh, 0.97f*dw, 0.55f*dh, Color::DARK_GREY);
	if(previewSprite != null)
		previewSprite->draw(0.80f*dw, 0.52f*dh);

	title.draw();
	Graphics::drawLine(0.05f*dw, title.bounds.h + 0.09f*dh, 0.95f*dw, title.bounds.h + 0.09f*dh, Color::WHITE);

	playerLabel.draw();
	vehicleNameLabel.draw();

	if(isAccelerationChartVisible)
		accelerationChart.draw();
	else if(isPowerTorqueGraphVisible)
		powerTorqueGraph.draw();
	else
		vehicleInfoChart.draw();

	if(focus == FOCUS_ON_APPEARANCE_SELECTOR)
	{
		appearancePrevButton.highlighted = blinkCycle and appearancePrevButton.bounds.contains(mousePos);
		appearancePrevButton.draw();

		appearanceNextButton.highlighted = blinkCycle and appearanceNextButton.bounds.contains(mousePos);
		appearanceNextButton.draw();

		appearanceLabel.draw();
	}

	menu.focusedEntryBgColor = (focus == FOCUS_ON_LIST? Color::WHITE : Color::BLUE);
	menu.draw();

	menuUpButton.highlighted = blinkCycle and menuUpButton.bounds.contains(mousePos);
	menuUpButton.draw();

	menuDownButton.highlighted = blinkCycle and menuDownButton.bounds.contains(mousePos);
	menuDownButton.draw();

	backButton.highlighted = blinkCycle and backButton.bounds.contains(mousePos);
	backButton.draw();

	selectButton.highlighted = blinkCycle and selectButton.bounds.contains(mousePos);
	selectButton.draw();

	detailsButton.highlighted = blinkCycle and detailsButton.bounds.contains(mousePos);
	detailsButton.draw();

	orderButton.highlighted = blinkCycle and orderButton.bounds.contains(mousePos);
	orderButton.draw();

	appearanceButton.highlighted = blinkCycle and appearanceButton.bounds.contains(mousePos);
	appearanceButton.draw();

	automaticButton.highlighted = blinkCycle and automaticButton.bounds.contains(mousePos);
	automaticButton.draw();

	if(focus == FOCUS_ON_ORDER_MENU)
		orderMenu.draw();
}

void VehicleSelectionSimpleListState::onMouseButtonPressed(Mouse::Button button, int x, int y)
{
	if(selectButton.bounds.contains(x, y))
		this->onKeyPressed(Keyboard::KEY_ENTER);

	else if(backButton.bounds.contains(x, y))
		this->onKeyPressed(Keyboard::KEY_ESCAPE);

	else if(detailsButton.bounds.contains(x, y))
	{
		focus = FOCUS_ON_SIDE_MENU;
		sideMenuFocus = SIDE_MENU_DETAILS;
		onKeyPressed(Keyboard::KEY_ENTER);
	}

	else if(orderButton.bounds.contains(x, y))
	{
		focus = FOCUS_ON_SIDE_MENU;
		sideMenuFocus = SIDE_MENU_ORDER;
		onKeyPressed(Keyboard::KEY_ENTER);
	}

	else if(appearanceButton.bounds.contains(x, y))
	{
		focus = FOCUS_ON_SIDE_MENU;
		sideMenuFocus = SIDE_MENU_APPEARANCE;
		onKeyPressed(Keyboard::KEY_ENTER);
	}

	else if(automaticButton.bounds.contains(x, y))
	{
		focus = FOCUS_ON_SIDE_MENU;
		sideMenuFocus = SIDE_MENU_AUTO;
		onKeyPressed(Keyboard::KEY_ENTER);
	}

	else if(focus == FOCUS_ON_APPEARANCE_SELECTOR)
	{
		if(appearancePrevButton.bounds.contains(x, y))
			this->onKeyPressed(Keyboard::KEY_ARROW_LEFT);

		else if(appearanceNextButton.bounds.contains(x, y))
			this->onKeyPressed(Keyboard::KEY_ARROW_RIGHT);

		else
		{
			focus = FOCUS_ON_LIST;
			sndCursorOut->play();
		}
	}

	else if(focus == FOCUS_ON_ORDER_MENU)
	{
		if(orderMenu.bounds.contains(x, y))
		{
			orderMenu.setSelectedIndexByLocation(x, y);
			this->onKeyPressed(Keyboard::KEY_ENTER);
		}
		else
		{
			focus = FOCUS_ON_LIST;
			sndCursorOut->play();
		}
	}

	else if(focus == FOCUS_ON_LIST)
	{
		if(menuUpButton.bounds.contains(x, y))
			this->onKeyPressed(Keyboard::KEY_ARROW_UP);

		else if(menuDownButton.bounds.contains(x, y))
			this->onKeyPressed(Keyboard::KEY_ARROW_DOWN);

		else if(menu.bounds.contains(x, y))
		{
			if(menu.getIndexAtLocation(x, y) != menu.getSelectedIndex())
			{
				sndCursorMove->play();
				menu.setSelectedIndexByLocation(x, y);
			}
		}
	}

	else if(menu.bounds.contains(x, y))
	{
		focus = FOCUS_ON_LIST;
		if(menu.getIndexAtLocation(x, y) != menu.getSelectedIndex())
		{
			sndCursorMove->play();
			menu.setSelectedIndexByLocation(x, y);
		}
	}
}

void VehicleSelectionSimpleListState::onMouseWheelMoved(int amount)
{
	if(menu.bounds.contains(Mouse::getPosition())) while(amount != 0)
	{
		if(amount > 0)
		{
			menu.moveCursorUp();
			amount--;
		}
		else
		{
			menu.moveCursorDown();
			amount++;
		}
	}
}

void VehicleSelectionSimpleListState::onJoystickAxisMoved(unsigned joystick, unsigned axis, float value)
{
	GameLogic::passJoystickAxisToKeyboardArrows(this, axis, value);
}

void VehicleSelectionSimpleListState::onJoystickButtonPressed(unsigned joystick, unsigned button)
{
	GameLogic::passJoystickButtonsToEnterReturnKeys(this, button);
}

void VehicleSelectionSimpleListState::onKeyPressed(Keyboard::Key key)
{
	if(key == Keyboard::KEY_2)
	{
		playerIndex = 0;  // for safety
		game.enterState(Carse::VEHICLE_SELECTION_SHOWROOM_LAYOUT_STATE_ID);
		sndCursorOut->play();
	}

	else if(key == Keyboard::KEY_SPACE)
	{
		if(isAccelerationChartVisible)
		{
			isAccelerationChartVisible = false;
			isPowerTorqueGraphVisible = true;
		}
		else if(isPowerTorqueGraphVisible)
		{
			isAccelerationChartVisible = false;
			isPowerTorqueGraphVisible = false;
		}
		else
		{
			isAccelerationChartVisible = true;
			isPowerTorqueGraphVisible = false;
		}
		sndCursorMove->play();
	}

	else switch(focus)
	{
		case FOCUS_ON_LIST:
			onKeyPressedFocusOnList(key);
			break;

		case FOCUS_ON_SIDE_MENU:
			onKeyPressedFocusOnSideMenu(key);
			break;

		case FOCUS_ON_APPEARANCE_SELECTOR:
			onKeyPressedFocusOnAppearanceSelector(key);
			break;

		case FOCUS_ON_ORDER_MENU:
			onKeyPressedFocusOnOrderMenu(key);
			break;

		default:break;
	}
}

void VehicleSelectionSimpleListState::onKeyPressedFocusOnList(Keyboard::Key key)
{
	switch(key)
	{
		case Keyboard::KEY_ESCAPE:
		{
			sndCursorOut->play();
			game.enterState(game.logic.currentMainMenuStateId);
			if(playerIndex == 0)
				game.enterState(game.logic.currentMainMenuStateId);
			else
			{
				playerIndex--;
				game.enterState(getId());
			}
			break;
		}

		case Keyboard::KEY_ENTER:
		{
			sndCursorIn->play();
			game.logic.nextMatch.players[playerIndex].vehicleSpec = game.logic.getVehicleList()[vehicleIndexList[menu.getSelectedIndex()]];
			game.logic.nextMatch.players[playerIndex].alternateSpriteIndex = alternateSpriteIndexSelections[menu.getSelectedIndex()];
			game.logic.nextMatch.players[playerIndex].enableAutomaticShifting = autoShiftingChosen;
			if(game.logic.nextMatch.race.playerCount == 0 or playerIndex == game.logic.nextMatch.race.playerCount - 1)
			{
				playerIndex = 0;
				game.enterState(game.logic.currentMainMenuStateId);
			}
			else
			{
				playerIndex++;
				game.enterState(getId());
			}
			break;
		}

		case Keyboard::KEY_ARROW_UP:
		{
			menu.moveCursorUp();
			sndCursorMove->play();
			keydownTimestamp = fgeal::uptime();
			break;
		}

		case Keyboard::KEY_ARROW_DOWN:
		{
			menu.moveCursorDown();
			sndCursorMove->play();
			keydownTimestamp = fgeal::uptime();
			break;
		}

		case Keyboard::KEY_ARROW_LEFT:
		case Keyboard::KEY_ARROW_RIGHT:
		{
			focus = FOCUS_ON_SIDE_MENU;
			updateSideMenuButtonsAppearance();
			sndCursorMove->play();
			break;
		}

		default:break;
	}
}

void VehicleSelectionSimpleListState::onKeyPressedFocusOnSideMenu(Keyboard::Key key)
{
	if(key == Keyboard::KEY_ARROW_UP and sideMenuFocus > 0)
	{
		sideMenuFocus = static_cast<SideMenuFocus>(sideMenuFocus-1);
		updateSideMenuButtonsAppearance();
		sndCursorMove->play();
	}

	else if(key == Keyboard::KEY_ARROW_DOWN and sideMenuFocus < 3)
	{
		sideMenuFocus = static_cast<SideMenuFocus>(sideMenuFocus+1);
		updateSideMenuButtonsAppearance();
		sndCursorMove->play();
	}

	else if(key == Keyboard::KEY_ARROW_LEFT or key == Keyboard::KEY_ARROW_RIGHT)
	{
		focus = FOCUS_ON_LIST;
		updateSideMenuButtonsAppearance(true);
		sndCursorMove->play();
	}

	else if(key == Keyboard::KEY_ENTER) switch(sideMenuFocus)
	{
		case SIDE_MENU_DETAILS:
		{
			if(isAccelerationChartVisible)
			{
				isAccelerationChartVisible = false;
				isPowerTorqueGraphVisible = true;
			}
			else if(isPowerTorqueGraphVisible)
			{
				isAccelerationChartVisible = false;
				isPowerTorqueGraphVisible = false;
			}
			else
			{
				isAccelerationChartVisible = true;
				isPowerTorqueGraphVisible = false;
			}
			sndCursorMove->play();
			break;
		}

		case SIDE_MENU_ORDER:
		{
			focus = FOCUS_ON_ORDER_MENU;
			sndCursorIn->play();
			break;
		}

		case SIDE_MENU_APPEARANCE:
		{
			if(hasAlternateSprites)
			{
				focus = FOCUS_ON_APPEARANCE_SELECTOR;
				updateSideMenuButtonsAppearance(true);
			}
			sndCursorIn->play();
			sndCursorOut->play();
			break;
		}

		case SIDE_MENU_AUTO:
		{
			autoShiftingChosen = !autoShiftingChosen;
			automaticButton.text.setContent(autoShiftingChosen? "A" : "M");
			sndCursorIn->play();
			break;
		}

		default:break;
	}
}

void VehicleSelectionSimpleListState::onKeyPressedFocusOnAppearanceSelector(Keyboard::Key key)
{
	if(key == Keyboard::KEY_ENTER or key == Keyboard::KEY_ESCAPE)
	{
		focus = FOCUS_ON_LIST;
		sndCursorOut->play();
	}
	else if(key == Keyboard::KEY_ARROW_LEFT or key == Keyboard::KEY_ARROW_RIGHT)
	{
		const unsigned selected = menu.getSelectedIndex(),
					   alternateSpritesCount = game.logic.getVehicleList()[vehicleIndexList[selected]].alternateSprites.size();
		if(alternateSpritesCount > 0)
		{
			if(key == Keyboard::KEY_ARROW_RIGHT)  // forwards
			{
				if(alternateSpriteIndexSelections[selected] == (int) alternateSpritesCount - 1)
					alternateSpriteIndexSelections[selected] = -1;
				else
					alternateSpriteIndexSelections[selected]++;
			}
			else  // backwards
			{
				if(alternateSpriteIndexSelections[selected] == -1)
					alternateSpriteIndexSelections[selected] = alternateSpritesCount - 1;
				else
					alternateSpriteIndexSelections[selected]--;
			}
			sndCursorMove->play();
		}
	}
}

void VehicleSelectionSimpleListState::onKeyPressedFocusOnOrderMenu(Keyboard::Key key)
{
	if(key == Keyboard::KEY_ESCAPE)
	{
		focus = FOCUS_ON_LIST;
		sndCursorOut->play();
	}
	else if(key == Keyboard::KEY_ENTER)
	{
		const unsigned previousSelectedVehicleIndex = vehicleIndexList[menu.getSelectedIndex()];
		const vector<unsigned> vehicleListBackup = vehicleIndexList;

		if(orderMenu.getSelectedIndex() < VehicleSorter::ORDER_COUNT)
		{
			vehicleSorter.order = static_cast<VehicleSorter::Order>(orderMenu.getSelectedIndex());
			std::sort(vehicleIndexList.begin(), vehicleIndexList.end(), vehicleSorter);
		}

		for(unsigned i = 0; i < vehicleIndexList.size(); i++)
			menu.getEntryAt(i).label = game.logic.getVehicleList()[vehicleIndexList[i]].name;
		menu.updateDrawableText();

		const vector<int> alternateSpriteIndexSelectionsBackup = alternateSpriteIndexSelections;

		for(unsigned i = 0; i < alternateSpriteIndexSelections.size(); i++)
			alternateSpriteIndexSelections[i] = -1;

		for(unsigned i = 0; i < vehicleIndexList.size(); i++)
		{
			if(vehicleIndexList[i] == previousSelectedVehicleIndex)
				menu.setSelectedIndex(i);

			for(unsigned j = 0; j < vehicleListBackup.size(); j++)
			{
				if(vehicleIndexList[i] == vehicleListBackup[j])
				{
					alternateSpriteIndexSelections[i] = alternateSpriteIndexSelectionsBackup[j];
					break;
				}
			}
		}

		previewSpriteVehicleIndex = 0;
		previewSpriteVehicleAlternateSpriteIndex = -1;
		delete previewSprite;
		previewSprite = null;

		focus = FOCUS_ON_LIST;
		sndCursorOut->play();
	}
	else if(key == Keyboard::KEY_ARROW_UP)
	{
		orderMenu.moveCursorUp();
		sndCursorMove->play();
	}
	else if(key == Keyboard::KEY_ARROW_DOWN)
	{
		orderMenu.moveCursorDown();
		sndCursorMove->play();
	}
}

void VehicleSelectionSimpleListState::updateSideMenuButtonsAppearance(bool clear)
{
	detailsButton.backgroundColor = orderButton.backgroundColor = appearanceButton.backgroundColor = automaticButton.backgroundColor = menu.backgroundColor;
	detailsButton.text.setColor(Color::WHITE);
	orderButton.text.setColor(Color::WHITE);
	appearanceButton.text.setColor(Color::WHITE);
	automaticButton.text.setColor(Color::WHITE);

	if(not hasAlternateSprites)
	{
		appearanceButton.backgroundColor = Color::GREY;
		appearanceButton.text.setColor(Color::DARK_GREY);
	}

	if(not clear) switch(sideMenuFocus)
	{
		case SIDE_MENU_DETAILS: detailsButton.backgroundColor = Color::WHITE; detailsButton.text.setColor(Color::BLUE); break;
		case SIDE_MENU_ORDER: orderButton.backgroundColor = Color::WHITE; orderButton.text.setColor(Color::BLUE); break;
		case SIDE_MENU_AUTO: automaticButton.backgroundColor = Color::WHITE; automaticButton.text.setColor(Color::BLUE); break;
		case SIDE_MENU_APPEARANCE:
			if(hasAlternateSprites) {
				appearanceButton.backgroundColor = Color::WHITE;
				appearanceButton.text.setColor(Color::BLUE);
			}
			else {
				appearanceButton.backgroundColor = Color::LIGHT_GREY;
				appearanceButton.text.setColor(Color::GREY);
			}
			break;
		default:break;
	}
}
