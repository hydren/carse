/*
 * vehicle_selection_simple_list_state.hpp
 *
 *  Created on: 17 de set de 2018
 *      Author: carlosfaruolo
 */

#ifndef VEHICLE_SELECTION_SIMPLE_LIST_STATE_HPP_
#define VEHICLE_SELECTION_SIMPLE_LIST_STATE_HPP_
#include <ciso646>

#include "vehicle_selection_state.hpp"

#include "fgeal/extra/sprite.hpp"

class VehicleSelectionSimpleListState extends public AbstractVehicleSelectionState
{
	fgeal::Font* fontTitle, *fontInfo, *fontSmall, *fontMenu;
	fgeal::Sound* sndCursorMove, *sndCursorIn, *sndCursorOut;

	fgeal::Menu menu, orderMenu;

	fgeal::ArrowIconButton menuUpButton, menuDownButton, appearancePrevButton, appearanceNextButton;
	fgeal::Button selectButton, backButton, detailsButton, orderButton, appearanceButton, automaticButton;

	fgeal::Label title, playerLabel, vehicleNameLabel, appearanceLabel;

	fgeal::Sprite* previewSprite;
	unsigned previewSpriteVehicleIndex;
	int previewSpriteVehicleAlternateSpriteIndex;
	bool hasAlternateSprites;

	enum Focus { FOCUS_ON_LIST, FOCUS_ON_SIDE_MENU, FOCUS_ON_APPEARANCE_SELECTOR, FOCUS_ON_ORDER_MENU } focus;
	enum SideMenuFocus { SIDE_MENU_DETAILS, SIDE_MENU_ORDER, SIDE_MENU_APPEARANCE, SIDE_MENU_AUTO } sideMenuFocus;

	float keydownTimestamp;

	public:
	virtual int getId();

	VehicleSelectionSimpleListState(Carse* game);
	~VehicleSelectionSimpleListState();

	virtual void initialize();
	virtual void onEnter();
	virtual void onLeave();

	virtual void render();
	virtual void update(float delta);

	virtual void onKeyPressed(fgeal::Keyboard::Key);
	virtual void onMouseButtonPressed(fgeal::Mouse::Button button, int x, int y);
	virtual void onMouseWheelMoved(int change);
	virtual void onJoystickAxisMoved(unsigned, unsigned, float);
	virtual void onJoystickButtonPressed(unsigned, unsigned);

	private:
	void updateSideMenuButtonsAppearance(bool clear=false);

	void onKeyPressedFocusOnList(fgeal::Keyboard::Key);
	void onKeyPressedFocusOnSideMenu(fgeal::Keyboard::Key);
	void onKeyPressedFocusOnAppearanceSelector(fgeal::Keyboard::Key);
	void onKeyPressedFocusOnOrderMenu(fgeal::Keyboard::Key);
};

#endif /* VEHICLE_SELECTION_SIMPLE_LIST_STATE_HPP_ */
