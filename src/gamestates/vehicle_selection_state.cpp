/*
 * vehicle_selection_state.cpp
 *
 *  Created on: 6 de jun de 2021
 *      Author: hydren
 */

#include "vehicle_selection_state.hpp"

#include "carse.hpp"

#include "fgeal/extra/primitives.hpp"

#include "futil/snprintf.h"
#include "futil/round.h"
#include "futil/string_actions.hpp"

using std::vector;
using std::string;
using fgeal::Color;
using fgeal::Point;
using fgeal::Graphics;

AbstractVehicleSelectionState::AbstractVehicleSelectionState(Carse* game)
: State(*game), game(*game), lastDisplaySize(), playerIndex(0), autoShiftingChosen(), vehicleSorter(*game),
  isAccelerationChartVisible(), isAccelerationChartExpired(), isPowerTorqueGraphVisible(), isPowerTorqueGraphExpired(),
  isVehicleInfoChartExpired()
{
	for(unsigned i = 0; i < game->logic.getVehicleList().size(); i++)
		vehicleIndexList.push_back(i);
}

static inline double epsilon(int placesCount)
{
	switch(placesCount)
	{
		case -3: return .001;
		case -2: return .01;
		case -1: return .1;
		default: case 0: return 1;
	}
}

static string toStrRounded(float value, int placesCount=0)
{
	char buffer[32];
	if(placesCount > 0)
		futil::snprintf(buffer, sizeof(buffer)/sizeof(*buffer), "%#.*f", placesCount, value);
	else
	{
		value = futil::round(value*epsilon(placesCount))/epsilon(placesCount);
		futil::snprintf(buffer, sizeof(buffer)/sizeof(*buffer), "%.0f", value);
	}
	return string(buffer);
}

// =====================================================================================================================

bool AbstractVehicleSelectionState::VehicleSorter::operator() (unsigned ia, unsigned ib)
{
	const Pseudo3DVehicle::Spec& a = game.logic.getVehicleList()[ia], &b = game.logic.getVehicleList()[ib];

	// sort by type
	if(a.body.vehicleType != b.body.vehicleType)
		return a.body.vehicleType < b.body.vehicleType;
	else
	{
		// sort by category
		if(a.categories != b.categories)
			return a.categories < b.categories;
		else
		{
			// always put "Default" vehicles first
			if(a.brand != b.brand)
			{
				if(a.brand == "Default")
					return true;

				else if(b.brand == "Default")
					return false;
			}
			switch(order)
			{
				default:
				case ORDER_BY_BRAND_AND_NAME:
				{
					if(a.brand != b.brand)
						return a.brand < b.brand;  // sort by brand
					else
						return a.name < b.name;  // sort by name
				}
				case ORDER_BY_BRAND_AND_YEAR:
				{
					if(a.brand != b.brand)
						return a.brand < b.brand;  // sort by brand
					else
						return a.year < b.year;  // sort by year
				}
				case ORDER_BY_BRAND_AND_PERF:
				{
					if(a.brand != b.brand)
						return a.brand < b.brand;  // sort by brand
					else
						return a.getEstimatedQuarterMileTime() > b.getEstimatedQuarterMileTime();  // sort by performance
				}
				case ORDER_BY_YEAR:
				{
					return a.year < b.year;  // sort by year
				}
				case ORDER_BY_PERF:
				{
					return a.getEstimatedQuarterMileTime() > b.getEstimatedQuarterMileTime();  // sort by performance
				}
				case ORDER_BY_POWER:
				{
					return a.engine.maximumPower < b.engine.maximumPower;  // sort by max. power
				}
				case ORDER_BY_WEIGHT:
				{
					return a.body.mass < b.body.mass;  // sort by mass
				}
				case ORDER_BY_DRIVETRAIN:
				{
					// if any of the two is AWD, give it less priority
					if(a.body.driveWheelType == Mechanics::DRIVE_WHEEL_ALL or b.body.driveWheelType == Mechanics::DRIVE_WHEEL_ALL)
						if(a.body.driveWheelType != b.body.driveWheelType)
							return a.body.driveWheelType > b.body.driveWheelType;
						else
							return a.body.engineLocation < b.body.engineLocation;
					else
						if(a.body.engineLocation != b.body.engineLocation)
							return a.body.engineLocation < b.body.engineLocation;
						else
							return a.body.driveWheelType < b.body.driveWheelType;
				}
				case ORDER_BY_ENGINE:
				{
					return a.engine.configuration < b.engine.configuration;  // sort by engine config.
				}
			}
		}
	}
}

// =====================================================================================================================

AbstractVehicleSelectionState::LineChart::LineChart(unsigned initialSeriesCount)
: series(initialSeriesCount), maximum(fgeal::Vector2D::create(5, 5)), interval(fgeal::Vector2D::create(1, 1)),
  gridColor(Color::BLACK), textColor(Color::BLACK), font(null)
{
	series[0].color = Color::BLUE;
	series[0].style = DataSeries::STYLE_LINE;
}

void AbstractVehicleSelectionState::LineChart::draw() const
{
	PlainComponent::draw();
	char buffer[4];
	const Point graduationPadding = { font != null? font->getTextWidth("999") : 4, font != null? font->getTextHeight() : 4 };
	const float primitiveFontSize = 0.05f*bounds.h;

	if(font != null)
	{
		if(not horizontalAxisLegend.empty())
			font->drawText(horizontalAxisLegend, bounds.x + bounds.w - 3*graduationPadding.x,  bounds.y + bounds.h - 2*graduationPadding.y, textColor);
		if(not verticalAxisLegend.empty())
			font->drawText(verticalAxisLegend, bounds.x + 1.25f*graduationPadding.x, bounds.y + 0.5f*graduationPadding.y, textColor);
		if(not title.empty())
			font->drawText(title, bounds.x + 0.5*bounds.w - 3*graduationPadding.x,  bounds.y + 0.5f*graduationPadding.y, textColor);
	}
	else
	{
		if(not horizontalAxisLegend.empty())
			fgeal::primitives::drawString(horizontalAxisLegend, bounds.x + bounds.w - 3*graduationPadding.x,  bounds.y + bounds.h - 2*graduationPadding.y, primitiveFontSize, 0.75f*primitiveFontSize, textColor);
		if(not verticalAxisLegend.empty())
			fgeal::primitives::drawString(verticalAxisLegend, bounds.x + 1.25f*graduationPadding.x, bounds.y + 0.5f*graduationPadding.y, primitiveFontSize, 0.75f*primitiveFontSize, textColor);
		if(not title.empty())
			fgeal::primitives::drawString(title, bounds.x + 0.5*bounds.w - 3*graduationPadding.x,  bounds.y + 0.5f*graduationPadding.y, primitiveFontSize, 0.75f*primitiveFontSize, textColor);
	}

	const unsigned horizontalLinesCount = ceil(maximum.y/interval.y);
	const float horizontalLinesSpacing = (bounds.h - graduationPadding.y)/horizontalLinesCount;
	for(unsigned i = 0; i < horizontalLinesCount; i++)
	{
		Graphics::drawLine(bounds.x + graduationPadding.x, bounds.y + i*horizontalLinesSpacing, bounds.x + bounds.w, bounds.y + i*horizontalLinesSpacing, gridColor);
		const unsigned gradValue = floor((horizontalLinesCount - i)*interval.y);
		if(font != null)
		{
			futil::snprintf(buffer, sizeof(buffer)/sizeof(char), "%u", gradValue >= 1000? 999 : gradValue);
			font->drawText(string(buffer), bounds.x, bounds.y + i*horizontalLinesSpacing, textColor);
		}
		else fgeal::primitives::drawDigits(gradValue, bounds.x, bounds.y + i*horizontalLinesSpacing, primitiveFontSize, textColor);
	}
	Graphics::drawLine(bounds.x + graduationPadding.x, bounds.y + bounds.h - graduationPadding.y, bounds.x + bounds.w, bounds.y + bounds.h - graduationPadding.y, gridColor);

	const unsigned verticalLinesCount = ceil(maximum.x/interval.x);
	const float verticalLinesSpacing = (bounds.w - graduationPadding.x)/verticalLinesCount;
	for(unsigned i = 0; i < verticalLinesCount; i++)
	{
		Graphics::drawLine(bounds.x + graduationPadding.x + i*verticalLinesSpacing, bounds.y, bounds.x + graduationPadding.x + i*verticalLinesSpacing, bounds.y + bounds.h - graduationPadding.y, gridColor);
		const unsigned gradValue = (i*interval.x);
		if(font != null)
		{
			futil::snprintf(buffer, sizeof(buffer)/sizeof(char), "%u", gradValue >= 1000? 999 : gradValue);
			font->drawText(string(buffer), bounds.x + graduationPadding.x + i*verticalLinesSpacing, bounds.y + bounds.h - graduationPadding.y, textColor);
		}
		else fgeal::primitives::drawDigits(gradValue, bounds.x + graduationPadding.x + i*verticalLinesSpacing, bounds.y + bounds.h - graduationPadding.y, primitiveFontSize, textColor);
	}

	for(unsigned s = 0; s < series.size(); s++)
		if(series[s].style == DataSeries::STYLE_LINE)
			for(unsigned i = 0; i < series[s].dataSet.size()-1 and series[s].dataSet[i].y <= maximum.y; i++)
				Graphics::drawLine(bounds.x + graduationPadding.x + (bounds.w - graduationPadding.x)*(series[s].dataSet[i].x/maximum.x),
								   bounds.y + (bounds.h - graduationPadding.y)*(1 - series[s].dataSet[i].y/maximum.y),
								   bounds.x + graduationPadding.x + (bounds.w - graduationPadding.x)*(series[s].dataSet[i+1].x/maximum.x),
								   bounds.y + (bounds.h - graduationPadding.y)*(1 - series[s].dataSet[i+1].y/maximum.y), series[s].color);
		else
			for(unsigned i = 0; i < series[s].dataSet.size() and series[s].dataSet[i].y <= maximum.y; i++)
				if(series[s].style == DataSeries::STYLE_SQUARE)
					Graphics::drawFilledRectangle(bounds.x + graduationPadding.x + (bounds.w - graduationPadding.x)*(series[s].dataSet[i].x/maximum.x) - 0.01775*bounds.h,
											      bounds.y + (bounds.h - graduationPadding.y)*(1 - series[s].dataSet[i].y/maximum.y) - 0.01775*bounds.h, 0.0355*bounds.h, 0.02*bounds.h, series[s].color);
				else
					Graphics::drawFilledCircle(bounds.x + graduationPadding.x + (bounds.w - graduationPadding.x)*(series[s].dataSet[i].x/maximum.x),
											   bounds.y + (bounds.h - graduationPadding.y)*(1 - series[s].dataSet[i].y/maximum.y), 0.02*bounds.h, series[s].color);
}

AbstractVehicleSelectionState::AccelerationGraph::AccelerationGraph()
: LineChart(2), speedDataSeries(series[0]), speedGearChangeDataSeries(series[1]),
  simulationType(), tireFrictionFactor(0.85), rollingResistanceFactor(0.013), speedFactor(1)
{
	speedGearChangeDataSeries.style = DataSeries::STYLE_POINT;
	speedGearChangeDataSeries.color = Color::YELLOW;
	maximum.x = 40;
	maximum.y = 80;
	interval.x = 5;
	interval.y = 20;
	title = "ACCELERATION";
	horizontalAxisLegend = "TIME(SEC)";
}

void AbstractVehicleSelectionState::AccelerationGraph::generate(const Pseudo3DVehicle::Spec& spec)
{
	const float delta = 0.01f;
	Pseudo3DVehicle vehicle;
	vehicle.setSpec(spec);
	vehicle.body.simulationType = simulationType;
	vehicle.body.automaticShiftingEnabled = true;

	vehicle.body.reset();
	vehicle.body.engine.throttlePosition = 1.0f;  // start at full throttle

	// testing AWD vehicles with full RPM at start for best results (ps: unsuitable for other drivetrains)
	if(vehicle.body.spec.driveWheelType == Mechanics::DRIVE_WHEEL_ALL)
	{
		vehicle.body.engine.gear = 0;
		vehicle.body.engine.rpm = vehicle.body.engine.spec.maxRpm;
		vehicle.body.shiftGear(1);
	}

	Point first = { 0, 0 };
	float accTime = -1, qmTime = -1;
	speedGearChangeDataSeries.dataSet.clear();
	speedDataSeries.dataSet.clear();
	speedDataSeries.dataSet.push_back(first);
	for(float t = delta, lt = 0; t <= maximum.x; t += delta)
	{
		const int gearBefore = vehicle.body.engine.gear;
		vehicle.body.updatePowertrain(delta);
		vehicle.position += vehicle.body.speed*delta;

		// slip ratio governor for improved grip during test run
		if(simulationType == Mechanics::SIMULATION_TYPE_PACEJKA_BASED and vehicle.body.spec.driveWheelType != Mechanics::DRIVE_WHEEL_ALL)
		{
			if(vehicle.body.slipRatio > 0.06)
				vehicle.body.engine.throttlePosition -= 0.75f * delta;
			else
				vehicle.body.engine.throttlePosition += 0.75f * delta;

			     if(vehicle.body.engine.throttlePosition < 0.f) vehicle.body.engine.throttlePosition = 0.f;
			else if(vehicle.body.engine.throttlePosition > 1.f) vehicle.body.engine.throttlePosition = 1.f;
		}

		// record acc. 0-60 (or 0-100) time
		if(accTime == -1 and vehicle.body.speed >= 27.3f)
			accTime = t+delta;

		// record quarter mile time
		if(qmTime == -1 and vehicle.position >= 402.336f)
			qmTime = t+delta;

		// break loop if y goes of the chart and insert last entry interpolated
		if(vehicle.body.speed*speedFactor > maximum.y)
		{
			const float speedPrevious = (speedDataSeries.dataSet.empty()? 0 : speedDataSeries.dataSet.back().y),
						partialDelta = (maximum.y - speedPrevious) * delta / (vehicle.body.speed*speedFactor - speedPrevious);
			Point entry = { t + partialDelta, maximum.y };
			speedDataSeries.dataSet.push_back(entry);
			break;
		}

		// record gear shift
		if(vehicle.body.engine.gear > gearBefore)
		{
			Point shiftEvent = { t, vehicle.body.speed*speedFactor };
			speedDataSeries.dataSet.push_back(shiftEvent);
			speedGearChangeDataSeries.dataSet.push_back(shiftEvent);
		}
		// record snapshot
		else if(t - lt >= interval.x)
		{
			Point entry = { t, vehicle.body.speed*speedFactor };
			speedDataSeries.dataSet.push_back(entry);
			lt = t;
		}

		// insert last entry before loop end
		if(t+delta > maximum.x)
		{
			Point entry = { t, vehicle.body.speed*speedFactor };
			speedDataSeries.dataSet.push_back(entry);
		}
	}

	// figure out top speed
	float topSpeed = 0, topSpeedTime = 0;
	for(float t = 0; t - topSpeedTime < 1.f and t < 300.f; t += delta)  // if more than 1 sec. passes without increasing top speed or 5min passed, stop search
	{
		vehicle.body.updatePowertrain(delta);
		vehicle.position += vehicle.body.speed*delta;

		// record acc. 0-60 (or 0-100) time case it haven't been reached yet
		if(accTime == -1 and vehicle.body.speed >= 27.3f)
			accTime = speedDataSeries.dataSet.back().x+t+delta;

		// record quarter mile time case it haven't been reached yet
		if(qmTime == -1 and vehicle.position >= 402.336f)
			qmTime = speedDataSeries.dataSet.back().x+t+delta;

		// update top speed
		if(vehicle.body.speed > topSpeed)
		{
			topSpeed = vehicle.body.speed;
			topSpeedTime = t;  // record time of current top speed
		}
	}
	topSpeed *= speedFactor;

	// figure out quarter mile time case it haven't been reached yet
	for(float t = 0, topSpeedTime = 0; t - topSpeedTime < 1.f and t < 300.f; t += delta)  // if more than 1 sec. passes without increasing top speed or 5min passed, stop search
	{
		vehicle.body.updatePowertrain(delta);
		vehicle.position += vehicle.body.speed*delta;

		// record quarter mile time case it haven't been reached yet
		if(qmTime == -1 and vehicle.position >= 402.336f)
			qmTime = speedDataSeries.dataSet.back().x+topSpeedTime+t+delta;
	}

	topSpeedLabel = "TOP SPEED: "+toStrRounded(topSpeed);
	accTimeLabel  = "ACC. TIME: "+(accTime == -1? "N/P" : toStrRounded(accTime, 1))+"s";
	quarterMileTimeLabel = " 1/4 MILE: "+(qmTime == -1? "slow!" : toStrRounded(qmTime, 1))+"s";
}

void AbstractVehicleSelectionState::AccelerationGraph::draw() const
{
	LineChart::draw();
	if(font != null)
	{
		const float lbw = 4 + font->getTextWidth("TOP SPEED: 999");
		font->drawText(topSpeedLabel, bounds.x + bounds.w - lbw, bounds.y + 4, textColor);
		font->drawText(accTimeLabel, bounds.x + bounds.w - lbw, bounds.y + 4 + font->getTextHeight(), textColor);
		font->drawText(quarterMileTimeLabel, bounds.x + bounds.w - lbw, bounds.y + 4 + font->getTextHeight()*2, textColor);
	}
}

AbstractVehicleSelectionState::PowerTorqueGraph::PowerTorqueGraph()
: LineChart(3), powerDataSeries(series[0]), torqueDataSeries(series[1]), maximumDataSeries(series[2]),
  powerFactor(1), torqueFactor(1)
{
	torqueDataSeries.color = Color::ORANGE;
	torqueDataSeries.style = DataSeries::STYLE_LINE;
	maximumDataSeries.color = Color::MAGENTA;
	maximumDataSeries.style = DataSeries::STYLE_POINT;
	maximumDataSeries.dataSet.resize(2);
	title = "POWER & TORQUE";
	horizontalAxisLegend = "RPM x 1000";
}

void AbstractVehicleSelectionState::PowerTorqueGraph::generate(const Pseudo3DVehicle::Spec& spec)
{
	fgeal::Point& maxPower=maximumDataSeries.dataSet[0], &maxTorque=maximumDataSeries.dataSet[1];
	powerDataSeries.dataSet.clear();
	torqueDataSeries.dataSet.clear();
	maxPower.x = maxPower.y = maxTorque.x = maxTorque.y = 0;
	for(float rpm = 1; rpm <= spec.engine.maxRpm; rpm += 100)
	{
		const float torqueNmAtRpm = spec.engine.maximumTorque * spec.engine.torqueCurveProfile.getTorqueFactor(rpm);
		Point data = { rpm/1000, torqueNmAtRpm * torqueFactor};
		torqueDataSeries.dataSet.push_back(data);
		if(data.y > maxTorque.y)
			maxTorque = data;
		data.y = powerFactor * torqueNmAtRpm * rpm/(5252.0 * 1.355818);
		powerDataSeries.dataSet.push_back(data);
		if(data.y > maxPower.y)
			maxPower = data;
	}

	maximum.x = static_cast<int>((spec.engine.maxRpm+950.f)/1000.f);
	interval.x = 1;
	while(maximum.x/interval.x > 12)
		interval.x *= 2;

	const float maxVerticalValue = std::max(maxPower.y * powerFactor, maxTorque.y * torqueFactor);
	maximum.y = 10;
	const int graduationSetups[] = { 50, 100, 200, 500, 750, 1000 };
	for(unsigned i = 0; maxVerticalValue > maximum.y and i < sizeof(graduationSetups)/sizeof(int); i++)
		maximum.y = graduationSetups[i];
	interval.y = maximum.y/5;

	maxPowerLabel  = " MAX POWER: " + toStrRounded(maxPower.y,  1) + " @ " + toStrRounded(maxPower.x*1000,  -2) + "rpm";
	maxTorqueLabel = "MAX TORQUE: " + toStrRounded(maxTorque.y, 1) + " @ " + toStrRounded(maxTorque.x*1000, -2) + "rpm";
}

void AbstractVehicleSelectionState::PowerTorqueGraph::draw() const
{
	LineChart::draw();
	if(font != null)
	{
		const float lbw = 4 + font->getTextWidth("MAX TORQUE: 999.9 @ 9999 rpm");
		font->drawText(maxPowerLabel, bounds.x + bounds.w - lbw, bounds.y + 4, textColor);
		font->drawText(maxTorqueLabel, bounds.x + bounds.w - lbw, bounds.y + 4 + font->getTextHeight(), textColor);
	}
}

// =====================================================================================================================

void AbstractVehicleSelectionState::VehicleInfoChart::generate(const Pseudo3DVehicle::Spec& vehicle, const GameLogic::UnitSet& uiUnits)
{
	while(entries.size() < (unsigned) showType + 6)
		addEntry("");

	while(entries.size() > (unsigned) showType + 6)
		entries.pop_back();

	using futil::to_string;

	unsigned i = 0;

	if(showType)
		entries[i++].label.assign("Type: ").append(vehicle.body.vehicleType == Mechanics::TYPE_CAR? "Car" : vehicle.body.vehicleType == Mechanics::TYPE_BIKE? "Bike" : "Other");

	const string txtEngineDesc = (vehicle.engine.aspiration.empty()? "" : vehicle.engine.aspiration + " ")
	                           + (vehicle.engine.displacement == 0?  "" : (vehicle.engine.displacement < 950 or uiUnits.engineDisplacement == GameLogic::UI_UNIT_ENGDISP_CC)? to_string(vehicle.engine.displacement)+"cc "
	                        		                                    : uiUnits.engineDisplacement == GameLogic::UI_UNIT_ENGDISP_LITRE? toStrRounded(vehicle.engine.displacement/1000.0, 1) + "L "
																		: toStrRounded(vehicle.engine.displacement/16.387064) + (uiUnits.engineDisplacement == GameLogic::UI_UNIT_ENGDISP_CID? "cid " : " cu in "))
	                           + (vehicle.engine.valvetrain.empty()? "" : vehicle.engine.valvetrain + " ")
	                           + (vehicle.engine.valveCount == 0?    "" : to_string(vehicle.engine.valveCount) + "-valve ")
							   + (vehicle.engine.strokeCount == 0 or vehicle.engine.strokeCount == 4? "" : vehicle.engine.strokeCount == 2? "two-stroke " : to_string(vehicle.engine.strokeCount) + "-stroke ")
	                           + (vehicle.engine.configuration.empty()? "" : vehicle.engine.configuration);

	entries[i++].label.assign("Engine: ").append(txtEngineDesc.empty()? "--" : txtEngineDesc);

	entries[i++].label.assign("Power: ").append(uiUnits.power == GameLogic::UI_UNIT_POWER_PS? toStrRounded(vehicle.engine.maximumPower * GameLogic::PHYSICS_HP_TO_PS) + "ps @ "
                                               :uiUnits.power == GameLogic::UI_UNIT_POWER_KW? toStrRounded(vehicle.engine.maximumPower * GameLogic::PHYSICS_HP_TO_KW,  1) + "kW @ "
                      		                                                                : to_string((int)vehicle.engine.maximumPower) + "hp @ ")
	                                    .append(toStrRounded(vehicle.engine.maximumPowerRpm,  -2) + "rpm");

	entries[i++].label.assign("Torque: ").append(uiUnits.torque == GameLogic::UI_UNIT_TORQUE_KGFM? toStrRounded(vehicle.engine.maximumTorque * GameLogic::PHYSICS_NM_TO_KGFM, 1) + "kgf.m @ "
                                                :uiUnits.torque == GameLogic::UI_UNIT_TORQUE_LBFT? toStrRounded(vehicle.engine.maximumTorque * GameLogic::PHYSICS_NM_TO_LBFT) + " lb-ft @ "
                                                                                                 : toStrRounded(vehicle.engine.maximumTorque) + "Nm @ ")
	                                     .append(toStrRounded(vehicle.engine.maximumTorqueRpm, -2) + "rpm");

	entries[i++].label.assign("Transmission: ").append(to_string(vehicle.engine.gearCount) + "-speed");

	entries[i++].label.assign("Weight: ").append(uiUnits.isImperialWeight? toStrRounded(vehicle.body.mass * 2.204623f) + "lbs"
                                                                         : toStrRounded(vehicle.body.mass) + "kg");

	entries[i++].label.assign("Drivetrain: ").append(vehicle.body.driveWheelType == Mechanics::DRIVE_WHEEL_ALL? "AWD"
                                                  : (vehicle.body.engineLocation == Mechanics::ENGINE_LOCATION_FRONT?  "F"
                                                  :  vehicle.body.engineLocation == Mechanics::ENGINE_LOCATION_REAR?   "R"
                                                  :  vehicle.body.engineLocation == Mechanics::ENGINE_LOCATION_MIDDLE? "M" : "X")
										          + string(vehicle.body.driveWheelType == Mechanics::DRIVE_WHEEL_FRONT? "F"
                            	 	                      :vehicle.body.driveWheelType == Mechanics::DRIVE_WHEEL_REAR?  "R" : "X"));
	selectedIndex = i;
	borderDisabled = backgroundDisabled = true;
	textHorizontalAlignment = textVerticalAlignment = ALIGN_LEADING;
}
