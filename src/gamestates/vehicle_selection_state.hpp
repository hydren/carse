/*
 * vehicle_selection_state.hpp
 *
 *  Created on: 6 de jun de 2021
 *      Author: hydren
 */

#ifndef GAMESTATES_VEHICLE_SELECTION_STATE_HPP_
#define GAMESTATES_VEHICLE_SELECTION_STATE_HPP_
#include <ciso646>

#include "vehicle.hpp"

#include "game_logic.hpp"

#include "fgeal/fgeal.hpp"
#include "fgeal/extra/gui.hpp"
#include "fgeal/extra/game.hpp"

#include "futil/language.hpp"

#include <vector>

// fwd decl.
class Carse;

class AbstractVehicleSelectionState extends public fgeal::Game::State
{
	protected:
	Carse& game;
	fgeal::Vector2D lastDisplaySize;
	unsigned playerIndex;

	std::vector<unsigned> vehicleIndexList;  // holds a custom ordered list of vehicles by its indexes

	std::vector<int> alternateSpriteIndexSelections;
	bool autoShiftingChosen;

	public:
	AbstractVehicleSelectionState(Carse* game);

	struct VehicleSorter
	{
		Carse& game;
		enum Order { ORDER_BY_BRAND_AND_NAME, ORDER_BY_BRAND_AND_YEAR, ORDER_BY_BRAND_AND_PERF, ORDER_BY_YEAR, ORDER_BY_PERF,
					 ORDER_BY_POWER, ORDER_BY_WEIGHT, ORDER_BY_DRIVETRAIN, ORDER_BY_ENGINE, ORDER_COUNT } order;

		VehicleSorter(Carse& game) : game(game), order() {}

		inline const char* getOrderDescription(Order order)
		{
			switch(order)
			{
				case ORDER_BY_BRAND_AND_NAME: return "brand/name";
				case ORDER_BY_BRAND_AND_YEAR: return "brand/year";
				case ORDER_BY_BRAND_AND_PERF: return "brand/performance";
				case ORDER_BY_YEAR: return "year";
				case ORDER_BY_PERF: return "performance";
				case ORDER_BY_POWER: return "max. power";
				case ORDER_BY_WEIGHT: return "weight";
				case ORDER_BY_DRIVETRAIN: return "drivetrain";
				case ORDER_BY_ENGINE: return "engine";
				case ORDER_COUNT: return "none";
				default: return "???";
			}
		}
		inline const char* getOrderDescription() { return getOrderDescription(order); }

		inline void setOrder(unsigned o) { order = static_cast<Order>(o); }
		inline void setNextOrder() { order = static_cast<Order>((order + 1) % VehicleSorter::ORDER_COUNT); }

		bool operator() (unsigned a, unsigned b);
	}
	vehicleSorter;

	struct LineChart extends fgeal::PlainComponent
	{
		struct DataSeries
		{
			std::vector<fgeal::Point> dataSet;  // values to be plotted
			std::string label;
			fgeal::Color color;
			enum Style { STYLE_LINE, STYLE_POINT, STYLE_SQUARE, STYLE_COUNT } style;  // line chart
		};
		std::vector<DataSeries> series;

		fgeal::Point maximum;  // maximum values (x, y) displayed in the chart

		fgeal::Point interval;  // the size (x, y) of the chart graduation

		std::string title, horizontalAxisLegend, verticalAxisLegend;

		fgeal::Color gridColor, textColor;

		fgeal::Font* font;

		LineChart(unsigned initialSeriesCount=1);

		void draw() const;
	};

	struct AccelerationGraph extends LineChart
	{
		DataSeries& speedDataSeries, &speedGearChangeDataSeries;

		Mechanics::SimulationType simulationType;  // simulation used in estimation

		float tireFrictionFactor, rollingResistanceFactor;  // constants to be used in simulation

		float speedFactor;  // a conversion factor for different speed units (use 3.6 for km/h, ~2.237 for mph; default = 1 (for m/s))

		std::string topSpeedLabel, accTimeLabel, quarterMileTimeLabel;  // labels for recorded top speed, time to reach 27 m/s (~60mph, ~100kph) and quarter mile time

		AccelerationGraph();

		// clear data and generate new one from given spec
		void generate(const Pseudo3DVehicle::Spec& spec);

		void draw() const;
	}
	accelerationChart;
	bool isAccelerationChartVisible, isAccelerationChartExpired;

	struct PowerTorqueGraph extends LineChart
	{
		DataSeries& powerDataSeries, &torqueDataSeries, &maximumDataSeries;

		float powerFactor, torqueFactor;  // a conversion factor for different power and torque units (use 1 for imperial hp (SAE) power and newton-metre (Nm) torque)

		std::string maxPowerLabel, maxTorqueLabel;  // labels for max power and max torque

		PowerTorqueGraph();

		// clear data and generate new one from given spec
		void generate(const Pseudo3DVehicle::Spec& spec);

		void draw() const;
	}
	powerTorqueGraph;
	bool isPowerTorqueGraphVisible, isPowerTorqueGraphExpired;

	struct VehicleInfoChart extends fgeal::Menu
	{
		bool showType;
		VehicleInfoChart(bool showType=true) : showType(true) {}

		// clear data and generate new one from given spec
		void generate(const Pseudo3DVehicle::Spec& spec, const GameLogic::UnitSet&);
	}
	vehicleInfoChart;
	bool isVehicleInfoChartExpired;
};

#endif /* GAMESTATES_VEHICLE_SELECTION_STATE_HPP_ */
