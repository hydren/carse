/*
 * main.cpp
 *
 *  Created on: 21/08/2014
 *      Author: hydren
 */
#include <ciso646>

#include <iostream>
#include <string>
#include <stdexcept>
#include <algorithm>

#include <cstdlib>
#include <ctime>

#include "carse.hpp"

#include "main_args.hpp"
#include <tclap/CmdLine.h>

#include "fgeal/fgeal.hpp"
#include "futil/string_actions.hpp"
#include "futil/string_split.hpp"

using std::cout;
using std::endl;
using std::string;

using fgeal::Display;
using fgeal::Image;
using fgeal::Graphics;
using fgeal::Color;
using fgeal::AdapterException;

using futil::starts_with;
using futil::split;

//#define CARSE_TCLAP_NO_HYPHEN_ARG_WORKAROUND

#ifdef CARSE_TCLAP_NO_HYPHEN_ARG_WORKAROUND
	// macro that removes hyphen from the arguments' name when TCLAP doesn't support it because its version is too old
	#define ahf(argname) futil::replace_all(argname, "-", "")
#else
	// macro that removes hyphen from the arguments' name when TCLAP doesn't support it because its version is too old
	#define ahf(argname) argname
#endif

const string CARSE_VERSION_STRING(CARSE_VERSION_STR);

void runSplash()
{
	Display& display = Display::getInstance();
	Graphics::drawFilledRectangle(0, 0, display.getWidth(), display.getHeight(), Color::WHITE);
	Image logoImage("assets/carse-logo.png"), loadingImage("assets/loading.png");
	logoImage.draw(display.getWidth()/2 - logoImage.getWidth()/2, display.getHeight()/2 - logoImage.getHeight()/2);
	loadingImage.draw(display.getWidth() - loadingImage.getWidth(), display.getHeight() - loadingImage.getHeight());
	display.refresh();
	fgeal::rest(0.5);
}

static struct CustomCmdLine extends TCLAP::CmdLine
{
	// configure custom arguments parser
	CustomCmdLine() : CmdLine("carse - pseudo-3D racing engine", ' ', CARSE_VERSION_STRING, true)
	{
		//trick to modify only the output from --version
		struct CustomOutput extends public TCLAP::StdOutput
		{
			virtual void version(TCLAP::CmdLineInterface& c)
			{
				cout << "carse " << CARSE_VERSION_STRING << endl;
			}
		};
		this->CmdLine::setOutput(new CustomOutput());
	}
	~CustomCmdLine() { delete this->CmdLine::getOutput(); }

	//trick to modify the output from --help to print args in reverse order of inclusion
	void reverseArgList() { std::reverse(_argList.begin(), ------_argList.end()); }

} CarseCmdLine;  // singleton instance

CmdLineArgs::CmdLineArgs()
: resolution           ("r", "resolution", "If in windowed mode, attempt to create a window of the given size. If in fullscreen mode, attempt to start in the given resolution", false, string(), "WIDTHxHEIGHT", CarseCmdLine),
  fullscreen           ("f", "fullscreen", "Start in fullscreen mode.", CarseCmdLine, false),
  centered             ("c", "centered", "Attempt to center the window. Does nothing in fullscreen mode", CarseCmdLine, false),
  fastPrimitives       ("x", ahf("faster-gfx"), "Flag that hints usage of faster graphics primitives, if possible", CarseCmdLine, false),
  smoothSprites        ("s", ahf("smooth-sprites"), "Flag that hints smoothing of sprites when scaling/rotating, if possible", CarseCmdLine, false),
  showFps              ("z", ahf("show-fps"), "Flag that enables FPS counter on game", CarseCmdLine, false),
  masterVolume         ("v", ahf("master-volume"), "Specifies the master volume, in the range [0-1] (0 being no sound, 1.0 being maximum volume)", false, 0.9f, "decimal", CarseCmdLine),


  raceOnlyMode         ("R", "race", "Skip menus and go straight to race with current vehicle and course.", CarseCmdLine, false),
  debugMode            ("D", ahf("debug-mode"), "When used in conjunction with the --race parameter, sets a predefined debug race course, in debug mode.", CarseCmdLine, false),
  raceType             ("T", ahf("race-type"), "When used together with the --race parameter, specifies the race type, represented by its index", false, 0, "integer", CarseCmdLine),
  lapCount             ("L", ahf("lap-count"), "When used together with the --race-type parameter, specifies the number of laps of the race (loop race types only).", false, 2, "unsigned integer", CarseCmdLine),
  playerCount          ("P", ahf("player-count"), "When used together with the --race-type parameter, specifies the number of players (split-screen) for the race", false, 1, "unsigned integer", CarseCmdLine),
  opponentCount        ("O", ahf("cpu-count"), "When used together with the --race-type parameter, specifies the number of CPU-controlled opponents for the race", false, 1, "unsigned integer", CarseCmdLine),
  trafficDensity       ("F", ahf("traffic-density"), "When used together with the --race-type parameter, specifies the traffic density, in the range [0-1] (0 being no traffic, 1.0 being max traffic)", false, 0.5f, "decimal", CarseCmdLine),

  randomCourse         ("X", ahf("random-course"), "When used in conjunction with the --race parameter, generates and sets a random race course", CarseCmdLine, false),
  courseIndex          ("C", "course", "When used in conjunction with the --race parameter, specifies the race course, represented by its index", false, 0, "unsigned integer", CarseCmdLine),

  vehicleIndex         ("V", "vehicle", "When used in conjunction with the --race parameter, specifies the player vehicle, represented by its index", false, 0, "unsigned integer", CarseCmdLine),
  vehicleAltSpriteIndex("A", ahf("vehicle-alternate-sprite"), "When used in conjunction with the --vehicle parameter, specifies the alternate player vehicle sprite, represented by its index", false, -1, "integer", CarseCmdLine),
  vehicleManualShifting("M", ahf("vehicle-manual"), "When used in conjunction with the --vehicle parameter, enables manual gear shifting", CarseCmdLine, false),
  extraPlayersVehicles ("",  ahf("extra-players-vehicles"), "When used in conjunction with the --race parameter, specifies the additional players vehicles. Pass vehicle indexes (each optionally followed by an 'A' and an alternate sprite index, optionally followed by a 'M' for manual) separated with commas (without spaces!)", false, "", "<positive integer>[A<integer>][M][,...]", CarseCmdLine),

  simulationType       ("S", ahf("simulation-type"), "When used in conjunction with the --race parameter, specifies simulation type, represented by its index", false, 0, "unsigned integer", CarseCmdLine),
  hudType              ("H", "hud", "When used in conjunction with the --race parameter, specifies HUD type, represented by its index", false, 0, "unsigned index", CarseCmdLine),
  cameraType           ("Z", "camera", "When used in conjunction with the --race parameter, specifies camera behavior type, represented by its index.", false, 'B', "unsigned integer", CarseCmdLine),
  imperialUnit         ("U", ahf("imperial-units"), "When used in conjunction with the --race parameter, uses imperial units instead of metric", CarseCmdLine, false) {}

// singleton instance
CmdLineArgs CmdArgs;

int main(int argc, char** argv)
{
	CarseCmdLine.reverseArgList();
	CarseCmdLine.parse(argc, argv);

	int screenWidth = 800, screenHeight = 600;
	if(CmdArgs.resolution.isSet())
	{
		std::vector<string> tokens = split(CmdArgs.resolution.getValue(), 'x');
		if(tokens.size() == 2)
		{
			int customWidth =  atoi(tokens[0].c_str());
			int customHeight = atoi(tokens[1].c_str());
			if(customWidth != 0 and customHeight != 0)
			{
				screenWidth = customWidth;
				screenHeight = customHeight;
			}
		}
	}

	//greeter :)
	cout << CarseCmdLine.getMessage() << " - version " << CARSE_VERSION_STRING << endl;

	try { fgeal::initialize(); }
	catch (const AdapterException& e) { cout << "failed to initialize: " << e.what() << endl; return EXIT_FAILURE; }

	Graphics::useFasterPrimitivesHint = CmdArgs.fastPrimitives.getValue();
	Image::useImageTransformSmoothingHint = CmdArgs.smoothSprites.getValue();

	Display::Options options;
	options.title = "carse";
	options.iconFilename = "assets/carse-icon.png";
	options.fullscreen = CmdArgs.fullscreen.getValue();
	options.width = screenWidth;
	options.height = screenHeight;
	if(CmdArgs.centered.getValue())
		options.positioning = Display::Options::POSITION_CENTERED;

	try { Display::create(options); }
	catch (const AdapterException& e) { cout << "failed to open display: " << e.what() << endl; return EXIT_FAILURE; }

	srand(time(null));
	try {
		runSplash();
		Carse game;
		game.showFPS = CmdArgs.showFps.getValue();
		game.logic.raceOnlyMode = CmdArgs.raceOnlyMode.isSet();
		if(CmdArgs.masterVolume.isSet())
		{
			if(CmdArgs.masterVolume.getValue() < 0.f or CmdArgs.masterVolume.getValue() > 1.f)
				cout << "volume argument out of the range [1, 0]. ignoring..." << endl;
			else
				game.logic.masterVolume = CmdArgs.masterVolume.getValue();
		}
		game.start();
	}
	catch(const AdapterException& e) { cout << e.what() << endl; return EXIT_FAILURE; }

	try { fgeal::finalize(); }
	catch (const AdapterException& e) { cout << "failed to deinitialize: " << e.what() << endl; return EXIT_FAILURE; }
	return EXIT_SUCCESS;
}
