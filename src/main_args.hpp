/*
 * main_args.hpp
 *
 *  Created on: 17 de jun de 2022
 *      Author: felipe
 */

#ifndef MAIN_ARGS_HPP_
#define MAIN_ARGS_HPP_

#include <string>
#include <tclap/ValueArg.h>
#include <tclap/SwitchArg.h>
#include <futil/language.hpp>

struct CmdLineArgs
{
	/// general args
	TCLAP::ValueArg<std::string> resolution;
	TCLAP::SwitchArg fullscreen, centered, fastPrimitives, smoothSprites, showFps;
	TCLAP::ValueArg<float> masterVolume;

	/// race-only args
	TCLAP::SwitchArg raceOnlyMode, debugMode;
	TCLAP::ValueArg<unsigned> raceType, lapCount, playerCount, opponentCount;
	TCLAP::ValueArg<float> trafficDensity;

	// course args
	TCLAP::SwitchArg randomCourse;
	TCLAP::ValueArg<unsigned> courseIndex;

	// vehicle args
	TCLAP::ValueArg<unsigned> vehicleIndex;
	TCLAP::ValueArg<int> vehicleAltSpriteIndex;
	TCLAP::SwitchArg vehicleManualShifting;
	TCLAP::ValueArg<std::string> extraPlayersVehicles;

	// misc. args
	TCLAP::ValueArg<unsigned> simulationType, hudType;
	TCLAP::ValueArg<char> cameraType;
	TCLAP::SwitchArg imperialUnit;

	CmdLineArgs();
};

// singleton instance defined on main.cpp
extern CmdLineArgs CmdArgs;

#endif /* MAIN_ARGS_HPP_ */
