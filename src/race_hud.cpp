/*
 * race_hud.cpp
 *
 *  Created on: 10 de jun de 2019
 *      Author: carlos.faruolo
 */

#include "race_hud.hpp"

#include "futil/string_actions.hpp"
#include "futil/snprintf.h"

#include <cmath>
#include <climits>
#include "futil/math_constants.h"

using std::string;

using fgeal::Rectangle;
using fgeal::Color;
using fgeal::Point;
using fgeal::Graphics;
using fgeal::Font;
using fgeal::Image;
namespace primitives = fgeal::primitives;

Hud::GenericDialGauge::GenericDialGauge(const Rectangle& b)
: Label(),
  angleMin(0.25*M_PI), angleMax(1.75*M_PI),
  needleThickness(2.0f), needleColor(Color::RED), needleLinePrimitiveInstead(true),
  boltRadius(16.0f), boltColor(Color::BLACK),
  graduationColor(Color::BLACK), graduationValuePositionOffset(0.35f),
  graduationPrimaryLineSize(0.5f), graduationSecondaryLineSize(0.3f), graduationTertiaryLineSize(0.2f),
  graduationValueScale(1.0f), graduationLevel(1),
  fixationOffset(0), pointerOffset(0), pointerSizeScale(1.0f),
  backgroundImage(null), foregroundImage(null), pointerImage(null), pointerImageIsShared(false), imagesAreShared(false),
  backgroundCacheImage(null)
{
	bounds = b;
	backgroundColor = Color::WHITE;
	borderThickness = 2.0f;
	borderColor = Color::BLACK;
	shape = PlainComponent::SHAPE_ELLIPSOIDAL;
	textHorizontalAlignment = ALIGN_CENTER;
}

Hud::GenericDialGauge::~GenericDialGauge()
{
	if(pointerImage != null and not pointerImageIsShared)
		delete pointerImage;

	if(not imagesAreShared)
	{
		if(backgroundImage != null) delete backgroundImage;
		if(foregroundImage != null) delete foregroundImage;
	}

	if(backgroundCacheImage != null)
		delete backgroundCacheImage;
}

void Hud::GenericDialGauge::compile(float min, float max, float graduationPrimarySize, float graduationSecondarySize, float graduationTertiarySize, float graduationValueOffset, bool cacheBackground)
{
	const Point center = {bounds.x + 0.5f*bounds.w, bounds.y + 0.5f*bounds.h};

	graduationPrimaryCache.clear();
	graduationPrimaryNumericCache.clear();
	graduationSecondaryCache.clear();
	graduationTertiaryCache.clear();

	if(graduationLevel >= 1)  // primary graduation
	{
		bool offsetApplied = false;
		for(float g = min; g <= max; g += graduationPrimarySize)
		{
			const float gAngle = -((angleMax-angleMin)*g + angleMin*max - angleMax*min)/(max-min),
						gradeSize = 0.5f - 0.2f * graduationPrimaryLineSize;
			graduationPrimaryCache.push_back(Line(center.x + gradeSize*bounds.w*sin(gAngle), center.y + gradeSize*bounds.h*cos(gAngle),
												  center.x + 0.5f*bounds.w*sin(gAngle),      center.y + 0.5f*bounds.h*cos(gAngle)));

			// numerical graduation
			const std::string str(futil::to_string(g * graduationValueScale));
			fgeal::GUIDrawableText tmpText = text;
			tmpText.setContent(str);
			graduationPrimaryNumericCache.push_back(NumericGraduation(str, center.x + graduationValuePositionOffset*bounds.w*sin(gAngle) - 0.5*tmpText.getWidth(),
																		   center.y + graduationValuePositionOffset*bounds.h*cos(gAngle) - 0.5*tmpText.getHeight()));
			if(not offsetApplied)
			{
				g += graduationValueOffset;
				offsetApplied = true;
			}
		}
	}

	if(graduationLevel >= 2)  // secondary graduation
	for(float g = min; g <= max; g += graduationSecondarySize)
	{
		const float gAngle = -((angleMax-angleMin)*g + angleMin*max - angleMax*min)/(max-min),
					gradeSize = 0.5f - 0.2f * graduationSecondaryLineSize;

		const Line line(center.x + gradeSize*bounds.w*sin(gAngle), center.y + gradeSize*bounds.h*cos(gAngle),
		          center.x + 0.5f*bounds.w*sin(gAngle),      center.y + 0.5f*bounds.h*cos(gAngle));

		// check if line will not occupy the same slot of a primary graduation line
		bool isSlotFree = true;
		for(unsigned i = 0; isSlotFree and i < graduationPrimaryCache.size(); i++)
			if(line.x2 == graduationPrimaryCache[i].x2 and line.y2 == graduationPrimaryCache[i].y2)
				isSlotFree = false;

		if(isSlotFree)
			graduationSecondaryCache.push_back(line);
	}

	if(graduationLevel >= 3)  // tertiary graduation
	for(float g = min; g <= max; g += graduationTertiarySize)
	{
		const float gAngle = -((angleMax-angleMin)*g + angleMin*max - angleMax*min)/(max-min),
					gradeSize = 0.5f - 0.2f * graduationTertiaryLineSize;

		const Line line(center.x + gradeSize*bounds.w*sin(gAngle), center.y + gradeSize*bounds.h*cos(gAngle),
				  center.x + 0.5f*bounds.w*sin(gAngle),      center.y + 0.5f*bounds.h*cos(gAngle));

		// check if line will not occupy the same slot of a primary or secondary graduation line
		bool isSlotFree = true;
		for(unsigned i = 0; isSlotFree and i < graduationPrimaryCache.size(); i++)
			if(line.x2 == graduationPrimaryCache[i].x2 and line.y2 == graduationPrimaryCache[i].y2)
				isSlotFree = false;
		for(unsigned i = 0; isSlotFree and i < graduationSecondaryCache.size(); i++)
			if(line.x2 == graduationSecondaryCache[i].x2 and line.y2 == graduationSecondaryCache[i].y2)
				isSlotFree = false;

		if(isSlotFree)
			graduationTertiaryCache.push_back(line);
	}

	backgroundImageScale.x = bounds.w / (backgroundImage != null? backgroundImage->getWidth() : 250);
	backgroundImageScale.y = bounds.h / (backgroundImage != null? backgroundImage->getHeight(): 250);

	foregroundImageScale.x = bounds.w / (foregroundImage != null? foregroundImage->getWidth() : 250);
	foregroundImageScale.y = bounds.h / (foregroundImage != null? foregroundImage->getHeight(): 250);

	if(pointerImage != null)
		pointerImageScale.x = pointerImageScale.y = 0.5*pointerSizeScale*bounds.h/pointerImage->getHeight();

	this->clearCache();

	if(cacheBackground)
	{
		const float oldx = bounds.x, oldy = bounds.y;
		bounds.x = 0;
		bounds.y = 0;
		this->compile(min, max, graduationPrimarySize, graduationSecondarySize, graduationTertiarySize, graduationValueOffset);
		backgroundCacheImage = new Image(bounds.w, bounds.h);
		Graphics::setDrawTarget(backgroundCacheImage);
		Graphics::drawFilledRectangle(0, 0, backgroundCacheImage->getWidth(), backgroundCacheImage->getHeight(), Color::_TRANSPARENT);
		this->drawBackground();
		Graphics::setDefaultDrawTarget();
		bounds.x = oldx;
		bounds.y = oldy;
	}
}

void Hud::GenericDialGauge::clearCache()
{
	if(backgroundCacheImage != null)
		delete backgroundCacheImage, backgroundCacheImage = null;
}

void Hud::GenericDialGauge::drawBackground()
{
	if(backgroundImage != null)
		backgroundImage->drawScaled(bounds.x, bounds.y, backgroundImageScale.x, backgroundImageScale.y);
	else
		PlainComponent::draw();

	if(graduationLevel >= 1)  // primary graduation
	for(unsigned i = 0; i < graduationPrimaryCache.size(); i++)
	{
		const Line& line = graduationPrimaryCache[i];
		Graphics::drawLine(line.x1, line.y1, line.x2, line.y2, graduationColor);
		// numerical graduation
		if(i < graduationPrimaryNumericCache.size())
		{
			const NumericGraduation& grad = graduationPrimaryNumericCache[i];
			fgeal::GUIDrawableText tmpText = text;
			tmpText.setColor(graduationColor);
			tmpText.setContent(grad.str);
			tmpText.draw(grad.x, grad.y);
		}
	}

	if(graduationLevel >= 2)  // secondary graduation
	for(unsigned i = 0; i < graduationSecondaryCache.size(); i++)
	{
		const Line& line = graduationSecondaryCache[i];
		Graphics::drawLine(line.x1, line.y1, line.x2, line.y2, graduationColor);
	}

	if(graduationLevel >= 3)  // tertiary graduation
	for(unsigned i = 0; i < graduationTertiaryCache.size(); i++)
	{
		const Line& line = graduationTertiaryCache[i];
		Graphics::drawLine(line.x1, line.y1, line.x2, line.y2, graduationColor);
	}

	text.draw(bounds.x + alignmentHorizontalOffset(), bounds.y + 0.3f * bounds.h);
}

void Hud::GenericDialGauge::draw(float angle)
{
	if(backgroundCacheImage != null)
		backgroundCacheImage->draw(bounds.x, bounds.y);
	else
		this->drawBackground();

	if(pointerImage == null)  // draw built-in, gfx-primitives-based needle as pointer
	{
		const float cosa = cos(angle), sina = sin(angle);
		const Point center = {bounds.x + 0.5f*bounds.w, bounds.y + 0.5f*bounds.h + fixationOffset*backgroundImageScale.y},
				    needle = {0.4f*sina*(pointerSizeScale*bounds.w + pointerOffset), 0.4f*cosa*(pointerSizeScale*bounds.h + pointerOffset)};

		if(needleLinePrimitiveInstead)
		{
			if(needleThickness == 0)
				Graphics::drawLine(center.x + pointerOffset*sina, center.y + pointerOffset*cosa,
								   center.x + needle.x, center.y + needle.y, needleColor);
			else
				Graphics::drawThickLine(center.x + pointerOffset*sina, center.y + pointerOffset*cosa,
										center.x + needle.x, center.y + needle.y, needleThickness, needleColor);
		}
		else  // using triangle primitive
		{
			const float baseWidth = pointerOffset + 0.25f*boltRadius;
			Graphics::drawFilledTriangle(center.x + baseWidth*sin(angle+M_PI_2), center.y + baseWidth*cos(angle+M_PI_2),
										 center.x + baseWidth*sin(angle-M_PI_2), center.y + baseWidth*cos(angle-M_PI_2),
										 center.x + needle.x, center.y + needle.y, needleColor);
		}
		Graphics::drawFilledEllipse(center.x, center.y, 0.5f*boltRadius*bounds.w/bounds.h, 0.5f*boltRadius*bounds.h/bounds.w, boltColor);
	}
	else
		pointerImage->drawScaledRotated(bounds.x + 0.5f*bounds.w, bounds.y + 0.5f*bounds.h + fixationOffset*backgroundImageScale.y, pointerImageScale.x, pointerImageScale.y, angle, 0.5f*pointerImage->getWidth(), pointerOffset);

	if(foregroundImage != null)
		foregroundImage->drawScaled(bounds.x, bounds.y, foregroundImageScale.x, foregroundImageScale.y);
}

Hud::GenericBarGauge::GenericBarGauge(const Rectangle& b)
: PlainComponent(), fillColor(Color::RED), fillGradientColor(Color::BLACK), style(), sectionCount(8), sectionSpacing(8.0f), colorGradient(), gradientThreshold(1.f)
{
	bounds = b;
	backgroundColor = Color::WHITE;
	borderThickness = 2.0f;
	borderColor = Color::BLACK;
}

Color Hud::GenericBarGauge::determineColor(float fillRatio)
{
	switch(colorGradient)
	{
		default:case Hud::GenericBarGauge::GRADIENT_NONE: return fillColor;
		case Hud::GenericBarGauge::GRADIENT_CHANGE_PARTIAL_FILL_COLOR_AT_THRESHOLD:
		case Hud::GenericBarGauge::GRADIENT_CHANGE_FILL_COLOR_AT_THRESHOLD: return fillRatio < gradientThreshold? fillColor : fillGradientColor;
		case Hud::GenericBarGauge::GRADIENT_CHANGE_FILL_COLOR_GRADUALLY:
			return Color::create(fillColor.r*(1-fillRatio)+fillGradientColor.r*fillRatio, fillColor.g*(1-fillRatio)+fillGradientColor.g*fillRatio, fillColor.b*(1-fillRatio)+fillGradientColor.b*fillRatio, fillColor.a*(1-fillRatio)+fillGradientColor.a*fillRatio);
	}
}

void Hud::GenericBarGauge::draw(float fillRatio)
{
	PlainComponent::draw();
	const fgeal::Rectangle fillArea = { bounds.x + borderThickness + 1, bounds.y + borderThickness + 1, bounds.w - 2*borderThickness - 2, bounds.h - 2*borderThickness - 2 };
	if(style == STYLE_FLAT)
	{
		Graphics::drawFilledRectangle(fillArea.x, fillArea.y, fillArea.w * fillRatio, fillArea.h, determineColor(fillRatio));
		if(colorGradient == GRADIENT_CHANGE_PARTIAL_FILL_COLOR_AT_THRESHOLD and fillRatio > gradientThreshold)
			Graphics::drawFilledRectangle(fillArea.x, fillArea.y, fillArea.w * gradientThreshold, fillArea.h, fillColor);
	}
	else if(style == STYLE_SECTIONED)
		for(unsigned i = 0; i < sectionCount; i++)
			if(i / (float) sectionCount <= fillRatio)
				Graphics::drawFilledRectangle(fillArea.x + i*fillArea.w/sectionCount + 0.5f*sectionSpacing, fillArea.y, fillArea.w/sectionCount - sectionSpacing, fillArea.h, determineColor(i / (float) sectionCount));
}

Hud::StringDisplayWidget::StringDisplayWidget()
: Label(), unlitBackgroundDigitsEnabled(), unlitBackgroundDigitCount()
{
	text.setColor(Color::GREEN);
	backgroundColor = Color::WHITE;
	backgroundDisabled = false;
	borderColor = Color::BLACK;
	borderDisabled = false;
	borderThickness = 2.0f;
	padding.x = padding.y = 1;
}

void Hud::StringDisplayWidget::pack(unsigned charCount)
{
	unlitBackgroundDigitCount = charCount;
	text.setContent(string(charCount, '0'));
	Label::pack();
}

void Hud::StringDisplayWidget::draw(const string& valueStr)
{
	text.setContent(valueStr);
	if(unlitBackgroundDigitsEnabled and text.drawCharFunction != null)
	{
		const string bgDigits(unlitBackgroundDigitCount, '8');
		PlainComponent::draw();
		primitives::drawCustomCharacters(text.drawCharFunction, bgDigits.c_str(), unlitBackgroundDigitCount,
				bounds.x + alignmentHorizontalOffset(text.getDrawCharFunctionTextWidth(bgDigits)),
				bounds.y + alignmentVerticalOffset(text.getHeight()),
				text.drawCharFunctionSize,
				(text.drawCharFunctionAspectRatio + text.drawCharFunctionSpacingFactor) * text.drawCharFunctionSize,
				text.getColor().getDarker().getDarker());
		text.draw(bounds.x + alignmentHorizontalOffset(), bounds.y + alignmentVerticalOffset());
	}
	else Label::draw();
}

void Hud::DigitDisplayWidget::draw(unsigned value)
{
	StringDisplayWidget::draw(futil::to_string(value));
}

void Hud::GearIndicatorWidget::draw(int gear)
{
	if(gear > 0) DigitDisplayWidget::draw(static_cast<unsigned>(gear));
	else switch(gear)
	{
		case 0:       StringDisplayWidget::draw("N"); break;
		case -1:      StringDisplayWidget::draw("R"); break;
		case -2:      StringDisplayWidget::draw("P"); break;
		case -3:      StringDisplayWidget::draw("L"); break;
		case -4:      StringDisplayWidget::draw("S"); break;
		case INT_MIN: StringDisplayWidget::draw("D"); break;
		default:      StringDisplayWidget::draw("?"); break;
	}
}

void Hud::TimerDisplayWidget::draw(float timeSeconds)
{
	int timeMs = timeSeconds * 1000;
	int timeSec = timeMs/1000; timeMs -= timeSec*1000;
	int timeMin = timeSec/60; timeSec -= timeMin*60;
	StringDisplayWidget::draw((timeMin < 10?  "0" :  "") + futil::to_string(timeMin)
	                        + (timeSec < 10? ":0" : ":") + futil::to_string(timeSec)
	         + (showMillisec? (timeMs  < 10? ":00"
	                         : timeMs < 100? ":0" : ":") + futil::to_string(timeMs) : ""));
}

void Hud::VehicleDashboard::setup(bool useCacheDialGauges)
{
	const float gaugeSize = 0.2f * bounds.h;

	if(hudType == HUD_TYPE_FULL_DIGITAL)
	{
		backgroundColor = Color::BLACK;
		shape = fgeal::PlainComponent::SHAPE_SLIM_ROUNDED_RECTANGULAR;
		borderColor = Color::create(42, 52, 57);
		barTachometer.backgroundColor = Color::BLACK;
		barTachometer.fillColor = Color::AZURE;
		barTachometer.style = Hud::GenericBarGauge::STYLE_SECTIONED;
		barTachometer.sectionCount = 20;
		numericSpeedometer.backgroundDisabled = true;
		numericSpeedometer.unlitBackgroundDigitsEnabled = true;
		numericSpeedometer.text.setColor(Color::AZURE);
		numericSpeedometer.text.drawCharFunction = primitives::drawSevenSegmentCharacter;
		numericSpeedometer.textHorizontalAlignment = Hud::GearIndicatorWidget::ALIGN_TRAILING;
		numericSpeedometer.borderDisabled = true;
		numericTachometer = numericSpeedometer;
		gearIndicator.backgroundColor = Color::BLACK;
		gearIndicator.unlitBackgroundDigitsEnabled = true;
		gearIndicator.text.setColor(numericSpeedometer.text.getColor());
		gearIndicator.text.drawCharFunction = primitives::drawSevenSegmentCharacter;
		gearIndicator.textHorizontalAlignment = Hud::GearIndicatorWidget::ALIGN_TRAILING;
		gearIndicator.borderDisabled = true;
		speedometerUnitLabel.text.drawCharFunction = primitives::drawSevenSegmentCharacter;
		speedometerUnitLabel.backgroundDisabled = true;
		speedometerUnitLabel.borderDisabled = true;
		speedometerUnitLabel.text.setColor(numericSpeedometer.text.getColor());
		automaticShiftingLabel = speedometerUnitLabel;
		automaticShiftingLabel.backgroundColor = numericSpeedometer.text.getColor();  // color used when manual shifting
		automaticShiftingLabel.text.setContent("AT");

		// sizing and positioning
		Rectangle digitalDashboardRegion = { 0, 0, 1.867f * gaugeSize, 0.85f * gaugeSize };
		if(isCockpitView)
		{
			digitalDashboardRegion.x = bounds.x + (bounds.w - digitalDashboardRegion.w)/2;
			digitalDashboardRegion.y = bounds.y + (bounds.h - digitalDashboardRegion.h)*0.8;
		}
		else
		{
			digitalDashboardRegion.x = bounds.x + 0.985f * bounds.w - digitalDashboardRegion.w;
			digitalDashboardRegion.y = bounds.y + 0.985f * bounds.h - digitalDashboardRegion.h;
			dashboardBackgroundImageRegion = digitalDashboardRegion;  // used to draw digital display bg
		}
		borderThickness = 0.05 * gaugeSize;

		barTachometer.bounds.x = digitalDashboardRegion.x + 0.1 * gaugeSize;
		barTachometer.bounds.y = digitalDashboardRegion.y + 0.55 * gaugeSize;
		barTachometer.bounds.w = (5/3.f) * gaugeSize;
		barTachometer.bounds.h = 0.2 * gaugeSize;
		barTachometer.sectionSpacing = (int) gaugeSize/30;

		numericSpeedometer.text.drawCharFunctionSize = 0.325 * gaugeSize;
		numericSpeedometer.pack(3);
		numericSpeedometer.bounds.x = digitalDashboardRegion.x + 0.18 * gaugeSize;
		numericSpeedometer.bounds.y = digitalDashboardRegion.y + 0.14 * gaugeSize;

		gearIndicator.text.drawCharFunctionSize = numericSpeedometer.text.drawCharFunctionSize/2;
		gearIndicator.pack(1);
		gearIndicator.bounds.x = barTachometer.bounds.x + barTachometer.bounds.w * 0.925f - gearIndicator.bounds.w;
		gearIndicator.bounds.y = numericSpeedometer.bounds.y;

		automaticShiftingLabel.text.drawCharFunctionSize = gearIndicator.text.drawCharFunctionSize/2;
		automaticShiftingLabel.pack();
		automaticShiftingLabel.bounds.x = gearIndicator.bounds.x - automaticShiftingLabel.bounds.w;
		automaticShiftingLabel.bounds.y = gearIndicator.bounds.y;

		speedometerUnitLabel.text.drawCharFunctionSize = gearIndicator.text.drawCharFunctionSize/2;
		speedometerUnitLabel.pack();
		speedometerUnitLabel.bounds.x = numericSpeedometer.bounds.x + numericSpeedometer.bounds.w * 0.95;
		speedometerUnitLabel.bounds.y = numericSpeedometer.bounds.y + numericSpeedometer.bounds.h * 0.95 - speedometerUnitLabel.bounds.h;

		numericTachometer.text.drawCharFunctionSize = numericSpeedometer.text.drawCharFunctionSize * 0.335;
		numericTachometer.pack(5);
		numericTachometer.bounds.x = barTachometer.bounds.x + barTachometer.bounds.w * 0.925f - numericTachometer.bounds.w;
		numericTachometer.bounds.y = barTachometer.bounds.y - 0.175 * gaugeSize;

		// XXX Copied from code below, need to refactor this mess
		if(isCockpitView)
		{
			scaleDashboard.x = scaleDashboard.y = (bounds.w/bounds.h > imgDashboard->getWidth()/(float)imgDashboard->getHeight())? bounds.w/imgDashboard->getWidth() : bounds.h/imgDashboard->getHeight();
			const Point posDashboardOffset = {
					bounds.x + (bounds.w  - imgDashboard->getWidth() * scaleDashboard.x)/2,
					bounds.y + (bounds.h - imgDashboard->getHeight()* scaleDashboard.y)/2
			};
			dashboardBackgroundImagePosition.x = bounds.x;
			dashboardBackgroundImagePosition.y = bounds.y;
			dashboardBackgroundImageRegion.x = -(posDashboardOffset.x - bounds.x)/scaleDashboard.x;
			dashboardBackgroundImageRegion.y = -(posDashboardOffset.y - bounds.y)/scaleDashboard.y;
			dashboardBackgroundImageRegion.w = bounds.w/scaleDashboard.x;
			dashboardBackgroundImageRegion.h = bounds.h/scaleDashboard.y;
			steeringWheelPosition.x = posDashboardOffset.x + 0.5 * imgDashboard->getWidth()  * scaleDashboard.x;
			steeringWheelPosition.y = posDashboardOffset.y + 0.925 * imgDashboard->getHeight() * scaleDashboard.y;
		}

		return;
	}

	// ------------------- set-up gear indicator -------------------
	gearIndicator.text.drawCharFunction = primitives::drawSevenSegmentCharacter;
	gearIndicator.unlitBackgroundDigitsEnabled = true;
	gearIndicator.borderDisabled = false;
	gearIndicator.borderThickness = 0.005f * bounds.h;
	gearIndicator.pack(1);
	gearIndicator.bounds.h = 0.04f * bounds.h;
	gearIndicator.bounds.w = 0.75f * gearIndicator.bounds.h;
	gearIndicator.text.drawCharFunctionSize = gearIndicator.bounds.h/2;
	if(isHudTypeUsingDialTacho())
	{
		gearIndicator.borderColor = Color::LIGHT_GREY;
		gearIndicator.backgroundColor = Color::BLACK;
		gearIndicator.text.setColor(Color::GREEN);
	}
	else
	{
		gearIndicator.borderColor = Color::BLACK;
		gearIndicator.backgroundColor = Color::BLACK;
		gearIndicator.text.setColor(Color::RED);
	}

	// ------------------- set-up tachometer -------------------
	switch(hudType)
	{
		default:
		case HUD_TYPE_DIALGAUGE_TACHO_NUMERIC_SPEEDO:
		case HUD_TYPE_DIALGAUGE_TACHO_AND_SPEEDO:
		{
			dialTachometer.bounds.w = dialTachometer.bounds.h = gaugeSize;
			if(dialTachometer.text.getContent().empty()) dialTachometer.text.setContent("rpm");
			dialTachometer.graduationPrimarySize = 1000.f * static_cast<int>(1+dialTachometer.max/13000.f);
			dialTachometer.graduationPrimaryLineSize = 0.5;
			dialTachometer.graduationValueScale = 0.001;
			dialTachometer.graduationValueOffset = (dialTachometer.graduationPrimarySize > 1000.f? -0.5f * dialTachometer.graduationPrimarySize : 0);
			dialTachometer.graduationSecondarySize = 0.5 * dialTachometer.graduationPrimarySize;
			dialTachometer.graduationSecondaryLineSize = 0.55;
			dialTachometer.graduationTertiarySize = 0.1 * dialTachometer.graduationPrimarySize;
			dialTachometer.graduationTertiaryLineSize = 0.3;
			dialTachometer.borderThickness = 0.005 * bounds.h;
			dialTachometer.boltRadius = 0.025 * bounds.h;
			dialTachometer.needleThickness *= bounds.h;
		}
		break;

		case HUD_TYPE_BAR_TACHO_NUMERIC_SPEEDO:
		{
			barTachometer.bounds.w = 1.5f * gaugeSize;
			barTachometer.bounds.h = gearIndicator.bounds.h;
			barTachometer.borderThickness = gearIndicator.borderThickness/2;
			barTachometer.style = Hud::GenericBarGauge::STYLE_FLAT;
			barTachometer.borderDisabled = false;
			barTachometer.fillColor = Color::RED;
			barTachometer.fillGradientColor = Color::create(192);
			barTachometer.colorGradient = Hud::GenericBarGauge::GRADIENT_CHANGE_PARTIAL_FILL_COLOR_AT_THRESHOLD;
			barTachometer.gradientThreshold = 0.85f;
			barTachometer.backgroundDisabled = true;
		}
		break;

		case HUD_TYPE_SECBAR_TACHO_NUMERIC_SPEEDO:
		{
			barTachometer.bounds.w = 1.5f * gaugeSize;
			barTachometer.bounds.h = gearIndicator.bounds.h;
			barTachometer.borderThickness = gearIndicator.borderThickness/2;
			barTachometer.style = Hud::GenericBarGauge::STYLE_SECTIONED;
			barTachometer.fillColor = Color::CHARTREUSE;
			barTachometer.fillGradientColor = Color::RED;
			barTachometer.colorGradient = Hud::GenericBarGauge::GRADIENT_CHANGE_PARTIAL_FILL_COLOR_AT_THRESHOLD;
			barTachometer.gradientThreshold = 0.85f;
			barTachometer.borderDisabled = true;
			barTachometer.sectionCount = 20;
			barTachometer.sectionSpacing = (1/32.f) * gaugeSize;
			barTachometer.backgroundDisabled = false;
			barTachometer.backgroundColor = Color::BLACK;
		}
		break;
	}

	// ------------------- set-up speedometer -------------------
	numericSpeedometer.textHorizontalAlignment = Hud::GearIndicatorWidget::ALIGN_TRAILING;
	numericSpeedometer.text.isMonospaced = true;
	numericSpeedometer.text.drawCharFunction = primitives::drawSevenSegmentCharacter;
	numericSpeedometer.shape = fgeal::PlainComponent::SHAPE_RECTANGULAR;
	numericSpeedometer.borderColor = isCockpitView? Color::DARK_GREY.getDarker() : Color::LIGHT_GREY;
	numericSpeedometer.backgroundColor = Color::BLACK;

	speedometerUnitLabel.text.setColor(Color::WHITE);
	speedometerUnitLabel.pack();

	automaticShiftingLabel.backgroundDisabled = true;
	automaticShiftingLabel.backgroundColor = Color::GREEN;  // color used when manual shifting
	automaticShiftingLabel.text.setContent("AT");
	automaticShiftingLabel.pack();

	if(isHudTypeUsingDialSpeedo())
	{
		dialSpeedometer.bounds.w = dialSpeedometer.bounds.h = gaugeSize * (isCockpitView? 1.0 : 1.33);
		dialSpeedometer.text.setContent(speedometerUnitLabel.text.getContent());
		dialSpeedometer.graduationPrimarySize = 10 * (static_cast<int>(dialSpeedometer.graduationValueScale * dialSpeedometer.max/120) + 1) / dialSpeedometer.graduationValueScale;
		dialSpeedometer.graduationPrimaryLineSize = 0.25;
		dialSpeedometer.graduationSecondarySize = dialSpeedometer.graduationPrimarySize/2;
		dialSpeedometer.graduationSecondaryLineSize = 0.4;
		dialSpeedometer.graduationValuePositionOffset = 0.375;
		dialSpeedometer.borderThickness = 0.005 * bounds.h;
		dialSpeedometer.boltRadius = 0.025 * bounds.h;
		dialSpeedometer.needleThickness *= bounds.h;

		numericSpeedometer.text.setFont(null);
		numericSpeedometer.unlitBackgroundDigitsEnabled = true;
		numericSpeedometer.borderThickness = gearIndicator.borderThickness;
		numericSpeedometer.padding.y = 0.003 * bounds.h;
		numericSpeedometer.pack(3);
		numericSpeedometer.bounds.h = (isCockpitView? 0.040 : 0.05) * bounds.h;
		numericSpeedometer.bounds.w = (isCockpitView? 1.625 : 1.50) * numericSpeedometer.bounds.h;
		numericSpeedometer.backgroundDisabled = false;
		numericSpeedometer.borderDisabled = false;
		numericSpeedometer.text.setColor(Color::GREEN);
		numericSpeedometer.text.drawCharFunctionSize = numericSpeedometer.bounds.h/2;
	}
	else  // numeric-display-only speedometer
	{
		numericSpeedometer.unlitBackgroundDigitsEnabled = false;
		numericSpeedometer.borderThickness = gearIndicator.borderThickness;
		numericSpeedometer.padding.y = 1;
		numericSpeedometer.pack(3);
		numericSpeedometer.backgroundDisabled = numericSpeedometer.borderDisabled = not isCockpitView;
		numericSpeedometer.text.setColor(isCockpitView? Color::LIGHT_GREY : Color::WHITE);
	}

	// ------------------- set-up hud elements' position -------------------
	Point bgOffset = {0, 0};
	if(isCockpitView)
	{
		scaleDashboard.x = scaleDashboard.y = (bounds.w/bounds.h > imgDashboard->getWidth()/(float)imgDashboard->getHeight())? bounds.w/imgDashboard->getWidth() : bounds.h/imgDashboard->getHeight();
		const Point posDashboardOffset = {
				bounds.x + (bounds.w  - imgDashboard->getWidth() * scaleDashboard.x)/2,
				bounds.y + (bounds.h - imgDashboard->getHeight()* scaleDashboard.y)/2
		};
		dashboardBackgroundImagePosition.x = bounds.x;
		dashboardBackgroundImagePosition.y = bounds.y;
		dashboardBackgroundImageRegion.x = -(posDashboardOffset.x - bounds.x)/scaleDashboard.x;
		dashboardBackgroundImageRegion.y = -(posDashboardOffset.y - bounds.y)/scaleDashboard.y;
		dashboardBackgroundImageRegion.w = bounds.w/scaleDashboard.x;
		dashboardBackgroundImageRegion.h = bounds.h/scaleDashboard.y;
		steeringWheelPosition.x = posDashboardOffset.x + 0.5 * imgDashboard->getWidth()  * scaleDashboard.x;
		steeringWheelPosition.y = posDashboardOffset.y + 0.925 * imgDashboard->getHeight() * scaleDashboard.y;

		bgOffset.x = posDashboardOffset.x;
		bgOffset.y = posDashboardOffset.y;
	}

	switch(hudType)  // TODO this part needs some serious refactoring...
	{
		default:
		case HUD_TYPE_DIALGAUGE_TACHO_NUMERIC_SPEEDO:
		{
			if(isCockpitView)
			{
				dialTachometer.bounds.x = bgOffset.x + 0.5125 * imgDashboard->getWidth()  * scaleDashboard.x;
				dialTachometer.bounds.y = bgOffset.y + 0.6666 * imgDashboard->getHeight() * scaleDashboard.y;
				dialTachometer.compile(useCacheDialGauges);

				numericSpeedometer.bounds.x = dialTachometer.bounds.x - numericSpeedometer.bounds.w - 0.005f * bounds.h;
				numericSpeedometer.bounds.y = dialTachometer.bounds.y + 0.25 * dialTachometer.bounds.h;
				speedometerUnitLabel.bounds.x = numericSpeedometer.bounds.x + numericSpeedometer.bounds.w - speedometerUnitLabel.bounds.w;
				speedometerUnitLabel.bounds.y = numericSpeedometer.bounds.y + numericSpeedometer.bounds.h + 0.001f * bounds.h;

				gearIndicator.bounds.x = dialTachometer.bounds.x + 0.5f * (dialTachometer.bounds.w - gearIndicator.bounds.w);
				gearIndicator.bounds.y = dialTachometer.bounds.y + 0.7f * dialTachometer.bounds.h;
				automaticShiftingLabel.bounds.x = dialTachometer.bounds.x + 0.005f * bounds.h;
				automaticShiftingLabel.bounds.y = dialTachometer.bounds.y + 0.005f * bounds.h;
			}
			else
			{
				dialTachometer.bounds.x = bounds.x + 0.985f * bounds.w - dialTachometer.bounds.w;
				dialTachometer.bounds.y = bounds.y + 0.960f * bounds.h - dialTachometer.bounds.h;
				dialTachometer.compile(useCacheDialGauges);

				numericSpeedometer.bounds.x = dialTachometer.bounds.x - numericSpeedometer.bounds.w;
				numericSpeedometer.bounds.y = dialTachometer.bounds.y + 0.7f * dialTachometer.bounds.h;
				speedometerUnitLabel.bounds.x = numericSpeedometer.bounds.x + numericSpeedometer.bounds.w + 0.005f * bounds.h;
				speedometerUnitLabel.bounds.y = numericSpeedometer.bounds.y + numericSpeedometer.bounds.h - speedometerUnitLabel.bounds.h - 0.002f * bounds.h;

				gearIndicator.bounds.x = dialTachometer.bounds.x + 0.5f * (dialTachometer.bounds.w - gearIndicator.bounds.w);
				gearIndicator.bounds.y = dialTachometer.bounds.y + 0.7f * dialTachometer.bounds.h;
			}
			break;
		}
		case HUD_TYPE_DIALGAUGE_TACHO_AND_SPEEDO:
		{
			if(isCockpitView)
			{
				gearIndicator.bounds.x = bounds.x + (bounds.w - gearIndicator.bounds.w)/2;
				gearIndicator.bounds.y = bounds.y + 0.775f * bounds.h;
				automaticShiftingLabel.bounds.x = gearIndicator.bounds.x + (gearIndicator.bounds.w - automaticShiftingLabel.bounds.w)/2;
				automaticShiftingLabel.bounds.y = gearIndicator.bounds.y - gearIndicator.bounds.h - 0.005*bounds.h;

				dialTachometer.bounds.x = bgOffset.x + 0.375 * imgDashboard->getWidth()  * scaleDashboard.x;
				dialTachometer.bounds.y = bgOffset.y + 0.666 * imgDashboard->getHeight() * scaleDashboard.y;
				dialTachometer.compile(useCacheDialGauges);

				dialSpeedometer.bounds.x = dialTachometer.bounds.x + 0.1375 * imgDashboard->getWidth()  * scaleDashboard.x;
				dialSpeedometer.bounds.y = dialTachometer.bounds.y;
				dialSpeedometer.compile(useCacheDialGauges);

				numericSpeedometer.bounds.x = dialSpeedometer.bounds.x + (dialSpeedometer.bounds.w - numericSpeedometer.bounds.w)/2;
				numericSpeedometer.bounds.y = dialSpeedometer.bounds.y + 0.675f * dialSpeedometer.bounds.h;
			}
			else
			{
				dialTachometer.bounds.x = bounds.x + 0.845f * bounds.w - dialTachometer.bounds.w;
				dialTachometer.bounds.y = bounds.y + 0.990f * bounds.h - dialTachometer.bounds.h;
				dialTachometer.compile(useCacheDialGauges);

				dialSpeedometer.bounds.x = bounds.x + 0.985f * bounds.w - dialSpeedometer.bounds.w;
				dialSpeedometer.bounds.y = bounds.y + 0.850f * bounds.h - dialSpeedometer.bounds.h;
				dialSpeedometer.compile(useCacheDialGauges);

				numericSpeedometer.bounds.x = dialSpeedometer.bounds.x + (dialSpeedometer.bounds.w - numericSpeedometer.bounds.w)/2;
				numericSpeedometer.bounds.y = dialSpeedometer.bounds.y + 0.65f * dialSpeedometer.bounds.h;

				gearIndicator.bounds.x = dialTachometer.bounds.x + 0.5f * (dialTachometer.bounds.w - gearIndicator.bounds.w);
				gearIndicator.bounds.y = dialTachometer.bounds.y + 0.7f * dialTachometer.bounds.h;
			}
			break;
		}
		case HUD_TYPE_BAR_TACHO_NUMERIC_SPEEDO:
		case HUD_TYPE_SECBAR_TACHO_NUMERIC_SPEEDO:
		{
			if(isCockpitView)
			{
				barTachometer.bounds.x = bgOffset.x + 0.4215 * imgDashboard->getWidth()  * scaleDashboard.x;
				barTachometer.bounds.y = bgOffset.y + 0.7750 * imgDashboard->getHeight() * scaleDashboard.y;

				gearIndicator.bounds.x = barTachometer.bounds.x - gearIndicator.bounds.w;
				gearIndicator.bounds.y = barTachometer.bounds.y;

				automaticShiftingLabel.bounds.x = gearIndicator.bounds.x - automaticShiftingLabel.bounds.w - 0.01 * bounds.h;
				automaticShiftingLabel.bounds.y = gearIndicator.bounds.y + (gearIndicator.bounds.h - automaticShiftingLabel.bounds.h)/2;

				numericSpeedometer.bounds.x = gearIndicator.bounds.x + (gearIndicator.bounds.w + barTachometer.bounds.w - numericSpeedometer.bounds.w)/2;
				numericSpeedometer.bounds.y = barTachometer.bounds.y - numericSpeedometer.bounds.h - 0.01f * bounds.h;

				speedometerUnitLabel.bounds.x = numericSpeedometer.bounds.x + numericSpeedometer.bounds.w + 0.005f * bounds.h;
				speedometerUnitLabel.bounds.y = numericSpeedometer.bounds.y + numericSpeedometer.bounds.h - speedometerUnitLabel.bounds.h - 0.005f * bounds.h;
			}
			else
			{
				barTachometer.bounds.x = bounds.x + 0.99f * bounds.w - barTachometer.bounds.w;
				barTachometer.bounds.y = bounds.y + 0.95f * bounds.h;

				speedometerUnitLabel.bounds.x = barTachometer.bounds.x + barTachometer.bounds.w - speedometerUnitLabel.bounds.w;
				numericSpeedometer.bounds.x = speedometerUnitLabel.bounds.x - numericSpeedometer.bounds.w;
				numericSpeedometer.bounds.y = barTachometer.bounds.y - numericSpeedometer.bounds.h - 0.01f * bounds.h;
				speedometerUnitLabel.bounds.y = numericSpeedometer.bounds.y + numericSpeedometer.bounds.h - speedometerUnitLabel.bounds.h - 0.002f * bounds.h;

				gearIndicator.bounds.x = barTachometer.bounds.x - gearIndicator.bounds.w;
				gearIndicator.bounds.y = barTachometer.bounds.y;
			}
			break;
		}
	}
}

void Hud::VehicleDashboard::draw(float vehicleSpeed, float engineRpm, int gear, bool autoShiftFlag, float steeringWheelAngle)
{
	if(isCockpitView and imgDashboard != null)
		imgDashboard->drawScaledRegion(dashboardBackgroundImagePosition, scaleDashboard, fgeal::Image::FLIP_NONE, dashboardBackgroundImageRegion);

	if(not isCockpitView and hudType == HUD_TYPE_FULL_DIGITAL)
	{
		Graphics::drawFilledRoundedRectangle(dashboardBackgroundImageRegion, dashboardBackgroundImageRegion.h * 0.0625f, borderColor);
		Graphics::drawFilledRoundedRectangle(dashboardBackgroundImageRegion.getSpacedOutline(borderDisabled? 0 : -borderThickness), dashboardBackgroundImageRegion.h* 0.0625f, backgroundColor);
	}

	if(isHudTypeUsingDialTacho())
		dialTachometer.draw(engineRpm);

	else if(isHudTypeUsingBarTacho())
		barTachometer.draw(engineRpm);

	gearIndicator.draw(gear);

	if(isHudTypeUsingDialSpeedo())
		dialSpeedometer.draw(vehicleSpeed);
	else
		speedometerUnitLabel.draw();

	numericSpeedometer.draw(vehicleSpeed * dialSpeedometer.graduationValueScale);

	if(hudType == HUD_TYPE_FULL_DIGITAL)
		numericTachometer.draw(engineRpm);

	if(isCockpitView or hudType == HUD_TYPE_FULL_DIGITAL)
	{
		automaticShiftingLabel.text.setColor(autoShiftFlag? automaticShiftingLabel.backgroundColor : automaticShiftingLabel.backgroundColor.getDarker().getDarker().getDarker());
		automaticShiftingLabel.draw();
	}

	if(isCockpitView and imgSteeringWheel != null)
		imgSteeringWheel->drawScaledRotated(steeringWheelPosition, scaleDashboard, steeringWheelAngle, fgeal::Image::IMAGE_CENTER);
}

void Hud::VehicleDashboard::freeResources()
{
	if(dialTachometer.backgroundImage != null)
		delete dialTachometer.backgroundImage, dialTachometer.backgroundImage = null;
	if(dialTachometer.foregroundImage != null)
		delete dialTachometer.foregroundImage, dialTachometer.foregroundImage = null;
	if(dialTachometer.pointerImage != null)
		delete dialTachometer.pointerImage, dialTachometer.pointerImage = null;
	dialTachometer.clearCache();

	if(dialSpeedometer.backgroundImage != null)
		delete dialSpeedometer.backgroundImage, dialSpeedometer.backgroundImage = null;
	if(dialSpeedometer.foregroundImage != null)
		delete dialSpeedometer.foregroundImage, dialSpeedometer.foregroundImage = null;
	if(dialSpeedometer.pointerImage != null)
			delete dialSpeedometer.pointerImage, dialSpeedometer.pointerImage = null;
	dialSpeedometer.clearCache();

	if(imgDashboard != null)
		delete imgDashboard;
	if(imgSteeringWheel != null)
		delete imgSteeringWheel;
}
