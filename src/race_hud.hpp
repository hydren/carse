/*
 * race_hud.hpp
 *
 *  Created on: 26 de abr de 2017
 *      Author: carlosfaruolo
 */

#ifndef RACE_HUD_HPP_
#define RACE_HUD_HPP_
#include <ciso646>

#include "fgeal/fgeal.hpp"
#include "fgeal/extra/gui.hpp"

#include <algorithm>
#include <vector>
#include <map>

namespace Hud
{
	/** A generic dial-type gauge. Can drawn either custom images or native primitives.
	 *  To drawn using custom images, the image's fields must be non-null. Otherwise native versions will be drawn.
	 *  The font used to display graduation values is the one from the drawable text inherited by fgeal::Label.
	 *  The inherited drawable text can be used to draw something on the background, like a label or unit indicator. */
	struct GenericDialGauge extends fgeal::Label
	{
		/** The minimum and maximum angle applied on the rotating pointer. */
		float angleMin, angleMax;

		/** The thickness of the pointing needle (built-in pointer). Note that this only takes effect if line primitives are being used. */
		float needleThickness;

		/** The needle's color (built-in pointer). */
		fgeal::Color needleColor;

		/** Flag that trigger usage of line primitives instead of triangles to draw the pointing needle (built-in pointer). By default it's set true.*/
		bool needleLinePrimitiveInstead;

		/** The radius the needle's "bolt" (built-in pointer). Note that it also affects the needle's thickness if triangle primitives are being used. */
		float boltRadius;

		/** The bolt's color (built-in pointer). */
		fgeal::Color boltColor;

		/** The graduation's color. */
		fgeal::Color graduationColor;

		/** an optional, relative offset, from the center, to the position of graduation values, in the range ]0, 1] */
		float graduationValuePositionOffset;

		/** the (non-zero) size of the line of each graduation level, in the range ]0, 1] */
		float graduationPrimaryLineSize, graduationSecondaryLineSize, graduationTertiaryLineSize;

		/** An optional scaling factor for the graduation values shown. It's better used if set power of 10, like 0.1, 0.01, 0.001, etc. */
		float graduationValueScale;

		/** The level of graduation to show (currently 0 to 3). Setting as 0 makes the gauge to display no graduation at all. */
		short graduationLevel;

		/** An optional vertical offset to the pointer's fixation position on the gauge. */
		float fixationOffset;

		/** An optional offset applied to the pointer in relation to its fixation point. */
		float pointerOffset;

		/** An optional scale factor applied to the pointer's size. Normally (scale=1.0), the pointer size is equal to the gauge radius.*/
		float pointerSizeScale;

		/** Custom gauge background and foreground images. */
		fgeal::Image* backgroundImage, *foregroundImage;

		/**	Custom pointer image. */
		fgeal::Image* pointerImage;

		/** If true, indicates that the pointer image used by this gauge is shared, and thus, should not be deleted when this gauge is deleted. */
		bool pointerImageIsShared;

		/** If true, indicates that the background/foreground images used by this gauge are shared, and thus, should not be deleted when this gauge is deleted. */
		bool imagesAreShared;

		struct Line { float x1, y1, x2, y2; Line(float x1, float y1, float x2, float y2) : x1(x1), y1(y1), x2(x2), y2(y2) {}  };

		struct NumericGraduation { std::string str; float x, y; NumericGraduation(std::string str, float x, float y) : str(str), x(x), y(y) {} };

		private:

		/* A vector of cached coordinates to be drawn. It's necessary to call compile() to update this cache.  */
		std::vector<Line> graduationPrimaryCache, graduationSecondaryCache, graduationTertiaryCache;

		/* A vector of cached numbers and its coordinates to be drawn. It's necessary to call compile() to update this cache. */
		std::vector<NumericGraduation> graduationPrimaryNumericCache;

		/* The scales of the background, foreground and pointer images */
		fgeal::Vector2D backgroundImageScale, foregroundImageScale, pointerImageScale;

		/* A image that serves as a cache for the dial background final result. It's necessary to call compile() to update this cache. If null, the background is drawn on-demand. */
		fgeal::Image* backgroundCacheImage;

		void drawBackground();

		public:

		GenericDialGauge(const fgeal::Rectangle& bounds=fgeal::Rectangle());
		~GenericDialGauge();

		// prepares this dial for drawing, using the given parameters; only needs to be called once before drawing for the first time
		void compile(float min, float max, float graduationPrimarySize, float graduationSecondarySize, float graduationTertiarySize, float graduationValueOffset, bool cacheBackground=false);

		// draws this dial with the pointer at the given angle
		void draw(float angle);

		// clears this dial's drawing cache; after this call, another call to compile() will be needed to draw again; note that this function have no effect if the cache is not being used...
		void clearCache();
	};

	/** A value-typed version of the GenericDialGauge. It holds value parameters except for the main value to measure, which needs to be passed to draw method.
	 *  Note that the type must be able to be cast to float, support arithmetic operations and support comparations. */
	template <typename NumberType>
	struct DialGauge extends GenericDialGauge
	{
		/** the minimum and maximum expected values. */
		NumberType min, max;

		/** the grade's size. */
		NumberType graduationPrimarySize, graduationSecondarySize, graduationTertiarySize;

		/** an optional value that is offset'd from each graduation interval (except the first). */
		NumberType graduationValueOffset;

		public:

		DialGauge(NumberType min=NumberType(), NumberType max=NumberType(), const fgeal::Rectangle& bounds=fgeal::Rectangle())
		: GenericDialGauge(bounds),
		  min(min), max(max),
		  graduationPrimarySize(), graduationSecondarySize(), graduationTertiarySize(), graduationValueOffset(0)
		{
			setRecommendedGraduationSizes();
		}

		void setRecommendedGraduationSizes()
		{
			graduationPrimarySize = 0.1*(max-min);
			graduationSecondarySize = 0.01*(max-min);
			graduationTertiarySize = 0.001*(max-min);
		}

		void compile(bool cacheBackground=false)
		{
			GenericDialGauge::compile(min, max, graduationPrimarySize, graduationSecondarySize, graduationTertiarySize, graduationValueOffset, cacheBackground);
		}

		void draw(const NumberType& value)
		{
			GenericDialGauge::draw(-((angleMax-angleMin)*value + angleMin*max - angleMax*min)/(max-min));
		}
	};

	// TODO improve BarGauge with more options
	/** A generic bar-type gauge */
	struct GenericBarGauge extends fgeal::PlainComponent
	{
		/** the filling bar color. */
		fgeal::Color fillColor, fillGradientColor;

		/** the bar style */
		enum Style
		{
			STYLE_FLAT,  // a flat, colored bar
			STYLE_SECTIONED  // a sectioned bar composed of many individual rectangles
		} style;

		/** the amount of sections of the bar, if applicable (default=8) */
		unsigned sectionCount;

		/** the spacing between bar sections, if applicable (default=8px) */
		float sectionSpacing;

		/** the color gradient behavior, if any */
		enum ColorGradient
		{
			GRADIENT_NONE,  // no gradient, uses fill color only
			GRADIENT_CHANGE_FILL_COLOR_GRADUALLY,  // gradient with fill color and fill gradient color
			GRADIENT_CHANGE_FILL_COLOR_AT_THRESHOLD,  // uses fill color when value is up to threshold fill ratio, otherwise uses fill gradient color
			GRADIENT_CHANGE_PARTIAL_FILL_COLOR_AT_THRESHOLD,  // uses fill color up to threshold fill ratio, then uses fill gradient color
			GRADIENT_TYPE_COUNT
		} colorGradient;

		float gradientThreshold;  // the threshold used by the gradient behaviors

		GenericBarGauge(const fgeal::Rectangle& bounds=fgeal::Rectangle());
		void draw(float fillRatio);

		private:
		fgeal::Color determineColor(float fillRatio);
	};

	/** A value-typed version of the GenericBarGauge class. It holds value parameters except for the main value to measure, which needs to be passed to draw method.
	 *  Note that the type must be able to be cast to float, support arithmetic operations and support comparations */
	template <typename NumberType>
	struct BarGauge extends GenericBarGauge
	{
		/** the minimum and maximum expected values. */
		NumberType min, max;

		BarGauge(NumberType min=NumberType(), NumberType max=NumberType(), const fgeal::Rectangle& bounds=fgeal::Rectangle())
		: GenericBarGauge(bounds),
		  min(min), max(max)
		{}

		void draw(const NumberType& value)
		{
			GenericBarGauge::draw((std::min(value, max)-min)/(max-min));
		}
	};

	/** A widget that displays a text value, possibly stylised. */
	struct StringDisplayWidget extends fgeal::Label
	{
		// if true, darker-colored '8's are drawn in the background using the current drawCharacterFunction (if any); useful for imitating seven-segment display bg
		bool unlitBackgroundDigitsEnabled;

		// number of digits to draw unlit in the background if unlitBackgroundDigitsEnabled is true; it's advised to match this to charCount passed to pack()
		unsigned unlitBackgroundDigitCount;

		StringDisplayWidget();

		/** Draws this widget, with the given string displayed. */
		void draw(const std::string& str);

		/** Sets a suggested size (width, height) to be able to draw the specified ammount of characters within its bounds. */
		void pack(unsigned charCount=1);
	};

	/** A widget that displays a numeric value (composed only of digits, up to 32), possibly stylised. */
	struct DigitDisplayWidget extends StringDisplayWidget
	{
		/** Draws this widget, with the given value displayed. */
		void draw(unsigned value);

		template<typename NumberType>
		void draw(NumberType value)
		{
			this->draw((unsigned) fabs(static_cast<double>(value)));
		}

		inline void packToValue(float maxValue)
		{
			pack(countDigits(maxValue));
		}

		inline static unsigned countDigits(unsigned value)
		{
			unsigned digits = 1, pten=10; while(pten <= value) { digits++; pten *= 10; } return digits;
		}
	};

	/** A value-typed version of the NumericDisplayWidget that allows mapping of specific values to its specific string representations. */
	template <typename NumberType>
	struct SpecialDigitDisplayWidget extends DigitDisplayWidget
	{
		/** Optional mapping of specific values to its string representations. */
		std::map<NumberType, std::string> specialCases;

		void draw(NumberType value)
		{
			if(specialCases.count(value))
				StringDisplayWidget::draw(specialCases[value]);
			else
				DigitDisplayWidget::draw(value);
		}
	};

	/** A widget that displays a time, possibly stylised. */
	struct TimerDisplayWidget extends StringDisplayWidget
	{
		bool showMillisec;

		TimerDisplayWidget() : StringDisplayWidget(), showMillisec(false)
		{
			text.isMonospaced = true;
		}

		/** Draws this widget, with the given time (in seconds) displayed within. The time will be drawn in the format MM:SS:ssss. */
		void draw(float timeSeconds);

		/** Recomputes this widget's bounds to be able to draw the time (with or without milliseconds) within its display. */
		inline void pack() { StringDisplayWidget::pack(showMillisec? 9 : 5); }
	};

	/** A widget intended to display a transmission gear (or state). Valid values are positive integers and a few specific special values.
	 *  Special values: 0 (displays 'N'), -1 (displays 'R'), -2 (displays 'P'), -3 (displays 'L'), -4 (displays 'S', except LCD style), INT_MIN (displays 'D') or '?' otherwise. */
	struct GearIndicatorWidget extends DigitDisplayWidget
	{
		/** Draws this widget, with the given gear indicated. */
		void draw(int gear);
	};

	struct VehicleDashboard extends fgeal::PlainComponent
	{
		DialGauge<float> dialTachometer, dialSpeedometer;
		BarGauge<float> barTachometer;  // TODO add bar speedometer
		DigitDisplayWidget numericTachometer, numericSpeedometer;
		GearIndicatorWidget gearIndicator;

		fgeal::Rectangle dashboardBackgroundImageRegion;
		fgeal::Point dashboardBackgroundImagePosition, steeringWheelPosition;
		fgeal::Vector2D scaleDashboard;

		fgeal::Label speedometerUnitLabel, automaticShiftingLabel;

		fgeal::Image* imgDashboard, *imgSteeringWheel;

		enum Type
		{
			HUD_TYPE_DIALGAUGE_TACHO_NUMERIC_SPEEDO,
			HUD_TYPE_DIALGAUGE_TACHO_AND_SPEEDO,
			HUD_TYPE_BAR_TACHO_NUMERIC_SPEEDO,
			HUD_TYPE_SECBAR_TACHO_NUMERIC_SPEEDO,
			HUD_TYPE_FULL_DIGITAL,

			HUD_TYPE_COUNT  // for counting purposes...
		} hudType;  // TODO rename "HUD type" terminology to "HUD mode"

		bool isCockpitView;

		inline bool isHudTypeUsingDialTacho() const { return hudType == HUD_TYPE_DIALGAUGE_TACHO_NUMERIC_SPEEDO or hudType == HUD_TYPE_DIALGAUGE_TACHO_AND_SPEEDO; }
		inline bool isHudTypeUsingBarTacho() const { return hudType == HUD_TYPE_BAR_TACHO_NUMERIC_SPEEDO or hudType == HUD_TYPE_SECBAR_TACHO_NUMERIC_SPEEDO or hudType == HUD_TYPE_FULL_DIGITAL; }
		inline bool isHudTypeUsingDialSpeedo() const { return hudType == HUD_TYPE_DIALGAUGE_TACHO_AND_SPEEDO; }

		void setup(bool cacheDialGauges=false);
		void draw(float vehicleSpeed, float engineRpm=0, int gear=0, bool autoShiftFlag=false, float steeringWheelAngle=0);
		void freeResources();

		// experimental optional extra - loading settings from file
		void loadFromFile(const std::string& specFilename);
	};
}

#endif /* RACE_HUD_HPP_ */
