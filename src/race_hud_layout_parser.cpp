/*
 * race_hud_layout_parser.cpp
 *
 *  Created on: 6 de fev de 2023
 *      Author: hydren
 */

#include "game_logic.hpp"
#include "futil/properties.hpp"
#include "futil/string_actions.hpp"
#include "futil/string_split.hpp"
#include "util/filename_context.hpp"

using std::string;
using futil::Properties;
using fgeal::Image;
using fgeal::Color;

Color parseColor(const string& str) { return Color::parseCStr(str.c_str()); }

void parseDialGaugeLayout(Hud::DialGauge<float>& dialGauge, const string& specFilename, const string& prefix)
{
	string key;
	Properties prop;
	prop.load(specFilename);
	const string baseDir = specFilename.substr(0, specFilename.find_last_of("/\\")+1);

	if(not dialGauge.imagesAreShared and dialGauge.backgroundImage != null)
		delete dialGauge.backgroundImage;

	const string bgImage = prop.get(prefix + "background", "default");
	if(bgImage != "default" and bgImage != "builtin" and bgImage != "none")
		dialGauge.backgroundImage = new Image(getContextualizedFilename(bgImage, baseDir, GameLogic::DASHBOARDS_FOLDER+"/"));
	else
		dialGauge.backgroundImage = null;

	key = prefix + "dial_graduation_level";
	if(prop.containsKey(key))
		dialGauge.graduationLevel = prop.getParsedCStrAllowDefault<int, atoi>(key, dialGauge.backgroundImage == null? 3 : 0);

	key = prefix + "dial_graduation_color";
	if(prop.containsKey(key))
		dialGauge.graduationColor = prop.getParsedAllowDefault<Color, parseColor>(key, Color::BLACK);
	else
		dialGauge.graduationColor = Color::BLACK;

	key = prefix + "value_min";
	if(prop.containsKey(key))
		dialGauge.min = prop.getParsedCStrAllowDefault<double, atof>(key, dialGauge.min);

	key = prefix + "value_max";
	if(prop.containsKey(key))
		dialGauge.max = prop.getParsedCStrAllowDefault<double, atof>(key, dialGauge.max);

	key = prefix + "dial_angle_min";
	if(prop.containsKey(key))
		dialGauge.angleMin = prop.getParsedCStrAllowDefault<double, atof>(key, 45.0) * M_PI/180.0;
	key = prefix + "dial_angle_max";
	if(prop.containsKey(key))
		dialGauge.angleMax = prop.getParsedCStrAllowDefault<double, atof>(key, 315.0) * M_PI/180.0 ;

	if(not dialGauge.pointerImageIsShared and dialGauge.pointerImage != null)
		delete dialGauge.pointerImage;

	const string pointerType = prop.get(prefix + "dial_pointer", "default");
	if(pointerType == "default" or pointerType == "line")
	{
		dialGauge.pointerImage = null;
		dialGauge.pointerOffset = 0;
		dialGauge.needleLinePrimitiveInstead = true;
		dialGauge.needleThickness = 0;
		dialGauge.needleColor = prop.getParsedAllowDefault<Color, parseColor>(prefix + "dial_pointer_color", Color::RED);
	}
	else if(pointerType == "thick" or pointerType == "thick line")
	{
		dialGauge.pointerImage = null;
		dialGauge.pointerOffset = 0;
		dialGauge.needleLinePrimitiveInstead = true;
		dialGauge.needleThickness = 0.004;
		dialGauge.needleColor = prop.getParsedAllowDefault<Color, parseColor>(prefix + "dial_pointer_color", Color::RED);
	}
	else if(pointerType == "triangle" or pointerType == "triangular needle")
	{
		dialGauge.pointerImage = null;
		dialGauge.pointerOffset = 0;
		dialGauge.needleLinePrimitiveInstead = false;
		dialGauge.needleColor = prop.getParsedAllowDefault<Color, parseColor>(prefix + "dial_pointer_color", Color::RED);
	}
	else
	{
		dialGauge.pointerImage = new Image(getContextualizedFilename(pointerType, baseDir, GameLogic::DASHBOARDS_FOLDER+"/"));

		// TODO revise this field since it is used even in the non-image pointer code
		const string OffsetStr = prop.get(prefix + "dial_pointer_image_offset", "0");
		if(not OffsetStr.empty())
			dialGauge.pointerOffset = atof(futil::trim(futil::split(OffsetStr, ',').back()).c_str());
	}

	const string fixtureStr = prop.get(prefix + "dial_pointer_fixture_offset", "0");
	if(not fixtureStr.empty())
		dialGauge.fixationOffset = atof(futil::trim(futil::split(fixtureStr, ',').back()).c_str());
	else
		dialGauge.fixationOffset = 0;
}

void Hud::VehicleDashboard::loadFromFile(const string& specFilename)
{
	if(specFilename.empty()) return;
	string key;
	Properties prop;
	prop.load(specFilename);

	key = "tachometer_type";
	if(prop.containsKey(key))
	{
		if(prop.get(key) == "dial")
			parseDialGaugeLayout(dialTachometer, specFilename, "tachometer_");
	}

	key = "speedometer_type";
	if(prop.containsKey(key))
	{
		if(prop.get(key) == "dial")
			parseDialGaugeLayout(dialSpeedometer, specFilename, "speedometer_");

		else if(prop.get(key) == "numeric")
		{
			numericSpeedometer.textHorizontalAlignment = Hud::GearIndicatorWidget::ALIGN_TRAILING;
			numericSpeedometer.text.isMonospaced = true;
			numericSpeedometer.text.drawCharFunction = fgeal::primitives::drawSevenSegmentCharacter;
			numericSpeedometer.unlitBackgroundDigitsEnabled = true;
			numericSpeedometer.shape = fgeal::PlainComponent::SHAPE_RECTANGULAR;
			numericSpeedometer.borderColor = Color::LIGHT_GREY;
			numericSpeedometer.backgroundColor = Color::BLACK;
		}
	}

	if(prop.get("tachometer_type") == "dial")
	{
		if(prop.get("speedometer_type") == "dial")
			hudType = Hud::VehicleDashboard::HUD_TYPE_DIALGAUGE_TACHO_AND_SPEEDO;
		else
			hudType = Hud::VehicleDashboard::HUD_TYPE_DIALGAUGE_TACHO_NUMERIC_SPEEDO;
	}
	else if(prop.get("tachometer_type") == "bar")
		hudType = Hud::VehicleDashboard::HUD_TYPE_BAR_TACHO_NUMERIC_SPEEDO;

	else if(prop.get("tachometer_type") == "sectioned bar")
		hudType = Hud::VehicleDashboard::HUD_TYPE_SECBAR_TACHO_NUMERIC_SPEEDO;

	if(prop.get("tachometer_type") == "digital" and prop.get("speedometer_type") == "digital")
		hudType = Hud::VehicleDashboard::HUD_TYPE_FULL_DIGITAL;

	// TODO support more combinations...
}
