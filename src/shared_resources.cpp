/*
 * shared_resources.cpp
 *
 *  Created on: 9 de ago de 2018
 *      Author: carlosfaruolo
 */

#include "carse.hpp"

Carse::SharedResources::SharedResources()
: sndCursorMove("assets/sound/cursor_move.ogg"),
  sndCursorIn("assets/sound/cursor_accept.ogg"),
  sndCursorOut("assets/sound/cursor_out.ogg")
{}
