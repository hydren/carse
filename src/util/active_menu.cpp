/*
 * active_menu.cpp
 *
 *  Created on: 12 de jul de 2023
 *      Author: hydren
 */

#include "active_menu.hpp"

ActiveMenu::~ActiveMenu()
{
	for(unsigned i = 0; i < entries.size(); i++)
		if(entries[i].userData != null)
			delete static_cast<EntryActions*>(entries[i].userData), entries[i].userData=null;
}

void ActiveMenu::addEntry(EntryActions* actions, int index)
{
	Menu::addEntry(std::string(), index, actions);
}

void ActiveMenu::updateLabel(int index, bool alsoUpdateTexts)
{
	if(entries[index].userData != null)
		entries[index].label = static_cast<EntryActions*>(entries[index].userData)->getUpdatedLabel();

	if(alsoUpdateTexts)
		Menu::updateDrawableText();
}

void ActiveMenu::updateLabels(bool alsoUpdateTexts)
{
	for(unsigned i = 0; i < entries.size(); i++)
		updateLabel(i, false);

	if(alsoUpdateTexts)
		Menu::updateDrawableText();
}

void ActiveMenu::triggerSelectedEntry(bool backwards)
{
	if(entries[selectedIndex].enabled and entries[selectedIndex].userData != null)
		static_cast<EntryActions*>(entries[selectedIndex].userData)->onTrigger(backwards);
}
