/*
 * active_menu.hpp
 *
 *  Created on: 12 de jul de 2023
 *      Author: hydren
 */

#ifndef UTIL_ACTION_MENU_HPP_
#define UTIL_ACTION_MENU_HPP_

#include "fgeal/extra/gui.hpp"

#include "entry_actions.hpp"


struct ActiveMenu extends public fgeal::Menu
{
	void* userData;
	ActiveMenu() : Menu(), userData(null) {}
	virtual ~ActiveMenu();
	using Menu::addEntry;
	void addEntry(EntryActions* entryActions, int index=-1);
	void updateLabel(int index, bool alsoUpdateText=true);
	void updateLabels(bool alsoUpdateTexts=true);
	void triggerSelectedEntry(bool backwards=false);
};

#endif /* UTIL_ACTION_MENU_HPP_ */
