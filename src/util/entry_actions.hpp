/*
 * entry_actions.hpp
 *
 *  Created on: 9 de jan de 2021
 *      Author: hydren
 */

#ifndef UTIL_ENTRY_ACTIONS_HPP_
#define UTIL_ENTRY_ACTIONS_HPP_

#include <string>

#include "futil/language.hpp"

struct EntryActions
{
	virtual ~EntryActions(){}
	virtual std::string getUpdatedLabel() abstract;
	virtual void onTrigger(bool backwards=false) abstract;
};

#endif /* UTIL_ENTRY_ACTIONS_HPP_ */
