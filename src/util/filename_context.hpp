/*
 * filename_context.hpp
 *
 *  Created on: 26 de jun de 2020
 *      Author: hydren
 */

#ifndef UTIL_FILENAME_CONTEXT_HPP_
#define UTIL_FILENAME_CONTEXT_HPP_

#include "fgeal/filesystem.hpp"

#include <string>
#include <cstdlib>

/// Returns the user config folder, with corresponds to local app data in the OS
inline std::string getResolvedUserConfigFolder()
{
	#if _WIN32

	if(std::getenv("APPDATA") != NULL)
		return std::string(std::getenv("APPDATA")).append("/carse");
	else
		return std::string(std::getenv("windir")).append("/Application Data/carse");

	#elif __APPLE__

	return std::string(std::getenv("HOME")).append("/Library/Application Support/carse");

	#else  // assume linux

	if(std::getenv("XDG_CONFIG_HOME") != NULL)
		return std::string(std::getenv("XDG_CONFIG_HOME")).append("/carse");
	else
		return std::string(std::getenv("HOME")).append("/.carse");

	#endif
}

/** Attempt to get a contextualized filename.
 *  First it attempts to check if "baseDir1 + specifiedFilename" is a valid file and returns it if true.
 *  If not, then it tries the same with "baseDir2 + specifiedFilename".
 *  If not, then it tries the same with "baseDir3 + specifiedFilename".
 *  If not, then it tries the same with "current working dir. + specifiedFilename".
 *  If not, then it tries the same with "user config dir. + specifiedFilename".
 *  If not, then it tries the same with "specifiedFilename" alone by itself.
 *  If not, then we could not come up with a valid filename and an empty string is returned. */
inline std::string getContextualizedFilename(const std::string& specifiedFilename, const std::string& baseDir1, const std::string& baseDir2, const std::string& baseDir3)
{
	if(fgeal::filesystem::isFilenameArchive(baseDir1 + specifiedFilename))
		return baseDir1 + specifiedFilename;

	if(fgeal::filesystem::isFilenameArchive(baseDir2 + specifiedFilename))
		return baseDir2 + specifiedFilename;

	if(fgeal::filesystem::isFilenameArchive(baseDir3 + specifiedFilename))
		return baseDir3 + specifiedFilename;

	if(fgeal::filesystem::isFilenameArchive(fgeal::filesystem::getCurrentWorkingDirectory() + specifiedFilename))
		return fgeal::filesystem::getCurrentWorkingDirectory() + specifiedFilename;

	if(fgeal::filesystem::isFilenameArchive(getResolvedUserConfigFolder() + "/" + specifiedFilename))
		return getResolvedUserConfigFolder() + "/" + specifiedFilename;

	if(fgeal::filesystem::isFilenameArchive(specifiedFilename))
		return specifiedFilename;

	return std::string();
}

/// Same as the 4-argument version, but with only two "baseDir" option.
inline std::string getContextualizedFilename(const std::string& specifiedFilename, const std::string& baseDir1, const std::string& baseDir2)
{
	return getContextualizedFilename(specifiedFilename, baseDir1, baseDir2, baseDir2);
}

/// Same as the 3-argument version, but with only one "baseDir" option.
inline std::string getContextualizedFilename(const std::string& specifiedFilename, const std::string& baseDir)
{
	return getContextualizedFilename(specifiedFilename, baseDir, baseDir, baseDir);
}

#endif /* UTIL_FILENAME_CONTEXT_HPP_ */
