/*
 * sizing.hpp (formely util.hpp and util.h)
 *
 *  Created on: 6 de dez de 2017
 *      Author: carlosfaruolo
 */

#ifndef UTIL_SIZING_HPP_
#define UTIL_SIZING_HPP_

#define scaledToSize(imgPtr, size) (size).getWidth()/(float)((imgPtr)->getWidth()), (size).getHeight()/(float)((imgPtr)->getHeight())
#define scaledToRect(imgPtr, rect) (rect).w/(float)((imgPtr)->getWidth()), (rect).h/(float)((imgPtr)->getHeight())

// functor to produce a resolution-relative size (size based on a 480px tall display)
struct FontSizer
{
	const unsigned referenceSize;
	FontSizer(unsigned rs) : referenceSize(rs) {}
	inline unsigned operator()(unsigned size) const { return size * referenceSize / 480.f; }
};

template <typename T> inline
int sgn(T val) { return (T(0) < val) - (val < T(0)); }

#endif /* UTIL_SIZING_HPP_ */
