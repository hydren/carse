/*
 * vehicle.cpp
 *
 *  Created on: 6 de abr de 2017
 *      Author: carlosfaruolo
 */

#include "vehicle.hpp"

#include "futil/string_actions.hpp"

#include <stdexcept>
#include <cstdlib>
#include <cmath>

using std::string;
using std::vector;
using fgeal::Vector2D;
using fgeal::Point;
using fgeal::Image;
using fgeal::Sprite;
using fgeal::Sound;

Pseudo3DVehicle::Pseudo3DVehicle()
: body(Engine(), Mechanics::Spec::EXAMPLE_CAR),
  position(), horizontalPosition(), verticalPosition(), verticalPositionFactor(), overscaleFactor(), isDisplayedAsPlayer(), hidden(),
  steeringWheelPosition(),
  strafeSpeed(), verticalSpeed(),
  pseudoAngle(), corneringStiffness(), curvePull(),
  virtualOrientation(), powerBoostIncreaseFactor(), powerBoostDuration(), powerBoostLastActivationTime(0),
  powerBoostCount(), finishedLapsCount(),
  isPowerBoostActive(false), onAir(false), onLongAir(false), justHardLanded(false),
  isTireBurnoutOccurring(false), isCrashing(false),
  engineSound(),
  sndWheelspinBurnoutIntro(null), sndWheelspinBurnoutLoop(null),
  sndSideslipBurnoutIntro(null), sndSideslipBurnoutLoop(null),
  sndOffroadRunningLoop(null),
  sndCrashImpact(null), sndJumpImpact(null),
  sndBoostStart(null), sndBoostLoop(null), sndBoostEnd(null)
{}

Pseudo3DVehicle::~Pseudo3DVehicle()
{
	const vector<Sound*> sounds = getAllSounds();
	const_foreach(Sound*, sound, vector<Sound*>, sounds)
		delete sound;
}

vector<Sound*> Pseudo3DVehicle::getAllSounds()
{
	Sound* soundsArray[] = { sndWheelspinBurnoutIntro, sndWheelspinBurnoutLoop, sndSideslipBurnoutIntro, sndSideslipBurnoutLoop, sndOffroadRunningLoop, sndCrashImpact, sndJumpImpact , sndBoostStart, sndBoostLoop, sndBoostEnd };
	vector<Sound*> soundsVector(sizeof(soundsArray)/sizeof(*soundsArray));
	for(unsigned i = 0; i < soundsVector.size(); i++) soundsVector[i] = soundsArray[i];
	return soundsVector;
}

void Pseudo3DVehicle::setSpec(const Spec& spec, int alternateSpriteIndex)
{
	this->spec = spec.logic;
	body = Mechanics(Engine(spec.engine), spec.body);
	animation.setSpec(alternateSpriteIndex == -1? spec.sprite : spec.alternateSprites[alternateSpriteIndex]);
	engineSound.setProfile(spec.soundProfile, spec.engine.maxRpm);

	// update engine info data (optional) TODO maybe this is not needed anymore...
	if(body.engine.spec.displacement == 0)
		body.engine.spec.displacement = body.spec.vehicleType == Mechanics::TYPE_BIKE? 500 : 3000;
}

#define loadIfNull(snd, filename) if(snd == null) snd = new Sound(filename)

void Pseudo3DVehicle::loadSoundEffectAssets()
{
	loadIfNull(sndWheelspinBurnoutIntro, "assets/sound/tire_burnout_stand1_intro.ogg");
	loadIfNull(sndWheelspinBurnoutIntro, "assets/sound/tire_burnout_stand1_intro.ogg");
	loadIfNull(sndWheelspinBurnoutLoop, "assets/sound/tire_burnout_stand1_loop.ogg");
	loadIfNull(sndSideslipBurnoutIntro, "assets/sound/tire_burnout_normal1_intro.ogg");
	loadIfNull(sndSideslipBurnoutLoop, "assets/sound/tire_burnout_normal1_loop.ogg");
	loadIfNull(sndOffroadRunningLoop, "assets/sound/on_gravel.ogg");
	loadIfNull(sndCrashImpact, "assets/sound/crash.ogg");
	loadIfNull(sndJumpImpact, "assets/sound/landing.ogg");
	loadIfNull(sndBoostStart, "assets/sound/boost-start.ogg");
	loadIfNull(sndBoostLoop, "assets/sound/boost-loop.ogg");
	loadIfNull(sndBoostEnd, "assets/sound/boost-end.ogg");
}

void Pseudo3DVehicle::setSoundVolume(float volume)
{
	const vector<Sound*> sounds = getAllSounds();
	const_foreach(Sound*, sound, vector<Sound*>, sounds)
		sound->setVolume(volume);
	engineSound.setVolume(volume);
}

void Pseudo3DVehicle::haltSounds()
{
	const vector<Sound*> sounds = getAllSounds();
	const_foreach(Sound*, sound, vector<Sound*>, sounds)
		sound->stop();
	engineSound.halt();
}

void Pseudo3DVehicle::loadAssetsData()
{
	animation.loadAssetsData();
	engineSound.loadAssetsData();
	loadSoundEffectAssets();
}

void Pseudo3DVehicle::loadAssetsData(const Pseudo3DVehicle* baseVehicle)
{
	animation.useAssetsDataFrom(baseVehicle->animation);
	engineSound.useAssetsDataFrom(baseVehicle->engineSound);
	loadSoundEffectAssets();
}

void Pseudo3DVehicle::draw(float x, float y, float angle, float distanceScale, float cropYRatio) const
{
	float speedFactor;
	if(spec.frameDurationProportionalToSpeed)
		if(body.wheelAngularSpeed != 0)
			speedFactor = animation.spec.animationSpeedFactor * 2.0*M_PI / (body.wheelAngularSpeed * animation.sprites[animation.getCurrentAnimationIndex(pseudoAngle)]->frameSequence.size());
		else
			speedFactor = -1;
	else
		speedFactor = 1.0f;

	const float cropY = animation.spec.frameHeight * cropYRatio;
	if(hidden or cropY < 1)
		return;

	if(isDisplayedAsPlayer)
	{
		x = screenPosition.x;
		y = screenPosition.y - verticalPosition * verticalPositionFactor;
		distanceScale = overscaleFactor;
	}

	animation.draw(x, y, pseudoAngle + (body.spec.vehicleType == Mechanics::TYPE_CAR? angle : 0), distanceScale, speedFactor, isDisplayedAsPlayer? 0 : cropY);
}

float Pseudo3DVehicle::Spec::getEstimatedQuarterMileTime() const
{
	return 5.825f * std::pow(2.20462f * body.mass / engine.maximumPower, 1/3.f);
}
