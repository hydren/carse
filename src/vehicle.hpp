/*
 * vehicle.hpp
 *
 *  Created on: 29 de mar de 2017
 *      Author: carlosfaruolo
 */

#ifndef PSEUDO3D_VEHICLE_HPP_
#define PSEUDO3D_VEHICLE_HPP_
#include <ciso646>

#include "vehicle_mechanics.hpp"
#include "engine_sound.hpp"
#include "vehicle_gfx.hpp"

#include <string>
#include <vector>

struct Pseudo3DVehicle
{
	struct LogicSpec
	{
		bool frameDurationProportionalToSpeed;  // if true, the duration of each frame is computed according to vehicle speed.
	} spec;

	// physics simulation
	Mechanics body;

	// drawing class
	Pseudo3DVehicleAnimation animation;

	// additional drawing parameters
	float position;  // the z-position (the position along the course).
	float horizontalPosition, verticalPosition;

	// drawing logic
	fgeal::Point screenPosition;  // used for player vehicle perspective
	float verticalPositionFactor, overscaleFactor;  // used for player vehicle perspective
	bool isDisplayedAsPlayer;  // indicates that this should be drawn from a player vehicle perspective
	bool hidden;  // flag to trigger visibility; if true, the draw() call does nothing

	// logic data
	float steeringWheelPosition;  // the steering wheel angle, which varies from -1.0 (fully counter-clockwise), to 0 (neutral) and to 1.0 (fully clockwise)
	float strafeSpeed, verticalSpeed;  // velocities ("forward" speed is in body class)
	float pseudoAngle, corneringStiffness, curvePull;  // "curving" variables
	float virtualOrientation;  // "virtual" vehicle orientation angle in the map (which direction is pointing in map)
	float powerBoostIncreaseFactor, powerBoostDuration, powerBoostLastActivationTime;
	unsigned powerBoostCount;
	unsigned finishedLapsCount;
	bool isPowerBoostActive;
	bool onAir, onLongAir, justHardLanded;
	bool isTireBurnoutOccurring, isCrashing;

	// sound effect
	EngineSoundSimulator engineSound;

	fgeal::Sound *sndWheelspinBurnoutIntro, *sndWheelspinBurnoutLoop,
				 *sndSideslipBurnoutIntro, *sndSideslipBurnoutLoop,
				 *sndOffroadRunningLoop,
				 *sndCrashImpact, *sndJumpImpact,
				 *sndBoostStart, *sndBoostLoop, *sndBoostEnd;

	static const float SPRITE_WORLD_SCALE_FACTOR;

	// the vehicle spec top structure; constains spec for all parts of the vehicles
	struct Spec
	{
		std::string name, authors, credits, comments, brand, filename;  // general information
		std::vector<std::string> categories;  // A list of names of categories which this vehicle can be classified.
		unsigned short year;

		LogicSpec logic;  // game logic vehicle spec
		Engine::Spec engine;  // engine spec
		Mechanics::Spec body;  // body/chassis spec (TODO maybe rename to chassis?)
		Pseudo3DVehicleAnimation::Spec sprite;  // graphics spec
		std::vector<Pseudo3DVehicleAnimation::Spec> alternateSprites;  // additional alternate graphics spec (optional)
		EngineSoundProfile soundProfile;  // sound spec (optional)

		float getEstimatedQuarterMileTime() const;

		/* Loads data from the given filename, parse its vehicle spec data and store in this object. */
		void loadFromFile(const std::string& filename);

		/* Creates a vehicle spec. by loading and parsing the data in the given filename. */
		inline static Spec createFromFile(const std::string& filename) { Spec spec; spec.loadFromFile(filename); return spec; }
	};

	Pseudo3DVehicle();
	~Pseudo3DVehicle();

	/** Sets the attributes of this vehicle according to the specifications given by the Spec argument.
	 *  The optional 'alternateSpriteIndex' argument specifies whether to use the standard sprite or an alternate one (-1 means use standard sprite). */
	void setSpec(const Spec&, int alternateSpriteIndex=-1);

	void loadAssetsData();

	void loadAssetsData(const Pseudo3DVehicle* baseVehicle);

	void setSoundVolume(float volume);

	void haltSounds();

	inline float getAnimationWidth() const { return animation.spec.frameWidth * animation.sprites.back()->scale.x; }
	inline float getAnimationHeight() const { return animation.spec.frameHeight * animation.sprites.back()->scale.y; }

	/** Draws this vehicle at the given position (x, y).
	 *  The 'angle' argument specifies the angle to be depicted.
	 *  The 'distanceScale' specifies how far the vehicle is depicted.
	 *  The 'cropYRatio' parameter specifies how much to crop the sprite vertically (bottom-up) relative to sprite height. */
	void draw(float x, float y, float angle=0, float distanceScale=1.0, float cropYRatio=0) const;

	private:
	void loadSoundEffectAssets();
	std::vector<fgeal::Sound*> getAllSounds();
};

#endif /* PSEUDO3D_VEHICLE_HPP_ */
