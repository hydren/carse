/*
 * vehicle_ai.cpp
 *
 *  Created on: 16 de mar de 2021
 *      Author: hydren
 */

#include "vehicle_ai.hpp"

#include "gamestates/pseudo3d_race_state.hpp"

#include "util/sizing.hpp"

template <typename T> static inline
T pow2(T val) { return val*val; }

Pseudo3DVehicleAI::Pseudo3DVehicleAI(Pseudo3DCourse::Spec& courseSpec, float& coursePositionFactor, bool& raceOnSceneIntro)
: course(courseSpec), coursePositionFactor(coursePositionFactor), raceOnSceneIntro(raceOnSceneIntro) {}

void Pseudo3DVehicleAI::updateVehicleControlAIByPace(Pseudo3DVehicle& vehicle, float delta, float pace)
{
	const float kineticFrictionCoefficient = 0.85, // FIXME get this value properly
				brakingDistance = pow2(vehicle.body.speed)/(2 * kineticFrictionCoefficient * vehicle.body.gravityAccelerationFactor),
				aheadDistance = 0.5f*brakingDistance/vehicle.corneringStiffness,
				stepDistance = 100.f*(course.roadSegmentLength/coursePositionFactor);

	float estimatedMaxCurvePullAhead = 0;
	for(float d = 0; d < aheadDistance; d += stepDistance)
	{
		const unsigned courseSegmnetIndexAhead = static_cast<int>((vehicle.position + d) * coursePositionFactor / course.roadSegmentLength) % course.lines.size();
		const float estimatedCurvePullAhead = pow2(vehicle.body.speed) *  1.25f * course.lines[courseSegmnetIndexAhead].curve / course.roadSegmentLength;
		if(std::abs(estimatedCurvePullAhead) > std::abs(estimatedMaxCurvePullAhead))
			estimatedMaxCurvePullAhead = estimatedCurvePullAhead;
	}

	// update throttle
	if(std::abs(vehicle.horizontalPosition * coursePositionFactor) / course.roadWidth > 1.f)
		vehicle.body.engine.throttlePosition = std::max(vehicle.body.engine.throttlePosition - delta, 0.f);

	else if(std::abs(estimatedMaxCurvePullAhead) > 1.f)
	{
		if(std::abs(estimatedMaxCurvePullAhead) > Pseudo3DRaceState::MAXIMUM_STRAFE_SPEED_FACTOR * vehicle.corneringStiffness)
			vehicle.body.engine.throttlePosition = std::max(vehicle.body.engine.throttlePosition - delta, 0.f);
		else
			vehicle.body.engine.throttlePosition = std::min(vehicle.body.engine.throttlePosition + delta, pace);
	}
	else
		vehicle.body.engine.throttlePosition = not raceOnSceneIntro? pace : 0;

	const float currentCurvePull = vehicle.curvePull;
	vehicle.curvePull = estimatedMaxCurvePullAhead;
	updateBraking(vehicle, delta);  // update braking
	updateSteering(vehicle, delta);  // update steering
	vehicle.curvePull = currentCurvePull;
}

void Pseudo3DVehicleAI::updateVehicleControlAIBySpeed(Pseudo3DVehicle& vehicle, float delta, float targetSpeed, bool doSteering)
{
	// update throttle
	if(raceOnSceneIntro)
		vehicle.body.engine.throttlePosition = 0;

	else if(std::abs(vehicle.horizontalPosition * coursePositionFactor) / course.roadWidth > 1.f
			or (std::abs(vehicle.curvePull) > 1.f and std::abs(vehicle.curvePull) > Pseudo3DRaceState::MAXIMUM_STRAFE_SPEED_FACTOR * vehicle.corneringStiffness)
			or vehicle.body.speed > targetSpeed)
		vehicle.body.engine.throttlePosition = std::max(vehicle.body.engine.throttlePosition - delta, 0.f);
	else
		vehicle.body.engine.throttlePosition = std::min(vehicle.body.engine.throttlePosition + delta, 1.f);

	// update braking
	updateBraking(vehicle, delta);

	// update steering
	if(doSteering)
		updateSteering(vehicle, delta);
}

void Pseudo3DVehicleAI::updateBraking(Pseudo3DVehicle& vehicle, float delta)
{
	if(std::abs(vehicle.horizontalPosition * coursePositionFactor) / course.roadWidth > 1.25f)
		vehicle.body.brakePedalPosition = std::min(vehicle.body.brakePedalPosition + delta, 1.f);

	else if(std::abs(vehicle.curvePull) > Pseudo3DRaceState::MAXIMUM_STRAFE_SPEED_FACTOR * vehicle.corneringStiffness)
		vehicle.body.brakePedalPosition = std::min(vehicle.body.brakePedalPosition + delta, 1.f);

	else
		vehicle.body.brakePedalPosition = std::max(vehicle.body.brakePedalPosition - delta, 0.f);
}

void Pseudo3DVehicleAI::updateSteering(Pseudo3DVehicle& vehicle, float delta)
{
	const float hrpos = (vehicle.horizontalPosition * coursePositionFactor) / course.roadWidth;
	if(hrpos < -0.8f  or (hrpos < -0.2f and vehicle.curvePull != 0 and vehicle.curvePull > vehicle.strafeSpeed))
		vehicle.steeringWheelPosition += (1 - vehicle.steeringWheelPosition) * 10.f * delta;

	else if(hrpos > 0.8f or (hrpos > 0.2f and vehicle.curvePull != 0 and vehicle.curvePull < vehicle.strafeSpeed))
		vehicle.steeringWheelPosition += (-1 - vehicle.steeringWheelPosition) * 10.f * delta;

	else
		vehicle.steeringWheelPosition -= vehicle.steeringWheelPosition * 10.f * delta;
}
