/*
 * vehicle_ai.hpp
 *
 *  Created on: 16 de mar de 2021
 *      Author: hydren
 */

#ifndef VEHICLE_AI_HPP_
#define VEHICLE_AI_HPP_

#include "course.hpp"

class Pseudo3DVehicleAI
{
	Pseudo3DCourse::Spec& course;
	float& coursePositionFactor;
	bool& raceOnSceneIntro;

	public:
	Pseudo3DVehicleAI(Pseudo3DCourse::Spec& courseSpec, float& coursePositionFactor, bool& raceOnSceneIntro);

	void updateVehicleControlAIByPace(Pseudo3DVehicle&, float delta, float pace);
	void updateVehicleControlAIBySpeed(Pseudo3DVehicle&, float delta, float targetSpeed, bool doSteering=true);

	private:
	void updateSteering(Pseudo3DVehicle&, float delta);
	void updateBraking(Pseudo3DVehicle&, float delta);
};

#endif /* VEHICLE_AI_HPP_ */
