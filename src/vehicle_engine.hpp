/*
 * vehicle_engine.hpp
 *
 *  Created on: 29 de mar de 2017
 *      Author: carlosfaruolo
 */

#ifndef VEHICLE_ENGINE_HPP_
#define VEHICLE_ENGINE_HPP_
#include <ciso646>

#include "futil/language.hpp"
#include "futil/properties.hpp"

#include <string>
#include <vector>

struct Engine
{
	/** A class meant to hold data related to the engine's torque curve.
	 * 	Data is held as a vector of line sections that interpolate a data table.
	 * 	There are 2 static methods that work as proper constructors for this class
	 * */
	struct TorqueCurveProfile
	{
		std::vector< std::vector<float> > parameters;  // TODO this could be a map...

		enum PowerBandType {
			POWER_BAND_DEFAULT,       // same as typical
			POWER_BAND_TYPICAL,       // peak power at 91.5% of RPM range / peak torque at 55.8% of RPM range
			POWER_BAND_PEAKY,         // peak power at 94.0% of RPM range / peak torque at 63.4% of RPM range
			POWER_BAND_TORQUEY,       // peak power at 73.2% of RPM range / peak torque at 36.6% of RPM range
			POWER_BAND_SEMI_TORQUEY,  // peak power at 80.9% of RPM range / peak torque at 45.7% of RPM range
			POWER_BAND_WIDE,          // peak power at 97.6% of RPM range / peak torque at 55.1% of RPM range
			POWER_BAND_NARROW,        // peak power at 84.3% of RPM range / peak torque at 54.4% of RPM range
			POWER_BAND_SEMI_PEAKY,    // peak power at 92.7% of RPM range / peak torque at 59.9% of RPM range
			POWER_BAND_EXTRA_TORQUEY, // peak power at 67.5% of RPM range / peak torque at 26.7% of RPM range
			POWER_BAND_EXTRA_PEAKY,   // peak power at 100 % of RPM range / peak torque at 71.0% of RPM range

			//special cases
			POWER_BAND_QUASIFLAT,         // peak power at 100% of RPM range / nearly constant torque from 1000 RPM on
			POWER_BAND_TYPICAL_ELECTRIC,  // peak power at 83.7% of RPM range / peak torque at 1000 RPM
		};

		/** Returns the preficted fraction of the maximum torque output in the given RPM. */
		float getTorqueFactor(float rpm) const;

		/** Returns the RPM which theorectically give the maximum torque of this curve. */
		float getRpmMaxTorque() const;

		/** Creates a torque curve as two linear functions (increasing then decreasing), given the redline RPM, power band type and (optional) RPM of maximum torque.
		 *  If 'rpmMaxPowerPtr' is not null, stores the RPM of maximum power on the variable.
		 *  If 'maxPowerPtr' is not null, stores the maximum normalized power on the variable. */
		static TorqueCurveProfile createAsDualLinear(float maxRpm, PowerBandType powerBandtype, float rpmMaxTorque=-1, float* rpmMaxPowerPtr=null, float* maxNormPowerPtr=null);

		/** Creates a torque curve as a simple quadratic function (downward openning), given the redline RPM and power band type.
		 *  If 'rpmMaxPowerPtr' is not null, stores the RPM of maximum power on the variable.
		 *  If 'maxPowerPtr' is not null, stores the maximum normalized power on the variable. */
		static TorqueCurveProfile createAsSingleQuadratic(float maxRpm, PowerBandType powerBandtype, float* rpmMaxPowerPtr=null, float* maxNormPowerPtr=null);
	};

	struct Spec
	{
		TorqueCurveProfile torqueCurveProfile;  // the torque curve data
		float minRpm, maxRpm;  // workable RPM range
		float maximumTorque;  // the maximum torque output of the engine (TODO comment here the torque unit)
		float disengagedRevSpeedFactor;  // a adjustement factor to engine rev speed when no gear is engaged

		unsigned gearCount;  // the number of gears on the transmission (excluding the reverse gear)
		std::vector<float> gearRatio;  // forward gear ratios
		float reverseGearRatio;  // the transmission's reverse gear ratio
		float differentialRatio;  // the transmission's differential ratio, also known as final drive ratio or axle ratio
		float transmissionEfficiency;  // the mean ratio between torque produced by this engine and effective output after transmission losses, in the range [0.0, 1.0]

		// read-only informative fields!
		std::string configuration, aspiration, valvetrain;
		unsigned displacement, cylinderCount, valveCount, strokeCount;
		float maximumPower, maximumPowerRpm, maximumTorqueRpm;

		Spec(float maxRpm=7000, float maxPower=300, TorqueCurveProfile::PowerBandType powerBand=TorqueCurveProfile::POWER_BAND_TYPICAL, unsigned gearCount=5);
	} spec;

	float rpm, throttlePosition;
	int gear;

	Engine(Spec spec=Spec()) : spec(spec), rpm(), throttlePosition(), gear(1) {}

	/** Reset the engine's state. */
	void reset();

	/** Returns an estimated engine friction torque at given RPM */
	float getFrictionTorqueAt(float rpm) const;

	/** Returns this engine's torque in the current RPM. */
	inline float getCurrentTorque() const { return this->getTorqueAt(this->rpm); }

	/** Returns this engine's torque in the given RPM. */
	float getTorqueAt(float rpm) const;

	/** Returns the current driving force. */
	float getDriveTorque() const;

	/** Returns the current engine angular speed (which derives from the current RPM). */
	float getAngularSpeed() const;

	/** Updates the engine's state (RPM, gear, etc), given the current wheel angular speed. */
	void update(float delta, float wheelAngularSpeed);
};

#endif /* VEHICLE_ENGINE_HPP_ */
