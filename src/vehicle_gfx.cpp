/*
 * vehicle_gfx.cpp
 *
 *  Created on: 16 de set de 2019
 *      Author: carlosfaruolo
 */

#include "vehicle_gfx.hpp"

#include "futil/string_actions.hpp"

#include <stdexcept>
#include <cstdlib>
#include <cmath>

using std::string;
using fgeal::Vector2D;
using fgeal::Point;
using fgeal::Image;
using fgeal::Sprite;

Pseudo3DVehicleAnimation::Pseudo3DVehicleAnimation()
: spec(), sprites(), brakelightSprite(null), shadowSprite(null), smokeSprite(null), backfireSprite(null),
  brakelightsAnimationEnabled(), smokeAnimationEnabled(), backfireAnimationEnabled(), assetsAreShared(false)
{}

Pseudo3DVehicleAnimation::~Pseudo3DVehicleAnimation()
{
	freeAssetsData();
}

void Pseudo3DVehicleAnimation::setSpec(const Spec& s)
{
	freeAssetsData();
	spec = s;
}

void Pseudo3DVehicleAnimation::loadAssetsData()
{
	Image& sheet = *new Image(spec.sheetFilename);

	if(sheet.getWidth() < (int) spec.frameWidth)
		throw std::runtime_error("Invalid sprite width value. Value is smaller than sprite sheet width (no whole sprites could be draw)");

	for(unsigned i = 0; i < spec.stateCount; i++)
	{
		Sprite* sprite = new Sprite(sheet, spec.frameWidth, spec.frameHeight, spec.frameDuration, spec.stateFrameCount[i], 0, i*spec.frameHeight);
		sprite->scale = spec.scale;
		if(i == 0) sprite->imageIsOwned = true;  // make first sprite "own" the sheet
		sprites.push_back(sprite);
	}

	if(spec.asymmetrical) for(unsigned i = 1; i < spec.stateCount; i++)
	{
		Sprite* sprite = new Sprite(sheet, spec.frameWidth, spec.frameHeight, spec.frameDuration, spec.stateFrameCount[i], 0, (spec.stateCount-1 + i)*spec.frameHeight);
		sprite->scale = spec.scale;
		sprites.push_back(sprite);
	}

	const unsigned effectiveStateCount = spec.asymmetrical? 2*spec.stateCount - 1 : spec.stateCount;

	if(not spec.brakelightsSheetFilename.empty())
	{
		Image& brakelightSpriteImage = *new Image(spec.brakelightsSheetFilename);
		if(spec.brakelightsMultipleSprites)
			brakelightSprite = new Sprite(brakelightSpriteImage, brakelightSpriteImage.getWidth(), brakelightSpriteImage.getHeight()/effectiveStateCount, -1, effectiveStateCount, 0, 0);
		else
			brakelightSprite = new Sprite(brakelightSpriteImage, brakelightSpriteImage.getWidth(), brakelightSpriteImage.getHeight(), -1);

		brakelightSprite->scale = spec.brakelightsSpriteScale;
		brakelightSprite->imageIsOwned = true;
	}

	if(not spec.shadowSheetFilename.empty())
	{
		Image& shadowSpriteImage = *new Image(spec.shadowSheetFilename);
		shadowSprite = new Sprite(shadowSpriteImage, shadowSpriteImage.getWidth(), shadowSpriteImage.getHeight()/effectiveStateCount, -1, effectiveStateCount, 0, 0);
		shadowSprite->scale = spec.scale;
		shadowSprite->imageIsOwned = true;
	}
	assetsAreShared = false;
}

void Pseudo3DVehicleAnimation::useAssetsDataFrom(const Pseudo3DVehicleAnimation& anim)
{
	if(spec.sheetFilename != anim.spec.sheetFilename)
		throw std::invalid_argument("Sprite spec. of the passed argument does not match this vehicle's.");

	sprites = anim.sprites;
	brakelightSprite = anim.brakelightSprite;
	shadowSprite = anim.shadowSprite;
	smokeSprite = anim.smokeSprite;
	backfireSprite = anim.backfireSprite;
	assetsAreShared = true;
}

void Pseudo3DVehicleAnimation::freeAssetsData()
{
	if(not assetsAreShared)
	{
		if(not sprites.empty())
			for(unsigned i = 0; i < sprites.size(); i++)
				if(sprites[i] != null)
					{ delete sprites[i]; sprites[i] = null; }

		if(brakelightSprite != null)
			delete brakelightSprite;

		if(shadowSprite != null)
			delete shadowSprite;
	}

	sprites.clear();
	brakelightSprite = shadowSprite = null;
}

unsigned Pseudo3DVehicleAnimation::getCurrentAnimationIndex(float angle) const
{
	unsigned animationIndex = 0;
	if(spec.depictedTurnAngleProximalInterpolation)
	{
		for(unsigned i = 1; i < spec.stateCount; i++)
			if(std::abs(std::abs(angle) - spec.depictedTurnAngle[i]) < std::abs(std::abs(angle) - spec.depictedTurnAngle[animationIndex]))
				animationIndex = i;
	}
	else for(unsigned i = 1; i < spec.stateCount; i++)
		if(std::abs(angle) >= spec.depictedTurnAngle[i])
			animationIndex = i;

	const bool isLeanRight = (angle > 0 and animationIndex != 0);

	// if asymmetrical, right-leaning sprites are after all left-leaning ones
	if(isLeanRight and spec.asymmetrical)
		animationIndex += (spec.stateCount-1);

	return animationIndex;
}

void Pseudo3DVehicleAnimation::draw(float x, float y, float angle, float overscale, float overspeedFactor, float cropY) const
{
	const unsigned animationIndex = getCurrentAnimationIndex(angle);
	Sprite& sprite = *sprites[animationIndex];
	sprite.flipmode = angle > 0 and ((animationIndex != 0 and not spec.asymmetrical) or (spec.stateCount == 1 and spec.asymmetrical))? Image::FLIP_HORIZONTAL : Image::FLIP_NONE;
	sprite.frameDuration = overspeedFactor * spec.animationSpeedFactor;
	sprite.computeCurrentFrame();

	const Vector2D originalSpriteScale = sprite.scale;
	sprite.scale *= overscale;

	const Point vehicleSpritePosition = {
		x - sprite.scale.x * 0.5f * spec.frameWidth,
		y - sprite.scale.y * (spec.frameHeight - spec.contactOffset)
	};

	if(shadowSprite != null)
	{
		const Vector2D originalScale = shadowSprite->scale;
		shadowSprite->scale *= overscale;

		const Point& shadowPosition = spec.shadowPositions[animationIndex];
		shadowSprite->frameSequenceCurrentIndex = animationIndex;
		shadowSprite->flipmode = sprite.flipmode;

		shadowSprite->draw(
			vehicleSpritePosition.x + shadowPosition.x * sprite.scale.x,
			vehicleSpritePosition.y + shadowPosition.y * sprite.scale.y
		);

		shadowSprite->scale = originalScale;
	}

	sprite.croppingArea.h = cropY;
	sprite.draw(vehicleSpritePosition.x, vehicleSpritePosition.y);
	sprite.croppingArea.h = 0;

	if(brakelightsAnimationEnabled and brakelightSprite != null)
	{
		const Vector2D originalScale = brakelightSprite->scale;
		brakelightSprite->scale *= overscale;

		if(spec.brakelightsMultipleSprites)
			brakelightSprite->frameSequenceCurrentIndex = animationIndex;

		const float scaledBrakelightPositionX = spec.brakelightsPositions[animationIndex].x * sprite.scale.x,
					scaledBrakelightPositionY = spec.brakelightsPositions[animationIndex].y * sprite.scale.y,
					scaledBrakelightOffsetX = spec.brakelightsOffset.x * brakelightSprite->scale.x,
					scaledBrakelightOffsetY = spec.brakelightsOffset.y * brakelightSprite->scale.y,
					scaledTurnOffset = (spec.brakelightsPositions[animationIndex].x - spec.brakelightsPositions[0].x)*sprite.scale.x,
					scaledFlipOffset = sprite.flipmode != Image::FLIP_HORIZONTAL? 0 : 2*scaledTurnOffset;

		if(not spec.brakelightsMirrowed)
			brakelightSprite->flipmode = sprite.flipmode;

		brakelightSprite->draw(
			vehicleSpritePosition.x + scaledBrakelightPositionX - scaledFlipOffset + scaledBrakelightOffsetX,
			vehicleSpritePosition.y + scaledBrakelightPositionY + scaledBrakelightOffsetY
		);

		if(spec.brakelightsMirrowed)
		{
			const float scaledFrameWidth = spec.frameWidth*sprite.scale.x;

			brakelightSprite->draw(
				vehicleSpritePosition.x + scaledFrameWidth - scaledBrakelightPositionX - scaledFlipOffset + scaledBrakelightOffsetX + 2*scaledTurnOffset,
				vehicleSpritePosition.y + scaledBrakelightPositionY + scaledBrakelightOffsetY
			);
		}

		brakelightSprite->scale = originalScale;
	}

	if(backfireAnimationEnabled and backfireSprite != null)
	{
		const Vector2D originalScale = backfireSprite->scale;
		backfireSprite->scale *= overscale;

		const float scaledBackfirePositionX = spec.backfirePositions[animationIndex].x * sprite.scale.x,
					scaledBackfirePositionY = spec.backfirePositions[animationIndex].y * sprite.scale.y,
					scaledBackfireOffsetX = spec.backfireOffset.x * backfireSprite->scale.x,
					scaledBackfireOffsetY = spec.backfireOffset.y * backfireSprite->scale.y,
					scaledTurnOffset = (spec.backfirePositions[animationIndex].x - spec.backfirePositions[0].x)*sprite.scale.x,
					scaledFlipOffset = sprite.flipmode != Image::FLIP_HORIZONTAL? 0 : 2*scaledTurnOffset;

		backfireSprite->computeCurrentFrame();
		backfireSprite->flipmode = spec.backfireMirrowed? Image::FLIP_NONE : sprite.flipmode;

		backfireSprite->draw(vehicleSpritePosition.x + scaledBackfirePositionX + scaledBackfireOffsetX - scaledFlipOffset,
							 vehicleSpritePosition.y + scaledBackfirePositionY + scaledBackfireOffsetY);

		if(spec.backfireExtraPositionOffset != Vector2D::NULL_VECTOR)
			backfireSprite->draw(vehicleSpritePosition.x + scaledBackfirePositionX + scaledBackfireOffsetX - scaledFlipOffset + spec.backfireExtraPositionOffset.x * sprite.scale.x,
								 vehicleSpritePosition.y + scaledBackfirePositionY + scaledBackfireOffsetY + spec.backfireExtraPositionOffset.y * sprite.scale.y);

		// right-side backfire (if requested)
		if(spec.backfireMirrowed)
		{
			const float scaledFrameWidth = spec.frameWidth*sprite.scale.x;
			backfireSprite->flipmode = Image::FLIP_HORIZONTAL;
			backfireSprite->draw(vehicleSpritePosition.x + scaledFrameWidth - scaledBackfirePositionX + scaledBackfireOffsetX - scaledFlipOffset + 2*scaledTurnOffset,
								 vehicleSpritePosition.y + scaledBackfirePositionY + scaledBackfireOffsetY);

			if(spec.backfireExtraPositionOffset != Vector2D::NULL_VECTOR)
				backfireSprite->draw(vehicleSpritePosition.x + scaledFrameWidth - scaledBackfirePositionX + scaledBackfireOffsetX - scaledFlipOffset + 2*scaledTurnOffset - spec.backfireExtraPositionOffset.x * sprite.scale.x,
									 vehicleSpritePosition.y + scaledBackfirePositionY + scaledBackfireOffsetY - spec.backfireExtraPositionOffset.y * sprite.scale.y);
		}

		backfireSprite->scale = originalScale;
	}

	if(smokeAnimationEnabled and smokeSprite != null)
	{
		const Vector2D originalScale = smokeSprite->scale;
		smokeSprite->scale *= overscale;

		const float maxDepictedTurnAngle = spec.depictedTurnAngle.size() < 2? 0 : spec.depictedTurnAngle.back();

		const Point smokeSpritePosition = {
				vehicleSpritePosition.x + 0.5f*(sprite.scale.x*(sprite.width - spec.depictedVehicleWidth) - smokeSprite->width*smokeSprite->scale.x)
				+ ((angle > 0? -1.f : 1.f)*10.f*animationIndex*maxDepictedTurnAngle),
				vehicleSpritePosition.y + sprite.height*sprite.scale.y - smokeSprite->height*smokeSprite->scale.y  // should have included ` - sprite.offset*sprite.scale.x`, but don't look good
		};

		// left smoke
		smokeSprite->computeCurrentFrame();
		smokeSprite->flipmode = Image::FLIP_NONE;
		smokeSprite->draw(smokeSpritePosition.x, smokeSpritePosition.y);

		// right smoke
		smokeSprite->computeCurrentFrame();
		smokeSprite->flipmode = Image::FLIP_HORIZONTAL;
		smokeSprite->draw(smokeSpritePosition.x + spec.depictedVehicleWidth*sprite.scale.x, smokeSpritePosition.y);

		smokeSprite->scale = originalScale;
	}

	sprite.scale = originalSpriteScale;
}

