/*
 * vehicle_gfx.hpp
 *
 *  Created on: 7 de ago de 2017
 *      Author: carlosfaruolo
 */

#ifndef VEHICLE_GFX_HPP_
#define VEHICLE_GFX_HPP_
#include <ciso646>

#include <string>
#include <vector>

#include "fgeal/fgeal.hpp"
#include "fgeal/extra/sprite.hpp"

/** A class used to draw pseudo-3D vehicle animations. */
struct Pseudo3DVehicleAnimation
{
	/** A class containing data used to draw pseudo-3D vehicle animations. */
	struct Spec
	{
		// Main vehicle sprites

		/** The filename of the image containing the sprite sheet. */
		std::string sheetFilename;

		/** The amount of states of this animation. */
		unsigned stateCount;

		/** The width of the sprite frame. */
		unsigned frameWidth;

		/** The width of the sprite frame. */
		unsigned frameHeight;

		/** The offset between the sprite's bottom and the depicted contact point of the vehicle (i.e the
		 *  distance between the car tires' bottom and the sprite's bottom). */
		unsigned contactOffset;

		/** The scaling factor of this animation. Applies to all frames. */
		fgeal::Vector2D scale;

		//TODO make it possible to specify frame duration for each state (maybe even for each frame)
		/** The time duration of each frame. Applies to all frames.
		 *  If 'frameDurationProportionalToSpeed' is true, this value is ignored. */
		float frameDuration;

		//TODO make it possible to specify this factor for each state individually (maybe even for each frame)
		/** A scale factor applied to the animation speed. */
		float animationSpeedFactor;

		/** A vector containing the amount of frames of each state. Each index corresponds to each state. */
		std::vector<unsigned> stateFrameCount;

		/** If true, the sprite is not horizontally symmetrical and, therefore, includes right-leaning
		 *  versions of each state (except the first). When the sprite contains only a single state,
		 *  this flag also triggers mirrowing of such sole sprite (which normally is not). */
		bool asymmetrical;

		/** A vector containing the turning angles depicted on the sprite in each frame. Each index
		 *  corresponds to each state. This is used to adjust how quickly the animation will switch states
		 *  depending on the vehicle's pseudo angle. */
		std::vector<float> depictedTurnAngle;

		/** The width of the vehicle as depicted in the sprite (in pixels). This is used to align animation
		 *  effects, such as burning rubber's smoking animation, etc. */
		unsigned depictedVehicleWidth;

		/** If true, the state shown on animation will be the one with the closest angle to the requested
		 *  angle. Otherwise, the state with the highest angle less than requested angle will be shown. */
		bool depictedTurnAngleProximalInterpolation;

		// Brakelights sprites

		/** The filename of the image containing the sprite for the optional brakelights overlay.
		 *  Note that the actual brakelight image portraited in the sheet should be in the same scale as
		 *  the vehicle sprite sheet.*/
		std::string brakelightsSheetFilename;

		/** The scaling factor of the brakelights overlay. Applies to all frames. */
		fgeal::Vector2D brakelightsSpriteScale;

		/** The positions of the brakelights within the animation's coordinates.
		 *  Note that this is a vector because each of the vehicle animation's states may have the
		 *  brakelights located at different coordinates. */
		std::vector<fgeal::Point> brakelightsPositions;

		/** An optional offset applied to the brakelights sprites' positions, meant to center them when
		 *  necessary, i.e. mirrowed sprites with surrounding alpha regions.  */
		fgeal::Vector2D brakelightsOffset;

		/** If true (default), a mirrowed version of the brakeligthts animation is drawn as well.
		 *  The position of the mirrowed brakelights is a mirrowed version of the 'brakelightsPositions',
		 *  minus the animation width. */
		bool brakelightsMirrowed;

		/** If false, it's assumed that there only a single brakelight sprite, and it will be used
		 *  for all animation states. If true, the brakelight animation is assumed instead to have
		 *  multiple sprites, one per each animation state; the brakelight sheet is also assumed to contain
		 *  N equally sized frames, where N is the number of states of this animation. */
		bool brakelightsMultipleSprites;

		// Shadow layer/sprites

		/** The filename of the image containing the sprite for the optional shadow overlay.
		 *  If this field is specified, the shadow sprite will be drawn before the vehicle sprite.
		 *  Note that this sprites should depict the vehicle's shadow as it was being cast from
		 *  top-down position. Also, the shadow image should be in the same scale as the vehicle sprite.
		 *  If this field is assigned 'none' or is left unspecified, it's assumed that there is no shadow
		 *  to draw/cast. If 'default' or 'builtin' is specified, a black, translucent ellipsis is drawn
		 *  below the vehicle, with a width equivalent to 95% of the depicted vehicle width. */
		std::string shadowSheetFilename;

		/** The positions of the shadows within the animation's coordinates.
		 *  Note that this is a vector because each of the vehicle animation's states may have the
		 *  shadow located at different coordinates. By default, (0, 0) coordinates are assumed. */
		std::vector<fgeal::Point> shadowPositions;

		// Backfire exhaust sprites

		/** The positions of the backfires within the animation's coordinates.
		 *  It's suggested to use the position of the exhaust tip in each animation
		 *  Note that this is a vector because each of the vehicle animation's states may have the
		 *  backfire located at different coordinates. */
		std::vector<fgeal::Point> backfirePositions;

		/** If true (default), a mirrowed version of the backfires animation is drawn as well.
		 *  The position of the mirrowed backfires is a mirrowed version of the 'backfirePositions',
		 *  minus the animation width. */
		bool backfireMirrowed;

		/** An optional offset applied to the backfires sprites' positions, meant to center them when
		 *  necessary, i.e. mirrowed sprites with surrounding alpha regions.  */
		fgeal::Vector2D backfireOffset;

		/** An optional offset for the position of an extra backfire effect. If non-zero, then an
		 *  additional backfire effect is displayed, with position equal to the main backfire position,
		 *  plus this offset. If zero, it is ignored. */
		fgeal::Vector2D backfireExtraPositionOffset;

		// TODO support custom backfire sprites
	};

	Pseudo3DVehicleAnimation();
	~Pseudo3DVehicleAnimation();

	Spec spec;

	std::vector<fgeal::Sprite*> sprites;
	fgeal::Sprite* brakelightSprite, *shadowSprite, *smokeSprite, *backfireSprite;

	bool brakelightsAnimationEnabled, /// specifies whether to show brakelights animation, if available.
		 smokeAnimationEnabled,  /// specifies whether to show burnout/slipping animations, if available.
		 backfireAnimationEnabled;  /// specifies whether to show backfire animations, if available.

	/** Draws this vehicle at the given position (x, y).
	 *  The 'depictedAngle' argument specifies the angle to be depicted.
	 *  The 'scale' argument specifies an optional, additional scaling factor over the sprite (cummulative with spec's scale factor).
	 *  The 'speedFactor' argument specifies an optional, additional multiplier to the sprites' frame duration (cummulative with spec's speed factor).
	 *  The 'cropY' argument specifies how much to crop the sprite vertically (bottom-up). */
	void draw(float x, float y, float depictedAngle=0, float scale=1.0, float speedFactor=1.0, float cropY=0) const;

	unsigned getCurrentAnimationIndex(float depictedAngle=0) const;

	/** Loads the graphic assets' data from the filesystem. */
	void loadAssetsData();

	/** Loads the graphic assets' data from the 'anim' argument to use its sprite data in this animation as well.
	 *  Note that the given animation "owns" the resources used by this instance.  */
	void useAssetsDataFrom(const Pseudo3DVehicleAnimation& anim);

	// Disposes of loaded graphics assets.
	void freeAssetsData();

	/** Sets the attributes of this animation according to the specifications given by the Spec argument. */
	void setSpec(const Spec& s);

	private:
	bool assetsAreShared;
};

#endif /* VEHICLE_GFX_HPP_ */
