/*
 * vehicle_mechanics.cpp
 *
 *  Created on: 9 de out de 2017
 *      Author: carlosfaruolo
 */

#include "vehicle_mechanics.hpp"

#include "futil/math_constants.h"

#include <algorithm>

#include <cmath>
#include <cfloat>

// arbitrary multipliers. don't have foundation in physics, but serves as a fine-tuning for the gameplay
#define AIR_DRAG_ARBITRARY_ADJUST 0.8
#define BRAKE_PAD_TORQUE_ARBITRARY_ADJUST 7500
#define ROLLING_RESISTANCE_TORQUE_ARBITRARY_ADJUST 0.25

static const float RAD_TO_RPM = (30/M_PI),  // 60/2pi conversion to RPM
				   AUTO_SHIFTING_MINIMUM_TIME_INTERVAL_TO_SHIFT = 1,  // minimum time interval (in seconds) between shifts observed by automatic shifting
				   AUTO_SHIFTING_MAXIMUM_SLIP_RATIO_TO_SHIFT = 0.10;  // maximum slip ratio for when shifting is still done by automatic shifting

// TODO parametrize this and move to engine spec
static const float MPG_FUEL_CONSUMPTION_FACTOR = (55 * 3.78541)/(3600 * 19);  // fuel consumption rate (in L/s) for a car with 19mpg at 55mph;

const Mechanics::Spec Mechanics::Spec::EXAMPLE_CAR =  { TYPE_CAR,  ENGINE_LOCATION_FRONT, 1250.f, 500.f, 1800.f, 1250.f, 4400.f, 0.3f, 0.2f, 72.f, DRIVE_WHEEL_FRONT, 325.f, 4.1f, 2500.f, 0.5f },  //@suppress("Invalid arguments")
					  Mechanics::Spec::EXAMPLE_BIKE = { TYPE_BIKE, ENGINE_LOCATION_MIDDLE, 200.f, 400.f,  700.f, 1000.f, 2000.f, 0.4f, 0.1f, 20.f, DRIVE_WHEEL_REAR,  300.f, 4.5f, 1500.f, 0.4f };  //@suppress("Invalid arguments")

template <typename T> static inline int sgn(T val) { return (T(0) < val) - (val < T(0)); }
template <typename T> static inline T pow2(T val) { return val*val; }

Mechanics::Mechanics(const Engine& eng, Spec spec)
: spec(spec), engine(eng),
  speed(), acceleration(),
  slopeAngle(), wheelAngularSpeed(),

  tireFrictionFactor(1.0),
  rollingResistanceFactor(0.02),
  gravityAccelerationFactor(9.8),
  airDensityFactor(1.2),

  brakePedalPosition(),
  automaticShiftingEnabled(), automaticShiftingLastTime(),
  fuelAmount(spec.fuelCapacity*0.5f),

  arbitraryForceFactor(1.0),

  allDrivenWheelsInertia(spec.driveWheelInertia * (spec.vehicleType == TYPE_BIKE? 2 : 4) / (spec.driveWheelType == DRIVE_WHEEL_ALL? 1 : 2)),

  rollingResistanceForce(), airDragForce(), brakingForce(), slopePullForce(), downforce(),

  simulationType(SIMULATION_TYPE_WHEEL_LOAD_CAP),
  slipRatio(), differentialSlipRatio()
{}

void Mechanics::reset()
{
	engine.reset();
	speed = acceleration = 0;
	slopeAngle = 0;
	wheelAngularSpeed = 0;
	brakePedalPosition = 0;
	fuelAmount = spec.fuelCapacity*0.5f;
	arbitraryForceFactor = 1.0;
	slipRatio = differentialSlipRatio = 0;

	brakingForce = 0;
	rollingResistanceForce = 0;
	slopePullForce = 0;
	airDragForce = 0;
	downforce = 0;
}

void Mechanics::updatePowertrain(float delta)
{
	const float rpmLoad = (engine.rpm - engine.spec.minRpm)/(engine.spec.maxRpm - engine.spec.minRpm),
//				rpmFuelConsumptionFactor = 1 + 3*(rpmLoad - 1)*rpmLoad;  // quadratic (3x^2 - 3x + 1); image [0.25, 1]
				rpmFuelConsumptionFactor = 1 + ((6.275 - 2.25*rpmLoad)*rpmLoad - 4.025)*rpmLoad;  // cubic (-2.25x^3 + 6.275x^2 - 4.025x + 1); image [0.25, 1]
	fuelAmount -= 4 * rpmFuelConsumptionFactor * MPG_FUEL_CONSUMPTION_FACTOR * delta;
	if(fuelAmount < 0) fuelAmount = 0;

	if(automaticShiftingEnabled and slipRatio < AUTO_SHIFTING_MAXIMUM_SLIP_RATIO_TO_SHIFT
	and automaticShiftingLastTime > AUTO_SHIFTING_MINIMUM_TIME_INTERVAL_TO_SHIFT)
	{
		const int nextGear = engine.gear+1, prevGear = engine.gear-1;
		bool shifted = false, stagedThrottle = false;
		const float throttlePositionBackup = engine.throttlePosition;
		if(engine.throttlePosition < 1.0f)
		{
			engine.throttlePosition = 1.0f;
			stagedThrottle = true;
		}
		if(nextGear-1 < (int) engine.spec.gearCount)
		{
			const float nextGearRpm = wheelAngularSpeed * engine.spec.gearRatio[nextGear-1] * engine.spec.differentialRatio * RAD_TO_RPM;
			if(nextGearRpm > 0)
			{
				const float nextGearDriveTorque = engine.getTorqueAt(nextGearRpm) * engine.spec.gearRatio[nextGear-1] * engine.spec.differentialRatio * engine.spec.transmissionEfficiency;
				if(engine.getDriveTorque() < nextGearDriveTorque)
				{
					if(stagedThrottle)
						engine.throttlePosition = throttlePositionBackup;
					shiftGear(nextGear);
					automaticShiftingLastTime = 0;
					shifted = true;
				}
			}
		}
		if(not shifted and prevGear > 0)
		{
			const float prevGearRpm = wheelAngularSpeed * engine.spec.gearRatio[prevGear-1] * engine.spec.differentialRatio * RAD_TO_RPM;
			if(prevGearRpm < engine.spec.maxRpm)
			{
				const float prevGearDriveTorque = engine.getTorqueAt(prevGearRpm) * engine.spec.gearRatio[prevGear-1] * engine.spec.differentialRatio * engine.spec.transmissionEfficiency;
				if(engine.getDriveTorque() < 0.9f*prevGearDriveTorque)
				{
					if(stagedThrottle)
						engine.throttlePosition = throttlePositionBackup;
					shiftGear(prevGear);
					automaticShiftingLastTime = 0;
					shifted = true;
				}
			}
		}
		if(not shifted and stagedThrottle)
			engine.throttlePosition = throttlePositionBackup;
	}
	automaticShiftingLastTime += delta;

	const float weight = spec.mass * gravityAccelerationFactor,
				airDragFactor = spec.dragArea * airDensityFactor,  // the vehicle's CdA (drag coefficient (Cd) * surface area) multiplied by air density
				downforceFactor = -spec.liftArea * airDensityFactor;  // the vehicle's ClA (lift coefficient (Cl) * surface area) with inverted signal multiplied by air density

	airDragForce = 0.5 * airDragFactor * pow2(speed) * AIR_DRAG_ARBITRARY_ADJUST;
	downforce = 0.5 * downforceFactor * pow2(speed);
	brakingForce = brakePedalPosition * tireFrictionFactor * (weight + downforce) * sgn(speed);  // a multiplier here could be added for stronger and easier braking
	rollingResistanceForce = rollingResistanceFactor * (weight + downforce) * sgn(speed);  // rolling friction is independant on wheel count since the weight will be divided between them
	slopePullForce = weight * sin(slopeAngle);

	// update drivetrain
	if(simulationType == SIMULATION_TYPE_PACEJKA_BASED)
		updateByPacejkaScheme(delta);
	else
		updateBySimplifiedScheme(delta);

	// compute total net force
	float totalForce = (
		// drive force is ready to be get AFTER updating drivetrain
		arbitraryForceFactor*getDriveForce()

		// discounts for slope gravity pull and air drag friction
		- (slopePullForce + airDragForce)

		// pacejka scheme already accounts for braking force and rolling resistance force
		- (simulationType != SIMULATION_TYPE_PACEJKA_BASED? (brakingForce + rollingResistanceForce) : 0)
	);

	// update acceleration
	acceleration = totalForce/spec.mass;  // divide forces by mass

	// update speed
	speed += delta*acceleration;
}

void Mechanics::shiftGear(int gear)
{
	if(gear < 0 or gear > (int) engine.spec.gearCount)
		return;

	if(gear != 0 and engine.gear != 0)
	{
		const float driveshaftRpm = wheelAngularSpeed
									* engine.spec.gearRatio[gear-1]
									* engine.spec.differentialRatio * RAD_TO_RPM;

		// add a portion of the discrepancy between the driveshaft RPM and the engine RPM (simulate losses due to shift time)
		engine.rpm += 0.25*(driveshaftRpm - engine.rpm);
	}
	else if(engine.gear == 0)
	{
		const float engineAngularSpeed = engine.rpm
										/(engine.spec.gearRatio[gear-1]
										* engine.spec.differentialRatio * RAD_TO_RPM);

		wheelAngularSpeed += (engineAngularSpeed - wheelAngularSpeed);
	}

	engine.gear = gear;
}

float Mechanics::getDriveForce() const
{
	switch(simulationType)
	{
		default:
		case SIMULATION_TYPE_SLIPLESS:			return getDriveForceBySimplifiedSchemeSlipless();
		case SIMULATION_TYPE_WHEEL_LOAD_CAP:	return getDriveForceBySimplifiedSchemeFakeSlip();
		case SIMULATION_TYPE_PACEJKA_BASED:		return getDriveForceByPacejkaScheme();
	}
}

float Mechanics::getDrivenWheelsWeightLoad() const
{
	const float transferedWeightLoad = spec.mass * acceleration * (spec.centerOfGravityHeight/spec.wheelbase);
	const float weightLoad = spec.mass * gravityAccelerationFactor + downforce;

	// TODO account for wheel weight load transfer even when AWD
	// this causes inaccurate behavior because weight transfer induces different slip ratios in the front and rear tires' which do not cancel out
	if(spec.driveWheelType == DRIVE_WHEEL_ALL)
		return weightLoad;

	if(spec.driveWheelType == DRIVE_WHEEL_REAR)
		return spec.weightDistribution*weightLoad + transferedWeightLoad;

	if(spec.driveWheelType == DRIVE_WHEEL_FRONT)
		return (1-spec.weightDistribution)*weightLoad - transferedWeightLoad;

	// the execution should not get to this point
	return weightLoad;
}

float Mechanics::getMaximumWheelAngularSpeed() const
{
	return engine.spec.maxRpm/(engine.spec.gearRatio[engine.spec.gearCount-1] * engine.spec.differentialRatio * RAD_TO_RPM);
}

float Mechanics::getEstimatedTopSpeed() const
{
	return std::min(getMaximumWheelAngularSpeed() * spec.driveWheelRadius,  // geartrain-limited top speed
					std::pow(2*engine.spec.maximumPower*745.699f/(airDensityFactor*spec.dragArea), 1/3.f));  // drag-limited top speed (XXX this formula ignores rolling resistance)
}

// ------------------------------------------------------------------------------------------------
// ---- SIMPLIFIED SCHEME -------------------------------------------------------------------------

void Mechanics::updateBySimplifiedScheme(float delta)
{
	// this formula assumes no wheel slipping.
	wheelAngularSpeed = speed / spec.driveWheelRadius;  // set new wheel angular speed
	engine.update(delta, wheelAngularSpeed);
}

float Mechanics::getDriveForceBySimplifiedSchemeSlipless() const
{
	// all engine power
	return engine.getDriveTorque() / spec.driveWheelRadius;
}

float Mechanics::getDriveForceBySimplifiedSchemeFakeSlip() const
{
	// all engine power, up to the maximum allowed by tire grip (driven wheels load)
	return std::min(engine.getDriveTorque() / spec.driveWheelRadius, getDrivenWheelsWeightLoad() * tireFrictionFactor);
}

// ------------------------------------------------------------------------------------------------
// ----- PACEJKA SCHEME ---------------------------------------------------------------------------

/*
 * More info about car physics
 * http://www.asawicki.info/Mirror/Car%20Physics%20for%20Games/Car%20Physics%20for%20Games.html
 * http://vehiclephysics.com/advanced/misc-topics-explained/#tire-friction
 * https://en.wikipedia.org/wiki/Friction#Coefficient_of_friction
 * https://en.wikipedia.org/wiki/Downforce
 * http://www3.wolframalpha.com/input/?i=r(v,+w)+%3D+(w+-+v)%2Fv
 * http://www.edy.es/dev/2011/12/facts-and-myths-on-the-pacejka-curves/
 * http://white-smoke.wikifoundry.com/page/Tyre+curve+fitting+and+validation
 * http://web.archive.org/web/20050308061534/home.planet.nl/~monstrous/tutstab.html
 * http://hpwizard.com/aerodynamics.html
 * http://www.vespalabs.org/projects/vespa-cfd-3d-model/openfoam-motorbike-tutorial
*/

void Mechanics::updateByPacejkaScheme(float delta)
{
	updateSlipRatio(delta);

	const float tractionForce = getNormalizedTractionForce() * tireFrictionFactor * getDrivenWheelsWeightLoad();
	const float tractionTorque = tractionForce * spec.driveWheelRadius;

	// TODO braking torque calculation should have a brake pad slip ratio on its on
	const float brakingTorque = brakePedalPosition * sgn(wheelAngularSpeed) * BRAKE_PAD_TORQUE_ARBITRARY_ADJUST;
	const float rollingResistanceTorque = rollingResistanceForce * spec.driveWheelRadius * ROLLING_RESISTANCE_TORQUE_ARBITRARY_ADJUST;

	const float totalTorque = engine.getDriveTorque() - tractionTorque - brakingTorque - rollingResistanceTorque;

	const float wheelAngularAcceleration = totalTorque / allDrivenWheelsInertia;  // XXX we're assuming no inertia from the engine components.

	wheelAngularSpeed += delta * wheelAngularAcceleration;  // update wheel angular speed
	engine.update(delta, wheelAngularSpeed);  // updated engine RPM based on the wheel angular speed
}

float Mechanics::getDriveForceByPacejkaScheme() const
{
	return getNormalizedTractionForce() * getDrivenWheelsWeightLoad() * tireFrictionFactor;
}

void Mechanics::updateSlipRatio(float delta)
{
	// slip ratio computation don't work properly on low speeds due to numerical instability when dividing by values closer and closer to zero
	// in an attempt to attenuate the issue, we follow an approach suggested by Bernard and Clover in [SAE950311]
	static const double B_CONSTANT = 0.91,     // constant
						TAU_CONSTANT = 0.02,   // oscillation period (experimental)
						LOWEST_STABLE_SPEED = 5.0f;

	// approach suggested by Bernard and Clover in [SAE950311].
	const double deltaRatio = (((double) wheelAngularSpeed * (double) spec.driveWheelRadius - (double) speed) - fabs((double) speed) * differentialSlipRatio) / B_CONSTANT;
	differentialSlipRatio += deltaRatio * delta;

	// The differential equation tends to oscillate at low speeds.
	// To counter this, use a derived value in the force equations.
	//
	//   SR = SR + Tau * d SR/dt, where Tau is close to the oscillation period
	//
	// (Thanks to Gregor Veble in rec.autos.simulators)
	slipRatio = differentialSlipRatio + (speed > LOWEST_STABLE_SPEED? 0 : TAU_CONSTANT * deltaRatio);
}

float Mechanics::getNormalizedTractionForce() const
{
	// approximation/simplification based on a simplified Pacejka's formula from Marco Monster's website "Car Physics for Games".
	return slipRatio < 0.06? (20.0*slipRatio)  // 0 to 6% slip ratio gives traction from 0 up to 120%
		 : slipRatio < 0.20? (9.0 - 10.0*slipRatio)/7.0  // 6 to 20% slip ratio gives traction from 120% up to 100%
		 : slipRatio < 1.00? (1.075 - 0.375*slipRatio)  // 20% to 100% slip ratio gives traction from 100 down to 70%
				 	 	   : 0.7;  // over 100% slip ratio gives traction 70%
}
