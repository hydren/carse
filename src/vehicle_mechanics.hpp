/*
 * vehicle_mechanics.hpp
 *
 *  Created on: 9 de out de 2017
 *      Author: carlosfaruolo
 */

#ifndef VEHICLE_MECHANICS_HPP_
#define VEHICLE_MECHANICS_HPP_
#include <ciso646>

#include "vehicle_engine.hpp"

/** Class that performs simulations of vehicle powertrain physics.
 *
 * Limitations:
 *
 * - Doesn't do turning physics.
 * - Supports only some vehicle setups, currently only with wheels divided in 2 clusters: front wheel(s) and rear wheel(s). (transversally bipartite wheels)
 *   Example: cars, bikes, trikes. A 6-wheeled vehicle, for example, is not supported.
 *
 *   TODO support more types of vehicles (jetskis, motorboats, hovercrafts, hovercars, trikes, etc)
 *  */
struct Mechanics
{
	enum VehicleType { TYPE_CAR, TYPE_BIKE, TYPE_OTHER };
	enum DriveWheelType { DRIVE_WHEEL_ALL, DRIVE_WHEEL_FRONT, DRIVE_WHEEL_REAR };  // non-wheeled vehicles leave unspecified (which is 0, same as ALL)
	enum EngineLocation { ENGINE_LOCATION_FRONT, ENGINE_LOCATION_MIDDLE, ENGINE_LOCATION_REAR };

	struct Spec
	{
		VehicleType vehicleType;  // the type of vehicle
		EngineLocation engineLocation;  // the approximate engine location
		float mass;  // the vehicle's mass, in Kg
		float centerOfGravityHeight;  // the height of the vehicle's center of gravity. it can be aproximated as half the height, if it's available; otherwise hardcode it.
		float width, height, length;  // in mm
		float dragArea;  // the product of the vehicle's drag coefficient (Cd) and its reference area (frontal, cross-sectional area);  dimensionless, by the way.
		float liftArea;  // the product of the vehicle's lift coefficient (Cl) and its reference area (frontal, cross-sectional area);  dimensionless, by the way.
		float fuelCapacity;  // the maximum amount of fuel possible to store, in L

		// values specific for wheeled vehicles
		DriveWheelType driveWheelType;  // specified whether this vehicle is a front-wheel-drive, rear-wheel-drive or a all-wheel-drive
		float driveWheelRadius;  // the radius of the driven wheels, in mm
		float driveWheelInertia;  // the moment of inertia for each of the driven wheels (individually), in kg/m^2; it's implied that the value is the same for each driven wheel
		float wheelbase;  // the distance between the centers of the front and rear wheels.
		float weightDistribution;  // the static weight distribution of the rear wheels; the front wheels distribution are, implicitly, 1 minus this value.

		static const Spec EXAMPLE_CAR, EXAMPLE_BIKE;
	} spec;

	Engine engine;

	float speed, acceleration;  // in m/s and m/s^2
	float slopeAngle;  // the angle of the current slope which the vehicle is above.
	float wheelAngularSpeed;  // in radians per second

	float tireFrictionFactor,         // tire friction coefficient
		  rollingResistanceFactor,    // rolling resistance or rolling friction coefficient (Crr) of the tires
		  gravityAccelerationFactor,  // current gravity acceleration, in m/s^2
		  airDensityFactor;           // ambient air density, in kg/m^3

	float brakePedalPosition;  // the brake pedal position, which varies from 0 (totally unpressed) to 1.0 (fully pressed)

	bool automaticShiftingEnabled;
	float automaticShiftingLastTime;

	float fuelAmount;  // current amount of fuel, in L

	float arbitraryForceFactor;  // an arbitrary force factor applyied to the net drive force when computing acceleration (default is 1.0)

	// pre-computed variable, used by simulation but only calculated once, during this instance creation (in other words, needs to be re-computed if spec is changed)
	float allDrivenWheelsInertia;  // the sum of the moments of inertia of all driven wheels

	// information variables, used only to store values meant to inform or debug
	float rollingResistanceForce, airDragForce, brakingForce, slopePullForce, downforce;  // in newtons

	// types of wheel simulation
	enum SimulationType
	{
		SIMULATION_TYPE_SLIPLESS,  // no wheel slip
		SIMULATION_TYPE_WHEEL_LOAD_CAP,  // no wheel slip but engine output is limited by the drive wheels weight load
		SIMULATION_TYPE_PACEJKA_BASED,  // longitudinal slip ratio simulation, Pacejka friction, scheme based on suggestions from SAE950311 and Gregor Veble

		SIMULATION_TYPE_COUNT  // for counting purposes...
	}
	simulationType;

	Mechanics(const Engine& engine, Spec);

	/** Resets the powertrain state to idle. */
	void reset();

	/** Updates the powertrain, given the time step. */
	void updatePowertrain(float timeStep);

	void shiftGear(int gear);

	/** Returns the current resulting force coming from the powertrain that accelerates the car. */
	float getDriveForce() const;

	/** Returns the current total weight load on the driven wheels. */
	float getDrivenWheelsWeightLoad() const;

	/** Returns the maximum angular speed of the driven wheels (in radians) as allowed by this vehicle's geartrain. */
	float getMaximumWheelAngularSpeed() const;

	/** Returns an estimation of this vehicle's top speed (in m/s) */
	float getEstimatedTopSpeed() const;

	private:

	// Simplified scheme-related
	void updateBySimplifiedScheme(float);
	float getDriveForceBySimplifiedSchemeSlipless() const;
	float getDriveForceBySimplifiedSchemeFakeSlip() const;

	public:  // make these public for debugging when using pacejka scheme

	// Pacejka scheme-related
	void updateByPacejkaScheme(float);
	float getDriveForceByPacejkaScheme() const;
	float getNormalizedTractionForce() const;

	// these refer to the longitudinal slip ratio
	double slipRatio, differentialSlipRatio;
	void updateSlipRatio(float delta);
};

#endif /* VEHICLE_MECHANICS_HPP_ */
