/*
 * vehicle_spec_parser.cpp
 *
 *  Created on: 3 de dez de 2018
 *      Author: carlosfaruolo
 */

#include "vehicle.hpp"

#include "game_logic.hpp"

#include "util/filename_context.hpp"

#include "futil/properties.hpp"
#include "futil/string_actions.hpp"
#include "futil/string_split.hpp"
#include "futil/string_parse.hpp"
#include "futil/round.h"

#include <exception>
#include <cstdlib>
#include <cmath>

#include <iostream>
using std::cout;
using std::endl;

using std::vector;
using std::string;
using futil::contains;
using futil::Properties;

// to reduce typing is good
#define isValueSpecified(prop, key) (prop.containsKey(key) and prop.get(key) != "default")
#define isValidValue(value) (not value.empty() and value != "default")

template <typename T> static inline T pow2(T val) { return val*val; }
template <typename T> static inline T pow4(T val) { T val2 = val*val; return val2*val2; }

// fwd declare
static void loadPowertrainSpec(Pseudo3DVehicle::Spec&, const Properties&);
static void loadChassisSpec(Pseudo3DVehicle::Spec&, const Properties&);
static void loadAnimationSpec(Pseudo3DVehicleAnimation::Spec&, const Properties&, const Mechanics::Spec&);

// ========================================================================================================================

// vehicle spec default constants
static const float
	DEFAULT_VEHICLE_MASS_CAR  = 1250,  // kg
	DEFAULT_VEHICLE_MASS_BIKE = 200,   // kg

	DEFAULT_CD_CAR  = 0.31,  // drag coefficient (Cd) of a 300ZX Z32
	DEFAULT_CD_BIKE = 0.60,  // estimated drag coefficient (Cd) of a common sporty bike
	DEFAULT_CL_CAR  = 0.20,  // estimated lift coefficient (Cl) of a 300ZX Z32
	DEFAULT_CL_BIKE = 0.10,  // estimated lift coefficient (Cl) of a common sporty bike

	DEFAULT_FRONTAL_AREA_CAR  = 1.81,  // frontal area (in square-meters) of a 300ZX Z32
	DEFAULT_FRONTAL_AREA_BIKE = 0.70,  // estimated frontal area (in square-meters) of a common sporty bike

	DEFAULT_FRONTAL_AREA_RATIO_CAR = 0.8,  // ratio of frontal area and cross-section bounding box area for common sports cars
	DEFAULT_FRONTAL_AREA_RATIO_BIKE = 0.5,  // ratio of frontal area and cross-section bounding box area for common sports bikes

	DEFAULT_FR_WEIGHT_DISTRIBUTION = 0.45,
	DEFAULT_MR_WEIGHT_DISTRIBUTION = 0.55,
	DEFAULT_RR_WEIGHT_DISTRIBUTION = 0.65,
	DEFAULT_FF_WEIGHT_DISTRIBUTION = 0.40,
	DEFAULT_MA_WEIGHT_DISTRIBUTION = 0.565,

	DEFAULT_MAXIMUM_RPM_CAR = 7000,
	DEFAULT_MAXIMUM_POWER_CAR = 320,  // bhp
	DEFAULT_MAXIMUM_RPM_BIKE = 10000,
	DEFAULT_MAXIMUM_POWER_BIKE = 100,  // bhp
	DEFAULT_WHEEL_RADIUS = 325,  // mm (equivalent to 25.6 inch (650mm) diameter wheels, or 245/50 R16 or 135/90 R16 tires)
	DEFAULT_WHEEL_WIDTH = 245,  // mm (corresponding to a 245/50 R16 tire)

	// for the time being, assume 70% efficiency for cars and 80% for bikes
	DEFAULT_TRANSMISSION_EFFICIENCY_CAR =  0.7,
	DEFAULT_TRANSMISSION_EFFICIENCY_BIKE = 0.8;

static const unsigned DEFAULT_GEAR_COUNT = 5;

const float Pseudo3DVehicle::SPRITE_WORLD_SCALE_FACTOR = 24.0/895.0;

void Pseudo3DVehicle::Spec::loadFromFile(const string& filename)
{
	Properties prop;
	prop.load(filename);
	prop.eraseEmptyEntries(true);

	if(not prop.containsKey("definition") or prop.get("definition") != "vehicle")
		throw std::logic_error("Could not load vehicle spec from file due to missing key values");

	this->filename = filename;

	// aux. var
	string value;

	// info data
	name = prop.get("vehicle_name", name);
	authors = prop.get("authors", authors);
	credits = prop.get("credits", credits);
	comments = prop.get("comments", comments);
	brand = prop.get("vehicle_brand", (name == "Default Car" or name == "Default Bike")? "Default" : brand);
	year = prop.getParsedCStr<int, atoi>("vehicle_year");

	value = prop.get("vehicle_category");
	if(not value.empty())
	{
		futil::split(categories, value, ',');
		foreach(string&, s, vector<string>, categories)
			s = futil::trim(s);
	}

	// TODO read more data from properties

	// physics data
	loadChassisSpec(*this, prop);
	loadPowertrainSpec(*this, prop);

	// sound data
	value = prop.get("sound");
	if(value == "custom")
		soundProfile = GameLogic::createEngineSoundProfileFromFile(filename);  // read sound data if a custom one is specified
	else if(value != "no")
		soundProfile = GameLogic::getInstance().getPresetEngineSoundProfile(value);

	// sprite data
	prop.put("filename", filename);  // done so we can later get properties filename
	prop.put("base_dir", filename.substr(0, filename.find_last_of("/\\")+1));  // done so we can later get properties base dir
	loadAnimationSpec(sprite, prop, body);

	// attempt to read up to 32 alternative sprites
	for(unsigned i = 0; i < 32; i++)
	{
		value = prop.get("alternate_sprite_sheet" + futil::to_string(i) + "_definition_file");
		if(not value.empty() and value != "default")
		{
			const string alternateSpritePropFilename = getContextualizedFilename(value, prop.get("base_dir"), GameLogic::VEHICLES_FOLDER+"/");
			if(alternateSpritePropFilename.empty())
			{
				cout << "warning: alternate sprite sheet " << i << " definition file could not be found!"
				<< " (specified by \"" << filename << "\")" << endl;
				continue;
			}

			Properties alternateSpriteProp;
			alternateSpriteProp.load(alternateSpritePropFilename);

			// load some additional properties from main properties file
			alternateSpriteProp["vehicle_width"] = prop.get("vehicle_width");
			alternateSpriteProp["vehicle_height"] = prop.get("vehicle_height");
			alternateSpriteProp["vehicle_width_height_ratio"] = prop.get("vehicle_width_height_ratio");
			alternateSpriteProp.put("filename", alternateSpritePropFilename);
			alternateSpriteProp.put("base_dir", prop.get("base_dir"));

			alternateSprites.push_back(Pseudo3DVehicleAnimation::Spec());
			loadAnimationSpec(alternateSprites.back(), alternateSpriteProp, body);
		}
		else
		{
			const string altSheetFile = prop.get("alternate_sprite_sheet" + futil::to_string(i) + "_file");
			if(not altSheetFile.empty() and altSheetFile != "default")
			{
				const string alternateSpriteSheetFile = getContextualizedFilename(altSheetFile, prop.get("base_dir"), GameLogic::VEHICLES_FOLDER+"/");
				if(alternateSpriteSheetFile.empty())
				{
					cout << "warning: alternate sprite sheet " << i << " file could not be found!"
					<< " (specified by \"" << filename << "\")" << endl;
					continue;
				}

				unsigned inheritedProfileIndex = 0;
				const string altSheetInherit = prop.get("alternate_sprite_sheet" + futil::to_string(i) + "_inherit_from");
				if(not altSheetInherit.empty())
				{
					const unsigned inheritIndex = altSheetInherit == "default"? 0 : atoi(altSheetInherit.c_str());
					if(inheritIndex < 1 + alternateSprites.size())
						inheritedProfileIndex = inheritIndex;
					else
						cout << "warning: alternate sprite sheet #" << i << " specifies an out of bounds sheet index (" << inheritIndex << ") to inherit. inheriting default instead." << endl;
				}

				if(inheritedProfileIndex == 0)
					alternateSprites.push_back(sprite);
				else
					alternateSprites.push_back(alternateSprites[inheritedProfileIndex-1]);

				alternateSprites.back().sheetFilename = alternateSpriteSheetFile;
			}
		}
	}

	// logic data
	logic.frameDurationProportionalToSpeed = (prop.get("sprite_frame_duration") == "dynamic");  // TODO rename this property to something else
	if(logic.frameDurationProportionalToSpeed)
	{
		sprite.frameDuration = -1;
		foreach(Pseudo3DVehicleAnimation::Spec&, spec, vector<Pseudo3DVehicleAnimation::Spec>, alternateSprites)
			spec.frameDuration = -1;
	}

	// ------------------------------------------------------------------------------------------------------------------------------------------------
	// Attempt to estimate some missing properties from sprite data
	// These attempts need to be done after loading sprite data to make sure that some fields are ready ('depictedVehicleWidth', 'sprite_sheet_file', etc)

	// attempt to estimate missing width from sprite info
	if(not isValueSpecified(prop, "vehicle_width"))
		body.width = sprite.depictedVehicleWidth * (sprite.scale.x / SPRITE_WORLD_SCALE_FACTOR);

	// attempt to estimate missing center's of gravity height
	if(not isValueSpecified(prop, "center_of_gravity_height"))
	{
		if(isValueSpecified(prop, "vehicle_height"))
			body.centerOfGravityHeight = body.height/2;  // aprox. half the height
		else
			body.centerOfGravityHeight = 0.3506f * body.width;  // proportion aprox. of a 300ZX Z32
	}

	// attempt to estimate missing wheelbase value
	if(not isValueSpecified(prop, "wheelbase"))
	{
		if(isValueSpecified(prop, "vehicle_length"))
			body.wheelbase = 0.5682f * body.length;  // proportion aprox. of a 300ZX Z32

		else if(isValueSpecified(prop, "vehicle_width"))
			body.wheelbase = 2.5251f * body.width;  // proportion aprox. of a 300ZX Z32

		else if(isValueSpecified(prop, "vehicle_height"))
			body.wheelbase = 3.6016f * body.height;  // proportion aprox. of a 300ZX Z32

		else
			body.wheelbase = 2.5251f * body.width;  // proportion aprox. of a 300ZX Z32
	}
}

// ========================================================================================================================

double atof_halved(const char* cstr) { return 0.5 * atof(cstr); }  // does atof and returns half the result

inline static float getMomentOfInertiaThickCylindricalShell(float innerRadius, float outerRadius, float length, float density)
{
	return 0.5f * M_PI * density * length * (pow4(outerRadius) - pow4(innerRadius));
}

inline static float getMomentOfInertiaCylinder(float radius, float length, float density)
{
	return 0.5f * M_PI * density * length * pow4(radius);
}

static void loadChassisSpec(Pseudo3DVehicle::Spec& spec, const Properties& prop)
{
	// aux. var
	string value;

	value = futil::to_lower(prop.get("vehicle_type", "default"));
	if(value == "default") spec.body.vehicleType = Mechanics::TYPE_CAR;  // this choice may change in the future
	else if(value == "car") spec.body.vehicleType = Mechanics::TYPE_CAR;
	else if(value == "bike") spec.body.vehicleType = Mechanics::TYPE_BIKE;
	else spec.body.vehicleType = Mechanics::TYPE_OTHER;

	spec.body.mass = prop.getParsedCStrAllowDefault<double, atof>("vehicle_mass", spec.body.vehicleType == Mechanics::TYPE_BIKE? DEFAULT_VEHICLE_MASS_BIKE : DEFAULT_VEHICLE_MASS_CAR);

	value = prop.get("drive_wheel", prop.get("driven_wheels", "default"));
	if(value == "all") spec.body.driveWheelType = Mechanics::DRIVE_WHEEL_ALL;
	else if(value == "front") spec.body.driveWheelType = Mechanics::DRIVE_WHEEL_FRONT;
	else if(value == "rear" or value == "default") spec.body.driveWheelType = Mechanics::DRIVE_WHEEL_REAR;

	// set some default values
	spec.body.driveWheelRadius = DEFAULT_WHEEL_RADIUS;
	float driveWheelWidth = DEFAULT_WHEEL_WIDTH;
	float driveWheelRimRadius = 0.625f * spec.body.driveWheelRadius;  // aproximation of the rim radius value 62.5% of tire diameter/radius (5/8 ratio)

	// a tire code meant to be used with wheels consisting of rim and tires
	value = prop.get("drive_wheel_tire", prop.get("driven_wheels_tires", prop.get("tires")));
	if(not value.empty())
	{
		vector<string> tokens;
//		if(contains(value, "-"))  // historical notation tire codes
//		{
			// TODO parse historical notation tire codes
			/* Taken from Wikipedia:
			 * "Prior to 1964, tires were all made to a 90% aspect ratio. Tire size was specified as the tire width in inches and the diameter in inches – for example, 6.50-15.
			 *  From 1965 to the early 1970s, tires were made to an 80% aspect ratio. Tire size was again specified by width in inches and diameter in inches. To differentiate
			 *  from the earlier 90-ratio tires, the decimal point is usually omitted from the width – for example, 685-15 for a tire 6.85 inches wide. Starting in 1972 tires
			 *  were specified by load rating, using a letter code. In practice, a higher load rating tire was also a wider tire. In this system a tire had a letter, optionally
			 *  followed by "R" for radial tires, followed by the aspect ratio, a dash and the diameter – C78-15 or CR78-15 for bias and radial, respectively. Each diameter of
			 *  wheel had a separate sequence of load ratings; thus, a C78-14 and a C78-15 are not the same width. An aspect ratio of 78% was typical for letter-sized tires,
			 *  although 70% was also common and lower profiles down to 50% were occasionally seen
			 */
//		} else
		if(contains(value, "R"))  // probably a radial tire
		{
			futil::split(tokens, value, 'R');
			if(tokens.size() > 1)
			{
				const string tireCodeRimSize = futil::trim(tokens[1]);
				if(futil::parseable<unsigned>(tireCodeRimSize))
				{
					const string tireCodeFirstPart = futil::trim(tokens[0]);
					if(contains(tireCodeFirstPart, "/"))  // metric tire code
					{
						tokens.clear();
						futil::split(tokens, tireCodeFirstPart, '/');
						if(tokens.size() > 1)
						{
							tokens[0] = futil::trim(tokens[0]);
							if(futil::starts_with(tokens[0], "P"))  // ignoring letter for vehicle class
								tokens[0] = tokens[0].substr(1);
							else if(futil::starts_with(tokens[0], "LT"))  // ignoring letter for vehicle class
								tokens[0] = tokens[0].substr(2);

							const string tireCodeWidth = tokens[0], tireCodeAspectOrDiameter = futil::trim(tokens[1]);
							if(futil::parseable<unsigned>(tireCodeWidth) and futil::parseable<unsigned>(tireCodeAspectOrDiameter))
							{
								driveWheelWidth = atoi(tireCodeWidth.c_str());  // width from tire code
								driveWheelRimRadius = 0.5 * atoi(tireCodeRimSize.c_str()) * 25.4;  // rim size from tire code is set in inches, needs to be converted to mm (diameter expected, so we take half for radius)
								const int aspectOrDiameter = atoi(tireCodeAspectOrDiameter.c_str());
								if(aspectOrDiameter > 200)  // if this value is greater than 200, it's the diameter (japanese notation)
									spec.body.driveWheelRadius = aspectOrDiameter;
								else  // value is the aspect ratio of the side-wall height (common notation)
									spec.body.driveWheelRadius = driveWheelRimRadius + driveWheelWidth * aspectOrDiameter * 0.01;  // value is a percentage in integer notation, needs to be converted to ratio form
							}
							else cout << "warning: could not parse tire size (invalid values) from tire code \"" << value << "\" from vehicle spec " << spec.filename << " so it will be ignored..." << endl;
						}
						else cout << "warning: could not parse tire size (missing values) from tire code \"" << value << "\" from vehicle spec " << spec.filename << " so it will be ignored..." << endl;
					}
//					if(contains(tireCodeFirstPart, "X"))  // USC notation
//					{
//						// TODO parse 00X00.0R00 notation (USC)
//					}
					else cout << "warning: could not parse tire size (invalid format) from tire code \"" << value << "\" from vehicle spec " << spec.filename << " so it will be ignored..." << endl;
				}
				else cout << "warning: could not parse rim size from tire code \"" << value << "\" from vehicle spec " << spec.filename << " so it will be ignored..." << endl;
			}
			else cout << "warning: invalid tire code \"" << value << "\" from vehicle spec " << spec.filename << " so it will be ignored..." << endl;
		}
		else cout << "warning: could not identify format of tire code \"" << value << "\" from vehicle spec " << spec.filename << " so it will be ignored..." << endl;
	}

	// specifies (or override tire code's) drive wheel radius
	spec.body.driveWheelRadius = prop.getParsedCStrAllowDefault<double, atof>("drive_wheel_radius",
								 prop.getParsedCStrAllowDefault<double, atof>("driven_wheels_radius",
								 prop.getParsedCStrAllowDefault<double, atof>("wheel_radius",
								 prop.getParsedCStrAllowDefault<double, atof>("tire_radius",
								 prop.getParsedCStrAllowDefault<double, atof_halved>("drive_wheel_diameter",
								 prop.getParsedCStrAllowDefault<double, atof_halved>("driven_wheels_diameter",
								 prop.getParsedCStrAllowDefault<double, atof_halved>("wheel_diameter",
								 prop.getParsedCStrAllowDefault<double, atof_halved>("tire_diameter", spec.body.driveWheelRadius)))))))) * 0.001;  // value was parsed in mm, needs to be converted to meters

	// specifies (or override tire code's) drive wheel width
	driveWheelWidth = prop.getParsedCStrAllowDefault<double, atof>("drive_wheel_width",
					  prop.getParsedCStrAllowDefault<double, atof>("driven_wheels_width",
					  prop.getParsedCStrAllowDefault<double, atof>("wheel_width",
					  prop.getParsedCStrAllowDefault<double, atof>("tire_width", driveWheelWidth)))) * 0.001;  // value was parsed in mm, needs to be converted to meters

	// specifies (or override tire code's) drive wheel rim size
	driveWheelRimRadius = prop.getParsedCStrAllowDefault<double, atof>("drive_wheel_rim_radius",
			              prop.getParsedCStrAllowDefault<double, atof>("driven_wheels_rims_radius",
						  prop.getParsedCStrAllowDefault<double, atof_halved>("drive_wheel_rim_diameter",
						  prop.getParsedCStrAllowDefault<double, atof_halved>("driven_wheels_rims_diameter",
						  prop.getParsedCStrAllowDefault<double, atof>("rim_radius",
						  prop.getParsedCStrAllowDefault<double, atof_halved>("rim_diameter", driveWheelRimRadius)))))) * 0.001;  // value was parsed in mm, needs to be converted to meters

	// estimate drive wheel moment of inertia
	const float TIRE_DENSITY = 390,  // in kg/m^3
				RIM_DENSITY = 5300,  // using the a value between cast aluminum alloy (about 2770kg/m^3 density) and steel (about 7850kg/m^3 density)
				TIRE_SIDE_THICKNESS = 0.01f,  // in meters (10mm)
				TIRE_TREAD_THICKNESS = 0.04f * spec.body.driveWheelRadius,  // estimated tire thickness at tread side as 4% of tire radius
				RIM_THICKNESS = 0.015f,  // in meters (15mm)

				outerTireShellInertia = getMomentOfInertiaThickCylindricalShell(spec.body.driveWheelRadius - TIRE_TREAD_THICKNESS, spec.body.driveWheelRadius, driveWheelWidth, TIRE_DENSITY),
				sideTireShellInertia = getMomentOfInertiaThickCylindricalShell(driveWheelRimRadius, spec.body.driveWheelRadius - TIRE_TREAD_THICKNESS, TIRE_SIDE_THICKNESS, TIRE_DENSITY),
				rimOuterShellInertia = getMomentOfInertiaThickCylindricalShell(driveWheelRimRadius - RIM_THICKNESS, driveWheelRimRadius, driveWheelWidth, RIM_DENSITY),
				rimCentralDiskInertia = getMomentOfInertiaCylinder(driveWheelRimRadius - RIM_THICKNESS, RIM_THICKNESS, RIM_DENSITY),
				driveWheelInertia = outerTireShellInertia + 2*sideTireShellInertia + rimOuterShellInertia + rimCentralDiskInertia,
				arbitraryAdjustmentFactor = 20.0f;

	// old way to estimate drive wheel moment of inertia; estimated wheel (tire+rim) density. (33cm radius or 660mm diameter tire with 75kg mass). Actual value varies by tire (brand, weight, type, etc) and rim (brand , weight, shape, material, etc)
//	const float AVERAGE_WHEEL_DENSITY = 75.0/pow2(0.33),  // d = m/r^2, assuming wheel width = 1/PI in the original formula d = m/(PI * r^2 * width)
//				wheelMass = AVERAGE_WHEEL_DENSITY * pow2(spec.body.driveWheelRadius),  // m = d*r^2, assuming wheel width = 1/PI
//				driveWheelInertia = wheelMass * pow2(spec.body.driveWheelRadius) * 0.5,  // I = (mr^2)/2
//				arbitraryAdjustmentFactor = 1/0.2;

	spec.body.driveWheelInertia = driveWheelInertia * arbitraryAdjustmentFactor;

	value = prop.get("engine_location", "default");
	if(value == "mid" or value == "middle") spec.body.engineLocation = Mechanics::ENGINE_LOCATION_MIDDLE;
	else if(value == "rear") spec.body.engineLocation = Mechanics::ENGINE_LOCATION_REAR;
	else if(value == "front") spec.body.engineLocation = Mechanics::ENGINE_LOCATION_FRONT;
	else  // either default or unrecognized
	{
		spec.body.engineLocation = spec.body.vehicleType == Mechanics::TYPE_BIKE? Mechanics::ENGINE_LOCATION_MIDDLE : Mechanics::ENGINE_LOCATION_FRONT;
		if(value != "default") cout << "warning: unrecognized engine location specified (" << value << "). using default instead..." << endl;
	}

	// estimating weight distribution from drivetrain
	float defaultEstimatedWeightDistribution = 0.5;
	if(spec.body.engineLocation == Mechanics::ENGINE_LOCATION_FRONT)
	{
		if(spec.body.driveWheelType == Mechanics::DRIVE_WHEEL_FRONT)     defaultEstimatedWeightDistribution = DEFAULT_FF_WEIGHT_DISTRIBUTION;
		else if(spec.body.driveWheelType == Mechanics::DRIVE_WHEEL_REAR) defaultEstimatedWeightDistribution = DEFAULT_FR_WEIGHT_DISTRIBUTION;
		else if(spec.body.driveWheelType == Mechanics::DRIVE_WHEEL_ALL)  defaultEstimatedWeightDistribution = (DEFAULT_FF_WEIGHT_DISTRIBUTION + DEFAULT_FR_WEIGHT_DISTRIBUTION)/2;
	}
	else if(spec.body.engineLocation == Mechanics::ENGINE_LOCATION_REAR)
	{
		if(spec.body.driveWheelType == Mechanics::DRIVE_WHEEL_FRONT)     defaultEstimatedWeightDistribution = 1 - DEFAULT_FR_WEIGHT_DISTRIBUTION;  // well, it makes sense, right?
		else if(spec.body.driveWheelType == Mechanics::DRIVE_WHEEL_REAR) defaultEstimatedWeightDistribution = DEFAULT_RR_WEIGHT_DISTRIBUTION;
		else if(spec.body.driveWheelType == Mechanics::DRIVE_WHEEL_ALL)  defaultEstimatedWeightDistribution = ((1 - DEFAULT_FR_WEIGHT_DISTRIBUTION) + DEFAULT_RR_WEIGHT_DISTRIBUTION)/2;
	}
	else if(spec.body.engineLocation == Mechanics::ENGINE_LOCATION_MIDDLE)
	{
		if(spec.body.driveWheelType == Mechanics::DRIVE_WHEEL_FRONT)     defaultEstimatedWeightDistribution = 2*DEFAULT_MA_WEIGHT_DISTRIBUTION - DEFAULT_MR_WEIGHT_DISTRIBUTION;  // reverse logic of FA = (FF + FR)/2
		else if(spec.body.driveWheelType == Mechanics::DRIVE_WHEEL_REAR) defaultEstimatedWeightDistribution = DEFAULT_MR_WEIGHT_DISTRIBUTION;
		else if(spec.body.driveWheelType == Mechanics::DRIVE_WHEEL_ALL)  defaultEstimatedWeightDistribution = DEFAULT_MA_WEIGHT_DISTRIBUTION;
	}

	spec.body.weightDistribution = prop.getParsedCStrAllowDefault<double, atof>("weight_distribution", defaultEstimatedWeightDistribution);
	spec.body.width =  prop.getParsedCStrAllowDefault<double, atof>("vehicle_width", spec.body.width);
	spec.body.height = prop.getParsedCStrAllowDefault<double, atof>("vehicle_height", spec.body.height);
	spec.body.length = prop.getParsedCStrAllowDefault<double, atof>("vehicle_length", spec.body.length);
	spec.body.centerOfGravityHeight = prop.getParsedCStrAllowDefault<double, atof>("center_of_gravity_height", spec.body.centerOfGravityHeight);
	spec.body.wheelbase = prop.getParsedCStrAllowDefault<double, atof>("wheelbase", spec.body.wheelbase);

	const float referenceArea = prop.getParsedCStrAllowDefault<double, atof>("vehicle_frontal_area", (isValueSpecified(prop, "vehicle_width") and isValueSpecified(prop, "vehicle_height")?
			 (spec.body.vehicleType == Mechanics::TYPE_BIKE? DEFAULT_FRONTAL_AREA_RATIO_BIKE : DEFAULT_FRONTAL_AREA_RATIO_CAR) * (spec.body.width * spec.body.height * 1e-6)
			:(spec.body.vehicleType == Mechanics::TYPE_BIKE? DEFAULT_FRONTAL_AREA_BIKE : DEFAULT_FRONTAL_AREA_CAR)));

	const float dragCoefficient = prop.getParsedCStrAllowDefault<double, atof>("drag_coefficient", (spec.body.vehicleType == Mechanics::TYPE_BIKE? DEFAULT_CD_BIKE : DEFAULT_CD_CAR));
	spec.body.dragArea = prop.getParsedCStrAllowDefault<double, atof>("drag_area", (referenceArea * dragCoefficient));

	const float liftCoefficient = prop.getParsedCStrAllowDefault<double, atof>("lift_coefficient", (spec.body.vehicleType == Mechanics::TYPE_BIKE? DEFAULT_CL_BIKE : DEFAULT_CL_CAR));
	spec.body.liftArea = prop.getParsedCStrAllowDefault<double, atof>("lift_area", (referenceArea * liftCoefficient));
}

// ========================================================================================================================

static void loadPowertrainSpec(Pseudo3DVehicle::Spec& spec, const Properties& prop)
{
	// aux. var
	string value;

	spec.engine.configuration = prop.get("engine_configuration");  // info-only
	spec.engine.aspiration = prop.get("engine_aspiration");  // info-only
	spec.engine.valvetrain = prop.get("engine_valvetrain");  // info-only
	spec.engine.displacement = prop.getParsedCStr<double, atof>("engine_displacement", 0);

	value = prop.get("engine_cylinder_count");
	if(not value.empty())
		spec.engine.cylinderCount = atoi(value.c_str());
	else  // estimate cylinder count from engine configuration string
	{
		value = futil::to_lower(futil::trim(spec.engine.configuration));
		if(value == "single-cylinder" or value == "single cylinder")
			spec.engine.cylinderCount = 1;
		else
		{
			static const string prefixes[] = { "straight-", "inline-", "v", "v-", "vr", "vr-", "flat-", "boxer-", "w", "w-", "u", "u-", "h", "h-", "x", "x-", "r", "r-" };
			int typeCylinderCount = -1;
			for(unsigned i = 0; i < sizeof(prefixes)/sizeof(*prefixes) and typeCylinderCount == -1; i++)
				for(unsigned cc = 64; cc > 1 and typeCylinderCount == -1; cc--)
					if(value.find(prefixes[i]+futil::to_string(cc)) != string::npos)
						typeCylinderCount = cc;
			if(typeCylinderCount != -1)
				spec.engine.cylinderCount = typeCylinderCount;
		}
	}

	spec.engine.valveCount = prop.getParsedCStr<int, atoi>("engine_valve_count", 0);  // info-only
	spec.engine.strokeCount = prop.getParsedCStrAllowDefault<int, atoi>("engine_stroke_count", 4);

	spec.engine.maxRpm = prop.getParsedCStrAllowDefault<int, atoi>("engine_maximum_rpm", spec.body.vehicleType == Mechanics::TYPE_BIKE? DEFAULT_MAXIMUM_RPM_BIKE : DEFAULT_MAXIMUM_RPM_CAR);
	spec.engine.minRpm = 1000;  // TODO read minRpm from spec file when values other than 1000 become supported
	spec.engine.maximumPower = prop.getParsedCStrAllowDefault<double, atof>("engine_maximum_power", spec.body.vehicleType == Mechanics::TYPE_BIKE? DEFAULT_MAXIMUM_POWER_BIKE : DEFAULT_MAXIMUM_POWER_CAR);

	// TODO re-enable these when proper formulas have been determined from parametrization
//	spec.engine.maximumPowerRpm = prop.getParsedCStrAllowDefault<double, atof>("engine_maximum_power_rpm", DEFAULT_MAXIMUM_POWER_RPM_RANGE);
//	spec.engine.maximumTorque = prop.getParsedCStrAllowDefault<double, atof>("engine_maximum_torque", -1);
//	spec.engine.maximumTorqueRpm = prop.getParsedCStrAllowDefault<double, atof>("engine_maximum_torque_rpm", -1);

	value = prop.get("engine_power_band", "typical");
	Engine::TorqueCurveProfile::PowerBandType powerBandType = Engine::TorqueCurveProfile::POWER_BAND_TYPICAL;
	if(value == "peaky")
		powerBandType = Engine::TorqueCurveProfile::POWER_BAND_PEAKY;
	else if(value == "torquey")
		powerBandType = Engine::TorqueCurveProfile::POWER_BAND_TORQUEY;
	else if(value == "semitorquey" or value == "semi-torquey" or value == "semi torquey")
		powerBandType = Engine::TorqueCurveProfile::POWER_BAND_SEMI_TORQUEY;
	else if(value == "wide")
		powerBandType = Engine::TorqueCurveProfile::POWER_BAND_WIDE;
	else if(value == "narrow")
		powerBandType = Engine::TorqueCurveProfile::POWER_BAND_NARROW;
	else if(value == "semipeaky" or value == "semi-peaky" or value == "semi peaky")
		powerBandType = Engine::TorqueCurveProfile::POWER_BAND_SEMI_PEAKY;
	else if(value == "extratorquey" or value == "extra-torquey" or value == "extra torquey")
		powerBandType = Engine::TorqueCurveProfile::POWER_BAND_EXTRA_TORQUEY;
	else if(value == "extrapeaky" or value == "extra-peaky" or value == "extra peaky")
		powerBandType = Engine::TorqueCurveProfile::POWER_BAND_EXTRA_PEAKY;
	else if(value == "quasiflat" or value == "quasi-flat" or value == "quasi flat" or value == "flat")
		powerBandType = Engine::TorqueCurveProfile::POWER_BAND_QUASIFLAT;
	else if(value == "typical electric" or value == "typical-electric" or value == "electric-typical" or value == "electric typical")
		powerBandType = Engine::TorqueCurveProfile::POWER_BAND_TYPICAL_ELECTRIC;

	// here an engine object is created just to collect some data
	Engine tmpEngine(Engine::Spec(spec.engine.maxRpm, spec.engine.maximumPower, powerBandType, DEFAULT_GEAR_COUNT));
	spec.engine.torqueCurveProfile = tmpEngine.spec.torqueCurveProfile;
	spec.engine.maximumPowerRpm = tmpEngine.spec.maximumPowerRpm;
	spec.engine.maximumTorque = tmpEngine.spec.maximumTorque;
	spec.engine.maximumTorqueRpm = tmpEngine.spec.maximumTorqueRpm;

	spec.engine.disengagedRevSpeedFactor = prop.getParsedCStrAllowDefault<double, atof>("engine_disengaged_rev_speed_factor", spec.body.vehicleType == Mechanics::TYPE_BIKE? 5.0 : 1.0);
	spec.engine.transmissionEfficiency = prop.getParsedCStrAllowDefault<double, atof>("transmission_efficiency", spec.body.vehicleType == Mechanics::TYPE_BIKE? DEFAULT_TRANSMISSION_EFFICIENCY_BIKE : DEFAULT_TRANSMISSION_EFFICIENCY_CAR);
	spec.engine.gearCount = prop.getParsedCStrAllowDefault<int, atoi>("gear_count", DEFAULT_GEAR_COUNT);
	spec.engine.gearRatio.resize(spec.engine.gearCount);

	// first, set default ratios, then override
	spec.engine.reverseGearRatio = 2.5;
	spec.engine.differentialRatio = spec.body.vehicleType == Mechanics::TYPE_BIKE? 7.275 : 4.750;
	spec.engine.gearRatio[spec.engine.gearCount-1] = 0.8;
	for(int g = spec.engine.gearCount-2; g >= 0; g--)
		spec.engine.gearRatio[g] = spec.engine.gearRatio[g+1] + 0.1*(spec.engine.gearCount-g) - 0.1*(spec.body.vehicleType == Mechanics::TYPE_BIKE);

	value = prop.get("gear_ratios");
	if(not value.empty())
	{
		if(value == "custom")
		{
			spec.engine.differentialRatio = prop.getParsedCStrAllowDefault<double, atof>("gear_differential_ratio", spec.engine.differentialRatio);
			spec.engine.reverseGearRatio =  prop.getParsedCStrAllowDefault<double, atof>("gear_reverse_ratio", spec.engine.reverseGearRatio);
			for(int g = 0; g < (int) spec.engine.gearCount; g++)
				spec.engine.gearRatio[g] = prop.getParsedCStrAllowDefault<double, atof>("gear_" + futil::to_string(g+1) + "_ratio", spec.engine.gearRatio[g]);
		}
		// TODO support some gear ratios' presets, like "typical", "sports", "rally", etc (maybe even some well known aftermarket transmission ratios)
		// TODO support parsing gear ratios written in a comma separated list directly in the gear_ratios field
	}

	spec.body.fuelCapacity = prop.getDoubleAllowDefault("fuel_capacity", spec.body.mass*0.028f + 14.2f);  // default fuel capacity scales with chassis mass
}

// ========================================================================================================================

// default sprite uint constants
static const unsigned
	DEFAULT_SPRITE_WIDTH_CAR = 60,
	DEFAULT_SPRITE_HEIGHT_CAR = 35,
	DEFAULT_SPRITE_WIDTH_BIKE = 54,
	DEFAULT_SPRITE_HEIGHT_BIKE = 72,
	DEFAULT_BRAKELIGHTS_SPRITE_WIDTH = 32,
	DEFAULT_BRAKELIGHTS_SPRITE_HEIGHT = 32;

// default sprite float constants
static const float
	PSEUDO_ANGLE_THRESHOLD = 0.1,
	SPRITE_IDEAL_MAX_DEPICTED_TURN_ANGLE = 45, // 45 degrees, pi/4 radians
	DEFAULT_SPRITE_MAX_DEPICTED_TURN_ANGLE = SPRITE_IDEAL_MAX_DEPICTED_TURN_ANGLE,
	DEFAULT_SPRITE_DEPICTED_VEHICLE_WIDTH_PROPORTION = 0.857142857143;  // ~0,857

static const string DEFAULT_CAR_SPRITE = "assets/sprites/car-default.png",
					DEFAULT_BIKE_SPRITE = "assets/sprites/bike-default.png",
					DEFAULT_CAR_SHEET = "assets/sprites/car-sheet-default.png",
					DEFAULT_BIKE_SHEET = "assets/sprites/bike-sheet-default.png";

static void loadAnimationSpec(Pseudo3DVehicleAnimation::Spec& spec, const Properties& prop, const Mechanics::Spec& body)
{
	// aux. vars
	string value;
	const bool isBike = (body.vehicleType == Mechanics::TYPE_BIKE);

	spec.sheetFilename = prop.get("sprite_sheet_file", "default");
	if(spec.sheetFilename != "default")
	{
		value = getContextualizedFilename(spec.sheetFilename, prop.get("base_dir"), GameLogic::VEHICLES_FOLDER+"/");
		if(not value.empty())
			spec.sheetFilename = value;
		else {
			cout << "warning: sheet file \"" << spec.sheetFilename << "\" could not be found!" << " (specified by \"" << prop.get("filename") << "\"). using default sheet instead..." << endl;
			spec.sheetFilename = "default";
		}
	}

	spec.stateCount = prop.getParsedCStrAllowDefault<int, atoi>("sprite_state_count", 1);
	spec.frameWidth = prop.getParsedCStrAllowDefault<int, atoi>("sprite_frame_width", isBike? DEFAULT_SPRITE_WIDTH_BIKE : DEFAULT_SPRITE_WIDTH_CAR);
	spec.frameHeight = prop.getParsedCStrAllowDefault<int, atoi>("sprite_frame_height", isBike? DEFAULT_SPRITE_HEIGHT_BIKE : DEFAULT_SPRITE_HEIGHT_CAR);
	spec.depictedVehicleWidth = prop.getParsedCStrAllowDefault<int, atoi>("sprite_vehicle_width", futil::round(spec.frameWidth*DEFAULT_SPRITE_DEPICTED_VEHICLE_WIDTH_PROPORTION));

	// default scale
	spec.scale.x = spec.scale.y = 1.0;

	value = futil::to_lower(futil::trim(prop.get("sprite_keep_aspect_ratio")));
	const bool keepAspectRatio = (value == "true" or value == "yes"),
			   isVehicleWidthSpecified = isValueSpecified(prop, "vehicle_width"),
			   isVehicleHeightSpecified = isValueSpecified(prop, "vehicle_height");

	const string spriteVehicleHeightStr = prop.get("sprite_vehicle_height"),
				 vehicleWidthHeightRatioStr = prop.get("vehicle_width_height_ratio");

	if(isVehicleWidthSpecified)  // if vehicle width was specified, compute recommended scale factor
	{
		// if allowed, adjust scale factor if vehicle height (in sprite) was specified and either vehicle height (in real-life) or vehicle width/height ratio were specified
		if(not keepAspectRatio and isValidValue(spriteVehicleHeightStr) and ((isVehicleHeightSpecified and body.height > 0) or isValidValue(vehicleWidthHeightRatioStr)))
		{
			// adjust scale factor to account for width/height ratio discrepancies
			const float spriteVehicleHeight = atoi(spriteVehicleHeightStr.c_str()),  // the vehicle height on the sprite, in pixels
						spriteWHRatio = spec.depictedVehicleWidth / spriteVehicleHeight,  // sprite width/height (WH) ratio
			            vehicleWHRatio = (isVehicleHeightSpecified and body.height > 0)? (body.width / body.height)  // calculate real-life width/height ratio from real-life width and height
																					   : atof(vehicleWidthHeightRatioStr.c_str()),  // use specified real-life width/height ratio
						ratioFixFactor = vehicleWHRatio / spriteWHRatio;  // multiplier that makes the sprite width/height ratio match the real-life width/height ratio

			// recommended scale factors, making sprite width/height ratio match the real-life width/height ratio
			spec.scale.x = (body.width / spec.depictedVehicleWidth) * Pseudo3DVehicle::SPRITE_WORLD_SCALE_FACTOR;
			spec.scale.y = spec.scale.x / ratioFixFactor;
		}
		else  // no data about vehicle height or width/height ratio given; assume no width/height ratio discrepancies between real-life
		{
			spec.scale.x = spec.scale.y = (body.width / spec.depictedVehicleWidth) * Pseudo3DVehicle::SPRITE_WORLD_SCALE_FACTOR;  // recommended scale factor assuming no width/height ratio discrepancies
		}
	}
	else if(isVehicleHeightSpecified and isValidValue(spriteVehicleHeightStr))  // otherwise if vehicle height is available, compute recommended scale factor
	{
		const float spriteVehicleHeight = atoi(spriteVehicleHeightStr.c_str());  // the vehicle height on the sprite, in pixels

		if(not keepAspectRatio and isValidValue(vehicleWidthHeightRatioStr))
		{
			const float vehicleWHRatio = atof(vehicleWidthHeightRatioStr.c_str()),  // vehicle width/height (WH) ratio
						spriteWHRatio = spec.depictedVehicleWidth / spriteVehicleHeight,  // sprite width/height (WH) ratio
						ratioFixFactor = vehicleWHRatio / spriteWHRatio;  // multiplier that makes the sprite width/height ratio match the real-life width/height ratio

			// recommended scale factors, making sprite width/height ratio match the real-life width/height ratio
			spec.scale.y = (body.height / spriteVehicleHeight) * Pseudo3DVehicle::SPRITE_WORLD_SCALE_FACTOR;
			spec.scale.x = spec.scale.y * ratioFixFactor;
		}
		else  // no data about vehicle width/height ratio given; assume no width/height ratio discrepancies between real-life
		{
			spec.scale.x = spec.scale.y = (body.height / spriteVehicleHeight) * Pseudo3DVehicle::SPRITE_WORLD_SCALE_FACTOR;  // recommended scale factor assuming no width/height ratio discrepancies
		}
	}

	// if scale factor is available, override previous definitions
	spec.scale.x = prop.getParsedCStrAllowDefault<double, atof>("sprite_scale_x", prop.getParsedCStrAllowDefault<double, atof>("sprite_scale", spec.scale.x));
	spec.scale.y = prop.getParsedCStrAllowDefault<double, atof>("sprite_scale_y", prop.getParsedCStrAllowDefault<double, atof>("sprite_scale", spec.scale.y));

	spec.contactOffset = prop.getParsedCStrAllowOptionedDefault<int, atoi>("sprite_contact_offset", 2);

	value = futil::to_lower(futil::trim(prop.get("sprite_asymmetric")));
	spec.asymmetrical = (value == "true" or value == "yes");

	// with these fields, values for all states (except state 0) must be specified, or no values should be specified at all.
	for(unsigned stateNumber = 0; stateNumber < spec.stateCount; stateNumber++)
	{
		value = prop.get("sprite_state" + futil::to_string(stateNumber) + "_depicted_turn_angle");
		if(isValidValue(value))
		{
			if(spec.depictedTurnAngle.empty())
			{
				if(stateNumber == 1)
					spec.depictedTurnAngle.push_back(0);

				else if(stateNumber > 1)
				{
					cout << "warning: missing depicted turn angles for sprite states before state " << stateNumber
						 << "; ignoring all values for safety..." << endl;
					break;
				}
			}

			spec.depictedTurnAngle.push_back(atof(value.c_str())/SPRITE_IDEAL_MAX_DEPICTED_TURN_ANGLE);
		}
		else if(not spec.depictedTurnAngle.empty())
		{
			cout << "warning: missing depicted turn angle for sprite state " << stateNumber << "; ignoring all values for safety..." << endl;
			spec.depictedTurnAngle.clear();
			break;
		}
	}

	// attempt to load turn angles from single property if they were not specified individually
	if(spec.depictedTurnAngle.empty())
	{
		value = prop.get("sprite_depicted_turn_angles");
		if(isValidValue(value))
		{
			vector<string> tokens = futil::split(value, ',');
			if(tokens.size() == spec.stateCount)
				for(unsigned i = 0; i < tokens.size(); i++)
					spec.depictedTurnAngle.push_back(atof(tokens[i].c_str())/SPRITE_IDEAL_MAX_DEPICTED_TURN_ANGLE);
			else
				cout << "warning: incorrect number of values specified for turn angles; ignoring values for safety..." << endl;
		}
	}

	// check depicted turn angles for consistency
	if(not spec.depictedTurnAngle.empty())
		for(unsigned i = 0; i < spec.depictedTurnAngle.size(); i++)
			for(unsigned j = i+1; j < spec.depictedTurnAngle.size(); j++)
			{
				if(spec.depictedTurnAngle[j] == spec.depictedTurnAngle[i])
				{
					cout << "warning: depicted turn angle specified for state " << i << " and state " << j
						 << " have the same value (" << spec.depictedTurnAngle[i] << ");"
						 << " ignoring values for safety..." << endl;
					spec.depictedTurnAngle.clear();
					goto depictedTurnAngleCheckEnd;
				}
				else if(spec.depictedTurnAngle[j] < spec.depictedTurnAngle[i])
				{
					cout << "warning: depicted turn angle specified for state " << j << " (" << spec.depictedTurnAngle[j] << ")"
					     << " is smaller than previous state " << i << " (" << spec.depictedTurnAngle[i] << ");"
						 << " ignoring values for safety..." << endl;
					spec.depictedTurnAngle.clear();
					goto depictedTurnAngleCheckEnd;
				}
			}
	depictedTurnAngleCheckEnd:

	// attempt to load max depicted turn angle when no individual turn angles were specified. also, do this ONLY when there is more than one frame.
	if(spec.depictedTurnAngle.empty() and spec.stateCount > 1)
	{
		spec.depictedTurnAngle.resize(spec.stateCount);

		const float maxTurnAngle = prop.getParsedCStrAllowDefault<double, atof>("sprite_max_depicted_turn_angle", DEFAULT_SPRITE_MAX_DEPICTED_TURN_ANGLE)/SPRITE_IDEAL_MAX_DEPICTED_TURN_ANGLE;

		value = futil::trim(prop.get("sprite_depicted_turn_angle_progression"));
		if(value == "linear") linearTurnAngleProgression:
			for(unsigned s = 0; s < spec.stateCount; s++)
				spec.depictedTurnAngle[s] = s*(maxTurnAngle/(spec.stateCount-1));  // linear sprite progression

		else if(value == "linear-with-threshold" or value == "linear with threshold") linearWithThresholdTurnAngleProgression:
		{
			// linear sprite progression with 1-index advance at threshold angle
			spec.depictedTurnAngle[0] = 0;
			spec.depictedTurnAngle[1] = PSEUDO_ANGLE_THRESHOLD;
			for(unsigned s = 2; s < spec.stateCount; s++)
				spec.depictedTurnAngle[s] = PSEUDO_ANGLE_THRESHOLD + (s-1)*(maxTurnAngle-PSEUDO_ANGLE_THRESHOLD)/(spec.stateCount-2);
		}

		else if(value == "exponential")
			for(unsigned s = 0; s < spec.stateCount; s++)
				spec.depictedTurnAngle[s] = log(1+s*(exp(maxTurnAngle)-1)/(spec.stateCount-1));  // exponential sprite progression

		else  // case for default progression
		{
			if(not value.empty() and value != "default")
				cout << "warning: unknown turn angle progression type; using default instead..." << endl;

			if(spec.stateCount == 2 and maxTurnAngle > 0.75*SPRITE_IDEAL_MAX_DEPICTED_TURN_ANGLE)
				goto linearTurnAngleProgression;
			else
				goto linearWithThresholdTurnAngleProgression;
		}
	}

	value = futil::to_lower(futil::trim(prop.get("sprite_depicted_turn_angle_proximal_interpolation")));
	spec.depictedTurnAngleProximalInterpolation = (value == "true" or value == "yes");

	const unsigned globalFrameCount = prop.getParsedCStrAllowDefault<int, atoi>("sprite_frame_count", 1);

	for(unsigned stateNumber = 0; stateNumber < spec.stateCount; stateNumber++)
		spec.stateFrameCount.push_back(prop.getParsedCStrAllowDefault<int, atoi>("sprite_state" + futil::to_string(stateNumber) + "_frame_count", globalFrameCount));

	spec.frameDuration = prop.getParsedCStrAllowDefault<double, atof>("sprite_frame_duration", 0.25);
	spec.animationSpeedFactor = prop.getParsedCStrAllowDefault<double, atof>("sprite_animation_speed_factor", 1.0);

	if(spec.sheetFilename == "default")
	{
		if(spec.stateCount > 1 or globalFrameCount > 1)  // special case to use animated version of default sprites
		{
			spec.sheetFilename = isBike? DEFAULT_BIKE_SHEET : DEFAULT_CAR_SHEET;
			if(spec.stateCount > 1)
				spec.stateCount = 2;
			for(unsigned stateNumber = 0; stateNumber < spec.stateCount; stateNumber++)
			{
				if(spec.stateFrameCount[stateNumber] > 1)
					spec.stateFrameCount[stateNumber] = 2;

				if(not spec.depictedTurnAngle.empty())
					spec.depictedTurnAngle[stateNumber] = stateNumber * 0.1f;
			}
		}
		else spec.sheetFilename = isBike? DEFAULT_BIKE_SPRITE : DEFAULT_CAR_SPRITE;  // default, non-animated sprites
		if(isBike)
			spec.scale.x = spec.scale.y = 0.625f;
	}

	spec.brakelightsSheetFilename = prop.get("brakelights_sprite_filename");

	bool usingDefaultBrakelights = false;
	if(spec.brakelightsSheetFilename == "builtin" or spec.brakelightsSheetFilename == "default")
	{
		usingDefaultBrakelights = true;
		spec.brakelightsSheetFilename = "assets/effects/default-brakelight-effect.png";
		spec.brakelightsSpriteScale.x = spec.brakelightsSpriteScale.y = 1.0f;  // if no specified sheet, assume no scaling
	}
	else if(not spec.brakelightsSheetFilename.empty() and spec.brakelightsSheetFilename != "none")
	{
		spec.brakelightsSheetFilename = getContextualizedFilename(spec.brakelightsSheetFilename, prop.get("base_dir"), GameLogic::VEHICLES_FOLDER+"/");

		if(spec.brakelightsSheetFilename.empty())
			cout << "warning: brakelight sprite file could not be found!" << " (specified by \"" << prop.get("filename") << "\"). ignoring brakelights definitions..." << endl;
		else
			spec.brakelightsSpriteScale = spec.scale;  // if specified sheet, assume same scale as main sprite
	}
	else if(spec.brakelightsSheetFilename != "none")
		spec.brakelightsSheetFilename.clear();

	// at this point either a valid brakelights sheet filename is set or it is empty (meaning that there won't be any brakelights effect)
	if(not spec.brakelightsSheetFilename.empty())
	{
		// if brakelights scale factor is available, override previous definitions
		spec.brakelightsSpriteScale.x = prop.getParsedCStrAllowDefault<double, atof>("brakelights_sprite_scale_x", prop.getParsedCStrAllowDefault<double, atof>("brakelights_sprite_scale", spec.brakelightsSpriteScale.x));
		spec.brakelightsSpriteScale.y = prop.getParsedCStrAllowDefault<double, atof>("brakelights_sprite_scale_y", prop.getParsedCStrAllowDefault<double, atof>("brakelights_sprite_scale", spec.brakelightsSpriteScale.y));

		value = futil::to_lower(futil::trim(prop.get("brakelights_multiple_sprites")));
		spec.brakelightsMultipleSprites = (value == "true" or value == "yes");

		for(unsigned stateNumber = 0; stateNumber < spec.stateCount * (spec.asymmetrical? 2 : 1); stateNumber++)
		{
			if(stateNumber == spec.stateCount) continue;  // avoids state 0b
			string key = "brakelights_position" + (stateNumber < spec.stateCount? futil::to_string(stateNumber)  // normal, left-leaning
																				: futil::to_string(stateNumber-spec.stateCount) + "b");  // right-leaning
			fgeal::Point brakelightsPosition;
			bool defaultedX = false, defaultedY = false;
			if(isValueSpecified(prop, key))
			{
				vector<string> tokens = futil::split(prop.get(key), ',');
				if(tokens.size() > 1)
				{
					brakelightsPosition.x = atof(tokens[0].c_str());
					brakelightsPosition.y = atof(tokens[1].c_str());
				}
			}
			else defaultedX = defaultedY = true;

			string key2 = key + "_x";
			if(isValueSpecified(prop, key2))
			{
				brakelightsPosition.x = atof(prop.get(key2).c_str());
				defaultedX = false;
			}

			key2 = key + "_y";
			if(isValueSpecified(prop, key2))
			{
				brakelightsPosition.y = atof(prop.get(key2).c_str());
				defaultedY = false;
			}

			if(defaultedX)
			{
				if(spec.brakelightsMultipleSprites)
					brakelightsPosition.x = 0;
				else if(stateNumber > spec.stateCount)  // default value for right-leaning versions by applying same delta between left-leaning positions
					brakelightsPosition.x = spec.brakelightsPositions[0].x * 2 - spec.brakelightsPositions[stateNumber-spec.stateCount].x;
				else
				{
					if(isBike)
						brakelightsPosition.x = 0.5*spec.frameWidth;
					else
						brakelightsPosition.x = 3.0*0.5*(spec.frameWidth - spec.depictedVehicleWidth) + (stateNumber % spec.stateCount)*0.0357*spec.frameWidth;
				}
			}
			if(defaultedY)
			{
				if(spec.brakelightsMultipleSprites)
					brakelightsPosition.y = 0;
				else if(stateNumber > spec.stateCount)  // default value for right-leaning versions by applying same delta between left-leaning positions
					brakelightsPosition.y = spec.brakelightsPositions[0].y * 2 - spec.brakelightsPositions[stateNumber-spec.stateCount].y;
				else
					brakelightsPosition.y = 0.5*spec.frameHeight;
			}

			spec.brakelightsPositions.push_back(brakelightsPosition);
		}

		spec.brakelightsOffset.x = prop.getParsedCStrAllowDefault<double, atof>("brakelights_sprite_offset_x", usingDefaultBrakelights? -0.5*DEFAULT_BRAKELIGHTS_SPRITE_WIDTH : 0);
		spec.brakelightsOffset.y = prop.getParsedCStrAllowDefault<double, atof>("brakelights_sprite_offset_y", usingDefaultBrakelights? -0.5*DEFAULT_BRAKELIGHTS_SPRITE_HEIGHT : 0);

		value = futil::to_lower(futil::trim(prop.get("brakelights_mirrowed")));
		spec.brakelightsMirrowed = isValidValue(value)? (value == "true" or value == "yes") : (isBike? false : true);
	}

	spec.shadowSheetFilename = prop.get("shadow_sprite_filename", "default");

	if(spec.shadowSheetFilename == "none" or spec.shadowSheetFilename == "default")
		spec.shadowSheetFilename.clear();

	else if(not spec.shadowSheetFilename.empty())
	{
		spec.shadowSheetFilename = getContextualizedFilename(spec.shadowSheetFilename, prop.get("base_dir"), GameLogic::VEHICLES_FOLDER+"/");

		if(spec.shadowSheetFilename.empty())
			cout << "warning: shadow sprite file could not be found!" << " (specified by \"" << prop.get("filename") << "\"). ignoring shadow definitions..." << endl;
	}

	if(not spec.shadowSheetFilename.empty()) for(unsigned stateNumber = 0; stateNumber < spec.stateCount; stateNumber++)
	{
		fgeal::Point shadowPosition;
		string key = "shadow_position" + futil::to_string(stateNumber);
		if(isValueSpecified(prop, key))
		{
			vector<string> tokens = futil::split(prop.get(key), ',');
			if(tokens.size() > 1)
			{
				shadowPosition.x = atof(tokens[0].c_str());
				shadowPosition.y = atof(tokens[1].c_str());
			}
		}
		else shadowPosition.x = shadowPosition.y = 0;

		string key2 = key + "_x";
		shadowPosition.x = isValueSpecified(prop, key2)? atof(prop.get(key2).c_str()) : shadowPosition.x;

		key2 = key + "_y";
		shadowPosition.y = isValueSpecified(prop, key2)? atof(prop.get(key2).c_str()) : shadowPosition.y;

		spec.shadowPositions.push_back(shadowPosition);
	}

	for(unsigned stateNumber = 0; stateNumber < spec.stateCount * (spec.asymmetrical? 2 : 1); stateNumber++)
	{
		if(stateNumber == spec.stateCount) continue;  // avoids state 0b
		string key = "backfire_position" + (stateNumber < spec.stateCount? futil::to_string(stateNumber)  // normal, left-leaning
																		 : futil::to_string(stateNumber-spec.stateCount) + "b");  // right-leaning
		fgeal::Point backfirePosition;
		if(isValueSpecified(prop, key))
		{
			vector<string> tokens = futil::split(prop.get(key), ',');
			if(tokens.size() > 1)
			{
				backfirePosition.x = atof(tokens[0].c_str());
				backfirePosition.y = atof(tokens[1].c_str());
			}
		}
		else if(stateNumber == 0)
			break;  // don't populate backfire positions if not even first is specified
		else if(stateNumber > spec.stateCount)  // default value for right-leaning versions by applying same delta between left-leaning positions
			backfirePosition = spec.backfirePositions[0] * 2 - spec.backfirePositions[stateNumber-spec.stateCount];
		else
			backfirePosition = spec.backfirePositions.back();  // copy previous if unspecified

		string key2 = key + "_x";
		backfirePosition.x = isValueSpecified(prop, key2)? atof(prop.get(key2).c_str()) : backfirePosition.x;

		key2 = key + "_y";
		backfirePosition.y = isValueSpecified(prop, key2)? atof(prop.get(key2).c_str()) : backfirePosition.y;

		spec.backfirePositions.push_back(backfirePosition);
	}

	value = futil::to_lower(futil::trim(prop.get("backfire_mirrowed")));
	spec.backfireMirrowed = isValidValue(value)? (value == "true" or value == "yes") : false;
	value = prop.get("backfire_extra_position_offset");
	if(not value.empty() and value != "default" and futil::contains(value, ","))
	{
		const vector<string> tokens = futil::split(value, ',');
		spec.backfireExtraPositionOffset.x = atof(tokens[0].c_str());
		spec.backfireExtraPositionOffset.y = atof(tokens[1].c_str());
	}
	else spec.backfireExtraPositionOffset.x = spec.backfireExtraPositionOffset.y = 0;
	spec.backfireExtraPositionOffset.x = prop.getParsedCStrAllowDefault<double, atof>("backfire_extra_position_offset_x", spec.backfireExtraPositionOffset.x);
	spec.backfireExtraPositionOffset.y = prop.getParsedCStrAllowDefault<double, atof>("backfire_extra_position_offset_y", spec.backfireExtraPositionOffset.y);
	spec.backfireOffset.x = spec.backfireOffset.y = -16;  // hardcoded value for hardcoded sprite
}
